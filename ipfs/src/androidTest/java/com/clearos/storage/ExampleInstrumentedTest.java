package com.clearos.storage;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.clearos.dstorage.AwsBlockReader;
import com.clearos.dstorage.AwsBlockWriter;
import com.clearos.dstorage.AwsLister;
import com.clearos.dstorage.EncryptionBlock;
import com.clearos.dstorage.BlockHashEncrypter;
import com.clearos.dstorage.LoggingStorageReporter;
import com.clearos.dlt.CryptoKey;
import com.goterl.lazysodium.exceptions.SodiumException;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Random;

import io.github.novacrypto.SecureCharBuffer;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("com.clearos.dstorage.test", appContext.getPackageName());
    }

    @Test
    public void testEncryption() throws SodiumException, IOException {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        BlockHashEncrypter encrypter = new BlockHashEncrypter(ck, "BlockHashFile");

        String message = "coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host";
        byte[] block = message.getBytes();
        EncryptionBlock cipher = encrypter.encryptBlock(block);
        byte[] decrypted = encrypter.decryptBlock(cipher.cipher, cipher.cid.toBase58());
        assertArrayEquals(block, decrypted);
    }

    /**
     * Tests IPFS blocking, hashing, encryption, and block-by-block upload to AWS.
     */
    @Test
    public void testSwtUpload() throws Exception {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        ck.initDataCustodian("322fef62fecf4b888232c6df185d35d6");
        BlockHashEncrypter encrypter = new BlockHashEncrypter(ck, "BlockHashFile", 32768);
        SwtGateway gateway = new SwtGateway(appContext, new SwtStaticCredentials(appContext).provider(), ck);
        AwsBlockWriter writer = new AwsBlockWriter("TRryMSNui5YtXxE3fFmW82", "clearshare", gateway, new LoggingStorageReporter());

        // Generate a random stream of doubles to write to storage.
        double[] randoms = new Random(123456).doubles(encrypter.getBlockSize()).toArray(); // ~240K
        byte[] bRandoms = new byte[encrypter.getBlockSize()*8];
        for (int i=0; i < encrypter.getBlockSize(); i++) {
            ByteBuffer.wrap(bRandoms, 8*i, 8).putDouble(randoms[i]);
        }

        InputStream inputRandoms = new ByteArrayInputStream(bRandoms);
        Object syncObject = new Object();
        encrypter.encryptFile(inputRandoms, writer, file -> {
            assertEquals(file.cid.toString(), "QmchAmK7SX7UnFpMcy24FXpwo8yPz6wgXfg95gcS29NnVt");
            assertEquals(file.dag.length, 8);
            synchronized (syncObject) {
                syncObject.notify();
            }
            return null;
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
        gateway.stop();
    }

    @Test
    public void testSwtDownload() throws Exception {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        String fileCid = "QmU9iATnnLTLtfvuZnoUuJFww3tqveK3fhBDaheKBU2FGY";
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        ck.initDataCustodian("322fef62fecf4b888232c6df185d35d6");
        BlockHashEncrypter encrypter = new BlockHashEncrypter(ck, "BlockHashFile");

        Object syncObject = new Object();
        SwtGateway gateway = new SwtGateway(appContext, new SwtStaticCredentials(appContext).provider(), ck);
        AwsBlockReader reader = new AwsBlockReader("TRryMSNui5YtXxE3fFmW82", fileCid, "clearshare", gateway, new LoggingStorageReporter());
        File recFile = new File("/storage/emulated/0/DCIM/Camera/D1.jpg");

        encrypter.decryptFile(reader, new FileOutputStream(recFile), BlockHashFile -> {
                assertNotNull(BlockHashFile);
                assertEquals(BlockHashFile.dag.length, 59);
                synchronized (syncObject){
                    syncObject.notify();
                }
                return null;
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
        gateway.stop();
    }

    @Test
    public void testSwtIndex() throws Exception {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Object syncObject = new Object();
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        ck.initDataCustodian("322fef62fecf4b888232c6df185d35d6");
        SwtGateway gateway = new SwtGateway(appContext, new SwtStaticCredentials(appContext).provider(), ck);
        AwsLister l = new AwsLister("TRryMSNui5YtXxE3fFmW82", "clearshare", 10, gateway);
        l.query("BlockHashFile", summary -> {
            assertTrue(summary.size() > 0);
            synchronized (syncObject){
                syncObject.notify();
            }
            return null;
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
        gateway.stop();
    }

    @Test
    public void testStorageClassServer() throws Exception {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Object syncObject = new Object();
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        ck.initDataCustodian("322fef62fecf4b888232c6df185d35d6");
        SwtGateway gateway = new SwtGateway(appContext, new SwtStaticCredentials(appContext).provider(), ck);
        gateway.getInstance().getConfig(config -> {
            assertTrue(config.getLimitations().getSize() > 0);
            synchronized (syncObject) {
                syncObject.notify();
            }
            return null;
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
        gateway.stop();
    }
}
