package com.clearos.dstorage;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.clearos.dlt.CryptoKey;
import com.goterl.lazysodium.exceptions.SodiumException;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import io.github.novacrypto.SecureCharBuffer;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("com.clearos.dstorage.test", appContext.getPackageName());
    }

    @Test
    public void testEncryption() throws SodiumException, IOException {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        BlockHashEncrypter encrypter = new BlockHashEncrypter(ck, "BlockHashFile");

        String message = "coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host";
        byte[] block = message.getBytes();
        EncryptionBlock cipher = encrypter.encryptBlock(block);
        byte[] decrypted = encrypter.decryptBlock(cipher.cipher, cipher.cid.toBase58());
        assertArrayEquals(block, decrypted);
    }

    /**
     * Tests IPFS blocking, hashing, encryption, and block-by-block upload to AWS.
     */
    @Test
    public void testAwsUpload() throws SodiumException, IOException, InterruptedException {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        BlockHashEncrypter encrypter = new BlockHashEncrypter(ck, "BlockHashFile");
        AwsBlockWriter writer = new AwsBlockWriter("TRryMSNui5YtXxE3fFmW82", "com.clearid.storage", new AwsStaticCredentials(appContext).provider(), new LoggingStorageReporter());

        File testFile = new File("/sdcard/Android/data/com.clearos.dstorage.test/P1.jpg");
        System.out.println(testFile);
        Object syncObject = new Object();
        encrypter.encryptFile(new FileInputStream(testFile), writer, file -> {
            assertEquals(file.cid.toString(), "QmU9iATnnLTLtfvuZnoUuJFww3tqveK3fhBDaheKBU2FGY");
            assertEquals(file.dag.length, 59);
            synchronized (syncObject) {
                syncObject.notify();
            }
            return null;
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }

    @Test
    public void testAwsDownload() throws SodiumException, IOException, InterruptedException {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        String fileCid = "QmU9iATnnLTLtfvuZnoUuJFww3tqveK3fhBDaheKBU2FGY";
        SecureCharBuffer words = new SecureCharBuffer();
        words.append("coral light army gather adapt blossom school alcohol coral light army gather already razor night mad hover close slender thought file curtain pitch host");
        CryptoKey ck = new CryptoKey(appContext, words, true);
        BlockHashEncrypter encrypter = new BlockHashEncrypter(ck, "BlockHashFile");

        Object syncObject = new Object();
        AwsBlockReader reader = new AwsBlockReader("TRryMSNui5YtXxE3fFmW82", fileCid, "com.clearid.storage", new AwsStaticCredentials(appContext).provider(), new LoggingStorageReporter());
        File recFile = new File("/sdcard/Android/data/com.clearos.dstorage.test/R1.jpg");

        encrypter.decryptFile(reader, new FileOutputStream(recFile), BlockHashFile -> {
                assertNotNull(BlockHashFile);
                assertEquals(BlockHashFile.dag.length, 59);
                synchronized (syncObject){
                    syncObject.notify();
                }
                return null;
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }

    @Test
    public void testAwsIndex() throws InterruptedException {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Object syncObject = new Object();
        AwsLister l = new AwsLister("TRryMSNui5YtXxE3fFmW82", "com.clearid.storage", 10, new AwsStaticCredentials(appContext).provider());
        l.query("BlockHashFile", summary -> {
            assertTrue(summary.size() > 0);
            synchronized (syncObject){
                syncObject.notify();
            }
            return null;
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }
}
