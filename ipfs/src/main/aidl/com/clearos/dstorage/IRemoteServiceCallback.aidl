// IRemoteServiceCallback.aidl
package com.clearos.dstorage;

import com.clearos.dstorage.ProgressResult;
import com.clearos.dstorage.BlockHashFile;

oneway interface IRemoteServiceCallback {
    /**
     * Callback with the storage operation type (put, get, del) and the unique key identifying the
     * file involved (i.e., `sharedDir/fileName`).
     */
    oneway void onDecentralizedPut(String fileName);
    oneway void onDecentralizedGet(in Uri contentUri, String fileName);
    oneway void onDecentralizedDel(String fileName);
    oneway void onDecentralizedError(String error);
    oneway void onRegistrationStatus(int status);
    oneway void onBlockProgress(in ProgressResult progress);
    oneway void onBlockStarted(in ProgressResult block);
    oneway void onProcessCompleted(in BlockHashFile file);
}
