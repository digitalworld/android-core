package com.clearos.dstorage;

public class AwsWriter implements StreamWriter {
    public boolean completed = false;
    private long length = 0;
    public BlockWriter parent;
    public Throwable error = null;
    public boolean finished = false;

    public AwsWriter(long uploadLength, BlockWriter _parent) {
        length = uploadLength;
        parent = _parent;
    }

    @Override
    public void onError(Throwable e) {
        finished = true;
        error = e;
    }
    @Override
    public void onUploaded(long uploadLength) {
        finished = true;
        if (uploadLength == length) {
            completed = true;
        }
    }

    @Override
    public BlockWriter getParent() {
        return parent;
    }
}
