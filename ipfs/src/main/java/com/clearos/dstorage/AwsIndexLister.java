package com.clearos.dstorage;

import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;

public class AwsIndexLister extends AsyncTask<Void,Void,Void> implements IndexLister {
    private AwsLister l;

    /**
     * Lists index files for a certain prefix; used to query contents in decentralized storage.
     * @param lister That has parameters and credentials and will handle the asynchronous call result.
     */
    AwsIndexLister(AwsLister lister) {
        l = lister;
    }

    @Override
    public AwsLister getLister() {
        return l;
    }

    @Override
    public void start() {
        this.execute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        AmazonS3Client s3Client = new AmazonS3Client(l.credentials);
        s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));

        Log.i("AWS-LIST", String.format("List contents with prefix %s in %s at %s", l.prefix, l.bucket, l.continuationToken));
        ListObjectsV2Request listRequest = new ListObjectsV2Request().withBucketName(l.bucket).withPrefix(l.prefix);
        if (l.continuationToken != null) {
            listRequest = listRequest.withContinuationToken(l.continuationToken);
        }

        try {
            ListObjectsV2Result listResult = s3Client.listObjectsV2(listRequest);
            l.onResult(listResult.getObjectSummaries(), listResult.isTruncated() ? listResult.getNextContinuationToken() : null);
        } catch (Exception e) {
            l.onError(e);
        }

        return null;
    }
}
