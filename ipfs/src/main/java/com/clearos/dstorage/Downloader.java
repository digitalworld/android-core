package com.clearos.dstorage;

public interface Downloader {
    StreamReader getReader();
    String getKey();
    String getCid();
    int getBlockId();
    void start();
}
