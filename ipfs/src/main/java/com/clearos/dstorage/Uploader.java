package com.clearos.dstorage;

public interface Uploader {
    StreamWriter getWriter();
    String getCid();
    String getKey();
    void start();
    int getBlockId();
    long getBlockLength();
}
