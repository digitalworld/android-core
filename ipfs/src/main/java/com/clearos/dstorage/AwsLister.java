package com.clearos.dstorage;

import android.util.Log;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.clearos.storage.SwtGateway;
import com.clearos.storage.SwtIndexLister;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class AwsLister {
    private String did;
    String bucket;
    AWSCredentialsProvider credentials;
    String prefix;
    String continuationToken = null;
    private SwtGateway gateway;
    private boolean isAWS;
    private Function<List<S3ObjectSummary>, Void> finalCallback = null;
    private int keyLimit;
    private List<S3ObjectSummary> objectList = new ArrayList<>();

    /**
     * Initializes a lister for asynchronously gathering index entries for a given encryption context.
     * @param _did DID that an index is being queried for.
     * @param _bucket Bucket whose objects will be listed.
     * @param limit Maximum number of keys to get. If -1, then get all of them.
     * @param _credentials Authentication credentials.
     */
    public AwsLister(String _did, String _bucket , int limit, AWSCredentialsProvider _credentials) {
        did = _did;
        bucket = _bucket;
        credentials = _credentials;
        keyLimit = limit;
        isAWS = true;
    }

    public AwsLister(String _did, String _bucket , int limit, SwtGateway _gateway) {
        did = _did;
        bucket = _bucket;
        gateway = _gateway;
        keyLimit = limit;
        isAWS = false;
    }

    public String getContinuationToken() {
        return continuationToken;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getDid() {
        return did;
    }

    public String getBucket() {
        return bucket;
    }

    public SwtGateway getGateway() {
        return gateway;
    }

    private IndexLister getLister() {
        if (isAWS) {
            return new AwsIndexLister(this);
        } else {
            return new SwtIndexLister(this);
        }
    }

    public void onResult(List<S3ObjectSummary> result, String nextContinuationToken) {
        // First, extend the list of results we have so far.
        objectList.addAll(result);
        // Next, see if we need to grab another page.
        if (continuationToken != null && (keyLimit == -1 || objectList.size() < keyLimit)) {
            continuationToken = nextContinuationToken;
            IndexLister index = getLister();
            index.start();
        } else if (finalCallback != null) {
            finalCallback.apply(objectList);
        }
    }

    /**
     *  Executes the web request to get a bucket list asynchronously.
     * @param encryptionContext Context to list keys for.
     */
    public void query(String encryptionContext, Function<List<S3ObjectSummary>, Void> callback) {
        finalCallback = callback;
        prefix = String.format("%s/%s", did, encryptionContext);
        IndexLister index = getLister();
        index.start();
    }

    public void onError(Throwable e) {
        Log.e("AWS-LIST", e.getMessage(), e);
        if (finalCallback != null) {
            finalCallback.apply(objectList);
        }
    }

    public static String humanReadableByteCountBin(long bytes) {
        long b = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        return b < 1024L ? bytes + " B"
                : b <= 0xfffccccccccccccL >> 40 ? String.format("%.1f KiB", bytes / 0x1p10)
                : b <= 0xfffccccccccccccL >> 30 ? String.format("%.1f MiB", bytes / 0x1p20)
                : b <= 0xfffccccccccccccL >> 20 ? String.format("%.1f GiB", bytes / 0x1p30)
                : b <= 0xfffccccccccccccL >> 10 ? String.format("%.1f TiB", bytes / 0x1p40)
                : b <= 0xfffccccccccccccL ? String.format("%.1f PiB", (bytes >> 10) / 0x1p40)
                : String.format("%.1f EiB", (bytes >> 20) / 0x1p40);
    }
}
