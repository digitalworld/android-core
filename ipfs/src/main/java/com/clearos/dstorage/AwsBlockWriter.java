package com.clearos.dstorage;

import android.annotation.SuppressLint;
import android.os.CancellationSignal;
import android.util.Log;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.clearos.storage.SwtGateway;
import com.clearos.storage.SwtIndexCreator;
import com.clearos.storage.SwtObjectExists;
import com.clearos.storage.SwtUploader;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.KeyPair;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Function;

import io.ipfs.multihash.Multihash;

public class AwsBlockWriter implements BlockWriter, ObjectExistsHandler {
    private KeyPair baseKeys;
    private AWSCredentialsProvider creds;
    private String did;
    private String bucket;
    private String keyPath;
    private BlockHashFile completeFile = null;
    private Map<String, Throwable> uploadErrors = new HashMap<>();
    private Map<String, Exception> contentErrors = new HashMap<>();
    private final ConcurrentSkipListSet<String> completedUploads = new ConcurrentSkipListSet<>();
    private StorageReporter reporter;
    private Function<BlockHashFile, Void> finishCallback = null;
    private IProgressHandler progressHandler = null;
    private CancellationSignal signal;
    private boolean indexUpdated = false;
    private boolean isAWS;
    private int totalExpectedBlocks = -1;
    private Function<ProgressResult, Void> concurrencyCallback;
    private SwtGateway gateway;

    private final static String TAG = "AwsBlockWriter";

    public AwsBlockWriter(KeyPair baseKeys, String keyPath, String _did, String _bucket, AWSCredentialsProvider credentials, StorageReporter _reporter) {
        this.baseKeys = baseKeys;
        this.keyPath = keyPath;
        creds = credentials;
        did = _did;
        bucket = _bucket;
        reporter = _reporter;
        isAWS = true;
    }

    public AwsBlockWriter(KeyPair baseKeys, String keyPath, String _did, String _bucket, SwtGateway gateway, StorageReporter _reporter) {
        this.baseKeys = baseKeys;
        this.keyPath = keyPath;
        did = _did;
        bucket = _bucket;
        reporter = _reporter;
        isAWS = false;
        this.gateway = gateway;
    }

    @Override
    public KeyPair getBaseKeys() {
        return baseKeys;
    }

    private Uploader getUploader(byte[] cipher, String cid, StreamWriter writer, int blockId) {
        if (isAWS) {
            return new AwsUploader(cipher, did, cid, bucket, creds, writer, blockId);
        } else {
            return new SwtUploader(cipher, did, cid, bucket, gateway, writer, blockId);
        }
    }

    @Override
    public void setConcurrencyCallback(Function<ProgressResult, Void> concurrencyCallback) {
        this.concurrencyCallback = concurrencyCallback;
    }

    @Override
    public boolean checkExists() {
        return true;
    }

    @SuppressLint("DefaultLocale")
    private String getBlockUniqueKey(String key, int blockIndex) {
        return String.format("%s/%d", key, blockIndex);
    }

    @Override
    public void onExistsCheck(ObjectExists checker) {
        Log.d(TAG, "OnExistsCheck fired for " + checker.getCid());
        ProgressResult r = new ProgressResult(
                checker.getCid(),
                checker.getBlockId(),
                checker.getCipher().length,
                -1, // This gets updated later below.
                totalExpectedBlocks);

        if (checker.getError() != null) {
            uploadErrors.put(checker.getCid(), checker.getError());
            completedUploads.add(getBlockUniqueKey(checker.getCid(), checker.getBlockId()));
            r.setBlocksDone(completedUploads.size());

            r.setBlockError(checker.getError());
            if (progressHandler != null) {
                progressHandler.onBlockProgress(r);
                progressHandler.onTotalProgress(r.getTotalPercentDone());
            }

            if (runFinishCheck()) checkUploads();
            concurrencyCallback.apply(r);

            return;
        }

        if (!checker.getExists()) {
            AwsWriter writer = new AwsWriter(checker.getCipher().length, this);
            Uploader uploader = getUploader(checker.getCipher(), checker.getCid(), writer, checker.getBlockId());
            uploader.start();
        } else {
            completedUploads.add(getBlockUniqueKey(checker.getCid(), checker.getBlockId()));
            r.setBlocksDone(completedUploads.size());

            concurrencyCallback.apply(r);
            if (progressHandler != null) {
                progressHandler.onBlockProgress(r);
                progressHandler.onTotalProgress(r.getTotalPercentDone());
            }
            if (runFinishCheck()) {
                checkUploads();
            }
        }
    }

    @Override
    public void onBegin(IProgressHandler progressHandler, CancellationSignal signal, int totalExpectedBlocks) {
        Log.d(TAG, String.format("OnBegin called for %d expected blocks.", totalExpectedBlocks));
        this.signal = signal;
        this.progressHandler = progressHandler;
        this.totalExpectedBlocks = totalExpectedBlocks;
    }

    @Override
    public void onGenericError(Throwable error) {
        Log.d(TAG, "Generic error triggering onFailure for reporter: " + error.getMessage());
        reporter.onFailure(error, completedUploads);
    }

    @Override
    public void onIndexUpdated(long uploadedBytes) {
        Log.d(TAG, String.format("OnIndexUpdate called with %d uploaded bytes.", uploadedBytes));
        indexUpdated = true;
        if (runFinishCheck()) {
            checkUploads();
        }
    }

    private ObjectExists getObjectQuery(byte[] cipher, String cid, int blockId) {
        if (isAWS) {
            return new AwsObjectExists(did, cid, bucket, creds, this, cipher, blockId);
        } else {
            return new SwtObjectExists(did, cid, bucket,gateway, this, cipher, blockId);
        }
    }

    public void onNext(byte[] cipher, Multihash cid, int blockIndex) {
        Log.d(TAG, "OnNext fired for CID " + cid.toBase58());
        if (!signal.isCanceled()) {
            // Before we create an uploader, first check if this block even needs uploaded.
            ObjectExists query = getObjectQuery(cipher, cid.toBase58(), blockIndex);
            query.start();
            if (progressHandler != null) {
                ProgressResult r = new ProgressResult(cid.toBase58(), blockIndex, cipher.length, -1, totalExpectedBlocks);
                progressHandler.onBlockStarted(r);
            }
        }
    }

    public void onIOError(IOException error) {
        Log.d(TAG, "IO error triggering onFailure for reporter: " + error.getMessage());
        reporter.onFailure(error, completedUploads);
    }
    public void onEncryptionError(SodiumException error) {
        Log.d(TAG, "Encryption error triggering onFailure for reporter: " + error.getMessage());
        reporter.onFailure(error, completedUploads);
    }

    /**
     * Checks the error state of a single block upload to AWS.
     * @param upload Block uploader async task.
     */
    @Override
    public void checkUpload(Uploader upload) {
        Log.d(TAG, "Checking upload status for " + upload.getKey());

        AwsWriter w = (AwsWriter) upload.getWriter();
        ProgressResult r = new ProgressResult(upload.getCid(),
                upload.getBlockId(),
                upload.getBlockLength(),
                -1, // This gets filled in later below.
                totalExpectedBlocks);

        if (!w.completed && !(uploadErrors.containsKey(upload.getKey()) || contentErrors.containsKey(upload.getKey()))) {
            if (w.error != null) {
                r.setBlockError(w.error);
                uploadErrors.put(upload.getKey(), w.error);
            } else {
                Exception contentError = new Exception("Incorrect number of bytes uploaded.");
                r.setBlockError(contentError);
                contentErrors.put(upload.getKey(), contentError);
            }
        } else {
            // This may be being called another time. Remove errors since we are successful this time.
            uploadErrors.remove(upload.getKey());
            contentErrors.remove(upload.getKey());
        }

        completedUploads.add(getBlockUniqueKey(upload.getKey(), upload.getBlockId()));
        r.setBlocksDone(completedUploads.size());

        if (progressHandler != null) {
            Log.d(TAG, "Firing block progress call for " + upload.getKey());
            progressHandler.onBlockProgress(r);
            progressHandler.onTotalProgress(r.getTotalPercentDone());
        }
        concurrencyCallback.apply(r);

        if (runFinishCheck()) {
            checkUploads();
        }
    }

    /**
     * Checks to see if the asynchronous uploads of all the blocks and index has completed.
     * @return True if everything is done.
     */
    private boolean runFinishCheck() {
        // onCompleted fires before the uploads are all done (because of the index file). Check that state
        // now and clean up. +1 is for the index file, which also gets added to completedUploads, but
        // is *not* part of the DAG.
        return (completeFile != null && completedUploads.size() == completeFile.dag.length + 1 && indexUpdated) || signal.isCanceled();
    }

    /**
     * Verifies the error state of all the uploads once they have all completed. Fires a single
     * error/completed handler for the overall state of the file.
     */
    private void checkUploads() {
        if (uploadErrors.size() > 0) reporter.onLoadErrors(uploadErrors);
        if (contentErrors.size() > 0) reporter.onContentErrors(contentErrors);
        reporter.onSuccess(completeFile, completedUploads);
        if (progressHandler != null) {
            progressHandler.onProcessCompleted(completeFile);
        }
        finishCallback.apply(completeFile);
    }

    private IndexCreator getIndexCreator(BlockHashFile file) {
        if (isAWS) {
            return new AwsIndexCreator(did, keyPath, file, bucket, creds, this);
        } else {
            return new SwtIndexCreator(gateway, did, keyPath, file, bucket, this);
        }
    }

    public void onCompleted(BlockHashFile file, Function<BlockHashFile, Void> callback) {
        completeFile = file;
        finishCallback = callback;

        if (!signal.isCanceled() && file != null) {
            // Before we create an uploader, first check if this index even needs uploaded.
            ObjectExists query = getObjectQuery(file.indexCipher, file.cid.toBase58(), file.dag.length + 1);
            query.start();

            // Also, update the index for this set of blocks.
            IndexCreator creator = getIndexCreator(file);
            creator.start();
        } else {
            callback.apply(file);
        }
    }
}
