package com.clearos.dstorage;

import android.os.AsyncTask;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AwsIndexCreator extends AsyncTask<Void,Void,Void> implements IndexCreator {

    private String key;
    private String bucket;
    private AWSCredentialsProvider creds;
    private BlockWriter writer;
    private BlockHashFile changedFile;

    /**
     * Uploads a block to AWS S3.
     * @param did DID of the user that will own the file.
     * @param keyPath '/'-separated list of folders to nest the CID under.
     * @param file The descriptor for the blocks of the uploaded file.
     * @param _bucket An encryption context (related to the app for which data is being uploaded).
     * @param credentials AWS access credentials that has S3 write access.
     */
    public AwsIndexCreator(String did, String keyPath, BlockHashFile file, String _bucket, AWSCredentialsProvider credentials, BlockWriter _writer) {
        if (keyPath != null) {
            key = String.format("%s/%s/%s", did, keyPath, file.cid.toBase58());
        } else {
            key = String.format("%s/i/%s", did, file.cid.toBase58());
        }
        bucket = _bucket;
        creds = credentials;
        writer = _writer;
        changedFile = file;
    }

    @Override
    public void start() {
        this.execute();
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public BlockWriter getWriter() {
        return writer;
    }

    @Override
    protected Void doInBackground(Void... params) {
        AmazonS3Client s3Client = new AmazonS3Client(creds);
        s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));

        Map<String, Object> index = new HashMap<>();
        index.put("created", new Date());

        Map<String, String> userMeta = new HashMap<>();
        userMeta.put("size", Long.toString(changedFile.size));

        try {
            byte[] block = BlockHashEncrypter.toCbor(index);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(block.length);
            metadata.setUserMetadata(userMeta);
            PutObjectRequest putRequest = new PutObjectRequest(bucket, key, new ByteArrayInputStream(block), metadata);
            PutObjectResult putResponse = s3Client.putObject(putRequest);
            writer.onIndexUpdated(block.length);
        } catch (Exception e) {
            writer.onGenericError(e);
        }

        return null;
    }
}
