package com.clearos.dstorage;

import android.annotation.SuppressLint;
import android.os.CancellationSignal;
import android.util.Log;

import com.clearos.storage.SwtDeleter;
import com.clearos.storage.SwtGateway;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.KeyPair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Function;

import io.grpc.stub.StreamObserver;
import io.ipfs.multihash.Multihash;
import tech.storewise.grpc_storage.Object;

public class AwsBlockDeleter implements BlockDeleter {
    private final ObjectMapper MAPPER =  CBORMapper.builder().build();

    private BlockHashEncrypter decrypter;
    private BlockHashFile file;
    private String fileCid;
    private String did;
    private String bucket;
    private Function<String, Void> doneCallback;
    private IProgressHandler progressHandler;
    private CancellationSignal signal;

    private KeyPair baseKeys;
    private ConcurrentSkipListSet<String> completedDeletes = new ConcurrentSkipListSet<>();
    private SwtGateway gateway;
    private StorageReporter reporter;
    private boolean isAWS = false;
    private boolean hasErrors = false;

    private final static String TAG = "AwsBlockDeleter";

    public AwsBlockDeleter(KeyPair baseKeys, SwtGateway gateway, String bucket, String did, String cid, StorageReporter reporter) {
        this.baseKeys = baseKeys;
        this.gateway = gateway;
        this.did = did;
        this.bucket = bucket;
        this.fileCid = cid;
        this.reporter = reporter;
    }

    @SuppressLint("DefaultLocale")
    private String getBlockUniqueKey(String key, int blockIndex) {
        return String.format("%s/%d", key, blockIndex);
    }

    @Override
    public void onDeleted(String cid, int blockId) {
        if (cid.equals(fileCid)) {
            reportAll();
        } else {
            completedDeletes.add(getBlockUniqueKey(cid, blockId));
            ProgressResult r = new ProgressResult(cid, blockId, -1, completedDeletes.size(), file.dag.length);
            if (progressHandler != null) {
                progressHandler.onBlockProgress(r);
                progressHandler.onTotalProgress(r.getTotalPercentDone());
            }

            if (signal.isCanceled() || completedDeletes.size() == file.dag.length) {
                finishDeletion();
            }
        }
    }

    @Override
    public void onError(String cid, int blockIndex, Throwable t) {
        Log.e(TAG, "Error deleting block with CID " + cid);
        if (cid.equals(fileCid)) {
            reportAll();
        } else {
            hasErrors = true;
            completedDeletes.add(getBlockUniqueKey(cid, blockIndex));
            ProgressResult r = new ProgressResult(cid, blockIndex, -1, completedDeletes.size(), file.dag.length);
            if (progressHandler != null) {
                progressHandler.onBlockProgress(r);
                progressHandler.onTotalProgress(r.getTotalPercentDone());
            }

            if (signal.isCanceled() || completedDeletes.size() == file.dag.length) {
                finishDeletion();
            }
        }
    }

    @Override
    public void onStart(BlockHashEncrypter decrypter, Function<String, Void> doneCallback, IProgressHandler progressHandler, CancellationSignal signal) {
        // Download the index file from AWS and decrypt it.
        this.decrypter = decrypter;
        this.doneCallback = doneCallback;
        this.progressHandler = progressHandler;
        this.signal = signal;

        String key = String.format("%s/%s", did, fileCid);
        gateway.getInstance().download(bucket, key, new StreamObserver<Object>() {
            @Override
            public void onNext(Object value) {
                Log.d(TAG, "Received index file from storage; decrypting");
                decryptIndex(value.getBody().toByteArray());
            }

            @Override
            public void onError(Throwable t) {
                reporter.onFailure(t, completedDeletes);
            }

            @Override
            public void onCompleted() {
                Log.d(TAG, "OnCompleted fired for index download.");
            }
        });
    }

    private void reportAll() {
        if (signal != null && signal.isCanceled()) {
            reporter.onCancelled(completedDeletes);
        } else {
            reporter.onSuccess(file, completedDeletes);
        }
        doneCallback.apply(fileCid);
    }

    private void finishDeletion() {
        if (!hasErrors) {
            Deleter indexDeleter = getDeleter(fileCid, -1);
            assert indexDeleter != null;
            indexDeleter.start();
        } else {
            reportAll();
        }
    }

    private Deleter getDeleter(String cid, int blockId) {
        if (!isAWS) {
            return new SwtDeleter(gateway, did, cid, bucket, this, blockId);
        } else {
            return null;
        }
    }

    /**
     * Decrypts the index file byte array for a file to delete.
     */
    private void decryptIndex(byte[] indexBytes) {
        byte[] block;
        try {
            block = decrypter.decryptBlock(baseKeys, indexBytes, fileCid);
        } catch (SodiumException | IOException e) {
            reporter.onFailure(e, null);
            block = null;
        }

        if (block != null) {
            // This is the index file, deserialize the block into its DAG.
            try {
                Map<String, java.lang.Object> responseMap = (Map<String, java.lang.Object>) MAPPER.readValue(block, Map.class);
                java.lang.Object _size = responseMap.getOrDefault("size", null);
                long size = -1;
                if (_size != null) size = (int)_size;

                List<String> _dag = (ArrayList<String>)responseMap.getOrDefault("dag", null);
                assert _dag != null;
                String[] dag = new String[_dag.size()];
                dag = _dag.toArray(dag);

                file = new BlockHashFile(Multihash.fromBase58(fileCid), dag, size, null);

                // Before we continue, let's verify that the CID matches the contents of this index
                responseMap.remove("headers");
                byte[] cborBytes = BlockHashEncrypter.toCbor(responseMap);
                Multihash indexCid = decrypter.getCid(cborBytes);
                if (!indexCid.toBase58().equals(fileCid)) {
                    throw new IOException("Decrypted CID of index file doesn't match file CID.");
                }

                // Queue up the deletion of the other blocks.
                int i = 0;
                while (!signal.isCanceled() && i < file.dag.length) {
                    String cid = file.dag[i];
                    Deleter deleter = getDeleter(cid, i);
                    assert deleter != null;
                    deleter.start();
                    i++;
                }

            } catch (Exception e) {
                reporter.onFailure(e, completedDeletes);
            }
        }
    }
}
