package com.clearos.dstorage;

import android.os.CancellationSignal;
import android.util.Log;

import com.clearos.dlt.DerivedKeyClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.goterl.lazysodium.LazySodiumAndroid;
import com.goterl.lazysodium.SodiumAndroid;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.interfaces.Hash;
import com.goterl.lazysodium.interfaces.SecretStream;
import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import io.ipfs.multihash.Multihash;

public class BlockHashEncrypter {
    private final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    private final SecretStream.Native streamNative = lazySodium;
    private final Hash.Native hashNative = lazySodium;

    private DerivedKeyClient keyClient;

    private final String TAG = "BlockHashEncrypter";
    byte TAG_PUSH = SecretStream.XCHACHA20POLY1305_TAG_PUSH;
    byte TAG_REKEY = SecretStream.XCHACHA20POLY1305_TAG_REKEY;
    byte TAG_MESSAGE = SecretStream.XCHACHA20POLY1305_TAG_MESSAGE;
    byte TAG_FINAL = SecretStream.XCHACHA20POLY1305_TAG_PUSH | SecretStream.XCHACHA20POLY1305_TAG_REKEY;

    public boolean stopped = false;
    private long maxMemory;

    public BlockHashEncrypter(DerivedKeyClient keyClient) {
        this(keyClient, 256*1024*1024);
    }

    /**
     * Initializes a client that can shard and encrypt data to upload to a storage provider.
     * @param keyClient client to use for encryption key operations.
     * @param maxMemory Maximum memory (in bytes) that should be used for processing files. The
     *                  maximum number of concurrent blocks being processed is set by this value.
     */
    public BlockHashEncrypter(DerivedKeyClient keyClient, long maxMemory) {
        this.maxMemory = maxMemory;
        this.keyClient = keyClient;
    }

    /**
     * Calculates the maximum number of concurrent blocks that can be processed at a time.
     * @param blockSize Size of each block.
     * @return The number of concurrent blocks so that the `maxMemory` will not be exceeded.
     */
    long concurrentBlockCount(int blockSize) {
        return Math.floorDiv(maxMemory, blockSize);
    }

    public void decryptFile(BlockReader fileContents,
                            OutputStream targetFile,
                            Function<BlockHashFile, Void> callback,
                            IProgressHandler progressCallback,
                            CancellationSignal signal) {
        fileContents.onBegin(this, targetFile, callback, progressCallback, signal);
    }

    /**
     * Checks if a new block can be submitted for processing. Note that this operation blocks the
     * current thread.
     * @param timeout Number of units to wait.
     * @param unit Unit to apply to timeout.
     * @throws RuntimeException if the timeout is reached before space is available to process a
     * new block.
     */
    boolean canProcessNewBlock(Stack<Integer> concurrentBlocks, int maxConcurrentBlocks, long timeout, TimeUnit unit) throws IOException {
        Log.d(TAG, "Checking process wait time for new concurrent blocks.");
        long eachWait = TimeUnit.MILLISECONDS.toMillis(250);
        long waited = 0;
        boolean available = false;
        while (waited < unit.toMillis(timeout) && !available) {
            if (concurrentBlocks.size() < maxConcurrentBlocks) available = true;

            // Don't wait if we already have something available.
            if (!available) {
                try {
                    Log.d(TAG, "Waiting 250 milliseconds before checking queue space availability.");
                    Thread.sleep(eachWait);
                    waited += eachWait;
                } catch (InterruptedException e) {
                    Log.d(TAG, "Thread interrupted while waiting for an open block.", e);
                }
            }
        }

        if (!available) {
            throw new IOException("No available space to process concurrent blocks.");
        } else {
            Log.d(TAG, String.format("Space was available on the upload queue after %d milliseconds.", waited));
        }

        return available;
    }

    /**
     * Encrypts the specified file stream block-by-block using derived keys based on the hashes
     * of the file contents. Waits for a maximum of 30 seconds for the block queue to have space
     * for another block to upload.
     * @param fileContents File stream to encrypt block-by-block.
     * @param writeTarget File stream to write encrypted cipher bytes to.
     * @param blockSize Size of each block to write; smaller blocks have higher de-dupe power.
     * @param doneCallback Function to call when the upload is completed.
     * @param progressHandler Object that handles progress events as each block is written to provide progress.
     */
    public void encryptFile(FileInputStream fileContents,
                            BlockWriter writeTarget,
                            long fileSize,
                            int blockSize,
                            Function<BlockHashFile, Void> doneCallback,
                            IProgressHandler progressHandler,
                            CancellationSignal signal,
                            Object weakRefObject) throws IOException {
        encryptFile(fileContents, writeTarget, fileSize, blockSize, doneCallback, progressHandler,
                    signal, 180, TimeUnit.SECONDS, weakRefObject);
    }

    /**
     * Encrypts the specified file stream block-by-block using derived keys based on the hashes
     * of the file contents.
     * @param fileContents File stream to encrypt block-by-block.
     * @param writeTarget File stream to write encrypted cipher bytes to.
     * @param blockSize Size of each block to write; smaller blocks have higher de-dupe power.
     * @param doneCallback Function to call when the upload is completed.
     * @param progressHandler Object that handles progress events as each block is written to provide progress.
     */
    public void encryptFile(FileInputStream fileContents,
                            BlockWriter writeTarget,
                            long fileSize,
                            int blockSize,
                            Function<BlockHashFile, Void> doneCallback,
                            IProgressHandler progressHandler,
                            CancellationSignal signal,
                            long timeout, TimeUnit unit,
                            Object weakRefObject) throws IOException {
        // Set up an array of latches to keep track of concurrent uploads.
        int maxConcurrentBlocks = (int)concurrentBlockCount(blockSize);
        Log.d(TAG, String.format("Initializing stack for maximum of %d concurrent blocks.", maxConcurrentBlocks));

        final Stack<Integer> concurrentBlocks = new Stack<>();

        int totalExpectedBlocks = (int)Math.floorDiv(fileSize, blockSize);
        final CountDownLatch blockCompletedLatch = new CountDownLatch(totalExpectedBlocks);
        writeTarget.onBegin(progressHandler, signal, totalExpectedBlocks);

        // Register the start of the encrypt/upload.
        writeTarget.setConcurrencyCallback(pr -> {
            // Synchronize on the fact that we just finished a block and the next one can begin.
            Integer blockIndex;
            synchronized (concurrentBlocks) {
                if (concurrentBlocks.size() > 0)
                    blockIndex = concurrentBlocks.pop();
                else
                    blockIndex = -1;
            }
            blockCompletedLatch.countDown();
            Log.d(TAG, String.format("Percentage complete for upload is %.2f with block %d", pr.getTotalPercentDone(), blockIndex));
            return null;
        });

        KeyPair baseKeys = writeTarget.getBaseKeys();
        byte[] block = new byte[blockSize];
        long bytesRead = 0;
        long total = 0;
        List<String> cidList = new ArrayList<>();
        boolean error = false;
        int blockIndex = 0;
        FileDescriptor f = fileContents.getFD();

        try {
            // As long as we have blocks to process, keep cranking. canProcessNewBlock blocks the
            // thread for up to timeout seconds before returning.
            while (f.valid() && blockSize < fileSize-total && !stopped && canProcessNewBlock(concurrentBlocks, maxConcurrentBlocks, timeout, unit)) {
                // Push the block index onto the concurrent uploads stack so that we don't exceed the
                // memory limits set for the encrypter.
                synchronized (concurrentBlocks) {
                    Log.d(TAG, String.format("Pushing %d block index to concurrent stack.", blockIndex));
                    concurrentBlocks.push(blockIndex);
                }

                bytesRead = fileContents.read(block, 0, blockSize);
                if (bytesRead == -1) break;
                total += bytesRead;

                // Now, encrypt the file and write the cipher contents to the output stream.
                Log.d(TAG, String.format("Encrypting block of length %d for block index %d.", blockSize, blockIndex));
                EncryptionBlock ipfsBlock = encryptBlock(baseKeys, block);
                if (ipfsBlock.cid != null && ipfsBlock.cipher != null) {
                    cidList.add(ipfsBlock.cid.toBase58());
                    writeTarget.onNext(ipfsBlock.cipher, ipfsBlock.cid, blockIndex);
                }

                blockIndex += 1;
                Log.d(TAG, "Starting block processing for block " + blockIndex);
            }

            // Grab the last few bytes from the file.
            if (f.valid() && fileContents.available() > 0) {
                Log.d(TAG, "Clearing up last few bytes of the file into final block");
                Arrays.fill(block, (byte) 0);
                bytesRead = fileContents.read(block, 0, fileContents.available());
                if (bytesRead > blockSize) {
                    throw new IOException("Final block is larger than block size; this is supposed to be impossible!");
                }

                EncryptionBlock ipfsBlock = encryptBlock(baseKeys, block);
                if (ipfsBlock.cid != null && ipfsBlock.cipher != null) {
                    cidList.add(ipfsBlock.cid.toBase58());
                    writeTarget.onNext(ipfsBlock.cipher, ipfsBlock.cid, blockIndex);
                }
            }

            if (!f.valid()) {
                Log.w(TAG, "File descriptor is not valid; cannot continue.");
                error = true;
            }
        }
        catch (IOException ioError) {
            writeTarget.onIOError(ioError);
            error = true;
        } catch (SodiumException encError) {
            writeTarget.onEncryptionError(encError);
            error = true;
        }
        finally {
            Log.d(TAG, "Finishing file scan and closing stream.");
            fileContents.close();
        }

        // Wait for the other uploads to finish.
        try {
            Log.d(TAG, "Waiting for all blocks to finish processing.");
            blockCompletedLatch.await();
        } catch (InterruptedException e) {
            Log.e(TAG, "Thread interrupted while waiting for all blocks to finish being processed.", e);
            error = true;
        }

        if (!error) {
            Log.d(TAG, "Continuing to write index file to decentralized storage.");
            String[] dag = new String[cidList.size()];
            dag = cidList.toArray(dag);

            Map<String, Object> indexObj = new HashMap<>();
            indexObj.put("size", total);
            indexObj.put("dag", dag);

            // Because of randomization in the nonce, the headers list will be unique every time encryption
            // is called. That is why the CID is based on the unencrypted data instead. Figure out the
            // CID before we add the headers in for encryption.
            byte[] cborBytes = BlockHashEncrypter.toCbor(indexObj);
            BlockHashFile result = null;
            try {
                EncryptionBlock indexCipher = encryptBlock(baseKeys, cborBytes);
                result = new BlockHashFile(indexCipher.cid, dag, total, indexCipher.cipher);
            } catch (SodiumException e) {
                writeTarget.onEncryptionError(e);
            }

            writeTarget.onCompleted(result, doneCallback);
        } else {
            // Null onCompleted arguments means that we failed.
            Log.d(TAG, "Firing callback with null result for failed upload.");
            writeTarget.onCompleted(null, doneCallback);
        }
    }

    public static byte[] toCbor(Map<String, Object> object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        return objectMapper.writeValueAsBytes(object);
    }

    /**
     * Returns a CID (multihash) for the given block of bytes.
     */
    Multihash getCid(byte[] block) throws SodiumException {
        byte[] hashedBytes = new byte[Hash.SHA256_BYTES];
        if (!hashNative.cryptoHashSha256(hashedBytes, block, block.length)) {
            throw new SodiumException("Unsuccessful sha-256 hash.");
        }

        return new Multihash(Multihash.Type.sha2_256, hashedBytes);
    }

    /**
     * Decrypts a single block using a customized, derived key.
     * @param baseKeys Base encryption keys from which to derive block keys.
     * @param cipher The block to encrypt.
     * @param cid CID of the unencrypted block.
     */
    public byte[] decryptBlock(KeyPair baseKeys, byte[] cipher, String cid) throws IOException, SodiumException {
        byte[] header = Arrays.copyOfRange(cipher, cipher.length-SecretStream.HEADERBYTES, cipher.length);
        SecretStream.State state = getBlockDecrypterState(baseKeys, cid, header);
        byte[] block = pullDecryptStream(state, Arrays.copyOfRange(cipher, 0, cipher.length-header.length));
        Multihash mHash = getCid(block);
        if (!mHash.toString().equals(cid)) {
            throw new IOException("IPFS hash of decrypted block does not match input cid.");
        }

        return block;
    }

    /**
     * Encrypts a single block using a customized, derived key.
     * @param block The block to encrypt.
     * @return The CID (multihash) and encrypted cipher bytes for the block.
     * @throws SodiumException If the encryption stream fails.
     */
    private EncryptionBlock encryptBlock(KeyPair baseKeys, byte[] block, Multihash cid) throws SodiumException {
        byte[] header = new byte[SecretStream.HEADERBYTES];
        SecretStream.State state = getBlockEncrypterState(baseKeys, cid.toString(), header);
        byte[] cipher = pushEncryptStream(state, block, TAG_FINAL);
        System.arraycopy(header,0, cipher, cipher.length-SecretStream.HEADERBYTES, header.length);
        return new EncryptionBlock(cipher, cid);
    }

    /**
     * Encrypts a single block using a customized, derived key.
     * @param block The block to encrypt.
     * @return The CID (multihash) and encrypted cipher bytes for the block.
     * @throws SodiumException If the encryption stream fails.
     */
    public EncryptionBlock encryptBlock(KeyPair baseKeys, byte[] block) throws SodiumException {
        Multihash mHash = getCid(block);
        return encryptBlock(baseKeys, block, mHash);
    }

    /**
     * Retrieves a customized decryption state based on the specified CID.
     * @param cid IPFS cid of the block.
     * @param header Header used to initialize the encryption stream.
     * @throws SodiumException If stream initialization fails in libsodium.
     */
    private SecretStream.State getBlockDecrypterState(KeyPair baseKeys, String cid, byte[] header) throws SodiumException {
        // We use the first few digits of the CID to seed a derived key.
        Key blockKey = keyClient.deriveKey(baseKeys, 0, cid.substring(0, 8));
        SecretStream.State result = new SecretStream.State.ByReference();
        if (!streamNative.cryptoSecretStreamInitPull(result, header, blockKey.getAsBytes())) {
            throw new SodiumException("Unable to initialize stream decryption for block.");
        }

        return result;
    }

    /**
     * Decrypts a single byte stream independently.
     * @param state Encryption state for the libsodium secret stream.
     * @param cipherBytes Bytes to decrypt.
     * @return The decrypted block bytes.
     * @throws SodiumException If the stream pull fails.
     */
    private byte[] pullDecryptStream(SecretStream.State state, byte[] cipherBytes) throws SodiumException {
        byte[] message = new byte[cipherBytes.length - SecretStream.ABYTES];
        byte[] tag = new byte[1];
        if (!streamNative.cryptoSecretStreamPull(state, message, tag, cipherBytes, cipherBytes.length)) {
            throw new SodiumException("Error pulling byte block from cipher stream.");
        }

        if (tag[0] == TAG_FINAL) {
            return message;
        } else {
            return null;
        }
    }

    /**
     * Retrieves a customized encryption state based on the specified CID.
     * @param cid IPFS cid of the block.
     * @param header Header bytes to use for encryption initialization.
     * @throws SodiumException If stream initialization fails in libsodium.
     */
    private SecretStream.State getBlockEncrypterState(KeyPair baseKeys, String cid, byte[] header) throws SodiumException {
        // We use the first few digits of the CID to seed a derived key.
        Key blockKey = keyClient.deriveKey(baseKeys, 0, cid.substring(0, 8));
        SecretStream.State result = new SecretStream.State.ByReference();
        if (!streamNative.cryptoSecretStreamInitPush(result, header, blockKey.getAsBytes())) {
            throw new SodiumException("Unable to initialize stream encryption for block.");
        }

        return result;
    }

    /**
     * Encrypts a single byte stream independently.
     * @param state Encryption state for the libsodium secret stream.
     * @param messageBytes Bytes to encrypt.
     * @param tag Should be either `TAG_PUSH` or `TAG_FINAL` depending on whether more bytes will be pushed to the stream later.
     * @return The encrypted cipher bytes.
     * @throws SodiumException If the stream push fails.
     */
    private byte[] pushEncryptStream(SecretStream.State state, byte[] messageBytes, byte tag) throws SodiumException {
        // Make space for the header bytes in the final cipher.
        byte[] cipher = new byte[SecretStream.ABYTES + messageBytes.length + SecretStream.HEADERBYTES];
        if (!streamNative.cryptoSecretStreamPush(state, cipher, messageBytes, messageBytes.length, tag)) {
            throw new SodiumException("Error pushing byte block to cipher stream.");
        }

        return cipher;
    }
}
