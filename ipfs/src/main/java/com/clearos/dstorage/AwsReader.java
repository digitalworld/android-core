package com.clearos.dstorage;

import android.util.Log;

import java.io.InputStream;

public class AwsReader implements StreamReader {
    public boolean completed = false;
    public AwsBlockReader parent;
    public Throwable error = null;
    public boolean finished = false;
    private String cid;

    public AwsReader(String _cid, AwsBlockReader _parent) {
        parent = _parent;
        cid = _cid;
    }

    @Override
    public Boolean isCompleted() {
        return completed;
    }

    @Override
    public Throwable getError() {
        return error;
    }

    @Override
    public void onError(Throwable e) {
        finished = true;
        error = e;
    }
    @Override
    public void onDownloaded(InputStream object, long contentLength) {
        finished = true;
        //Is there anything on the metadata to indicate failure?
        if (contentLength > 0) {
            completed = true;
            Log.i("READER", "Download completed; processing cache block.");
            parent.cacheBlock(cid, object, contentLength);
        }
    }

    @Override
    public AwsBlockReader getParent() {
        return parent;
    }
}
