package com.clearos.storage;

import android.util.Log;

import com.clearos.dlt.KeyClientApplication;
import com.clearos.dlt.LoggingRemoteErrorHandler;
import com.clearos.storage.provider.ApplicationStore;
import com.clearos.storage.provider.DecentralizedStoreService;
import com.clearos.storage.provider.IApplicationStoreHandler;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class DecentralizedStorageApplication extends KeyClientApplication implements IApplicationStoreHandler {
    private final String TAG = "DecentralizedStorageApplication";
    private DecentralizedStoreClient storeClient = null;
    private Map<String, ApplicationStore> dataStores = new HashMap<>();
    private Map<String, String> configuredDirectories;
    private final Object syncSetup = new Object();
    private boolean storageConnected = false;

    private String fileProviderName;
    private int xmlFilePathsResourceId;

    public void setupStorageClient(String appName, Function<Void, Void> callback) {
        synchronized (syncSetup) {
            if (storeClient == null || !storageConnected) {
                Log.d(TAG, "Calling create for decentralized storage client.");
                new DecentralizedStoreClient(getApplicationContext(), new LoggingRemoteErrorHandler())
                .create(getApplicationContext(), 10000, sc -> {
                    // Register this app with the service.
                    storeClient = (DecentralizedStoreClient)sc;
                    if (sc != null) {
                        Log.d(TAG, "Service binding successful; continuing with storage service registration.");
                        storeClient.register(appName, getKeyClient().getAppKeys().getDid(), status -> {
                            if (status != DecentralizedStoreService.ENABLED) {
                                threadSafeToast("Cannot register with ClearCLOUD service: status = " + status.toString());
                            } else {
                                storageConnected = true;
                                Log.d(TAG, "Registered application with ClearCLOUD: " + appName);
                            }
                            callback.apply(null);
                            return null;
                        });
                    } else {
                        Log.d(TAG, "Storage service bound successfully, but the service is null.");
                        callback.apply(null);
                    }
                    return null;
                });
            } else {
                callback.apply(null);
            }
        }
    }

    public int getXmlFilePathsResourceId() {
        return xmlFilePathsResourceId;
    }

    public Map<String, String> getConfiguredDirectories() {
        return configuredDirectories;
    }

    /**
     * Configures the data stores of a subclassing application based on the configured file provider
     * and XML paths.
     * @param fileProviderName Name of the FileProvider configured in the application manifest.
     * @param xmlFilePathsResourceId ResourceId of the XML file that defines paths for the provider.
     */
    public void configureDataStores(String fileProviderName, int xmlFilePathsResourceId) {
        this.fileProviderName = fileProviderName;
        this.xmlFilePathsResourceId = xmlFilePathsResourceId;
        try {
            configuredDirectories = ApplicationStore.getConfiguredFilePaths(getApplicationContext(), xmlFilePathsResourceId);
        } catch (Exception e) {
            Log.e(TAG, "Error configuring shared provider directories.", e);
        }
    }

    @Override
    public void onFileLoaded(File file) {
        Log.i(TAG, "File loaded from decentralized storage: " + file.getPath());
    }
    @Override
    public void onFileSaved(File file) {
        Log.i(TAG, "File saved to decentralized storage: " + file.getPath());
    }

    @Override
    public void onIOError(IOException e) {
        super.onIOError(e);
    }

    /**
     * Retrieves the application decentralized store for the specified shared directory.
     * @param sharedDirectory Directory configured in file paths for the FileProvider.
     */
    public ApplicationStore getDataStore(String sharedDirectory) {
        if (!dataStores.containsKey(sharedDirectory)) {
            ApplicationStore dStore = null;
            if (storeClient != null) {
                dStore = new ApplicationStore(
                        storeClient,
                        fileProviderName,
                        sharedDirectory,
                        this);
            }
            dataStores.put(sharedDirectory, dStore);
        }

        return dataStores.getOrDefault(sharedDirectory, null);
    }

    public DecentralizedStoreClient getStoreClient() {
        return storeClient;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        storeClient.destroy();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
