package com.clearos.storage.meta;

import android.os.AsyncTask;
import android.os.CancellationSignal;
import android.util.Log;

import androidx.sqlite.db.SimpleSQLiteQuery;

import com.clearos.dstorage.IProgressHandler;
import com.clearos.dstorage.BlockHashFile;
import com.clearos.dlt.IRemoteErrorHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class  DbBackup extends AsyncTask<Void, Float, Void> {
    private IProgressHandler progressHandler;
    private Function<BlockHashFile, Void> doneCallback;
    private MetaDatabaseLoader loader;
    private IRemoteErrorHandler handler;
    private CancellationSignal signal;

    private final static String TAG = "DbBackup";

    private CountDownLatch backupLatch;
    private Runnable backupRunnable = new Runnable() {
        @Override
        public void run() {
            // Run each of the process queues one-by-one. We use a latch so that additional runnables
            // won't start if one is already queued.
            backupLatch = new CountDownLatch(1);
            try {
                signal = new CancellationSignal();
                DbBackup.this.execute();
            } catch (Exception e) {
                Log.e(TAG, "Error during scheduled processing of queue.", e);
            }
            backupLatch.countDown();
        }
    };
    private ScheduledExecutorService scheduler;
    private int backupFrequency = 60;


    /**
     * Starts a backup of the metadata database for decentralized storage inside the given loader.
     * @param loader Loader whose database will be backed up.
     * @param handler Error handler for remote and IO errors.
     */
    DbBackup(MetaDatabaseLoader loader, IRemoteErrorHandler handler) {
        this(loader, handler, null, null);
    }

    /**
     * Starts a backup of the metadata database for decentralized storage inside the given loader.
     * @param loader Loader whose database will be backed up.
     * @param handler Error handler for remote and IO errors.
     * @param doneCallback Callback for when the backup is completed.
     * @param progressHandler Progress callback as each block is written.
     */
    DbBackup(MetaDatabaseLoader loader, IRemoteErrorHandler handler, IProgressHandler progressHandler, Function<BlockHashFile, Void> doneCallback) {
        this.progressHandler = progressHandler;
        this.doneCallback = doneCallback;
        this.handler = handler;
        this.loader = loader;
        this.signal = new CancellationSignal();

        if (progressHandler != null) {
            progressHandler.addProgressCallback(pr -> {
                publishProgress(pr.getTotalPercentDone());
                return null;
            });
        }
    }

    /**
     * Sets the backup frequency (in minutes) for the database.
     * @param backupFrequency Number of minutes between scheduled database backups.
     */
    public void setBackupFrequency(int backupFrequency) {
        Log.d(TAG, "Backup frequency has been reset to " + backupFrequency);
        this.backupFrequency = backupFrequency;
    }

    /**
     * Starts the scheduler that runs processing of database backup.
     * @param initialDelay Initial delay (in minutes) before the first backup should fire.
     */
    public void startScheduler(int initialDelay) {
        Log.d(TAG, "Starting backup scheduler for MetaDb.");
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(backupRunnable, initialDelay, backupFrequency, TimeUnit.MINUTES);
    }

    /**
     *  Cancels the scheduled task that runs processing every few minutes. It will not restart unless
     *  an explicit call to `startScheduler` is made.
     */
    public void cancelScheduler() {
        Log.d(TAG, "Shutting down backup scheduler.");
        scheduler.shutdown();
        try {
            scheduler.awaitTermination(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e(TAG, "Backup scheduler didn't terminate within requested time.", e);
        }
    }


    @Override
    protected Void doInBackground(Void... voids){
        Log.d(TAG, "Creating database checkpoint for metadata DB in Sqlite.");
        loader.getDb().dao().genericQuery(new SimpleSQLiteQuery("pragma wal_checkpoint(full)"));
        try {
            File metaDbFile = loader.getMetaDatabasePath();
            Log.d(TAG, "Got metaDb file path as " + metaDbFile.getPath());
            FileInputStream source = new FileInputStream(metaDbFile);
            loader.getStorage().upload(metaDbFile.length(), source, "ClearCLOUD metadata database backed–up successfully.", dbFile -> {
                if (dbFile != null) {
                    Log.d(TAG, "DbBackup upload successful. Latest CID is now " + dbFile.getCid());
                    loader.setLatestCid(dbFile.cid.toBase58());
                } else Log.d(TAG, "Error while backing up the metaDb; no BlockHashFile returned.");
                if (doneCallback != null) {
                    Log.d(TAG, "Firing callback for metaDb upload.");
                    doneCallback.apply(dbFile);
                }
                return null;
            }, progressHandler, signal, null);
        } catch (IOException e) {
            handler.onIOError(e);
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        super.onProgressUpdate(values);
        if (progressHandler != null) {
            progressHandler.onTotalProgress(values[0]);
        }
    }
}
