package com.clearos.storage.meta;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.clearos.dstorage.IProgressHandler;
import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.R;
import com.clearos.storage.Gateway;
import com.clearos.storage.SwtGateway;
import com.clearos.storage.db.MetaDatabase;
import com.clearos.storage.provider.DecentralizedStorage;
import com.clearos.dlt.SharedPrefs;
import com.clearos.dlt.IRemoteErrorHandler;
import com.clearos.updates.Utils;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.function.Function;

import io.grpc.stub.StreamObserver;
import tech.storewise.grpc_storage.Object;
import tech.storewise.grpc_storage.PutResponse;


public class MetaDatabaseLoader {
    private DecentralizedStorage storage;
    private Context appContext;
    private IRemoteErrorHandler handler;
    private MetaDatabase db;
    private String createdDbPath = null;

    private final String TAG = "MetaDbLoader";
    private final String CID_PREFS_KEY = "metaDatabaseCid";
    private final String CID_TIMESTAMP_PREFS_KEY = "metaDatabaseTimestamp";

    /**
     * Creates a new database loader.
     * @param appContext Full application context from calling app.
     * @param handler Handler for remote and IO errors.
     * @param progressCallback Function to call as each block of the database is read from decentralized storage.
     */
    public MetaDatabaseLoader(Context appContext, SwtGateway gateway, IRemoteErrorHandler handler,
                              Function<MetaDatabase, Void> doneCallback,
                              IProgressHandler progressCallback) {
        this.appContext = appContext;
        this.handler = handler;
        storage = new DecentralizedStorage(appContext, gateway, "backup", appContext.getPackageName(), handler, false);
        restoreDatabase(doneCallback, progressCallback);

        // Start the backup scheduler for the database.
        DbBackup dbBackup = new DbBackup(this, handler, null, null);
        dbBackup.startScheduler(60);
    }

    /**
     * Returns the Room database object for interacting with metadata database.
     */
    public MetaDatabase getDb() {
        return db;
    }

    DecentralizedStorage getStorage() {
        return storage;
    }

    private void backupDatabase(Function<BlockHashFile, Void> doneCallback, IProgressHandler progressHandler) {
        DbBackup backup = new DbBackup(this, handler, progressHandler, doneCallback);
        backup.execute();
    }

    /**
     * Restores the database from decentralized storage using the CID stored in SharedPrefs.
     * @param doneCallback Function to call once the database is ready.
     * @param progressCallback Function to call as each block is read from decentralized storage.
     */
    private void restoreDatabase(Function<MetaDatabase, Void> doneCallback, IProgressHandler progressCallback) {
        File metaDbFile = null;
        try {
            metaDbFile = getMetaDatabasePath();
        } catch (IOException e) {
            handler.onIOError(e);
        }

        db = Room.databaseBuilder(appContext, MetaDatabase.class, MetaDatabase.getDbName())
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        createdDbPath = db.getPath();
                    }
                })
                .enableMultiInstanceInvalidation()
                .build();
        doneCallback.apply(db);

//        if (metaDbFile != null && metaDbFile.exists()) {
//            Log.d(TAG, "Restoring database from file: " + metaDbFile.getPath());
//            db = Room.databaseBuilder(appContext, MetaDatabase.class, MetaDatabase.getDbName())
//                    .createFromFile(metaDbFile)
//                    .enableMultiInstanceInvalidation()
//                    .build();
//        } else {
//            Log.d(TAG, "Local database file does not exist... Running async restorer.");
//            DbRestore restorer = new DbRestore(this, handler, progressCallback, mdf -> {
//                if (mdf != null) {
//                    Log.d(TAG, "Restoring database from downloaded DS file: " + mdf.getPath());
//                    db = Room.databaseBuilder(appContext, MetaDatabase.class, MetaDatabase.getDbName())
//                            .createFromFile(mdf)
//                            .enableMultiInstanceInvalidation()
//                            .build();
//                } else {
//                    // Create a new database from scratch and keep track of the path to the file.
//                    Log.d(TAG, "Creating database from scratch/local file system only.");
//                    db = Room.databaseBuilder(appContext, MetaDatabase.class, MetaDatabase.getDbName())
//                            .addCallback(new RoomDatabase.Callback() {
//                                @Override
//                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
//                                    super.onCreate(db);
//                                    createdDbPath = db.getPath();
//                                }
//                            })
//                            .enableMultiInstanceInvalidation()
//                            .build();
//                }
//                doneCallback.apply(db);
//                return null;
//            });
//            restorer.execute();
//        }
    }

    /**
     * Sets the CID of the latest database backup in decentralized storage.
     * @param cid Hash of the database backup file.
     */
    void setLatestCid(String cid) {
        // First, save the CID to shared preferences.
        SharedPrefs prefs = SharedPrefs.getInstance(appContext);
        prefs.saveValue(CID_PREFS_KEY, cid);
        long timestamp = Instant.now().toEpochMilli();
        prefs.saveLong(CID_TIMESTAMP_PREFS_KEY, timestamp);

        // Next, upload the CID to decentralized storage for syncing between apps.
        MetaDbCid payload = new MetaDbCid(cid, timestamp);
        String jLoad = new Gson().toJson(payload);
        byte[] contents = jLoad.getBytes(StandardCharsets.US_ASCII);

        Gateway g = storage.getGateway().getInstance();
        String key;
        if (Utils.isClearDevice())
            key = String.format("%s/com.clearos.clearlife/metadb.cid", g.getStorageDid());
        else
            key = String.format("%s/com.clearos.digitallife/metadb.cid", g.getStorageDid());
        g.upload(appContext.getString(R.string.SwtBucket), key, contents, new StreamObserver<PutResponse>() {
            @Override
            public void onNext(PutResponse value) {
                Log.d(TAG, "onNext called from stream observer for CID upload.");
            }

            @Override
            public void onError(Throwable t) {
                Log.e(TAG, "Error saving the CID for latest metaDB backup to storage.", t);
            }

            @Override
            public void onCompleted() {
                Log.d(TAG, "Finished upload of CID for metaDb backup to decentralized storage.");
            }
        });
    }

    /**
     * Returns the CID of the latest database backup in decentralized storage.
     */
    void getLatestCid(Function<MetaDbCid, Void> callback) {
        SharedPrefs prefs = SharedPrefs.getInstance(appContext);
        String cached = prefs.getValue(CID_PREFS_KEY);
        long timestamp = prefs.getLong(CID_TIMESTAMP_PREFS_KEY);
        if (cached == null && timestamp == 0) {
            // Try getting the value from decentralized storage.
            Gateway g = storage.getGateway().getInstance();
            String key;
            if (Utils.isClearDevice())
                key = String.format("%s/com.clearos.clearlife/metadb.cid", g.getStorageDid());
            else
                key = String.format("%s/com.clearos.digitallife/metadb.cid", g.getStorageDid());
            g.download(appContext.getString(R.string.SwtBucket), key, new StreamObserver<Object>() {
                @Override
                public void onNext(Object value) {
                    Log.d(TAG, "onNext received object from MetaDb CID download.");
                    String contents = new String(value.getBody().toByteArray(), StandardCharsets.US_ASCII);
                    MetaDbCid payload = new Gson().fromJson(contents, MetaDbCid.class);
                    callback.apply(payload);
                }

                @Override
                public void onError(Throwable t) {
                    Log.e(TAG, "Error downloading CID from decentralized storage.");
                    callback.apply(null);
                }

                @Override
                public void onCompleted() {
                    Log.d(TAG, "completed download of latest metaDb CID.");
                }
            });
        } else {
            MetaDbCid result = new MetaDbCid(cached, timestamp);
            callback.apply(result);
        }
    }

    /**
     * Returns the path to a *local* file that is used for meta-database backups and restores.
     * @throws IOException If the directory/file cannot be created/accessed.
     */
    File getMetaDatabasePath() throws IOException {
        if (createdDbPath != null) {
            return new File(createdDbPath);
        }

        File dbFile = appContext.getDatabasePath(MetaDatabase.getDbName());
        assert dbFile.getParentFile() != null;

        if (!dbFile.getParentFile().exists() && !dbFile.getParentFile().mkdir()) {
            throw new IOException("Couldn't create ClearCLOUD databases directory");
        }

        return dbFile;
    }
}
