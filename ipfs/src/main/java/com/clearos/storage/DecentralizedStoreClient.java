package com.clearos.storage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.clearos.dstorage.IDecentralizedStore;
import com.clearos.dstorage.IRemoteServiceCallback;
import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.ProgressResult;
import com.clearos.dlt.AbstractServiceClient;
import com.clearos.dlt.IRemoteErrorHandler;
import com.clearos.storage.provider.DecentralizedStoreService;
import com.clearos.storage.provider.ProgressViewModel;
import com.clearos.updates.Utils;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class DecentralizedStoreClient extends AbstractServiceClient<IDecentralizedStore> {
    private ProgressViewModel viewModel = null;

    private Map<String, Function<FileDescriptor, Void>> remoteGetCallbacks = new HashMap<>();
    private Map<String, Function<String, Void>> remotePutCallbacks = new HashMap<>();
    private Map<String, Function<String, Void>> remoteDelCallbacks = new HashMap<>();
    private Map<String, Function<ProgressResult, Void>> remoteProgressCallbacks = new HashMap<>();
    private Map<String, Function<ProgressResult, Void>> remoteStartedCallbacks = new HashMap<>();
    private Function<BlockHashFile, Void> processCompletedCallback = null;

    private Function<Integer, Void> registrationStatusCallback = null;
    private final String TAG = "DecentralizedStoreClient";

    public DecentralizedStoreClient(Context appContext, IRemoteErrorHandler handler) {
        super(appContext, handler);
        setTag(TAG);
    }

    public void setViewModel(ProgressViewModel viewModel) {
        this.viewModel = viewModel;
    }
    public ProgressViewModel getViewModel() {
        return viewModel;
    }

    /**
     * Sets a function to call whenever any decentralized PUT/GET process completes. This gives
     * access to the BlockHashFile instance from the operation (as opposed to the local File object
     * on the file system).
     * @param processCompletedCallback Function to call with process completed values.
     */
    public void setProcessCompletedCallback(Function<BlockHashFile, Void> processCompletedCallback) {
        this.processCompletedCallback = processCompletedCallback;
    }
    public Function<BlockHashFile, Void> getProcessCompletedCallback() {
        return processCompletedCallback;
    }

    public void deleteFile(String filename, Function<String, Void> callback) {
        try {
            if (callback != null) {
                remoteDelCallbacks.put(filename, callback);
            }
            s().delete(filename);
        } catch (RemoteException e) {
            h().onRemoteError(e);
        }
    }

    /**
     * Stores a file in decentralized storage.
     */
    public void storeFile(String filename, String displayName, Uri contentUri,
                          Function<String, Void> callback,
                          Function<ProgressResult, Void> progressCallback,
                          Function<ProgressResult, Void> startedCallback) {
        try {
            if (callback != null) {
                remotePutCallbacks.put(filename, callback);
            }
            if (progressCallback != null) {
                remoteProgressCallbacks.put(filename, progressCallback);
            }
            if (startedCallback != null) {
                remoteStartedCallbacks.put(filename, startedCallback);
            }
            s().store(filename, displayName, contentUri);
        } catch (RemoteException e) {
            h().onRemoteError(e);
        }
    }

    /**
     * Returns a content URI that allows a file to be grabbed from decentralized storage.
     */
    public void loadFile(String filename, Function<FileDescriptor, Void> callback,
                         Function<ProgressResult, Void> progressCallback,
                         Function<ProgressResult, Void> startedCallback) {
        try {
            if (callback != null) {
                remoteGetCallbacks.put(filename, callback);
            }
            if (progressCallback != null) {
                remoteProgressCallbacks.put(filename, progressCallback);
            }
            if (startedCallback != null) {
                remoteStartedCallbacks.put(filename, startedCallback);
            }
            s().load(filename);
        } catch (RemoteException e) {
            h().onRemoteError(e);
        }
    }

    /**
     * Registers the calling app with ClearCLOUD.
     */
    public void register(String displayName, String did, @NonNull Function<Integer, Void> callback) {
        registrationStatusCallback = callback;
        try {
            s().registerPackage(displayName, did);
        } catch (RemoteException e) {
            h().onRemoteError(e);
        }
    }

    /**
     * Rotates app base encryption keys (or rotated keys) with the home server.
     */
    public void getRegistrationStatus(@NonNull Function<Integer, Void> callback) {
        registrationStatusCallback = callback;
        try {
            s().getPackageRegistrationStatus();
        } catch (RemoteException e) {
            h().onRemoteError(e);
            callback.apply(DecentralizedStoreService.UNREGISTERED);
        }
    }

    /**
     * This implementation is used to receive callbacks from the remote
     * service.
     */
    private IRemoteServiceCallback mCallback = new IRemoteServiceCallback.Stub() {
        @Override
        public void onDecentralizedPut(String fileName) {
            Log.d(TAG, "Decentralized put remote service callback fired.");
            Function<String, Void> callback = remotePutCallbacks.getOrDefault(fileName, null);
            if (callback != null) {
                callback.apply(fileName);
            } else {
                Log.w(TAG, "No callback to return stored file at " + fileName);
            }
        }

        @Override
        public void onRegistrationStatus(int status) {
            Log.d(TAG, "Registration status remote service callback fired.");
            registrationStatusCallback.apply(status);
        }

        @Override
        public void onDecentralizedError(String s) {
            Log.d(TAG, "Decentralized error remote service callback fired.");
            Log.e(TAG, "Error from decentralized remote server: " + s);
        }

        @Override
        public void onDecentralizedGet(Uri uri, String fileName) throws RemoteException {
            Log.d(TAG, "Decentralized get remote service callback fired.");
            ParcelFileDescriptor inputPFD;
            try {
                inputPFD = getAppContext().getContentResolver().openFileDescriptor(uri, "r");
            } catch (FileNotFoundException e) {
                throw new RemoteException("Could not find the file you are trying to store from the given Uri.");
            }

            if (inputPFD != null) {
                FileDescriptor f = inputPFD.getFileDescriptor();
                Function<FileDescriptor, Void> callback = remoteGetCallbacks.getOrDefault(fileName, null);
                if (callback != null) {
                    callback.apply(f);
                } else {
                    Log.w(TAG, "No callback to return request file at " + fileName);
                }
            } else {
                throw new RemoteException("Invalid parcel file descriptor for the content URI " + uri);
            }
        }

        @Override
        public void onDecentralizedDel(String fileName) {
            Log.d(TAG, "Decentralized delete remote service callback fired.");
            Function<String, Void> callback = remoteDelCallbacks.getOrDefault(fileName, null);
            if (callback != null) {
                callback.apply(fileName);
            } else {
                Log.w(TAG, "No callback to return stored file at " + fileName);
            }
        }

        @Override
        public void onBlockProgress(ProgressResult progress) {
            Log.d(TAG, "Decentralized block progress service callback fired.");
            String fileKey = progress.getFileKey();
            Function<ProgressResult, Void> callback = remoteProgressCallbacks.getOrDefault(fileKey, null);
            if (callback != null) {
                callback.apply(progress);
            } else {
                Log.w(TAG, "No callback to apply progress result at " + fileKey);
            }

            if (viewModel != null) {
                viewModel.appendResult(progress);
            }
        }

        @Override
        public void onBlockStarted(ProgressResult block) {
            Log.d(TAG, "Decentralized block started service callback fired.");
            String fileKey = block.getFileKey();
            Function<ProgressResult, Void> callback = remoteStartedCallbacks.getOrDefault(fileKey, null);
            if (callback != null) {
                callback.apply(block);
            } else {
                Log.w(TAG, "No callback to apply block start result at " + fileKey);
            }

            if (viewModel != null) {
                viewModel.appendResult(block);
            }
        }

        @Override
        public void onProcessCompleted(BlockHashFile file) {
            Log.d(TAG, "Decentralized process complete service callback fired.");
            if (processCompletedCallback != null) {
                processCompletedCallback.apply(file);
            } else {
                Log.w(TAG, "No callback to apply for process completed result " + file.cid);
            }
        }
    };

    @Override
    public void registerCallbacks(String packageName) throws RemoteException {
        s().registerCallback(mCallback);
    }

    @Override
    public void unregisterCallbacks(IDecentralizedStore service, String packageName) throws RemoteException {
        s().uregisterCallback(mCallback);
    }

    @Override
    public IDecentralizedStore getServiceStubInterface(IBinder service) {
        return IDecentralizedStore.Stub.asInterface(service);
    }

    @Override
    public Intent getServiceIntent() {
        Intent it = new Intent("StorageService");
        if (Utils.isClearDevice())
            it.setPackage("com.clearos.clearlife");
        else
            it.setPackage("com.clearos.digitallife");
        it.setAction("ClearCLOUD.DecentralizedStore");
        return it;
    }
}