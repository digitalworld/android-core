package com.clearos.storage.model;

import androidx.lifecycle.ViewModel;

import com.clearos.storage.db.DsAppDataSource;
import com.clearos.storage.db.DsApp;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class DsAppViewModel extends ViewModel {
    private final DsAppDataSource dataSource;

    public DsAppViewModel(DsAppDataSource dataSource) {
        this.dataSource = dataSource;
    }
    /**
     * Register one or more apps for decentralized storage access.
     */
    Completable registerApp(DsApp... apps) {
        return dataSource.registerApp(apps);
    }

    /**
     * Returns the database entry for the given package.
     */
    Single<DsApp> getSingleApp(String packageName) {
        return dataSource.getSingleApp(packageName);
    }

    /**
     * Lists all the apps that have registered with ClearCLOUD.
     */
    Flowable<List<DsApp>> getRegisteredApps() {
        return dataSource.getRegisteredApps();
    }

    /**
     * Sums the size of all files that belong to a package.
     */
    Single<Long> getPackageFileSize(String packageName) {
        return getPackageFileSize(packageName);
    }
}