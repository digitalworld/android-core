package com.clearos.storage;

/**
 * Basic (Free): standard latency and data transfer rates. 10/30 data redundancy.
 * Fast ($): low latency and high speed for data transfer. 10/30 data redundancy.
 * Premium ($$): low latency and high speed for data transfer. 20/30 data redundancy.
 */
public enum StorageClass {
    BASIC,
    FAST,
    PREMIUM
}
