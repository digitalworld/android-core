package com.clearos.storage;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;

import com.clearos.dlt.DecryptedMqttMessage;
import com.clearos.dlt.DerivedKeyClient;
import com.clearos.dlt.IRemoteErrorHandler;
import com.clearos.storage.db.MetaDatabase;
import com.clearos.storage.meta.MetaDatabaseLoader;
import com.clearos.storage.provider.DecentralizedStorage;
import com.goterl.lazysodium.exceptions.SodiumException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class DecentralizedLayer implements IRemoteErrorHandler {
    private static DecentralizedLayer mInstance;

    private final String TAG = "APP";
    private Context appContext;
    private Map<String, DecentralizedStorage> stores;
    private MetaDatabase metaDb;
    private DerivedKeyClient keyClient = null;
    private final CountDownLatch dbReady = new CountDownLatch(1);
    private SwtGateway gateway;

    public DecentralizedLayer(Context appContext, SwtGateway gateway, DerivedKeyClient keyClient) {
        if (mInstance == null) {
            mInstance = this;

            stores = new HashMap<>();
            this.appContext = appContext;
            this.keyClient = keyClient;
            this.gateway = gateway;
            Log.d(TAG, "Loaded decentralized layer context and key client. Starting database load.");
            new MetaDatabaseLoader(appContext, gateway, this, db -> {
                Log.d(TAG, "Database loader initializing complete; db = " + db);
                metaDb = db;
                // Trigger the count-down latch. The get method for the metaDb below calls await in case
                // the DocumentsProvider or other services are trying to access the database before it
                // has been restored. But, we don't want application startup to hang while the file is
                // downloaded...
                dbReady.countDown();
                return null;
            }, null);
        }
    }

    /**
     * Returns a key path to use in the bucket object's keys for a given package.
     * @param packageName Name of the package to return keys for.
     */
    private String getKeyPath(String packageName) {
        try {
            return keyClient.hashInBase58(packageName).substring(0, 8);
        } catch (SodiumException e) {
            onGenericError(e);
            return null;
        }
    }

    /**
     * Returns the default ClearCLOUD package decentralized store.
     * @param packageName Package name to return a decentralized store for.
     */
    public DecentralizedStorage getPackageStore(String packageName) {
        if (!stores.containsKey(packageName)) {
            String keyPath = getKeyPath(packageName);
            DecentralizedStorage s = new DecentralizedStorage(appContext, gateway, keyPath, packageName, this);
            stores.put(packageName, s);
        }
        return stores.get(packageName);
    }

    /**
     * Returns the default ClearCLOUD package decentralized store.
     */
    public DecentralizedStorage getPackageStore() {
        String packageName = appContext.getPackageName();
        if (!stores.containsKey(packageName)) {
            DecentralizedStorage s = new DecentralizedStorage(appContext, gateway, null, packageName, this);
            stores.put(packageName, s);
        }
        return stores.get(packageName);
    }

    public DerivedKeyClient getKeyClient() {
        return keyClient;
    }

    public MetaDatabase getMetaDb() {
        try {
            dbReady.await();
            return metaDb;
        } catch (InterruptedException e) {
            Log.e(TAG, "Error initializing metadata database; thread interrupted.", e);
            return null;
        }
    }

    @Override
    public void onGenericError(Exception e) {
        Log.e(TAG, "Generic error from derived key client children.", e);
    }
    @Override
    public void onRemoteError(RemoteException e) {
        Log.e(TAG, "Remote error in derived key client from document provider.", e);
    }
    @Override
    public void onIOError(IOException e) {
        Log.e(TAG, "I/O error in decentralized storage from document provider.", e);
    }
    @Override
    public void onNotificationReceived(DecryptedMqttMessage s) {
        Log.i(TAG, "Notification received by decentralized storage service: " + s);
    }

    public static synchronized DecentralizedLayer getInstance() {
        return mInstance;
    }
}
