package com.clearos.storage.provider;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract.Document;
import android.provider.DocumentsContract.Root;
import android.provider.DocumentsProvider;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.R;
import com.clearos.dlt.DerivedKeyClient;
import com.clearos.dlt.LoggingRemoteErrorHandler;
import com.clearos.storage.DecentralizedLayer;
import com.clearos.storage.SwtGateway;
import com.clearos.storage.db.Converters;
import com.clearos.storage.db.DsApp;
import com.clearos.storage.db.DsDao;
import com.clearos.storage.db.DsFile;
import com.clearos.storage.db.DsFileName;
import com.clearos.storage.db.DsFileNamesWithFiles;
import com.clearos.storage.db.DsFileWithNames;
import com.clearos.storage.db.MetaDatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import io.ipfs.multibase.Base58;

/**
 * Manages documents and exposes them to the Android system for sharing.
 */
public class DecentralizedStorageProvider extends DocumentsProvider {
    private static final String TAG = "ClearCLOUD";

    private static final String[] DEFAULT_ROOT_PROJECTION = new String[]{
            Root.COLUMN_ROOT_ID,
            Root.COLUMN_MIME_TYPES,
            Root.COLUMN_FLAGS,
            Root.COLUMN_ICON,
            Root.COLUMN_TITLE,
            Root.COLUMN_SUMMARY,
            Root.COLUMN_DOCUMENT_ID,
            Root.COLUMN_AVAILABLE_BYTES
    };

    private static final String[] DEFAULT_DOCUMENT_PROJECTION = new String[]{
            Document.COLUMN_DOCUMENT_ID,
            Document.COLUMN_MIME_TYPE,
            Document.COLUMN_DISPLAY_NAME,
            Document.COLUMN_LAST_MODIFIED,
            Document.COLUMN_FLAGS,
            Document.COLUMN_SIZE
    };

    private static final int MAX_SEARCH_RESULTS = 20;
    private static final int MAX_LAST_MODIFIED = 5;

    // A file object at the root of the file hierarchy.  Depending on your implementation, the root
    // does not need to be an existing file system directory.  For example, a tag-based document
    // provider might return a directory containing all tags, represented as child directories.
    private DsDao dao = null;
    private Context appContext;
    private DerivedKeyClient keyClient;
    private String rootDid;
    private CountDownLatch startupLatch;

    @Override
    public boolean onCreate() {
        appContext = getContext();
        assert appContext != null;

        startupLatch = new CountDownLatch(1);
        new Handler().postDelayed(() -> {
            keyClient = new DerivedKeyClient(getContext(), new LoggingRemoteErrorHandler());
            keyClient.create(appContext, 2000, kc -> {
                if (kc == null) {
                    Log.w(TAG, "Document provider could not initialize key client.");
                } else {
                    try {
                        // Note that this constructor only initializes the static, synchronized gateway object
                        // *only* if it hasn't been done already.
                        SwtGateway gateway = new SwtGateway(keyClient);
                    } catch (Exception e) {
                        Log.e("SWT", "Failure creating & starting SWT gateway.", e);
                    }

                    Log.i(TAG, "Key client for storage provider initialized and decentralized layer.");
                }
                startupLatch.countDown();
                return null;
            });
        }, 2500);

        Log.v(TAG, "onCreate for document provider.");
        return true;
    }

    @Override
    public Cursor queryRoots(String[] projection) throws FileNotFoundException {
        Log.v(TAG, "queryRoots");
        try {
            startupLatch.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.d(TAG, "Thread interrupted waiting for startup latch to trigger.");
        }

        // Create a cursor with either the requested fields, or the default projection.  This
        // cursor is returned to the Android system picker UI and used to display all roots from
        // this provider.
        final MatrixCursor result = new MatrixCursor(resolveRootProjection(projection));


        if (dao == null) {
            dao = MetaDatabase.getInstance(getContext()).dao();
        } else {
            Log.w(TAG, "Cannot initialize storage provider; decentralized layer does not exist.");
            return result;
        }

        final MatrixCursor.RowBuilder row = result.newRow();
        String displayName = appContext.getString(R.string.app_name);
        addPackageRoot(row, appContext.getPackageName(), displayName);

        return result;
    }

    private void addPackageRoot(MatrixCursor.RowBuilder row, String packageName, String displayName) {
        String summary = "Decentralized";

        DecentralizedStorage s = DecentralizedLayer.getInstance().getPackageStore(packageName);
        if (packageName.equals(appContext.getPackageName())) {
            rootDid = s.getDid();
        }
        StorageClassRoot root = s.getRoot();

        row.add(Root.COLUMN_ROOT_ID, packageName);
        row.add(Root.COLUMN_SUMMARY, summary);
        row.add(Root.COLUMN_FLAGS, Root.FLAG_SUPPORTS_CREATE |
                Root.FLAG_SUPPORTS_RECENTS |
                Root.FLAG_SUPPORTS_SEARCH);

        // COLUMN_TITLE is the root title (e.g. what will be displayed to identify your provider).
        row.add(Root.COLUMN_TITLE, displayName);

        // This document id must be unique within this provider and consistent across time.  The
        // system picker UI may save it and refer to it later.
        row.add(Root.COLUMN_DOCUMENT_ID, root.getRootKey());

        // The child MIME types are used to filter the roots and only present to the user roots
        // that contain the desired type somewhere in their file hierarchy.
        row.add(Root.COLUMN_MIME_TYPES, getChildMimeTypes());
        row.add(Root.COLUMN_AVAILABLE_BYTES, root.getFreeSpace());
        row.add(Root.COLUMN_ICON, getPackageIcon(packageName));
    }

    PackageInfo getPackageInfo(String packageName) {
        try {
            return appContext.getPackageManager().getPackageInfo(packageName, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error finding package icon.", e);
            return null;
        }
    }

    Drawable getPackageIcon(String packageName) {
        try {
            return appContext.getPackageManager().getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error finding package icon.", e);
            return null;
        }
    }

    @Override
    public Cursor queryRecentDocuments(String rootId, String[] projection)
            throws FileNotFoundException {
        Log.v(TAG, "queryRecentDocuments");

        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));
        List<DsFileName> recents = dao.listRecentFiles(rootId, MAX_LAST_MODIFIED).blockingSingle();
        for (DsFileName f : recents) {
            DsFile file = dao.searchFilesbyName(rootId, f.filename, 1).blockingFirst().get(0).getMostRecentFile();
            includeFile(result, file, rootId);
        }

        return result;
    }

    @Override
    public Cursor querySearchDocuments(String rootId, String query, String[] projection) {
        Log.v(TAG, "querySearchDocuments");

        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));
        List<DsFileNamesWithFiles> searches = dao.searchFilesbyName(rootId, query, MAX_SEARCH_RESULTS).blockingSingle();
        for (DsFileNamesWithFiles fn : searches) {
            for (DsFile f : fn.files) {
                includeFile(result, f, fn.fileName.packageName);
            }
        }

        return result;
    }

    private DecentralizedStorage getPackageStore(String packageName) throws FileNotFoundException {
        DecentralizedStorage storage = DecentralizedLayer.getInstance().getPackageStore(packageName);
        if (storage == null) {
            throw new FileNotFoundException(String.format("Could not find decentralized storage manager for %s",
                    packageName));
        }
        return storage;
    }

    @Override
    public AssetFileDescriptor openDocumentThumbnail(String documentId, Point sizeHint,
                                                     CancellationSignal signal)
            throws FileNotFoundException {
        Log.v(TAG, "openDocumentThumbnail");

        // We don't actually have any thumbnails made for these documents yet... Need to figure out
        // what to do about that.
        final DsFile file = getFileForDocId(documentId);
        // Get a filename to grab an authorized package for grabbing the file.
        final DsFileName fileName = dao.listFileNamesForCid(file.cid).blockingFirst().get(0);
        DecentralizedStorage storage = getPackageStore(fileName.packageName);

        final ParcelFileDescriptor pfd =
                ParcelFileDescriptor.open(file.getCidFile(storage), ParcelFileDescriptor.MODE_READ_ONLY);
        return new AssetFileDescriptor(pfd, 0, AssetFileDescriptor.UNKNOWN_LENGTH);
    }

    @Override
    public Cursor queryDocument(String documentId, String[] projection) {
        Log.v(TAG, "queryDocument with id = " + documentId);

        // Create a cursor with the requested projection, or the default projection.
        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));

        if (documentId.equals(rootDid)) {
            // We are querying the root of the overall decentralized storage. Show the various apps
            // that we have available who have registered for storage.
            List<DsApp> registeredApps = dao.getRegisteredApps().blockingSingle();
            if (registeredApps != null) {
                for (DsApp app : registeredApps) {
                    includeApp(result, app);
                }
            }

        } else {
            // We are providing a specific document based on its CID, or based on the name of
            // another package.
            DsFile file = getFileForDocId(documentId);
            if (file != null) {
                includeFile(result, file, null);
            }
        }
        return result;
    }

    /**
     * Add a representation of an app to a cursor.
     *
     * @param result the cursor to modify
     * @param app    The application that has registered with ClearCLOUD.
     */
    private void includeApp(MatrixCursor result, DsApp app) {
        int flags = 0;

        // Because we use a file system based on IPFS, the properties of the files are set. Files
        // are technically writeable, though the file CID will change. The metadata database keeps
        // a list of mappings of CID to file names.
        flags |= Document.FLAG_SUPPORTS_WRITE;

        final String displayName = app.displayName;
        final Date lastModified = app.added;
        final long appSize = dao.getPackageFileSize(app.packageName).blockingGet();

        final MatrixCursor.RowBuilder row = result.newRow();
        row.add(Document.COLUMN_DOCUMENT_ID, app.packageName);
        row.add(Document.COLUMN_DISPLAY_NAME, displayName);
        row.add(Document.COLUMN_SIZE, appSize);
        row.add(Document.COLUMN_MIME_TYPE, Document.MIME_TYPE_DIR);
        row.add(Document.COLUMN_LAST_MODIFIED, lastModified);
        row.add(Document.COLUMN_FLAGS, flags);

        // Add a custom icon
        Drawable customIcon = getPackageIcon(app.packageName);
        if (customIcon != null) {
            row.add(Document.COLUMN_ICON, customIcon);
        }
    }

    @Override
    public Cursor queryChildDocuments(String parentDocumentId, String[] projection, String sortOrder) {
        Log.v(TAG, "queryChildDocuments, parentDocumentId: " +
                parentDocumentId +
                " sortOrder: " +
                sortOrder);

        final MatrixCursor result = new MatrixCursor(resolveDocumentProjection(projection));
        final DsFile parent = getFileForDocId(parentDocumentId);
        for (String cid : parent.getChildren()) {
            DsFile file = getFileForDocId(cid);
            includeFile(result, file, null);
        }
        return result;
    }

    @Override
    public ParcelFileDescriptor openDocument(final String documentId, final String mode,
                                             CancellationSignal signal)
            throws FileNotFoundException {
        Log.v(TAG, "openDocument, mode: " + mode);

        final DsFile file = getFileForDocId(documentId);
        // Get a recent file name to grab a packageName that has access to cid file
        final DsFileName fileName = dao.listFileNamesForCid(file.cid).blockingFirst().get(0);
        final int accessMode = ParcelFileDescriptor.parseMode(mode);
        DecentralizedStorage storage = getPackageStore(fileName.packageName);
        File dsFile = file.getCidFile(storage);

        // Check if this is one of our random documents created by the OS using createDocument.
        boolean isRandom = Base58.decode(documentId).length == 32;

        if (!dsFile.exists() && !isRandom) {
            FileOutputStream oStream = new FileOutputStream(dsFile);
            DecentralizedFile dFile = storage.syncDownload(file.cid, oStream, signal);
            if (!dsFile.exists()) {
                throw new FileNotFoundException(String.format("Could not download %s from decentralized storage.", documentId));
            }
        }

        final boolean isWrite = (mode.indexOf('w') != -1);
        if (isWrite) {
            // Attach a close listener if the document is opened in write mode. Also, make a copy of
            // the file locally to a temporary, random CID (if it isn't already random), so that we
            // don't mess up the hash-based file naming scheme.
            File randFile = null;
            if (!isRandom) {
                String randHash = DecentralizedStorage.randomHash();
                randFile = new File(dsFile.getParentFile(), randHash);
                Path destination = randFile.toPath();
                try {
                    Files.copy(dsFile.toPath(), destination);
                } catch (IOException e) {
                    throw new FileNotFoundException(String.format("Error copying file from %s to %s preempting write that changes file hash.", file.cid, randHash));
                }
            }

            final File randSource = randFile;
            try {
                Handler handler = new Handler(getContext().getMainLooper());
                return ParcelFileDescriptor.open(dsFile, accessMode, handler,
                        new ParcelFileDescriptor.OnCloseListener() {
                            @Override
                            public void onClose(IOException e) {
                                // First, upload the new file to decentralized storage. Then, since
                                // its contents may have changed, rename the file to its new CID.
                                try {
                                    FileInputStream iFile = new FileInputStream(dsFile);
                                    BlockHashFile uFile = storage.syncUpload(dsFile.length(), iFile, signal, null);
                                    File newName = new File(dsFile.getParentFile(), uFile.cid.toBase58());
                                    if (!dsFile.renameTo(newName)) {
                                        Log.e(TAG, "Error renaming uploaded file to new CID.");
                                    }

                                    if (randSource != null) {
                                        File oldFile = new File(dsFile.getParentFile(), file.cid);
                                        if (!randSource.renameTo(oldFile)) {
                                            Log.e(TAG, "Error renaming random file to old CID.");
                                        }
                                    }
                                } catch (FileNotFoundException fnfe) {
                                    Log.i(TAG, "File not found exception while uploading modified file. Should be impossible.", fnfe);
                                }
                            }
                        });
            } catch (IOException e) {
                throw new FileNotFoundException("Failed to open document with id " + documentId +
                        " and mode " + mode);
            }
        } else {
            return ParcelFileDescriptor.open(dsFile, accessMode);
        }
    }

    /**
     * Creates a new document in the metadata database to pre-empt an upload of an *actual* document
     * to the decentralized storage.
     *
     * @param prefix Prefix (directory tree) in decentralized storage that the file will eventually
     *               be added to.
     */
    @Override
    public String createDocument(String prefix, String mimeType, String displayName) {
        Log.v(TAG, "createDocument");

        // First, create the DsFile; the file name is added separately.
        DsFile dsFile = new DsFile(DecentralizedStorage.randomHash());
        dsFile.isDirectory = false;
        dsFile.created = new Date();
        dsFile.children = null;
        dsFile.mimeType = mimeType;
        dsFile.size = 0;
        dao.addFiles(dsFile);

        DsFileName fileName = new DsFileName();
        fileName.touched = dsFile.created;
        fileName.filename = displayName;
        // null file source is the document provider.
        fileName.source = null;

        return dsFile.cid;
    }

    @Override
    public void deleteDocument(String documentId) throws FileNotFoundException {
        Log.v(TAG, "deleteDocument");
        // For deletes, just use the main package account.
        DecentralizedStorage storage = getPackageStore(appContext.getPackageName());
        storage.syncDelete(documentId);
    }

    @Override
    public String getDocumentType(String documentId) {
        DsFile file = getFileForDocId(documentId);
        return getTypeForFile(file);
    }

    /**
     * @param projection the requested root column projection
     * @return either the requested root column projection, or the default projection if the
     * requested projection is null.
     */
    private static String[] resolveRootProjection(String[] projection) {
        return projection != null ? projection : DEFAULT_ROOT_PROJECTION;
    }

    private static String[] resolveDocumentProjection(String[] projection) {
        return projection != null ? projection : DEFAULT_DOCUMENT_PROJECTION;
    }

    /**
     * Get a file's MIME type
     *
     * @param file the File object whose type we want
     * @return the MIME type of the file
     */
    private static String getTypeForFile(DsFile file) {
        if (file.isDirectory) {
            return Document.MIME_TYPE_DIR;
        } else {
            return file.mimeType;
        }
    }

    /**
     * Get the MIME data type of a document, given its filename.
     *
     * @param name the filename of the document
     * @return the MIME data type of a document
     */
    public static String getTypeForName(String name) {
        final int lastDot = name.lastIndexOf('.');
        if (lastDot >= 0) {
            final String extension = name.substring(lastDot + 1);
            final String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            if (mime != null) {
                return mime;
            }
        }
        return "application/octet-stream";
    }

    /**
     * Gets a string of unique MIME data types that the decentralized storage supports (which is
     * all of them).
     *
     * @return a string of the unique MIME data types.
     */
    private String getChildMimeTypes() {
        Set<String> mimeTypes = new HashSet<>();
        mimeTypes.add("image/*");
        mimeTypes.add("text/*");
        mimeTypes.add("application/*");
        mimeTypes.add("audio/*");
        mimeTypes.add("video/*");
        mimeTypes.add("font/*");

        // Flatten the list into a string and insert newlines between the MIME type strings.
        StringBuilder mimeTypesString = new StringBuilder();
        for (String mimeType : mimeTypes) {
            mimeTypesString.append(mimeType).append("\n");
        }

        return mimeTypesString.toString();
    }

    /**
     * Get the CID given a File.
     *
     * @param file the File whose document ID you want
     * @return the corresponding document ID
     */
    private String getDocIdForFile(DsFile file) {
        return file.cid;
    }

    /**
     * Add a representation of a file to a cursor.
     *
     * @param result the cursor to modify
     * @param file   the File object representing the desired file (may be null if given docID)
     */
    private void includeFile(MatrixCursor result, DsFile file, String packageName) {
        int flags = 0;

        // Because we use a file system based on IPFS, the properties of the files are set. Files
        // are technically writeable, though the file CID will change. The metadata database keeps
        // a list of mappings of CID to file names.
        flags |= Document.FLAG_SUPPORTS_WRITE;
        flags |= Document.FLAG_SUPPORTS_DELETE;

        // Even though there are many file names available, just grab the latest one. The
        // query already sorts related file names be most recent touched.

        DsFileName fileName;
        if (packageName == null) {
            fileName = dao.listFileNamesForCid(file.cid).blockingFirst().get(0);
        } else {
            fileName = dao.lookupFileName(packageName, file.cid).blockingFirst();
        }
        final String displayName = fileName.filename;
        final Date lastModified = file.created;
        final String mimeType = getTypeForFile(file);

        if (mimeType.startsWith("image/")) {
            // Allow the image to be represented by a thumbnail rather than an icon
            flags |= Document.FLAG_SUPPORTS_THUMBNAIL;
        }

        final MatrixCursor.RowBuilder row = result.newRow();
        row.add(Document.COLUMN_DOCUMENT_ID, file.cid);
        row.add(Document.COLUMN_DISPLAY_NAME, displayName);
        row.add(Document.COLUMN_SIZE, file.size);
        row.add(Document.COLUMN_MIME_TYPE, mimeType);
        row.add(Document.COLUMN_LAST_MODIFIED, lastModified);
        row.add(Document.COLUMN_FLAGS, flags);

        // Add a custom icon
        int customIcon = getFileIcon(displayName);
        if (customIcon > 0) {
            row.add(Document.COLUMN_ICON, customIcon);
        }
    }

    /**
     * Returns a custom icon based on the file name.
     *
     * @param fileName Name of the file to get an icon for.
     * @return Resource id to use as the icon.
     */
    int getFileIcon(String fileName) {
        return R.drawable.ic_clearcloud_icon;
    }

    /**
     * Translate your custom URI scheme into a File object.
     *
     * @param docId the document ID representing the desired file
     * @return a File represented by the given document ID
     */
    private DsFile getFileForDocId(String docId) {
        return dao.getSingleFile(docId).blockingGet().get(0);
    }
}