package com.clearos.storage.provider;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;

import com.clearos.dstorage.AwsBlockReader;
import com.clearos.dstorage.AwsBlockWriter;
import com.clearos.dstorage.AwsStaticCredentials;
import com.clearos.dstorage.IProgressHandler;
import com.clearos.dstorage.R;
import com.clearos.dlt.DerivedKeyClient;
import com.clearos.storage.DecentralizedLayer;
import com.clearos.dstorage.BlockHashEncrypter;
import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.LoggingStorageReporter;
import com.clearos.dstorage.StorageReporter;
import com.clearos.storage.db.DsFileCrossRefDsFileName;
import com.clearos.storage.meta.AsyncDao;
import com.clearos.storage.db.DbQueue;
import com.clearos.dlt.DidKeys;
import com.clearos.dlt.IRemoteErrorHandler;
import com.clearos.storage.SwtGateway;
import com.clearos.storage.db.DsFile;
import com.clearos.storage.db.DsFileName;
import com.google.protobuf.Empty;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import io.grpc.stub.StreamObserver;
import io.ipfs.multibase.Base58;

public class DecentralizedStorage {
    private Context applicationContext;
    private BlockHashEncrypter encrypter;
    private String keyPath;
    private DidKeys baseKeys;
    private boolean isAWS;
    private DerivedKeyClient keyClient;
    private StorageClassRoot root;
    private StorageReporter reporter = new LoggingStorageReporter();
    private IRemoteErrorHandler errorHandler;
    private DecentralizedStoreService service;
    private SwtGateway gateway;

    private Map<String, BlockHashFile> syncUploads = new HashMap<>();
    private Map<String, DecentralizedFile> syncDownloads = new HashMap<>();

    private Map<String, CancellationSignal> runningUploads = new HashMap<>();
    private Map<String, CancellationSignal> runningDownloads = new HashMap<>();
    private Set<String> runningDeletes = new HashSet<>();
    private Map<String, Function<File, Void>> downloadCallbacks = new HashMap<>();
    private Map<String, Function<String, Void>> deleteCallbacks = new HashMap<>();
    private Map<String, Function<BlockHashFile, Void>> uploadCallbacks = new HashMap<>();
    private Map<DecentralizedStoreOperation, Integer> maxConcurrentOps = new HashMap<>();

    private Map<DecentralizedStoreOperation, CountDownLatch> processBarrier = new HashMap<>();
    private String TAG;

    private static int KB = 1024;

    /**
     * Run the process queue operations every fifteen minutes if not called explicitly.
     */
    private int processFrequency = 15;

    private CountDownLatch syncProcessLatch;
    private Runnable processRunnable = new Runnable() {
        @Override
        public void run() {
            // Run each of the process queues one-by-one. We use a latch so that additional runnables
            // won't start if one is already queued.
            syncProcessLatch = new CountDownLatch(1);
            try {
                processQueue(DecentralizedStoreOperation.PUT);
                processQueue(DecentralizedStoreOperation.GET);
                processQueue(DecentralizedStoreOperation.DEL);
            } catch (Exception e) {
                Log.e(TAG, "Error during scheduled processing of queue.", e);
            }
            syncProcessLatch.countDown();
        }
    };
    private ScheduledExecutorService scheduler;

    /**
     * Initializes a client for decentralized storage.
     *
     * @param appContext application context from calling activity.
     * @param keyPath    '/'-separated path of "folders" to nest CIDs under (after the DID).
     * @param handler    error handler for remote and IO errors.
     */
    public DecentralizedStorage(Context appContext, SwtGateway gateway, String keyPath, String packageName, IRemoteErrorHandler handler) {
        this(appContext, gateway, keyPath, packageName, handler, true);
    }

    /**
     * Initializes a client for decentralized storage.
     *
     * @param appContext    application context from calling activity.
     * @param keyPath       '/'-separated path of "folders" to nest CIDs under (after the DID).
     * @param handler       error handler for remote and IO errors.
     * @param configureRoot When true, configure a storage class root that has information about
     *                      free space, etc. for the Decentralized Storage object.
     */
    public DecentralizedStorage(Context appContext, SwtGateway gateway, String keyPath, String packageName, IRemoteErrorHandler handler, boolean configureRoot) {
        applicationContext = appContext;
        this.keyPath = keyPath;
        keyClient = DecentralizedLayer.getInstance().getKeyClient();
        baseKeys = keyClient.getAppKeys(packageName, 0);
        encrypter = new BlockHashEncrypter(keyClient, 384 * (long) (1024 * 1024));
        isAWS = appContext.getResources().getBoolean(R.bool.useAWS);
        if (configureRoot) {
            root = new StorageClassRoot(keyClient);
        }
        errorHandler = handler;
        TAG = "dStore:" + packageName;

        this.gateway = gateway;
        startScheduler();
    }

    public IRemoteErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public Context getApplicationContext() {
        return applicationContext;
    }

    public SwtGateway getGateway() {
        return gateway;
    }

    /**
     * Shuts down this decentralized storage object.
     */
    public void shutdown() {
        Log.d(TAG, "Shutting down decentralized storage gracefully.");
        cancelScheduler();
        cancelProcessing();
        if (gateway != null) {
            // We are running in AWS mode...
            gateway.shutdown();
        }
    }

    /**
     * Sets the maximum number of operations that can be performed at the same time.
     */
    public void setMaxConcurrentOperations(DecentralizedStoreOperation operation, int maxConcurrent) {
        maxConcurrentOps.put(operation, maxConcurrent);
    }

    /**
     * Sets the frequency (in minutes) at which decentralized storage operations are processed. If
     * methods are called directly, they can be processed immediately.
     *
     * @param processFrequency minutes between process calls.
     */
    public void setProcessFrequency(int processFrequency) {
        this.processFrequency = processFrequency;
    }

    public DecentralizedStoreService getService() {
        return service;
    }

    /**
     * Sets the decentralized storage service for progress handling to remote clients.
     */
    public void setService(DecentralizedStoreService service) {
        this.service = service;
    }

    /**
     * Cancels all running decentralized storage operations.
     */
    public void cancelProcessing() {
        // First, tell all of the running jobs to stop.
        for (Map.Entry<String, CancellationSignal> entry : runningUploads.entrySet()) {
            entry.getValue().cancel();
        }
        for (Map.Entry<String, CancellationSignal> entry : runningDownloads.entrySet()) {
            entry.getValue().cancel();
        }
    }

    /**
     * Starts the scheduler that runs processing of queued files every `processFrequency` minutes.
     */
    public void startScheduler() {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(processRunnable, 15, processFrequency, TimeUnit.MINUTES);
    }

    /**
     * Cancels the scheduled task that runs processing every few minutes. It will not restart unless
     * an explicit call to `startScheduler` is made.
     */
    public void cancelScheduler() {
        scheduler.shutdown();
        try {
            scheduler.awaitTermination(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e("DSMAN", "Process scheduler didn't terminate within requested time.", e);
        }
    }

    /**
     * Gets the number of simultaneous decentralized operations that can be performed for the given category.
     */
    public Integer getMaxConcurrentOperations(DecentralizedStoreOperation operation) {
        return maxConcurrentOps.getOrDefault(operation, 3);
    }

    public StorageClassRoot getRoot() {
        return root;
    }

    public String getDid() {
        return keyClient.getDid();
    }

    /**
     * Checks whether a file operation against decentralized storage is in the queue already. If not
     * then a new object is created (but not added to the database yet).
     *
     * @param path Path of the existing file in the cache (for GET/DEL) or the Uri for PUT.
     * @param name CID for GET/DEL operations.
     */
    private void updateDbQueueEntry(DbQueue existing, String path, String name, DecentralizedStoreOperation operation) {
        existing.filepath = path;
        // For delete and get operations, we are interested also in the CID, which is
        // the filename specified (in the cache directory). For PUT operations, this is
        // actually the displayName that we are carrying through for the callback.
        existing.cid = name;
        existing.finished = null;
        existing.errorMsg = null;
        existing.cancelled = null;
        existing.started = null;
        existing.added = new Date();
        existing.progress = 0;
        existing.operation = operation;
    }

    /**
     * Converts a list of File targets to DbQueue objects that for inserting into the database.
     *
     * @param targets Files that will be queued for decentralized storage operation.
     */
    private void toQueueFiles(DecentralizedStoreOperation operation, Function<Pair<DbQueue[], DbQueue[]>, Void> callback, File... targets) {
        List<DbQueue> addFiles = new ArrayList<>();
        List<DbQueue> updateFiles = new ArrayList<>();

        String[] checkPaths = new String[targets.length];
        int i = 0;
        for (File f : targets) {
            checkPaths[i] = f.getPath();
            i++;
        }

        AsyncDao.getQueueFiles(applicationContext, operation, inDb -> {
            Map<String, DbQueue> existingQueue = new HashMap<>();
            for (DbQueue x : inDb) {
                existingQueue.put(x.filepath, x);
            }

            for (File f : targets) {
                // If one of these files is already in the database, just reset its fields so that it
                // can be processed again. Then include it in the "update" list.
                DbQueue existing;
                if (existingQueue.containsKey(f.getPath())) {
                    existing = existingQueue.get(f.getPath());
                    updateFiles.add(existing);
                } else {
                    existing = new DbQueue();
                    addFiles.add(existing);
                }
                updateDbQueueEntry(existing, f.getPath(), f.getName(), operation);
            }

            callback.apply(new Pair<>(addFiles.toArray(new DbQueue[0]), updateFiles.toArray(new DbQueue[0])));
            return null;
        }, checkPaths);
    }

    /**
     * Converts a list of File targets to DbQueue objects that for inserting into the database.
     *
     * @param targets Uri from another app that will be queued for decentralized storage PUT.
     */
    @SafeVarargs
    private final void toPutQueueFiles(Function<Pair<DbQueue[], DbQueue[]>, Void> callback, Pair<Uri, String>... targets) {
        List<DbQueue> addFiles = new ArrayList<>();
        List<DbQueue> updateFiles = new ArrayList<>();

        String[] checkPaths = new String[targets.length];
        int i = 0;
        for (Pair<Uri, String> t : targets) {
            checkPaths[i] = t.first.toString();
            i++;
        }

        AsyncDao.getQueueFiles(applicationContext, DecentralizedStoreOperation.PUT, inDb -> {
            Map<String, DbQueue> existingQueue = new HashMap<>();
            if (inDb != null) {
                for (DbQueue x : inDb) {
                    existingQueue.put(x.filepath, x);
                }
            }

            DbQueue existing;
            for (Pair<Uri, String> t : targets) {
                // If one of these files is already in the database, just reset its fields so that it
                // can be processed again. Then include it in the "update" list.
                String uri = t.first.toString();
                if (existingQueue.containsKey(uri)) {
                    existing = existingQueue.get(uri);
                    updateFiles.add(existing);
                } else {
                    existing = new DbQueue();
                    addFiles.add(existing);
                }
                updateDbQueueEntry(existing, uri, t.second, DecentralizedStoreOperation.PUT);
            }

            callback.apply(new Pair<>(addFiles.toArray(new DbQueue[0]), updateFiles.toArray(new DbQueue[0])));
            return null;
        }, checkPaths);
    }

    /**
     * Returns the number of open slots available for concurrent processing in the given operation.
     */
    private int openSlots(DecentralizedStoreOperation operation) {
        switch (operation) {
            case GET:
                return getMaxConcurrentOperations(operation) - runningDownloads.size();
            case PUT:
                return getMaxConcurrentOperations(operation) - runningUploads.size();
            case DEL:
                return getMaxConcurrentOperations(operation);
            default:
                return 3;
        }
    }

    /**
     * Processes the next `n` outstanding files that need uploaded/downloaded/deleted.
     */
    public void processQueue(DecentralizedStoreOperation operation) {
        Log.d(TAG, "Process queue called for operation " + operation.name());
        if (processBarrier.containsKey(operation)) {
            try {
                CountDownLatch barrier = processBarrier.get(operation);
                if (barrier != null) {
                    Log.d(TAG, "Awaiting process barrier for 30 seconds.");
                    if (!barrier.await(3, TimeUnit.SECONDS)) {
                        // We timed out on waiting for the existing operation to finish. Abort this
                        // particular method call. Another will be scheduled soon.
                        Log.d(TAG, "Process barrier did not complete within timeout; not starting another process.");
                        return;
                    }
                }
            } catch (InterruptedException e) {
                Log.i("DSPROC", "Processing thread for operation interrupted while waiting for next to start.");
                return;
            }
        }

        // Since this method can be called multiple times from different threads, we put a latch
        // in place to make sure only one instance of this method runs at any given time. The other
        // ones will wait on entry. This ensures that the database concurrency is good (i.e., started
        // file uploads will be recorded correctly before pending uploads are gotten).
        int limit = openSlots(operation);
        Log.d(TAG, String.format("There are %d open slots available for processing.", limit));
        if (limit == 0) return;
        else processBarrier.remove(operation);

        CountDownLatch pLatch = new CountDownLatch(1);
        processBarrier.put(operation, pLatch);
        Log.d(TAG, "Added process barrier with latch; processing pending files");
        AsyncDao.pendingFiles(applicationContext, operation, limit, pending -> {
            // This loop won't run if there aren't any pending files for the operation.
            if (pending != null) {
                Log.d(TAG, String.format("Found %d pending files to process; starting operations.", pending.length));
                for (DbQueue dbq : pending) {
                    startSingle(dbq);
                    dbq.started = new Date();
                }

                // Finally, update the database with the "started" status we just set. Since we are already
                // in a separate thread, we don't have to use the async interface here.
                if (pending.length > 0) {
                    Log.d(TAG, "Updating started date/time for pending files in database.");
                    AsyncDao.updateQueueFiles(applicationContext, v -> {
                        pLatch.countDown();
                        return null;
                    }, pending);
                }
            }
            processBarrier.remove(operation);

            return null;
        });
    }

    private void queueNextProcess() {
        // Wait for 1 second to see if there is another runnable processing right now.
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            if (syncProcessLatch == null || syncProcessLatch.getCount() == 0) {
                // Post the start of a new runnable since the existing ones are done.
                processRunnable.run();
            }
        }, 250);
    }

    /**
     * Creates a new DsFile entry in the database for a queued file that was successfully loaded
     * into decentralized storage. It also Creates or updates the FileName for reference and
     * associated connection between the fileName and DsFile.
     *
     * @param putFile     File that was successfully uploaded.
     * @param packageName name of the calling application that should have access
     */
    private void convertQueueToFile(String packageName, DbQueue putFile, BlockHashFile BlockHashFile, Function<BlockHashFile, Void> callback) {
        // Check to see if File Cid already exists
        String fileCid = BlockHashFile.cid.toBase58();
        Log.d(TAG, "Saving BlockHashFile of " + fileCid);

        AsyncDao.getSingleFile(applicationContext, fileCid, dsFile -> {
            DsFile file;
            // If file exists just update created
            if (dsFile != null) {
                file = dsFile;
            }
            // Otherwise create a new file
            else {
                file = new DsFile(fileCid);
                file.isDirectory = false;
                file.size = BlockHashFile.size;
                file.mimeType = DecentralizedStorageProvider.getTypeForName(putFile.filepath);
            }
            file.created = putFile.finished;

            Log.d(TAG, "Saving file cid of " + fileCid);

            // Check to see if this filename already exists
            AsyncDao.getSingleFileName(applicationContext, packageName, putFile.cid, dsFileName -> {
                DsFileName fileName;
                // If filename exists just update touched
                if (dsFileName != null) {
                    fileName = dsFileName;
                }
                // Otherwise create a new Filename
                else {
                    // Create new filename to add
                    fileName = new DsFileName();
                    // The putFile CID field is actually used to store the display name or filename.
                    fileName.filename = putFile.cid;
                    fileName.packageName = packageName;
                    fileName.source = "cc";
                }
                fileName.touched = new Date();

                // Check save or update for fileName
                if (dsFileName == null) {
                    // Add both
                    AsyncDao.addFiles(applicationContext, fBoolean -> {
                        AsyncDao.addFileNames(applicationContext, fnBoolean -> {
                            // Retrieve FileNameId to link
                            AsyncDao.getSingleFileName(applicationContext, packageName, fileName.filename, link -> {
                                connectFileToFileName(file.cid, link.fileNameId, BlockHashFile, callback);
                                return null;
                            });
                            return null;
                        }, fileName);
                        return null;
                    }, file);
                } else {
                    // add file and update fileName
                    AsyncDao.addFiles(applicationContext, fBoolean -> {
                        AsyncDao.updateFileNames(applicationContext, fnBoolean -> {
                            // Link new file to filename
                            connectFileToFileName(file.cid, fileName.fileNameId, BlockHashFile, callback);
                            return null;
                        }, fileName);
                        return null;
                    }, file);
                }
                return null;
            });
            return null;
        });
    }

        private void connectFileToFileName (String cid,int fileNameId, BlockHashFile
        BlockHashFile, Function < BlockHashFile, Void > callback){
            // Create a Cross Reference
            DsFileCrossRefDsFileName crossRef = new DsFileCrossRefDsFileName();
            crossRef.cid = cid;
            crossRef.rowid = fileNameId;
            // Save to database
            AsyncDao.addFileCrossRefFileNames(applicationContext, aBoolean -> {
                // Implement Callback on completion
                callback.apply(BlockHashFile);
                return null;
            }, crossRef);
        }

        @SuppressLint("Recycle")
        private void startSingle (DbQueue target){
            // Create a storage reporter that interfaces with the database.
            DbStorageReporter reporter = new DbStorageReporter(applicationContext, target);
            CancellationSignal signal;

            switch (target.operation) {
                case GET:
                    signal = new CancellationSignal();
                    runningDownloads.put(target.cid, signal);
                    try {
                        File targetFile = new File(target.filepath);
                        FileOutputStream getStream = new FileOutputStream(targetFile);
                        download(target.cid, getStream, df -> {
                            // Remove the running download from the list so that another can be queued.
                            String cid = df.getDescriptor().cid.toBase58();
                            runningDownloads.remove(cid);
                            // See if there is a callback that we are supposed to fire.
                            if (downloadCallbacks.containsKey(cid)) {
                                Function<File, Void> callback = downloadCallbacks.get(cid);
                                if (callback != null) {
                                    callback.apply(targetFile);
                                }
                            }
                            // Start the next download if there is one.
                            queueNextProcess();
                            return null;
                        }, null, signal, reporter);
                    } catch (IOException e) {
                        errorHandler.onIOError(e);
                    }
                    break;
                case PUT:
                    signal = new CancellationSignal();
                    runningUploads.put(target.filepath, signal);
                    final Function<BlockHashFile, Void> putCallback = uploadCallbacks.getOrDefault(target.filepath, null);

                    AssetFileDescriptor inputFD = null;
                    try {
                        File targetFile = new File(target.filepath);
                        FileInputStream putStream;
                        if (!target.filepath.contains("content:") && targetFile.exists()) {
                            putStream = new FileInputStream(targetFile);
                        } else {
                            // We are dealing with a contentUri from another app. Grab the file and
                            // process it like normal.
                            try {
                                Uri uri = Uri.parse(target.filepath);
                                inputFD = getApplicationContext().getContentResolver().openAssetFileDescriptor(uri, "r");
                            } catch (FileNotFoundException e) {
                                errorHandler.onRemoteError(new RemoteException("Could not find the file you are trying to store from the given Uri."));
                                // TODO Remove the queue file since it does not exist...
                                if (putCallback != null) {
                                    putCallback.apply(null);
                                }
                                return;
                            } catch (SecurityException se) {
                                errorHandler.onRemoteError(new RemoteException("Security/permissions error trying to access a file with given Uri."));
                                // TODO Remove the queue file since we don't have access... It will be
                                // re-added if an application requests a new upload.
                                if (putCallback != null) {
                                    putCallback.apply(null);
                                }
                                return;
                            } catch (Exception ge) {
                                errorHandler.onRemoteError(new RemoteException("Generic error trying to access a file with given Uri."));
                                // TODO Remove the queue file since we can't proceed.
                                if (putCallback != null) {
                                    putCallback.apply(null);
                                }
                                return;
                            }

                            if (inputFD != null) {
                                putStream = inputFD.createInputStream();
                            } else {
                                errorHandler.onRemoteError(new RemoteException("File descriptor was not valid for given URI " + target.filepath));
                                if (putCallback != null) {
                                    putCallback.apply(null);
                                }
                                return;
                            }
                        }

                        IProgressHandler serviceHandler = getServiceHandler(target);
                        final AssetFileDescriptor ref = inputFD;
                        upload(putStream.getChannel().size(), putStream, null, BlockHashFile -> {
                            // Remove the running upload from the list so that another can be queued.
                            if (ref != null) {
                                Log.d(TAG, "Closing asset file descriptor in upload callback.");
                                try {
                                    ref.close();
                                } catch (IOException e) {
                                    Log.e(TAG, "Error closing asset file descriptor.", e);
                                }
                            }
                            runningUploads.remove(target.filepath);

                            // See if there is a callback that we are supposed to fire.
                            if (putCallback != null) {
                                putCallback.apply(BlockHashFile);
                            }

                            // Start the next upload if there is one.
                            queueNextProcess();
                            return null;
                        }, serviceHandler, signal, reporter, ref);
                    } catch (IOException e) {
                        errorHandler.onIOError(e);
                        if (inputFD != null) {
                            Log.d(TAG, "Clearing up asset file descriptor on IO error.");
                        }
                    }
                    break;
                case DEL:
                    runningDeletes.add(target.cid);
                    delete(target.cid, e -> {
                        runningDeletes.remove(target.cid);
                        // See if there is a callback that we are supposed to fire.
                        if (deleteCallbacks.containsKey(target.cid)) {
                            Function<String, Void> callback = deleteCallbacks.get(target.cid);
                            if (callback != null) {
                                callback.apply(target.cid);
                            }
                        }
                        // Start the next delete if there is one.
                        queueNextProcess();
                        return null;
                    });
                    break;
            }
        }

        /**
         * Returns a progress handler that can trigger remote client callbacks.
         *
         * @param target The DbQueue item that is being processed.
         */
        private IProgressHandler getServiceHandler (DbQueue target){
            if (service == null) return null;
            Uri content = Uri.parse(target.filepath);
            String packageName = content.getAuthority();
            return service.getProgressHandler(packageName, content.getLastPathSegment());
        }

        private void addUploadCallback (String packageName, DbQueue
        putFile, Function < BlockHashFile, Void > callback){
            uploadCallbacks.put(putFile.filepath, BlockHashFile -> {
                if (BlockHashFile != null)
                    convertQueueToFile(packageName, putFile, BlockHashFile, callback);
                return null;
            });
        }

        /**
         * Queues a list of files for upload to decentralized storage.
         *
         * @param callback Function to call when any of the targets is finished uploading. Function is
         *                 called with the BlockHashFile representing the decentralized storage object.
         */
        @SafeVarargs
        public final void queueUploads (String
        packageName, Function < BlockHashFile, Void > callback, Pair < Uri, String >...targets){
            // The decentralized storage functions already check for block existence before trying to
            // upload the files, we can just queue them straight.

            // First, save the queued operation to the database so that it doesn't fall through the cracks.
            toPutQueueFiles(queueFiles -> {
                Log.d(TAG, String.format("Queue files transformed for database: add %d and update %d files.", queueFiles.first.length, queueFiles.second.length));
                AsyncDao.addQueueFiles(applicationContext, v -> {
                    Log.d(TAG, "Database add completed for queue files");
                    AsyncDao.updateQueueFiles(applicationContext, u -> {
                        Log.d(TAG, "Database update completed for queue files");
                        for (DbQueue adds : queueFiles.first) {
                            addUploadCallback(packageName, adds, callback);
                        }
                        for (DbQueue exists : queueFiles.second) {
                            addUploadCallback(packageName, exists, callback);
                        }

                        // Process the queue from the database, which will include all of the added files.
                        processQueue(DecentralizedStoreOperation.PUT);

                        return null;
                    }, queueFiles.second);
                    return null;
                }, queueFiles.first);

                return null;
            }, targets);
        }

        /**
         * Queues a list of files for upload to decentralized storage.
         *
         * @param targets  List of CIDs to download from the store; if they are already cached, they
         *                 aren't re-downloaded.
         * @param callback Function to call when any of the specified targets is finished downloading.
         *                 Function is called with the downloaded File object.
         */
        public void queueDownloads (Function < File, Void > callback, String...targets){
            // Build a list of files that point to the cache folder. Only queue the ones that aren't
            // already cached.
            List<File> files = new ArrayList<>();
            for (String s : targets) {
                File f = new File(applicationContext.getCacheDir(), s);
                if (!f.exists()) {
                    files.add(f);
                    if (callback != null) {
                        downloadCallbacks.put(s, callback);
                    }
                } else if (callback != null) {
                    callback.apply(f);
                }
            }

            queueOperation(DecentralizedStoreOperation.GET, files.toArray(new File[0]));
        }

        /**
         * Queues a list of files for upload to decentralized storage.
         *
         * @param targets  CIDs to delete from the local cache and the decentralized storage.
         * @param callback Function to call when any of the deletes completes (in the cloud). Function is
         *                 called with the CID that was deleted.
         */
        public void queueDeletes (Function < String, Void > callback, String...targets){
            List<File> files = new ArrayList<>();
            for (String s : targets) {
                File f = new File(applicationContext.getCacheDir(), s);
                if (f.exists()) {
                    if (!f.delete()) {
                        errorHandler.onIOError(new IOException(String.format("Unable to delete %s file from cache.", s)));
                    }
                }
                files.add(f);

                if (callback != null) {
                    deleteCallbacks.put(s, callback);
                }
            }

            queueOperation(DecentralizedStoreOperation.DEL, files.toArray(new File[0]));
        }

        private void queueOperation (DecentralizedStoreOperation operation, File...targets){
            // First, save the queued operation to the database so that it doesn't fall through the cracks.
            toQueueFiles(operation, queueFiles -> {
                AsyncDao.addQueueFiles(applicationContext, v -> {
                    AsyncDao.updateQueueFiles(applicationContext, u -> {
                        // Process the queue from the database, which will include all of the added files.
                        processQueue(operation);
                        return null;
                    });
                    return null;
                }, queueFiles.first);

                return null;
            }, targets);
        }

        /**
         * Calls the delete async method using a `CountDownLatch` to wait for it to complete.
         *
         * @param cid CID of the file to fetch in async storage.
         */
        public void syncDelete (String cid){
            CountDownLatch syncLatch = new CountDownLatch(1);
            delete(cid, c -> {
                syncLatch.countDown();
                return null;
            });
            try {
                syncLatch.await();
            } catch (InterruptedException e) {
                errorHandler.onGenericError(e);
            }
        }

        public void delete (String cid, Function < Void, Void > callback){
            gateway.getInstance().delete(
                    applicationContext.getResources().getString(R.string.S3bucket),
                    String.format("%s/%s", getDid(), cid),
                    new StreamObserver<Empty>() {
                        @Override
                        public void onNext(Empty value) {
                            Log.v("DSDEL", "onNext triggered with delete; highly suspect.");
                        }

                        @Override
                        public void onError(Throwable t) {
                            reporter.onFailure(t, new HashSet<>());
                        }

                        @Override
                        public void onCompleted() {
                            callback.apply(null);
                        }
                    });
        }

        /**
         * Uploads the specified file to decentralized storage.
         *
         * @param target    File to upload to decentralized storage.
         * @param toastText On success, what toast to display to the user.
         * @param callback  Function to call onSuccess with the IPFS details of the uploaded data.
         */
        public void upload (File target,
                String toastText,
                Function < BlockHashFile, Void > callback,
                IProgressHandler progressHandler,
                CancellationSignal signal,
                Object weakRefObject) throws IOException {
            FileInputStream file = new FileInputStream(target);
            upload(target.length(), file, toastText, callback, progressHandler, signal, weakRefObject);
        }

        /**
         * Uploads the specified string to decentralized storage as UTF-8 encoded bytes.
         *
         * @param streamSize Size of the stream being uploaded (in bytes).
         * @param target     String to upload to decentralized storage.
         * @param toastText  On success, what toast to display to the user.
         * @param callback   Function to call onSuccess with the IPFS details of the uploaded data.
         */
        public void upload ( long streamSize,
        String target,
        String toastText,
        Function<BlockHashFile, Void> callback,
        IProgressHandler progressHandler,
        CancellationSignal signal){
            File cached = null;
            FileInputStream cacheStream = null;
            try {
                cached = File.createTempFile("ds", "str", getApplicationContext().getCacheDir());
                FileOutputStream writer = new FileOutputStream(cached);
                writer.write(target.getBytes(StandardCharsets.UTF_8));
                writer.close();
                cacheStream = new FileInputStream(cached);
            } catch (IOException e) {
                Log.e(TAG, "Couldn't create temporary cache file for uploading string to storage.");
            }

            if (cacheStream != null) {
                final String cacheFileName = cached.getAbsolutePath();
                upload(streamSize, cacheStream, toastText, BlockHashFile -> {
                    if (!new File(cacheFileName).delete()) {
                        Log.d(TAG, "Could not delete the temporary cache file craeted for string upload to storage.");
                    }
                    callback.apply(BlockHashFile);
                    return null;
                }, progressHandler, signal, target);
            } else {
                callback.apply(null);
            }
        }

        public void downloadString (String cid,
                Function < DecentralizedFile, Void > callback,
                IProgressHandler progressCallback,
                CancellationSignal signal){
            ByteArrayOutputStream contents = new ByteArrayOutputStream();
            download(cid, contents, callback, progressCallback, signal);
        }

        /**
         * Performs a download from decentralized storage that blocks the current thread until it is
         * completed.
         *
         * @param target Output stream to write to with blocks from decentralized storage.
         * @param signal Cancellation signal to stop download.
         */
        public DecentralizedFile syncDownload (String cid, OutputStream target, CancellationSignal
        signal){
            CountDownLatch syncLatch = new CountDownLatch(1);
            final String syncId = randomHash();
            syncDownloads.put(syncId, null);
            download(cid, target, dFile -> {
                syncLatch.countDown();
                syncDownloads.put(syncId, dFile);
                return null;
            }, null, signal);

            try {
                syncLatch.await();
            } catch (InterruptedException e) {
                errorHandler.onGenericError(e);
            }

            DecentralizedFile result = syncDownloads.get(syncId);
            syncDownloads.remove(syncId);
            return result;
        }

        public void download (String cid,
                OutputStream output,
                Function < DecentralizedFile, Void > callback,
                IProgressHandler progressCallback,
                CancellationSignal signal){
            assert cid != null;
            download(cid, output, callback, progressCallback, signal, reporter);
        }

        private void download (String cid,
                OutputStream output,
                Function < DecentralizedFile, Void > callback,
                IProgressHandler progressCallback,
                CancellationSignal signal,
                StorageReporter storageReporter){
            assert cid != null;
            AwsBlockReader reader;
            if (isAWS) {
                reader = new AwsBlockReader(
                        baseKeys.getKeys(),
                        getDid(),
                        cid,
                        applicationContext.getResources().getString(R.string.S3bucket),
                        new AwsStaticCredentials(applicationContext).provider(),
                        storageReporter
                );
            } else {
                reader = new AwsBlockReader(
                        baseKeys.getKeys(),
                        getDid(),
                        cid,
                        applicationContext.getResources().getString(R.string.SwtBucket),
                        gateway,
                        storageReporter
                );
            }

            encrypter.decryptFile(reader, output, file -> {
                DecentralizedFile result = new DecentralizedFile(file, output);
                callback.apply(result);
                return null;
            }, progressCallback, signal);
        }

        static String randomHash () {
            byte[] array = new byte[32];
            new Random().nextBytes(array);
            return Base58.encode(array);
        }

        /**
         * Performs an upload to decentralized storage that blocks the current thread until it is
         * completed.
         *
         * @param streamSize Size of the stream being uploaded (in bytes).
         * @param target     Input stream to write to decentralized storage.
         * @param signal     Cancellation signal to stop upload.
         */
        public BlockHashFile syncUpload ( long streamSize, FileInputStream
        target, CancellationSignal signal,
                Object weakRefObject){
            CountDownLatch syncLatch = new CountDownLatch(1);
            final String syncId = randomHash();
            syncUploads.put(syncId, null);
            upload(streamSize, target, null, dFile -> {
                syncLatch.countDown();
                syncUploads.put(syncId, dFile);
                return null;
            }, null, signal, weakRefObject);

            try {
                syncLatch.await();
            } catch (InterruptedException e) {
                errorHandler.onGenericError(e);
            }

            BlockHashFile result = syncUploads.get(syncId);
            syncUploads.remove(syncId);
            return result;
        }

        /**
         * Uploads a byte stream to decentralized storage.
         *
         * @param streamSize      Size of the stream being uploaded (in bytes).
         * @param target          Stream to upload to decentralized storage.
         * @param toastText       On success, what toast to display to the user.
         * @param callback        Function to call onSuccess with the IPFS details of the uploaded data.
         * @param progressHandler Function to call as each block is finished; can be null for no progress.
         */
        public void upload ( long streamSize,
        FileInputStream target,
        final String toastText,
        Function<BlockHashFile, Void> callback,
        IProgressHandler progressHandler,
        CancellationSignal signal,
        Object weakRefObject){
            upload(streamSize, target, toastText, callback, progressHandler, signal, reporter, weakRefObject);
        }

        private void upload ( long streamSize,
        FileInputStream target,
        final String toastText,
        Function<BlockHashFile, Void> callback,
        IProgressHandler progressHandler,
        CancellationSignal signal,
        StorageReporter storageReporter,
        Object weakRefObject){
            // Create a block writer for this upload context. These cannot be reused at the moment
            AwsBlockWriter writer;
            if (isAWS) {
                writer = new AwsBlockWriter(
                        baseKeys.getKeys(),
                        keyPath,
                        getDid(),
                        applicationContext.getResources().getString(R.string.S3bucket),
                        new AwsStaticCredentials(applicationContext).provider(),
                        storageReporter
                );
            } else {
                writer = new AwsBlockWriter(
                        baseKeys.getKeys(),
                        keyPath,
                        getDid(),
                        applicationContext.getResources().getString(R.string.SwtBucket),
                        gateway,
                        storageReporter
                );
            }

            try {
                encrypter.encryptFile(target, writer, streamSize, calculateBlockSize(streamSize), file -> {
                    if (file != null)
                        Log.i("ClearSHARE", String.format("Decentralized storage upload succeeded %s", file.cid));
                    if (callback != null) {
                        callback.apply(file);
                    }
                    return null;
                }, progressHandler, signal, weakRefObject);
            } catch (IOException e) {
                Log.e("ClearSHARE", e.getMessage(), e);
            }
        }

        /**
         * Calculates the log base 2 of the file size using scaling by smallest block size (4KB).
         *
         * @param fileSize File size
         * @return The floored log2 value of the scaled file size.
         */
        private static int log2Floor ( long fileSize){
            return (int) Math.floor(Math.log(fileSize / 4096.) / Math.log(2.));
        }

        public static int calculateBlockSize ( long fileSize){
            // We decided to just use the standard 4MB block size. Smaller files are not supported
            // efficiently yet.
            int l = log2Floor(fileSize);
            if (l < 5) {
                return 4096 * KB;
                // return 4*KB;
            } else if (l > 21) {
                // Maximum supported block size is 256MB
                return 262144 * KB;
            } else {
                int result = (int) Math.pow(2, l - 3) * KB;
                return Math.max(result, 4096 * KB);
            }
        }
    }