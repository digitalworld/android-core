package com.clearos.storage.provider;


import com.clearos.dstorage.BlockHashFile;

import java.io.OutputStream;

public class DecentralizedFile {
    private BlockHashFile descriptor;
    private OutputStream contents;

    public DecentralizedFile(BlockHashFile _descriptor, OutputStream _contents) {
        descriptor = _descriptor;
        contents = _contents;
    }

    public BlockHashFile getDescriptor() {
        return descriptor;
    }

    public OutputStream getContents() {
        return contents;
    }
}