package com.clearos.storage.provider;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;

import androidx.core.content.FileProvider;

import com.clearos.dstorage.IDecentralizedStore;
import com.clearos.dstorage.IProgressHandler;
import com.clearos.dstorage.IRemoteServiceCallback;
import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.ProgressResult;
import com.clearos.dstorage.R;
import com.clearos.dlt.DecryptedMqttMessage;
import com.clearos.dlt.DerivedKeyClient;
import com.clearos.dlt.IRemoteErrorHandler;
import com.clearos.storage.DecentralizedLayer;
import com.clearos.storage.SwtGateway;
import com.clearos.storage.db.DsFile;
import com.clearos.storage.db.DsFileName;
import com.clearos.storage.db.DsFileNamesWithFiles;
import com.clearos.storage.meta.AsyncDao;
import com.clearos.storage.db.DsApp;
import com.clearos.storage.db.DsFileWithNames;
import com.clearos.updates.Utils;
import com.goterl.lazysodium.exceptions.SodiumException;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static androidx.core.content.FileProvider.getUriForFile;


public class DecentralizedStoreService extends Service implements IRemoteErrorHandler {
    private Map<String, DecentralizedStorage> stores;
    private Context appContext;
    private final String TAG = "DSSVC";
    private DerivedKeyClient keyClient = null;
    private DecentralizedLayer storageLayer;
    private final CountDownLatch keyClientReadyLatch = new CountDownLatch(1);
    // Countdown latch of 1 in case the application initialization becomes async at some point
    // and we need to count down the independent thread initializations.
    private final CountDownLatch storageLayerReadyLatch = new CountDownLatch(1);

    private static SwtGateway gateway;
    private final Map<String, IRemoteServiceCallback> callbacks = new HashMap<>();

    public static final int ENABLED = 2;
    public static final int UNREGISTERED = 0;
    public static final int DISABLED = 3;
    public static final int UNKNOWN = 4;

    @Override
    public void onRemoteError(RemoteException e) {
        Log.e(TAG, "Remote error in derived keys service.", e);
    }

    @Override
    public void onGenericError(Exception e) {
        Log.e(TAG, "Generic error in decentralized storage service.", e);
    }

    @Override
    public void onIOError(IOException e) {
        Log.e(TAG, "IO error in decentralized storage service.", e);
    }

    @Override
    public void onNotificationReceived(DecryptedMqttMessage s) {
        Log.i(TAG, "Notification received by decentralized storage service: " + s);
    }

    /**
     * Returns a new progress handler that can use the callbacks from this service in its event
     * handler methods.
     */
    public IProgressHandler getProgressHandler(String packageName, String fileKey) {
        return new ServiceProgressHandler(this, packageName, fileKey);
    }


    /**
     * Sets up the key client recursively (trying a maximum of three times) until it connects
     * successfully. Then, the decentralized layer and storage interfaces are created.
     *
     * @param depth Current depth in the recursive stack.
     */
    private void setupKeyClientAndStorage(int depth) {
        Log.d(TAG, "Attempting key client setup with recursive depth " + depth);
        if (keyClient == null) {
            // Only initialize the keyClient once; otherwise we end up with duplicate clients
            // running around.
            keyClient = new DerivedKeyClient(getApplicationContext());
        }

        if (depth < 3 && storageLayerReadyLatch.getCount() > 0) {
            keyClient.create(getApplicationContext(), 4000, kc -> {
                if (kc != null && keyClientReadyLatch.getCount() == 1) {
                    Log.d(TAG, "Key client setup successfully in DS service. Registering app keys.");
                    keyClientReadyLatch.countDown();

                    kc.registerAppKeys(ok -> {
                        Log.d(TAG, "Registered app keys.");
                        return null;
                    });

                    try {
                        // Note that this constructor only initializes the static, synchronized gateway object
                        // *only* if it hasn't been done already.
                        gateway = new SwtGateway(keyClient);
                    } catch (Exception e) {
                        Log.e("SWT", "Failure creating & starting SWT gateway.", e);
                    }

                    // Start restoring the database in another thread so that it is ready when needed.
                    storageLayer = new DecentralizedLayer(getApplicationContext(), gateway, kc);

                    // Create the default package store for ClearCLOUD.
                    Log.d(TAG, "Layer init finished; setting up decentralized storage for default context.");
                    DecentralizedStorage s = new DecentralizedStorage(appContext, gateway, null, appContext.getPackageName(), this);
                    stores.put(appContext.getPackageName(), s);

                    // Grab a list of all registered apps and create a store for each of them.
                    Log.d(TAG, "Listing registered apps for DS service initialization.");
                    AsyncDao.getRegisteredApps(appContext, registered -> {
                        if (registered != null) {
                            for (DsApp app : registered) {
                                Log.d(TAG, "Adding app store to DS service for " + app.packageName);
                                addAppStore(app.packageName);
                            }

                            Log.d(TAG, "Finished initializing all the registered app stores; counting down the ready latch.");
                            storageLayerReadyLatch.countDown();
                        }
                        return null;
                    });
                } else {
                    // Since this is a service, we don't have a reason not to try again. Each time
                    // there is a wait of 1500ms for the connection.
                    Log.d(TAG, "Key client binding failed; retrying with recursive setup again.");
                    setupKeyClientAndStorage(depth + 1);
                }
                return null;
            });
        }
    }

    /**
     * Waits for up to 10 seconds the decentralized storage layer to be ready.
     */
    private void waitStorageReady() {
        waitStorageReady(10);
    }

    /**
     * Waits for the decentralized storage layer to be ready.
     * @param timeout Number of seconds to wait for the service to be ready.
     */
    private void waitStorageReady(long timeout) {
        if (storageLayerReadyLatch.getCount() == 0) {
            Log.d(TAG, "Storage service latch says its done; returning immediately.");
            return;
        }

        try {
            Log.d(TAG, "Waiting for storage layer to be ready.");
            storageLayerReadyLatch.await(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Log.e(TAG, "Error waiting for the decentralized storage layer to be ready", e);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        // Grab the DID from the start command so that we can make sure we are deriving the same
        // keys in this process as the main app.
        return START_STICKY;
    }

    private String getKeyPath(String packageName) {
        try {
            return keyClient.hashInBase58(packageName).substring(0, 8);
        } catch (SodiumException e) {
            Log.e(TAG, "Error deriving a key path for " + packageName);
            return null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        stores = new HashMap<>();
        appContext = getApplicationContext();
        setupKeyClientAndStorage(0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (Map.Entry<String, DecentralizedStorage> store : stores.entrySet()) {
            store.getValue().shutdown();
        }
        if (keyClient != null)
            keyClient.destroy();
    }

    /**
     * Adds a decentralized storage interface for the given package, only if it doesn't exist.
     */
    private void addAppStore(String packageName) {
        if (!stores.containsKey(packageName)) {
            String keyPath = getKeyPath(packageName);
            DecentralizedStorage appStore = new DecentralizedStorage(appContext, gateway, keyPath, packageName, this);
            appStore.setService(this);
            stores.put(packageName, appStore);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the interface
        return binder;
    }

    private Collection<String> getCallingPackages() {
        int caller = Binder.getCallingUid();
        if (caller == 0) {
            return null;
        }
        String[] packages = getPackageManager().getPackagesForUid(caller);
        List<String> result = null;
        if (packages != null) {
            result = Arrays.asList(packages);
        }
        return result;
    }

    /**
     * Checks whether the specified package name matches the OS UID making the call.
     *
     * @param packageName Name of the package requesting access.
     * @throws RemoteException if the package name specified doesn't match process UID of the caller.
     */
    private void verifyPackageIdentity(String packageName) throws RemoteException {
        Log.d(TAG, "Verifying package identity for " + packageName);
        Collection<String> packages = getCallingPackages();
        if (packages != null) {
            if (!packages.contains(packageName)) {
                throw new RemoteException("Specified package name does not match process UID for caller.");
            }
        } else {
            throw new RemoteException("Specified package name does not match process UID for caller.");
        }
    }

    private DecentralizedStorage getPackageStore(String packageName) {
        Log.d(TAG, "Looking for package decentralized store " + packageName);
        if (!stores.containsKey(packageName)) {
            DecentralizedStorage s = new DecentralizedStorage(appContext, gateway, null, packageName, this);
            stores.put(packageName, s);
        }
        return stores.get(packageName);
    }

    private void sendLoadedFile(File source) {
        Log.d(TAG, "Sending loaded file for " + source.getPath());
        Intent resultIntent = new Intent(String.format("%s.ACTION_RETURN_FILE", getPackageName()));
        Uri result = null;
        try {
            result = FileProvider.getUriForFile(
                    this,
                    String.format("%s.fileprovider", getPackageName()),
                    source);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "The selected file can't be shared: " + source.toString(), e);
        }

        if (result != null) {
            Log.d(TAG, "Adding grant read URI permission to " + result);
            // Grant temporary read permission to the content URI.
            resultIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            // Put the Uri and MIME type in the result Intent
            resultIntent.setDataAndType(
                    result,
                    getContentResolver().getType(result));
            //this.setResult(RESULT_OK, resultIntent);
        } else {
            resultIntent.setDataAndType(null, "");
            //MainActivity.this.setResult(RESULT_CANCELED, resultIntent);
        }
    }

    private String getFirstCallingPackage() throws RemoteException {
        Collection<String> packages = getCallingPackages();
        if (packages != null) {
            return packages.toArray(new String[0])[0];
        } else {
            throw new RemoteException("Cannot determine calling package identity.");
        }
    }

    private void triggerPutCallback(String packageName, String fileName) {
        Log.d(TAG, String.format("Triggering PUT file callback for %s in %s.", fileName, packageName));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onDecentralizedPut(fileName);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing PUT callback for %s and %s", packageName, fileName));
            }
        }
    }

    /**
     * Triggers a remote client callback with block upload progress.
     *
     * @param packageName Name of the calling package to respond to.
     * @param result      Block progress result.
     */
    void triggerBlockProgress(String packageName, ProgressResult result) {
        Log.d(TAG, String.format("Triggering block progress callback for %s in %s.", result.getCid(), packageName));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onBlockProgress(result);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing block progress callback for %s and %s", packageName, result.getCid()));
            }
        }
    }

    void triggerBlockStarted(String packageName, ProgressResult block) {
        Log.d(TAG, String.format("Triggering block started callback for %s in %s.", block.getCid(), packageName));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onBlockStarted(block);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing block started callback for %s and %s", packageName, block.getCid()));
            }
        } else {
            Log.d(TAG, String.format("No callbacks available for package %s with block started.", packageName));
        }
    }

    void triggerProcessCompleted(String packageName, BlockHashFile file) {
        Log.d(TAG, String.format("Triggering process completed callback for %s in %s.", file.getCid(), packageName));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onProcessCompleted(file);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing process completed callback for %s and %s", packageName, file.getCid()));
            }
        }
    }

    private void triggerGetCallback(String packageName, Uri contentUri, String fileName) {
        Log.d(TAG, String.format("Triggering GET file callback for %s with %s in %s.", fileName, contentUri, packageName));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onDecentralizedGet(contentUri, fileName);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing GET callback for %s and %s", packageName, contentUri));
            }
        }
    }

    private void triggerDelCallback(String packageName, String fileName) {
        Log.d(TAG, String.format("Triggering DEL file callback for %s in %s.", fileName, packageName));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onDecentralizedDel(fileName);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing DEL callback for %s and %s", packageName, fileName));
            }
        }
    }

    private void triggerErrorCallback(String packageName, Exception e) {
        Log.d(TAG, String.format("Triggering ERR callback for %s with %s.", packageName, e.getMessage()));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onDecentralizedError(e.getMessage());
            } catch (RemoteException remoteErr) {
                Log.e(TAG, String.format("Error firing ERROR callback for %s.", packageName), remoteErr);
            }
        }
    }

    private void triggerRegistrationStatus(String packageName, int status) {
        Log.d(TAG, String.format("Triggering REG-STATUS callback for %s with %d.", packageName, status));
        IRemoteServiceCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onRegistrationStatus(status);
            } catch (RemoteException remoteErr) {
                Log.e(TAG, String.format("Error firing STATUS callback for %s.", packageName), remoteErr);
            }
        }
    }

    private final IDecentralizedStore.Stub binder = new IDecentralizedStore.Stub() {
        @Override
        public void registerPackage(String displayName, String did) throws RemoteException {
            waitStorageReady();
            String packageName = getFirstCallingPackage();
            // See if the app is already registered.
            try {
                Log.d(TAG, "Checking package existence in database: " + packageName);
                AsyncDao.getSingleApp(appContext, packageName, existing -> {
                    if (existing == null) {
                        DsApp app = new DsApp();
                        app.added = new Date();
                        app.packageName = packageName;
                        app.enabled = true;
                        app.displayName = displayName;
                        app.did = did;

                        // Save the registration to the database, then create a decentralized storage
                        // interface for the app.
                        Log.d(TAG, "Registering package in database: " + packageName);
                        AsyncDao.registerApp(appContext, aVoid -> {
                            Log.d(TAG, "Creating package decentralized store for " + packageName);
                            addAppStore(packageName);
                            triggerRegistrationStatus(packageName, ENABLED);
                            return null;
                        }, app);
                    } else
                        triggerRegistrationStatus(packageName, existing.enabled ? ENABLED : DISABLED);
                    return null;
                });

            } catch (Exception e) {
                triggerRegistrationStatus(packageName, UNKNOWN);
                triggerErrorCallback(packageName, e);
            }
        }

        @Override
        public void getPackageRegistrationStatus() throws RemoteException {
            waitStorageReady();
            String packageName = getFirstCallingPackage();
            Log.d(TAG, "Checking database registration record for " + packageName);
            AsyncDao.getSingleApp(appContext, packageName, existing -> {
                if (existing == null) {
                    Log.d(TAG, String.format("Registration check for %s shows unregistered.", packageName));
                    triggerRegistrationStatus(packageName, UNREGISTERED);
                } else {
                    Log.d(TAG, String.format("Registration check for %s shows registered.", packageName));
                    if (existing.enabled) {
                        triggerRegistrationStatus(packageName, ENABLED);
                    } else {
                        triggerRegistrationStatus(packageName, DISABLED);
                    }
                }
                return null;
            });
        }

        @Override
        public void registerCallback(IRemoteServiceCallback callback) throws RemoteException {
            String packageName = getFirstCallingPackage();
            String providerPackage = packageName + ".fileprovider";
            Log.d(TAG, "Registering application remote service callback for " + packageName);
            callbacks.put(packageName, callback);
            callbacks.put(providerPackage, callback);
        }

        @Override
        public void uregisterCallback(IRemoteServiceCallback callback) throws RemoteException {
            String packageName = getFirstCallingPackage();
            String providerPackage = packageName + ".fileprovider";
            Log.d(TAG, "Unregistering application remote service callback for " + packageName);
            callbacks.remove(packageName);
            callbacks.remove(providerPackage);
        }

        @Override
        public void delete(String fileName) throws RemoteException {
            waitStorageReady(1);
            String packageName = getFirstCallingPackage();
            Log.d(TAG, String.format("Delete file requested by %s for %s.", packageName, fileName));
            final DecentralizedStorage appStore = stores.getOrDefault(packageName, null);
            if (appStore == null) {
                throw new RemoteException("The specified package has not been registered. Call method register.");
            }

            AsyncDao.searchFilesbyName(appContext, packageName, fileName, 1, matches -> {
                if (matches != null && matches.length > 0) {
                    DsFileNamesWithFiles first = matches[0];
                    appStore.queueDeletes(cid -> {
                        if (cid != null) {
                            triggerDelCallback(packageName, fileName);
                        } else {
                            triggerErrorCallback(packageName, new Exception("Error deleting file from decentralized storage: " + fileName));
                        }
                        return null;
                    }, first.getMostRecentFile().cid);
                } else {
                    triggerErrorCallback(packageName, new Exception("The specified file does not exist for package " + packageName));
                }
                return null;
            });
        }

        @Override
        public void load(String fileName) throws RemoteException {
            waitStorageReady(1);
            String packageName = getFirstCallingPackage();
            Log.d(TAG, String.format("Load file requested by %s for %s.", packageName, fileName));
            final DecentralizedStorage appStore = stores.getOrDefault(packageName, null);
            if (appStore == null) {
                throw new RemoteException("The specified package has not been registered. Call method register.");
            }

            AsyncDao.getCrossRefs(appContext);

            // We need to do a reverse lookup of files to get their CID.
            AsyncDao.searchFilesbyName(appContext, packageName, fileName, 1, matches -> {
                if (matches != null) {
                    Log.d(TAG, "Found matching filename. Query most recent file");

                    // Grab first result for fileName
                    DsFileNamesWithFiles first = matches[0];

                    // Check for associated files else error
                    if (first.files != null && first.files.size() > 0) {
                        // Acquire the most recent file for cid
                        DsFile recent = first.getMostRecentFile();

                        // Check if exists do not download otherwise return local file
                        File dsFile = new File(appContext.getCacheDir(), recent.cid);
                        // Setup Content Providers for reading file
                        String providerName;
                        if (Utils.isClearDevice())
                            providerName = "com.clearos.clearlife.fileprovider";
                        else
                            providerName = "com.clearos.digitallife.fileprovider";
                        Uri contentUri = getUriForFile(appContext, providerName, dsFile);
                        appContext.grantUriPermission(packageName, contentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        if (dsFile.exists()) {
                            // Send cached file back
                            triggerGetCallback(packageName, contentUri, fileName);
                        } else {
                            // Download recent file from decentralized storage
                            appStore.queueDownloads(dFile -> {
                                if (dFile != null) {
                                    // Return file when download is complete
                                    triggerGetCallback(packageName, contentUri, fileName);
                                } else {
                                    triggerErrorCallback(packageName, new Exception("Error retrieving file from decentralized storage " + fileName));
                                }
                                return null;
                            }, recent.cid);
                        }
                    } else {
                        triggerErrorCallback(packageName, new Exception("No Matching Files for this filename " + fileName));
                    }
                } else {
                    triggerErrorCallback(packageName, new Exception("No Matching Filename for this package " + packageName));
                }
                return null;
            });
        }

        @Override
        public void store(String fileName, String displayName, Uri contentURI) throws RemoteException {
            waitStorageReady(1);
            String packageName = getFirstCallingPackage();
            Log.d(TAG, String.format("Store file requested by %s for %s with %s.", packageName, fileName, contentURI));
            Log.d(TAG, "Current app stores available are: " + stores.keySet().toString());
            DecentralizedStorage appStore = stores.getOrDefault(packageName, null);
            if (appStore == null) {
                throw new RemoteException("The specified package has not been registered. Call method register.");
            }

            // First, we need to get hold of the actual file using the ContentResolver, then we
            // can cache it locally and upload it.
            appStore.queueUploads(packageName, BlockHashFile -> {
                if (BlockHashFile != null) {
                    triggerPutCallback(packageName, fileName);
                } else {
                    triggerErrorCallback(packageName, new Exception("Error putting file into decentralized storage " + fileName));
                }
                return null;
            }, new Pair<>(contentURI, fileName));
        }
    };
}

