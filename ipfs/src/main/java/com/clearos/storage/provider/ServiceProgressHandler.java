package com.clearos.storage.provider;

import android.util.Log;

import com.clearos.dstorage.IProgressHandler;
import com.clearos.dstorage.BlockHashFile;
import com.clearos.dstorage.ProgressResult;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ServiceProgressHandler implements IProgressHandler {

    private List<Function<ProgressResult, Void>> extraProgressCallbacks;
    private DecentralizedStoreService service;
    private String packageName;
    private String fileKey;

    private final static String TAG = "ServiceProgressHandler";

    public ServiceProgressHandler(DecentralizedStoreService service, String packageName, String fileKey) {
        extraProgressCallbacks = new ArrayList<>();
        this.service = service;
        this.packageName = packageName;
        this.fileKey = fileKey;
    }

    @Override
    public void onBlockProgress(ProgressResult result) {
        result.setFileKey(fileKey);
        for (Function<ProgressResult, Void> cb: extraProgressCallbacks) cb.apply(result);

        service.triggerBlockProgress(packageName, result);
    }
    @Override
    public void onBlockStarted(ProgressResult block) {
        service.triggerBlockStarted(packageName, block);
    }
    @Override
    public void onProcessCompleted(BlockHashFile completeFile) {
        service.triggerProcessCompleted(packageName, completeFile);
    }
    @Override
    public void onTotalProgress(Float percentDone) {
        Log.d(TAG, String.format("Total progress on upload for %s is %f.", fileKey, percentDone));
    }
    @Override
    public void addProgressCallback(Function<ProgressResult, Void> callback) {
        extraProgressCallbacks.add(callback);
    }
}
