package com.clearos.storage;

/**
 * Basic (Free): data is sharded randomly worldwide.
 * Premium ($$): select countries where data is stored.
 */
public enum StorageGeolocation {
    BASIC,
    PREMIUM
}
