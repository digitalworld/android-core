package com.clearos.storage;

import com.google.gson.annotations.SerializedName;

public class StorageConfig {
    @SerializedName("class")
    private final StorageClass storageClass;
    @SerializedName("space")
    private final StorageSpace storageSpace;
    @SerializedName("geo")
    private final StorageGeolocation geolocation;
    private final double size;

    public StorageConfig(StorageClass _class, StorageSpace _space, StorageGeolocation _geolocation, double _size) {
        storageClass = _class;
        storageSpace = _space;
        geolocation = _geolocation;
        size = _size;
    }

    /**
     * Gets the basic/free version configuration for storage.
     */
    public static StorageConfig getBasicConfig() {
        return new StorageConfig(StorageClass.BASIC, StorageSpace.BASIC, StorageGeolocation.BASIC, 0.015);
    }

    public double getSize() {
        return size;
    }

    public StorageClass getStorageClass() {
        return storageClass;
    }

    public StorageGeolocation getGeolocation() {
        return geolocation;
    }

    public StorageSpace getStorageSpace() {
        return storageSpace;
    }
}
