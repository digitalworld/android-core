package com.clearos.storage.db;

import androidx.room.Entity;

import androidx.annotation.NonNull;
import androidx.room.ForeignKey;

@Entity(tableName = "filecrossfilename",
        primaryKeys = {"cid", "rowid"})
public class DsFileCrossRefDsFileName {
    @NonNull
    public String cid;
    @NonNull
    public int rowid;
}
