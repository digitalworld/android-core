package com.clearos.storage.db;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class LocalDsLogDataSource implements DsLogDataSource{
    private DsDao dao;

    public LocalDsLogDataSource(DsDao dao) { this.dao = dao; }

    /**
     * Add one or more logs resulting from GET operations to the decentralized storage gateway.
     */
    @Override
    public Completable addGetLogs(GetAuditLog... getLogs) {
        return dao.addGetLogs(getLogs);
    }

    /**
     * Add one or more logs resulting from PUT operations to the decentralized storage gateway.
     */
    @Override
    public Completable addPutLogs(PutAuditLog... putLogs) {
        return dao.addPutLogs(putLogs);
    }

    /**
     * Add one or more logs resulting from EXIST operations to the decentralized storage gateway.
     */
    @Override
    public Completable addExistLogs(ExistAuditLog... existLogs) {
        return dao.addExistLogs(existLogs);
    }

    /**
     * Add one or more logs resulting from LIST operations to the decentralized storage gateway.
     */
    @Override
    public Completable addListLogs(ListAuditLog... listLogs) {
        return dao.addListLogs(listLogs);
    }

    /**
     * Add one or more logs resulting from DELETE operations to the decentralized storage gateway.
     */
    @Override
    public Completable addDeleteLogs(DeleteAuditLog... deleteLogs) {
        return dao.addDeleteLogs(deleteLogs);
    }

    /**
     * Returns a list of the most recent GET audit logs against the decentralized server. These GET
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Override
    public Flowable<List<DsFileGetLogs>> listGetAuditLogs(String cid, int limit) {
        return dao.listGetAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent PUT audit logs against the decentralized server. These PUT
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Override
    public Flowable<List<DsFilePutLogs>> listPutAuditLogs(String cid, int limit) {
        return dao.listPutAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent EXIST audit logs against the decentralized server. These EXIST
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Override
    public Flowable<List<DsFileExistLogs>> listExistAuditLogs(String cid, int limit) {
        return dao.listExistAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent LIST audit logs against the decentralized server. These LIST
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Override
    public Flowable<List<DsFileListLogs>> listListAuditLogs(String cid, int limit) {
        return dao.listListAuditLogs(cid, limit);
    }

    /**
     * Returns a list of the most recent DELETE audit logs against the decentralized server. These DELETE
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Override
    public Flowable<List<DsFileDeleteLogs>> listDeleteAuditLogs(String cid, int limit) {
        return dao.listDeleteAuditLogs(cid, limit);
    }
}
