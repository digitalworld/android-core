package com.clearos.storage.db;

public class ParamConverter<T> {
    private Class<T> outType;

    public ParamConverter(Class<T> outType) {
        this.outType = outType;
    }

    @SuppressWarnings("unchecked")
    public T convert(Object value) {
        if (value.getClass() == outType) {
            return (T)value;
        } else {
            return null;
        }
    }
}
