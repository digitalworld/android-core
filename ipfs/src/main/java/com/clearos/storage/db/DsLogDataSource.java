package com.clearos.storage.db;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface DsLogDataSource {

    /**
     * Add one or more logs resulting from GET operations to the decentralized storage gateway.
     */
    Completable addGetLogs(GetAuditLog... getLogs);

    /**
     * Add one or more logs resulting from PUT operations to the decentralized storage gateway.
     */
    Completable addPutLogs(PutAuditLog... putLogs);

    /**
     * Add one or more logs resulting from EXIST operations to the decentralized storage gateway.
     */
    Completable addExistLogs(ExistAuditLog... existLogs);

    /**
     * Add one or more logs resulting from LIST operations to the decentralized storage gateway.
     */
    Completable addListLogs(ListAuditLog... listLogs);

    /**
     * Add one or more logs resulting from DELETE operations to the decentralized storage gateway.
     */
    Completable addDeleteLogs(DeleteAuditLog... deleteLogs);

    /**
     * Returns a list of the most recent GET audit logs against the decentralized server. These GET
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileGetLogs>> listGetAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent PUT audit logs against the decentralized server. These PUT
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFilePutLogs>> listPutAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent EXIST audit logs against the decentralized server. These EXIST
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileExistLogs>> listExistAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent LIST audit logs against the decentralized server. These LIST
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileListLogs>> listListAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent DELETE audit logs against the decentralized server. These DELETE
     * operations may originate on this device or another.
     * @param cid CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    Flowable<List<DsFileDeleteLogs>> listDeleteAuditLogs(String cid, int limit);
}
