package com.clearos.storage.db;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.clearos.storage.model.DbQueueViewModel;
import com.clearos.storage.model.DsAppViewModel;
import com.clearos.storage.model.DsFileViewModel;
import com.clearos.storage.model.DsLogViewModel;
import com.clearos.storage.model.ViewModelFactory;

public class Injection {
    private static final String TAG = "Injection";

    public static DsFileDataSource provideDsFileDataSource(Context context) {
        MetaDatabase database = MetaDatabase.getInstance(context);
        return new LocalDsFileDataSource(database.dao());
    }

    public static DbQueueDataSource provideDbQueueDataSource(Context context) {
        MetaDatabase database = MetaDatabase.getInstance(context);
        return new LocalDbQueueDataSource(database.dao());
    }

    public static DsAppDataSource provideDsAppDataSource(Context context) {
        MetaDatabase database = MetaDatabase.getInstance(context);
        return new LocalDsAppDataSource(database.dao());
    }

    public static DsLogDataSource provideDsLogDataSource(Context context) {
        MetaDatabase database = MetaDatabase.getInstance(context);
        return new LocalDsLogDataSource(database.dao());
    }

    public static ViewModelFactory provideViewModelFactory(Context context, @NonNull Class<?> modelClass) {
        if (modelClass.isAssignableFrom(DsFileViewModel.class)) {
            DsFileDataSource dataSource = provideDsFileDataSource(context);
            return new ViewModelFactory(dataSource);
        } else if (modelClass.isAssignableFrom(DbQueueViewModel.class)) {
            DbQueueDataSource dataSource = provideDbQueueDataSource(context);
            return new ViewModelFactory(dataSource);
        } else if (modelClass.isAssignableFrom(DsAppViewModel.class)) {
            DsAppDataSource dataSource = provideDsAppDataSource(context);
            return new ViewModelFactory(dataSource);
        } else if (modelClass.isAssignableFrom(DsLogViewModel.class)) {
            DsLogDataSource dataSource = provideDsLogDataSource(context);
            return new ViewModelFactory(dataSource);
        } else
            Log.w(TAG, "Unassignable model class for the view model factory: " + modelClass.getCanonicalName());

        return null;
    }
}