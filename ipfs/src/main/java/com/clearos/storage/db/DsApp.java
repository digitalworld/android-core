package com.clearos.storage.db;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Application that has registered with ClearCLOUD to have decentralized storage provided.
 */
@Entity(tableName = "apps")
public class DsApp {
    @PrimaryKey
    @NonNull
    public String packageName;

    public String did;
    public String displayName;
    public Date added;
    public boolean enabled;
}
