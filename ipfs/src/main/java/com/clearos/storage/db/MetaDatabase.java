package com.clearos.storage.db;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;


@Database(entities = {
        DeleteAuditLog.class,
        DsFile.class,
        DsFileName.class,
        DsFileCrossRefDsFileName.class,
        ExistAuditLog.class,
        GetAuditLog.class,
        ListAuditLog.class,
        PutAuditLog.class,
        DbQueue.class,
        DsApp.class}, version = 2) // MAKE SURE TO UPDATE AND ADD MIGRATION OBJECTS
@TypeConverters({Converters.class})
public abstract class MetaDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "clearshare.db";
    private static volatile MetaDatabase INSTANCE;
    public abstract DsDao dao();

    public static MetaDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (com.clearos.storage.db.MetaDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                                context.getApplicationContext(),
                                MetaDatabase.class,
                                DATABASE_NAME)
                            .fallbackToDestructiveMigrationFrom(1) // Version 1 was faulty and incorrect
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static String getDbName() {
        return "ClearSHAREMetaDb";
    }
}
