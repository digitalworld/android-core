package com.clearos.storage.db;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface DsAppDataSource {

    /**
     * Register one or more apps for decentralized storage access.
     */
    Completable registerApp(DsApp... apps);

    /**
     * Returns the database entry for the given package.
     */
    Single<DsApp> getSingleApp(String packageName);

    /**
     * Lists all the apps that have registered with ClearCLOUD.
     */
    Flowable<List<DsApp>> getRegisteredApps();

    /**
     * Sums the size of all files that belong to a package.
     */
    Single<Long> getPackageFileSize(String packageName);

}
