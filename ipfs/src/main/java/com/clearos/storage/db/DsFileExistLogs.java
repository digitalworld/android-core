package com.clearos.storage.db;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class DsFileExistLogs {
    @Embedded
    public DsFile file;
    @Relation(
            parentColumn = "cid",
            entityColumn = "fileCid"
    )
    public List<ExistAuditLog> existLogs;
}
