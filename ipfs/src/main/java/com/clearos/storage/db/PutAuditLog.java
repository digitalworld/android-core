package com.clearos.storage.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Fts4;
import androidx.room.PrimaryKey;

@Fts4
@Entity(tableName = "log_put")
public class PutAuditLog {
    @PrimaryKey
    @ColumnInfo(name = "rowid")
    public int id;

    public String fileCid;
    public int timestamp;
    public String error;
    public String addresses;
}
