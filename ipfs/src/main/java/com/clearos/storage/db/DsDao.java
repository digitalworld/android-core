package com.clearos.storage.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Transaction;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.clearos.storage.provider.DecentralizedStoreOperation;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface DsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable addFiles(DsFile... files);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    Completable addFileNames(DsFileName... filenames);

    @Update
    Completable updateFileNames(DsFileName... fileNames);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable addFileCrossRefFileNames(DsFileCrossRefDsFileName... dsFileCrossRefDsFileNames);

    @Insert
    Completable addGetLogs(GetAuditLog... getLogs);

    @Insert
    Completable addPutLogs(PutAuditLog... putLogs);

    @Insert
    Completable addExistLogs(ExistAuditLog... existLogs);

    @Insert
    Completable addListLogs(ListAuditLog... listLogs);

    @Insert
    Completable addDeleteLogs(DeleteAuditLog... deleteLogs);

    @Insert
    Completable addQueueFiles(DbQueue... queueFiles);

    @Update
    Completable updateQueueFiles(DbQueue... queueFiles);

    @RawQuery
    Single<Integer> genericQuery(SupportSQLiteQuery supportSQLiteQuery);

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    Completable registerApp(DsApp... apps);

    /**
     * Returns a single filename if exists in database
     *
     * @param fileName    the name of file you are searching for
     * @param packageName the name of the calling application
     * @return DsFileName if exists otherwise null
     */
    @Query("SELECT rowid,* FROM filenames " +
            "WHERE packageName LIKE :packageName " +
            "AND filename LIKE :fileName")
    Single<List<DsFileName>> getSingleFileName(String packageName, String fileName);

    @Query("SELECT * FROM filecrossfilename")
    Single<List<DsFileCrossRefDsFileName>> getCrossRefs();

    /**
     * Returns a list of cid's from cross reference table
     *
     * @param fileNameId the id for the filename
     * @return list of Strings of matching cid's
     */
    @Query("SELECT cid FROM filecrossfilename WHERE rowid = :fileNameId")
    Single<List<String>> getCids(int fileNameId);

    /**
     * Returns all filenames for a given cid
     *
     * @param cid the cid of the file
     * @return list of rowid's for given filenames
     */
    @Query("SELECT rowid,* FROM filenames " +
            "INNER JOIN filecrossfilename ON filecrossfilename.rowid = filenames.rowid " +
            "WHERE filecrossfilename.cid = :cid " +
            "ORDER BY filenames.touched DESC")
    Flowable<List<DsFileName>> listFileNamesForCid(String cid);

    /**
     * Returns the filename for a given cid and package
     *
     * @param cid the cid of the file
     * @param packageName the application name for filename
     * @return the filename for given cid and packageName
     */
    @Query("SELECT rowid,* FROM filenames " +
            "INNER JOIN filecrossfilename ON filecrossfilename.rowid = filenames.rowid " +
            "WHERE filecrossfilename.cid = :cid " +
            "AND filenames.packageName = :packageName")
    Flowable<DsFileName> lookupFileName(String packageName, String cid);


    /**
     * Returns filenames based on there rowid
     *
     * @param rowIds list of rowid to search for
     * @return list of filenames
     */
    @Query("SELECT rowid,* FROM filenames WHERE rowid IN (:rowIds)")
    Single<List<DsFileName>> getFileNames(List<Long> rowIds);

    /**
     * Returns a single file if exists in database
     *
     * @param cid the content crypto string for specific file
     * @return DsFile if exists otherwise null
     */
    @Query("SELECT * FROM files WHERE cid = :cid")
    Single<List<DsFile>> getSingleFile(String cid);

    /**
     * Returns a list of Files for a given set of cid's
     *
     * @param cids list of cids to search for
     * @return a list of files
     */
    @Query("SELECT * FROM files " +
            "WHERE cid IN (:cids) " +
            "ORDER BY files.created DESC")
    Single<List<DsFile>> getFiles(List<String> cids);

    /**
     * Returns the database entry for the given package.
     */
    @Query("SELECT * FROM apps WHERE packageName = :packageName")
    Single<DsApp> getSingleApp(String packageName);

    /**
     * Lists all the apps that have registered with ClearCLOUD.
     */
    @Query("SELECT * FROM apps WHERE enabled = 1 ORDER BY displayName ")
    Flowable<List<DsApp>> getRegisteredApps();

    /**
     * Sums the size of all files that belong to a package.
     */
    @Query("SELECT SUM(size) from files " +
            "INNER JOIN filenames WHERE packageName = :packageName AND isDirectory = 0")
    Single<Long> getPackageFileSize(String packageName);

    /**
     * Gets a single queue file from the database using its primary key fields.
     */
    @Query("SELECT * FROM queue WHERE filepath IN (:filepaths) AND operation = :op")
    Flowable<List<DbQueue>> getQueueFiles(DecentralizedStoreOperation op, String... filepaths);

    /**
     * Lists pending file operations that are queued in the database, but haven't been executed or
     * completed by the decentralized storage provider.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of pending file operations to return.
     */
    @Query("SELECT * FROM queue " +
            "WHERE started IS NULL AND operation = :op " +
            "LIMIT :limit")
    Flowable<List<DbQueue>> pendingFiles(DecentralizedStoreOperation op, int limit);

    /**
     * Lists failed file operations from the database; these were started but failed or were interrupted.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of failed file operations to return.
     */
    @Query("SELECT * FROM queue " +
            "WHERE errorMsg IS NOT NULL AND operation = :op " +
            "AND retries >= :maxRetries " +
            "LIMIT :limit")
    Flowable<List<DbQueue>> failedFiles(DecentralizedStoreOperation op, int limit, int maxRetries);

    /**
     * Lists completed file operations that were queued in the database.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of completed file operations to return.
     */
    @Query("SELECT * FROM queue " +
            "WHERE started IS NOT NULL AND operation = :op AND finished IS NOT NULL " +
            "LIMIT :limit")
    Flowable<List<DbQueue>> finishedFiles(DecentralizedStoreOperation op, int limit);

    /**
     * Lists running file operations that were queued in the database.
     *
     * @param op    Operation that was queued; one of `put`, `get`, `del`.
     * @param limit Maximum number of running file operations to return.
     */
    @Query("SELECT * FROM queue " +
            "WHERE started IS NOT NULL AND operation = :op AND finished IS NULL " +
            "AND errorMsg IS NULL " +
            "LIMIT :limit")
    Flowable<List<DbQueue>> runningFiles(DecentralizedStoreOperation op, int limit);

    /**
     * Searches the decentralized storage *metadata* by filename for files of a related CID.
     * This should be up-to-date if a sync has happened recently.
     *
     * @param fileName file name of the file to search for.
     * @param packageName name of the application requesting file
     */
    @Transaction
    @Query("SELECT *,rowid FROM filenames " +
            "WHERE filenames.filename = :fileName " +
            "AND filenames.packageName = :packageName " +
            "ORDER BY touched DESC " +
            "LIMIT :limit")
    Flowable<List<DsFileNamesWithFiles>> searchFilesbyName(String packageName, String fileName, int limit);

    /**
     * Lists the most recent files added to the decentralized storage.
     *
     * @param limit Maximum number of files to return.
     */
    @Transaction
    @Query("SELECT rowid,* FROM filenames " +
            "WHERE packageName = :packageName " +
            "ORDER BY touched DESC " +
            "LIMIT :limit")
    Flowable<List<DsFileName>> listRecentFiles(String packageName, int limit);

    /**
     * Returns a list of the most recent GET audit logs against the decentralized server. These GET
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Transaction
    @Query("SELECT files.* FROM files " +
            "INNER JOIN log_get ON files.cid = log_get.fileCid " +
            "WHERE files.cid = :cid " +
            "ORDER BY log_get.timestamp DESC " +
            "LIMIT :limit")
    Flowable<List<DsFileGetLogs>> listGetAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent PUT audit logs against the decentralized server. These PUT
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Transaction
    @Query("SELECT files.* FROM files " +
            "INNER JOIN log_put ON files.cid = log_put.fileCid " +
            "WHERE files.cid = :cid " +
            "ORDER BY log_put.timestamp DESC " +
            "LIMIT :limit")
    Flowable<List<DsFilePutLogs>> listPutAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent EXIST audit logs against the decentralized server. These EXIST
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Transaction
    @Query("SELECT files.* FROM files " +
            "INNER JOIN log_exist ON files.cid = log_exist.fileCid " +
            "WHERE files.cid = :cid " +
            "ORDER BY log_exist.timestamp DESC " +
            "LIMIT :limit")
    Flowable<List<DsFileExistLogs>> listExistAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent LIST audit logs against the decentralized server. These LIST
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Transaction
    @Query("SELECT files.* FROM files " +
            "INNER JOIN log_list ON files.cid = log_list.fileCid " +
            "WHERE files.cid = :cid " +
            "ORDER BY log_list.timestamp DESC " +
            "LIMIT :limit")
    Flowable<List<DsFileListLogs>> listListAuditLogs(String cid, int limit);

    /**
     * Returns a list of the most recent DELETE audit logs against the decentralized server. These DELETE
     * operations may originate on this device or another.
     *
     * @param cid   CID of the file to get logs for.
     * @param limit Maximum number of log entries to return.
     */
    @Transaction
    @Query("SELECT files.* FROM files " +
            "INNER JOIN log_delete ON files.cid = log_delete.fileCid " +
            "WHERE files.cid = :cid " +
            "ORDER BY log_delete.timestamp DESC " +
            "LIMIT :limit")
    Flowable<List<DsFileDeleteLogs>> listDeleteAuditLogs(String cid, int limit);
}