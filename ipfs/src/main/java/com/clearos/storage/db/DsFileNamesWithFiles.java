package com.clearos.storage.db;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class DsFileNamesWithFiles {
    @Embedded
    public DsFileName fileName;
    @Relation(
            parentColumn = "rowid",
            entity = DsFile.class,
            entityColumn = "cid",
            associateBy = @Junction(
                    value = DsFileCrossRefDsFileName.class,
                    parentColumn = "rowid",
                    entityColumn = "cid"
            )
    )
    public List<DsFile> files;

    private DsFile getLatestVersion() {
        long latest = 0;
        DsFile cid = null;
        for (DsFile f : files) {
            if (f.created.getTime() > latest) {
                cid = f;
                latest = f.created.getTime();
            }
        }
        return cid;
    }

    public DsFile getMostRecentFile() {
        return getLatestVersion();
    }
}
