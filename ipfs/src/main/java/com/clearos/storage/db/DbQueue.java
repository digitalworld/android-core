package com.clearos.storage.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.clearos.storage.provider.DecentralizedStoreOperation;

import java.util.Date;

@Entity(tableName = "queue", primaryKeys = {"filepath", "operation"})
public class DbQueue {
    @NonNull
    public String filepath;
    public Date added;
    public Date cancelled;
    public Date started;
    /**
     * Date time when the file was labeled "done"; this could mean that it 1) finished successfully;
     * 2) failed after too many retries; 3) failed due to error that cannot be retried.
     */
    public Date finished;
    public String errorMsg;
    @NonNull
    public DecentralizedStoreOperation operation;
    @ColumnInfo(name = "retries", defaultValue = "0")
    public int retryCount;
    public String cid;
    public int blocks;
    public int progress;
}
