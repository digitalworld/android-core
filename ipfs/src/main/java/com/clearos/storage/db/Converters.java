package com.clearos.storage.db;

import androidx.room.TypeConverter;

import com.clearos.storage.provider.DecentralizedStoreOperation;

import java.util.Date;

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static String operationToString(DecentralizedStoreOperation op) {
        return op.name();
    }

    @TypeConverter
    public static DecentralizedStoreOperation stringToOperation(String op) {
        return DecentralizedStoreOperation.valueOf(op);
    }
}
