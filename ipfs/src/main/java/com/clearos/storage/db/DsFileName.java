package com.clearos.storage.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Fts4;
import androidx.room.PrimaryKey;

import java.util.Date;

@Fts4
@Entity(tableName = "filenames")
public class DsFileName {
    @PrimaryKey
    @ColumnInfo(name = "rowid")
    public int fileNameId;
    @NonNull
    public String filename;
    @NonNull
    public String packageName;
    public String source;
    public Date touched;
}
