package com.clearos.storage;

import android.util.Log;

import com.clearos.dstorage.ObjectExists;
import com.clearos.dstorage.ObjectExistsHandler;

import io.grpc.stub.StreamObserver;
import tech.storewise.grpc_storage.ObjectInfo;

public class SwtObjectExists implements ObjectExists {
    private String key;
    private String cid;
    private String bucket;
    private ObjectExistsHandler query;
    private byte[] contents;
    private Boolean exists;
    private int blockId;
    private SwtGateway gateway;
    private Throwable error;

    private final static String TAG = "SwtObjectExists";

    public SwtObjectExists(String did, String _cid, String _bucket, SwtGateway gateway,
                           ObjectExistsHandler queryObj, byte[] _contents, int blockId) {
        key = String.format("%s/%s", did, _cid);
        cid = _cid;
        bucket = _bucket;
        query = queryObj;
        contents = _contents;
        this.blockId = blockId;
        this.gateway = gateway;
    }

    @Override
    public int getBlockId() {
        return blockId;
    }

    @Override
    public byte[] getCipher() {
        return contents;
    }

    @Override
    public Boolean getExists() {
        return exists;
    }

    @Override
    public String getCid() {
        return cid;
    }

    @Override
    public Throwable getError() {
        return error;
    }

    @Override
    public void start() {
        if (query.checkExists()) {
            Log.d(TAG, "Querying existence via SwtGateway.");
            gateway.getInstance().exists(bucket, key, new StreamObserver<ObjectInfo>() {
                @Override
                public void onNext(ObjectInfo value) {
                    exists = value.getKey() != null;
                    Log.d(TAG, String.format("Got response for existence %s; exists = %b", key, exists));
                    query.onExistsCheck(SwtObjectExists.this);
                }

                @Override
                public void onError(Throwable t) {
                    Log.d(TAG, "On error in gRPC gateway.", t);
                    query.onGenericError(t);
                }

                @Override
                public void onCompleted() {
                    Log.d(TAG, "OnCompleted fired on exists check.");
                }
            });
        } else {
            Log.d(TAG, "Skipping exists check for object; returning immediately.");
            exists = null;
            query.onExistsCheck(this);
        }
    }

}
