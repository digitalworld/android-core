package com.clearos.storage;

import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.clearos.dlt.DerivedKeyClient;
import com.clearos.dlt.DidAuthApiCallback;
import com.clearos.dlt.DidAuthClientApi;
import com.clearos.dlt.DidKeys;
import com.clearos.dlt.SharedPrefs;
import com.clearos.dlt.SuccessResponse;
import com.clearos.dlt.SystemProperties;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import com.google.protobuf.Timestamp;
import com.goterl.lazysodium.LazySodiumAndroid;
import com.goterl.lazysodium.SodiumAndroid;
import com.goterl.lazysodium.interfaces.Sign;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import okhttp3.Call;
import tech.storewise.grpc_storage.*;
import tech.storewise.grpc_storage.Object;

public class Gateway {
    private static final String CACHED_AUTH_PREFS_KEY = "dstorage/clearshare/auth";

    // private static StorageGrpc.StorageBlockingStub blockingStub;
    private static StorageGrpc.StorageStub asyncStub = null;
    private static ManagedChannel channel = null;
    private boolean started = false;
    private static long port;

    private static final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    private static final String TAG = "Gateway";

    private String homeServer;
    private DidKeys signingKeys;
    private DidAuthClientApi billingApi;
    private String contractSet = null;

    private static final String METADATA_SERVER = "staging.clearshare.network:443";

    Gateway(DerivedKeyClient keyClient, String contractSet) {
        billingApi = new DidAuthClientApi(keyClient);
        homeServer = keyClient.getHomeServer();
        signingKeys = keyClient.getAppKeys();
        this.contractSet = contractSet;
    }

    Gateway(DerivedKeyClient keyClient) {
        this(keyClient, null);
    }

    /**
     * Creates a gateway that is incapable of creating authenticated requests. This should be used
     * only for read-only access to public buckets.
     */
    Gateway() { }

    /**
     * Creates a gateway that is incapable of creating authenticated requests. This should be used
     * only for read-only access to public buckets.
     * @param contractSet The identifier of an existing contract set to use.
     */
    Gateway(String contractSet) {
        this.contractSet = contractSet;
    }

    /**
     * Returns the DID being used for authentication with the gateway.
     */
    public String getStorageDid() {
        return signingKeys.getDid();
    }

    public void start(String metadataStorageDir) throws Exception {
        if (started) {
            return;
        }

        started = true;
        bindStub();
    }

    private static synchronized StorageGrpc.StorageStub getStub() {
        if (asyncStub == null) bindStub();
        return asyncStub;
    }

    private static void bindStub() {
        if (channel == null)
            if (port == 0) port = 9200; //Default port started in another process.
            Log.d(TAG, String.format("Connecting channel to localhost at %d", port));
            channel = ManagedChannelBuilder.forAddress("localhost", (int)port).usePlaintext().build();

        // blockingStub = StorageGrpc.newBlockingStub(channel);
        asyncStub = StorageGrpc.newStub(channel);
    }

    public void stop() {

    }

    /**
     * Returns the grpc identifier needed for put and get operations on objects in buckets.
     * @param bucket Bucket name
     * @param key Unique key of the object in `bucket`.
     */
    private static ObjectIdentifier getObjectId(String bucket, String key) {
        return ObjectIdentifier.newBuilder().setBucket(bucket).setKey(key).build();
    }

    /**
     * Builds a grpc object from a byte block for storing in decentralized storage.
     */
    private static Object getObject(byte[] block) {
        return Object.newBuilder().setBody(ByteString.copyFrom(block)).build();
    }

    private PutRequest buildPutRequest(ObjectIdentifier objId, Object block) {
        return PutRequest.newBuilder().setId(objId).setObject(block).setAuth(buildDidAuth()).build();
    }

    private DidAuth buildDidAuth() {
        Instant dateNow = Instant.now();
        long currentMillis = dateNow.toEpochMilli();
        Timestamp now = Timestamp.newBuilder().setSeconds(currentMillis/1000).build();
        String message = String.format("date=%s00;svc=swt", dateNow.toString().replace('T', ' ').replace('Z', '0'));

        byte[] encodedMessage = message.getBytes(StandardCharsets.UTF_8);
        byte[] signedMessage = new byte[Sign.BYTES + encodedMessage.length];
        boolean res = ((Sign.Native)lazySodium).cryptoSign(signedMessage, encodedMessage, encodedMessage.length, signingKeys.getKeys().getSecretKey().getAsBytes());
        if (!res) {
            Log.e("GATEWAY-SIGN", "Could not sign your message.");
        }

        return DidAuth.newBuilder().setHomeServer(String.format("https://%s", homeServer))
                .setDate(now)
                .setDid(signingKeys.getDid())
                .setMsgBytes(ByteString.copyFrom(encodedMessage))
                .setSignature(ByteString.copyFrom(signedMessage)).build();
    }

    /**
     * Constructs DID-based authentication message packet for AWS-based storage.
     */
    private static DidAuth buildDidAuth(DidKeys signingKeys, String homeServer) {
        Instant dateNow = Instant.now();
        long currentMillis = dateNow.toEpochMilli();
        Timestamp now = Timestamp.newBuilder().setSeconds(currentMillis/1000).build();
        String message = String.format("date=%s00;svc=swt", dateNow.toString().replace('T', ' ').replace('Z', '0'));

        byte[] encodedMessage = message.getBytes(StandardCharsets.UTF_8);
        byte[] signedMessage = new byte[Sign.BYTES + encodedMessage.length];
        boolean res = ((Sign.Native)lazySodium).cryptoSign(signedMessage, encodedMessage, encodedMessage.length, signingKeys.getKeys().getSecretKey().getAsBytes());
        if (!res) {
            Log.e("GATEWAY-SIGN", "Could not sign your message.");
        }

        return DidAuth.newBuilder().setHomeServer(String.format("https://%s", homeServer))
                .setDate(now)
                .setDid(signingKeys.getDid())
                .setMsgBytes(ByteString.copyFrom(encodedMessage))
                .setSignature(ByteString.copyFrom(signedMessage)).build();
    }

    private GetRequest buildGetRequest(String bucket, String key) {
        return GetRequest.newBuilder().setId(getObjectId(bucket, key)).setAuth(buildDidAuth()).build();
    }

    private ExistRequest buildExistRequest(String bucket, String key) {
        return ExistRequest.newBuilder().setId(getObjectId(bucket, key)).setAuth(buildDidAuth()).build();
    }

    private DeleteRequest buildDeleteRequest(String bucket, String key) {
        return DeleteRequest.newBuilder().setId(getObjectId(bucket, key)).setAuth(buildDidAuth()).build();
    }

    private GetRequest buildUnauthenticatedGetRequest(String bucket, String key) {
        return GetRequest.newBuilder().setId(getObjectId(bucket, key)).build();
    }

    /**
     * Checks shared preferences to see if we have cached authorization for accessing ClearSHARE.
     * @return True if there is a valid cached authorization.
     */
    private static boolean haveClearShareAuth(Context context) {
        SharedPrefs prefs = SharedPrefs.getInstance(context);
        return prefs.getBoolean(CACHED_AUTH_PREFS_KEY, false);
    }

    /**
     * Caches the authorization result from the ClearSHARE API call.
     * @param okay True if the authorization exists for non-zero storage capacity.
     */
    private static void saveClearShareAuth(Context context, boolean okay) {
        SharedPrefs prefs = SharedPrefs.getInstance(context);
        prefs.saveBoolean(CACHED_AUTH_PREFS_KEY, okay);
    }


    /**
     * Auto-authorizes a device to use ClearGM based on valid OYP kes.
     * @param signingKeys DidAuth keys to sign the headers with.
     * @param callback Function to call with request response status.
     */
    public static void autoAuthorize(DerivedKeyClient keyClient, DidAuthClientApi api, DidKeys signingKeys,
                              Function<SuccessResponse, Void> callback,
                              StorageConfig config) {
        Instant dateNow = Instant.now();
        String message = String.format("oypDid=%s;date=%s00;serial=%s",
                keyClient.getOypDid(),
                dateNow.toString().replace('T', ' ').replace('Z', '0'),
                SystemProperties.getSerialNumber());

        Log.d(TAG, "Auto-authorizing ClearSHARE with " + message);

        Map<String, java.lang.Object> body = new HashMap<>();
        body.put("message", message);
        body.put("oypSignature", keyClient.oypSignMessage(message));
        body.put("enabled", true);
        body.put("storageClass", config.getStorageClass());
        body.put("spaceClass", config.getStorageSpace());
        body.put("geo", config.getGeolocation());
        body.put("size", config.getSize());

        api.post("/id/billing/autoclearshare", signingKeys, body,
                new DidAuthApiCallback<>(
                callback, "autoAuthorizeClearShare", SuccessResponse.class, api
        ));
    }


    public static Call getConfig(DerivedKeyClient keyClient, Function<StorageAuth, Void> callback) {
        DidKeys signingKeys = keyClient.getAppKeys();
        DidAuthClientApi billingApi = new DidAuthClientApi(keyClient);
        String homeServer = keyClient.getHomeServer();

        final String authPurpose = "clearshare";
        Map<String, java.lang.Object> body = new HashMap<>();
        DidAuth auth = buildDidAuth(signingKeys, homeServer);
        body.put("did", auth.getDid());
        body.put("message", auth.getMsg());
        body.put("signature", Base64.getEncoder().encodeToString(auth.getSignature().toByteArray()));
        body.put("purpose", authPurpose);
        body.put("reference", "");

        // If we don't have a cached authorization yet, make sure that we include an OYP signature
        // so that the endpoint can auto-authorize at the basic level.
        if (!haveClearShareAuth(keyClient.getAppContext())) {
            Log.d(TAG, "Creating OYP message for auto-authorize of ClearSHARE");
            Instant dateNow = Instant.now();
            String oypMessage = String.format("oypDid=%s;date=%s00;serial=%s",
                    keyClient.getOypDid(),
                    dateNow.toString().replace('T', ' ').replace('Z', '0'),
                    SystemProperties.getSerialNumber());
            body.put("oypMessage", oypMessage);
            body.put("oypSignature", keyClient.oypSignMessage(oypMessage));
        }

        return billingApi.post("/id/did/authorize", signingKeys, body,
                new DidAuthApiCallback<>(sa -> {
                    if (sa != null) {
                        Log.d(TAG, String.format("Caching result of storage config API auth: %.2f", sa.getLimitations().getSize()));
                        saveClearShareAuth(keyClient.getAppContext(), sa.getLimitations().getSize() > 0);
                        callback.apply(sa);
                    } else {
                        Log.d(TAG, "The initial config request failed; probably the user hasn't been approved.");
                        autoAuthorize(keyClient, billingApi, signingKeys, sr -> {
                            if (sr != null && sr.isSuccess()) {
                                getConfig(keyClient, callback);
                            } else {
                                Log.i(TAG, "Error trying to auto-register the ClearSHARE billing via OYP.");
                                callback.apply(null);
                            }

                            return null;
                        }, StorageConfig.getBasicConfig());
                    }

                    return null;
                }, "clearShareAuthConfigCheck", StorageAuth.class, billingApi)
        );
    }

    /**
     * Uploads a byte block to decentralized storage.
     * @param bucket Bucket name
     * @param key Unique key of the object in `bucket`.
     */
    public void upload(String bucket, String key, byte[] block, StreamObserver<PutResponse> handler) {
        PutRequest request = buildPutRequest(getObjectId(bucket, key), getObject(block));
        getStub().put(request, handler);
    }

    /**
     * Downloads an object from a public bucket.
     * @param bucket Name of the bucket to download from. Must have `public-` as a prefix.
     * @param key Name of the object to download from that bucket.
     */
    public void downloadPublic(String bucket, String key, StreamObserver<Object> handler) {
        getStub().get(buildUnauthenticatedGetRequest(bucket, key), handler);
    }

    public void download(String bucket, String key, StreamObserver<Object> handler) {
        getStub().get(buildGetRequest(bucket, key), handler);
    }

    public void exists(String bucket, String key, StreamObserver<ObjectInfo> handler) {
        Log.d(TAG, String.format("Exists request is being dispatched for %s: %s.", bucket, key));
        getStub().exist(buildExistRequest(bucket, key), handler);
    }

    private ListRequest buildListRequest(String bucket, String prefix, String continuationToken) {
        long maxEntries = 1000;
        ListRequest.Builder result = ListRequest.newBuilder().setBucket(bucket).setPrefix(prefix).setMaxSize(maxEntries);
        if (continuationToken != null) {
            result.setMarker(continuationToken);
        }
        return result.setAuth(buildDidAuth()).build();
    }

    public void listObjects(String bucket, String prefix, String continuationToken, StreamObserver<ListResponse> handler) {
        getStub().list(buildListRequest(bucket, prefix, continuationToken), handler);
    }

    public void delete(String bucket, String key, StreamObserver<Empty> handler) {
        getStub().delete(buildDeleteRequest(bucket, key), handler);
    }
}
