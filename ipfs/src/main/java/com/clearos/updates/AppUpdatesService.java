package com.clearos.updates;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.clearos.dlt.CryptoKeysService;
import com.clearos.dlt.DecryptedMqttMessage;
import com.clearos.dlt.IRemoteErrorHandler;
import com.clearos.dstorage.AwsStaticCredentials;
import com.clearos.dstorage.R;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentSkipListSet;

import static androidx.core.content.FileProvider.getUriForFile;

public class AppUpdatesService implements IRemoteErrorHandler {
    private final Context appContext;
    private final static String TAG = "AppUpdatesService";
    private final static String appUpdatesBucket = "clearos.marketplace";
    private final static String appUpdatesBetaBucket = "clearos.marketplace.beta";
    private final static Gson gson = new Gson();

    public final static String RESTART_REASON_EXTRA = "clearosRestartReason";
    public final static String RESTART_REASON_INSTALL_COMPLETED = "installCompleted";
    public final static String RESTART_REASON_APP_REQUESTED = "appRequestedRestart";
    private final static String RESTART_REASON_CLEARLIFE = "clearLifeUpdated";

    private final static String UPDATE_REASON_EXTRA = "clearLifeUpdateReason";
    private final static String UPDATE_REASON_CLEARLIFE = "minimumServiceVersionRequired";

    private final Map<String, IAppUpdateCallback> callbacks = new HashMap<>();
    private final ConcurrentSkipListSet<String> runningInstalls = new ConcurrentSkipListSet<>();
    private final Map<String, UpdateInfo> info = new HashMap<>();

    public static final String UPDATE_ON_CELL_FILE = "allow-cell-updates.dat";
    public static final String START_SERVICE_ACTION = "start-updates-service";
    public static final String STOP_SERVICE_ACTION = "stop-updates-service";
    public static final String EXTENSION_PACKAGE_NAME = "com.clearos.marketplace.privileged";

    private Network capableNetwork = null;
    private boolean isBetaEnrolled = false;
    private static long lastModifiedMobileUpdatesSetting = 0;
    private static boolean lastMobileUpdatesSetting = false;
    private static long clearlifeVersionCode = 0;

    @Override
    public void onRemoteError(RemoteException e) {
        Log.e(TAG, "Remote error in derived keys service.", e);
    }
    @Override
    public void onGenericError(Exception e) {
        Log.e(TAG, "Generic error in decentralized storage service.", e);
    }
    @Override
    public void onIOError(IOException e) {
        Log.e(TAG, "IO error in decentralized storage service.", e);
    }
    @Override
    public void onNotificationReceived(DecryptedMqttMessage s) {
        Log.i(TAG, "Notification received by decentralized storage service: " + s);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public AppUpdatesService(Context appContext, boolean isBetaEnrolled) {
        this.appContext = appContext;
        this.isBetaEnrolled = isBetaEnrolled;
        if (!isBetaEnrolled)
            registerNetworkMonitor();

        clearlifeVersionCode = getClearLifeVersionCode();
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private long getClearLifeVersionCode() {
        PackageInfo packageInfo;
        try {
            if (Utils.isClearDevice())
                packageInfo = appContext.getPackageManager().getPackageInfo("com.clearos.clearlife", 0);
            else
                packageInfo = appContext.getPackageManager().getPackageInfo("com.clearos.digitallife", 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Unable to find package version information.", e);
            return 0;
        }

        return packageInfo.getLongVersionCode();
    }


    /**
     * Returns the path to the file that stores the setting on whether to allow in-app updates
     * over mobile data. A file is used so that the services can easily track modified time and
     * decide whether to reload the latest state of the setting.
     */
    public static File getUpdateOnCellFile(Context appContext) {
        return new File(appContext.getFilesDir(), UPDATE_ON_CELL_FILE);
    }

    /**
     * Checks whether the user has enabled in-app updates over mobile data.
     * @return False if the setting hasn't been explicitly set.
     */
    public static boolean isUpdateOnCellEnabled(Context appContext) {
        File target = getUpdateOnCellFile(appContext);
        if (!target.exists())
            return false;

        long lastModified = target.lastModified();
        if (lastModified == lastModifiedMobileUpdatesSetting)
            return lastMobileUpdatesSetting;

        try {
            byte[] contents = Files.readAllBytes(target.toPath());
            lastMobileUpdatesSetting = contents[0] == 1;
            lastModifiedMobileUpdatesSetting = lastModified;

            return lastMobileUpdatesSetting;
        } catch (IOException e) {
            Log.e(TAG, "While retrieving in-app updates over cell setting", e);
            return false;
        }
    }

    private Collection<String> getCallingPackages() {
        int caller = Binder.getCallingUid();
        if (caller == 0) {
            return null;
        }
        String [] packages = appContext.getPackageManager().getPackagesForUid(caller);
        List<String> result = null;
        if (packages != null) {
            result = Arrays.asList(packages);
        }
        return result;
    }

    /**
     * Checks whether the specified package name matches the OS UID making the call.
     * @param packageName Name of the package requesting access.
     * @throws RemoteException if the package name specified doesn't match process UID of the caller.
     */
    private void verifyPackageIdentity(String packageName) throws RemoteException {
        Log.d(TAG, "Verifying package identity for " + packageName);
        Collection<String> packages = getCallingPackages();
        if (packages != null) {
            if (!packages.contains(packageName)) {
                throw new RemoteException("Specified package name does not match process UID for caller.");
            }
        } else {
            throw new RemoteException("Specified package name does not match process UID for caller.");
        }
    }

    private String getFirstCallingPackage() throws RemoteException {
        Collection<String> packages = getCallingPackages();
        if (packages != null) {
            return packages.toArray(new String[0])[0];
        } else {
            throw new RemoteException("Cannot determine calling package identity.");
        }
    }

    private void triggerDownloadProgress(String packageName, float progress) {
        Log.i(TAG, String.format("Triggering download progress callback for %s: %f.", packageName, progress));
        IAppUpdateCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onDownloadProgress(packageName, progress);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing download progress callback for %s.", packageName), e);
            }
        }
    }

    private void triggerInstallStarted(String packageName) {
        Log.i(TAG, String.format("Triggering install started callback for %s.", packageName));
        IAppUpdateCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onInstallStarted(packageName);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing install started callback for %s.", packageName), e);
            }
        }
    }

    private void triggerInstallFailed(String packageName, String errorMessage) {
        Log.i(TAG, String.format("Triggering install failed callback for %s: %s", packageName, errorMessage));
        IAppUpdateCallback callback = callbacks.getOrDefault(packageName, null);
        runningInstalls.remove(packageName);
        if (callback != null) {
            try {
                callback.onInstallFailed(packageName, errorMessage);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing install failed callback for %s.", packageName), e);
            }
        }
    }

    public static void restartClearLife(Context appContext) {
        Log.d(TAG, "Triggering restart app for ClearLIFE update.");
        Intent launchIntent;
        if (Utils.isClearDevice())
            launchIntent = new Intent("com.clearos.clearlife/.activity.SplashActivity");
        else
            launchIntent = new Intent("com.clearos.digitallife/.activity.SplashActivity");

        if (launchIntent != null) {
            launchIntent.putExtra(RESTART_REASON_EXTRA, RESTART_REASON_CLEARLIFE);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }

        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(appContext);
        stackBuilder.addNextIntentWithParentStack(launchIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) appContext.getSystemService(Context.ALARM_SERVICE);
        if (mgr != null) {
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, resultPendingIntent);
        }
    }

    private void triggerInstallCompleted(String packageName, String version) {
        Log.i(TAG, String.format("Triggering install completed callback for %s: %s", packageName, version));
        Intent launchIntent = appContext.getPackageManager().getLaunchIntentForPackage(packageName);
        if (launchIntent != null) {
            launchIntent.putExtra(RESTART_REASON_EXTRA, RESTART_REASON_INSTALL_COMPLETED);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            appContext.startActivity(launchIntent);
        }

        new Handler(appContext.getMainLooper()).post(() -> new ClearAppsVersionUploader(appContext).sendVersionFile());
    }

    private void triggerLatestVersion(String packageName, UpdateInfo info) {
        Log.i(TAG, String.format("Triggering latest version callback for %s: %s.", packageName, info.getVersionName()));
        IAppUpdateCallback callback = callbacks.getOrDefault(packageName, null);
        if (callback != null) {
            try {
                callback.onLatestVersion(packageName, info);
            } catch (RemoteException e) {
                Log.e(TAG, String.format("Error firing latest version callback for %s.", packageName), e);
            }
        }
    }

    private void registerNetworkMonitor() {
        ConnectivityManager cm =
                (ConnectivityManager)appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkRequest request = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED)
                    .build();
            ConnectivityManager.NetworkCallback callback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    super.onAvailable(network);
                    capableNetwork = network;
                    if (!cm.bindProcessToNetwork(network))
                        Log.w(TAG, "Unable to bind the current process to unmetered network.");
                }
            };
            cm.requestNetwork(request, callback);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void installPackage(String packageName, File apkFile) {
        if (apkFile.exists() && PrivilegedInstaller.isExtensionOk(appContext)) {
            triggerInstallStarted(packageName);
            try {
                Apk apkObj = new Apk(appContext, apkFile);
                PrivilegedInstaller installer = new PrivilegedInstaller(appContext, apkObj, status -> {
                    if (status != null && status.equals(PrivilegedInstaller.ACTION_INSTALL_COMPLETE)) {
                        if (packageName.equals(appContext.getPackageName())) {
                            Log.i(TAG, "Setting up app restart in 5 seconds because we are updating ourself!");
                            restartClearLife(appContext);
                        }
                        new Handler(appContext.getMainLooper()).postDelayed(() -> triggerInstallCompleted(packageName, apkObj.versionName), 1500);
                    } else
                        triggerInstallFailed(packageName, status);
                    // Since the install is finished, remove the package name from our concurrent stack.
                    runningInstalls.remove(packageName);
                    return null;
                });

                String providerName;
                if (Utils.isClearDevice())
                    providerName = appContext.getString(R.string.fileProviderName);
                else
                    providerName = appContext.getString(R.string.fileProviderNameDigitalLife);

                Uri contentUri = getUriForFile(appContext, providerName, apkFile);
                appContext.grantUriPermission(EXTENSION_PACKAGE_NAME, contentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Log.i(TAG, String.format("Calling install for URI %s by privileged extension.", contentUri.toString()));
                installer.installPackageInternal(contentUri);
            } catch (Exception e) {
                triggerInstallFailed(packageName, e.getMessage());
            }
        } else
            triggerInstallFailed(packageName, "The APK file does not exist or the privileged extension is not installed.");
    }

    /**
     * Returns the name of the file used for storing the latest APK version.
     */
    private String getVersionFileName(String packageName) {
        if (isBetaEnrolled)
            return String.format("%s/latest/betaVersion", packageName);
        else
            return String.format("%s/latest/version", packageName);
    }

    private @NonNull UpdateInfo getPackageVersionFromFile(String packageName) {
        if (info.containsKey(packageName)) {
            return Objects.requireNonNull(info.get(packageName));
        }

        String key = getVersionFileName(packageName);
        File target = new File(appContext.getCacheDir(), key);
        if (target.exists()) {
            try {
                String contents = new String(Files.readAllBytes(target.toPath()), StandardCharsets.UTF_8);
                Log.d(TAG, "Deserializing version JSON from " + contents);
                UpdateInfo updateInfo = gson.fromJson(contents, UpdateInfo.class);
                synchronized (info) {
                    info.put(packageName, updateInfo);
                }
                return updateInfo;
            } catch (IOException e) {
                Log.e(TAG, String.format("Error reading update info for %s from cached file.", packageName));
            }
        }

        return new UpdateInfo(packageName, "Unknown", 0, UpdateInfo.UNKNOWN, 0, 0, 0);
    }

    /**
     * Checks whether we have a cached version of the latest APK for a package. Makes sure that the
     * latest version has the same size as posted in the update info.
     * @param packageName Name of the package to check for.
     * @return False if the file does not exist, or if it has the wrong size.
     */
    private boolean checkCanInstallExistingFile(String packageName) {
        UpdateInfo packageInfo = getPackageVersionFromFile(packageName);
        @SuppressLint("DefaultLocale") String fileKey = String.format("repo/%s.%d.apk", packageName, packageInfo.getVersionCode());
        File target = new File(appContext.getExternalCacheDir(), fileKey);
        if (target.exists()) {
            if (target.length() == packageInfo.getFileSize())
                return true;
            else {
                if (!target.delete())
                    Log.w(TAG, String.format("Unable to delete cached version of APK %s for %s that has wrong file size.", target.getAbsolutePath(), packageName));
                return false;
            }
        } else
            return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public void getAnyUpdateInfo(String packageName, long versionCode) {
        // Check if we have info already. If so, check if an install is already running.
        UpdateInfo result = null;
        synchronized (info) {
            if (info.containsKey(packageName))
                result = info.get(packageName);
        }

        if (result != null) {
            if (runningInstalls.contains(packageName)) {
                result.setAvailability(UpdateInfo.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS);
                triggerLatestVersion(packageName, result);
                return;
            }
        }

        // Since we don't have a copy, download and deserialize it.
        String key = getVersionFileName(packageName);
        File target = new File(appContext.getCacheDir(), key);
        if (target.exists()) {
            if (!target.delete())
                Log.e(TAG, "Error deleting existing updates index file.");

            // Also remove the cached version of the object from local memory.
            synchronized (info) {
                info.remove(packageName);
            }
        }

        AwsPublicDownloader downloader = new AwsPublicDownloader(
                isBetaEnrolled ? appUpdatesBetaBucket : appUpdatesBucket,
                key,
                new AwsStaticCredentials(appContext).provider(),
                null,
                target,
                file -> {
                    if (!file.exists()) {
                        UpdateInfo missing = new UpdateInfo(packageName, "Unknown", 0, UpdateInfo.UNKNOWN, 0, 0, 0);
                        triggerLatestVersion(packageName, missing);
                        return null;
                    }

                    UpdateInfo updateInfo = getPackageVersionFromFile(packageName);

                    // Set the update availability by comparing the version codes. If an update
                    // was already running, we would have triggered it above already.
                    Log.d(TAG, String.format("Checking update compatibility: %d vs %d; network=%b",
                            updateInfo.getVersionCode(), versionCode,
                            isUpdateOnCellEnabled(appContext) || capableNetwork != null
                    ));

                    if (updateInfo.getVersionCode() == 0)
                        updateInfo.setAvailability(UpdateInfo.UNKNOWN);
                    else if (!isUpdateOnCellEnabled(appContext) && capableNetwork == null && !packageName.equals(appContext.getPackageName()))
                        updateInfo.setAvailability(UpdateInfo.NETWORK_NOT_AVAILABLE);
                    else if (!packageName.equals(CryptoKeysService.CLEARLIFE_PACKAGE) && !packageName.equals(CryptoKeysService.DIGITALLIFE_PACKAGE) && clearlifeVersionCode < updateInfo.getMinimumServiceVersionCode()) {
                        Log.i(TAG, String.format("ClearLIFE update required before updating %s", packageName));

                        new Handler(appContext.getMainLooper()).postDelayed(() -> {
                            Intent updateIntent = new Intent();
                            if (Utils.isClearDevice())
                                updateIntent.setComponent(new ComponentName("com.clearos.clearlife", "com.clearos.clearlife.activity.AppSettingsActivity"));
                            else
                                updateIntent.setComponent(new ComponentName("com.clearos.digitallife", "com.clearos.digitallife.activity.AppSettingsActivity"));
                            updateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            updateIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            updateIntent.putExtra(UPDATE_REASON_EXTRA, UPDATE_REASON_CLEARLIFE);
                            appContext.startActivity(updateIntent);
                        },500);

                        updateInfo.setAvailability(UpdateInfo.UPDATE_NOT_AVAILABLE);
                    } else if (updateInfo.getVersionCode() > versionCode && versionCode >= updateInfo.getMinimumVersionCode()) {
                        Log.i(TAG, "Selecting that an update is available");
                        updateInfo.setAvailability(UpdateInfo.UPDATE_AVAILABLE);
                    } else
                        updateInfo.setAvailability(UpdateInfo.UPDATE_NOT_AVAILABLE);

                    triggerLatestVersion(packageName, updateInfo);
                    return null;

                }, new CancellationSignal());
        downloader.execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public void startAnyAppUpdate(String packageName, long versionCode) throws RemoteException {
        // Only the package itself can request its own update.
        verifyPackageIdentity(packageName);

        // Mark that the update has started.
        if (runningInstalls.contains(packageName))
            // Don't start another package install, one is already going...
            return;
        else
            runningInstalls.add(packageName);

        if (!isUpdateOnCellEnabled(appContext) && capableNetwork == null && !packageName.equals(appContext.getPackageName()))
            throw new RemoteException("An un-metered network is not available to download the updated app from.");

        // Download the latest APK from decentralized storage.
        String key = String.format("repo/%s.apk", packageName);
        @SuppressLint("DefaultLocale") String fileKey = String.format("repo/%s.%d.apk", packageName, versionCode);
        File target = new File(appContext.getExternalCacheDir(), fileKey);
        if (target.exists()) {
            // We need to check the file size to see if the files match before trying to install
            // again.
            if (checkCanInstallExistingFile(packageName)) {
                Log.d(TAG, "Target APK already exists, triggering install.");
                installPackage(packageName, target);
                return;
            }
        }

        Log.d(TAG, "Starting public downloader.");
        AwsPublicDownloader downloader = new AwsPublicDownloader(
                isBetaEnrolled ? appUpdatesBetaBucket : appUpdatesBucket,
                key,
                new AwsStaticCredentials(appContext).provider(),
                percent -> {
                    triggerDownloadProgress(packageName, percent);
                    return null;
                },
                target,
                file -> {
                    if (file != null)
                        installPackage(packageName, file);
                    else
                        triggerInstallFailed(packageName, "Error downloading APK from remote storage.");
                    return null;
                }, new CancellationSignal());
        downloader.execute();
    }

    public void registerCallback(IAppUpdateCallback callback) throws RemoteException {
        String packageName = getFirstCallingPackage();
        Log.d(TAG, "Registering application remote service callback for " + packageName);
        callbacks.put(packageName, callback);
    }

    public void unregisterCallback(IAppUpdateCallback callback) throws RemoteException {
        String packageName = getFirstCallingPackage();
        Log.d(TAG, "Unregistering application remote service callback for " + packageName);
        callbacks.remove(packageName);
    }

    /**
     * Ask the service to restart the app after a given time.
     *
     * @param packageName Name of the package to restart.
     * @param delaySeconds How many seconds to wait before restart.
     */
    public void restartApp(String packageName, int delaySeconds) throws RemoteException {
        verifyPackageIdentity(packageName);
        Log.i(TAG, String.format("Scheduling restart of %s after %d seconds by app request.", packageName, delaySeconds));
        new Handler().postDelayed(() -> {
            Intent launchIntent = appContext.getPackageManager().getLaunchIntentForPackage(packageName);
            if (launchIntent != null) {
                launchIntent.putExtra(RESTART_REASON_EXTRA, RESTART_REASON_APP_REQUESTED);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                appContext.startActivity(launchIntent);
            }
        }, delaySeconds*1000);
    }
}
