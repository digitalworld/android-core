package com.clearos.updates;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.clearos.dlt.SystemProperties;
import com.clearos.dstorage.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ClearAppsVersionUploader {
    private final String TAG = "ClearAppsVersionUploader";
    private final Context appContext;

    public ClearAppsVersionUploader(Context appContext) {
        this.appContext = appContext;
    }

    public void sendVersionFile() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/uuuu");
        Instant now = Instant.now();
        ZonedDateTime mountainDateTime = now.atZone(ZoneId.of("MST"));
        Map<String, AppVersion> versionMap = new HashMap<>();
        PackageManager pm = appContext.getPackageManager();

        for (String packageName : Constants.CLEAR_APPS) {
            AppVersion appVersion = new AppVersion();
            try {
                PackageInfo info = pm.getPackageInfo(packageName, 0);
                if (info != null) {
                    appVersion.setDate(dtf.format(mountainDateTime));
                    appVersion.setPhoneModel(Build.MODEL);
                    appVersion.setPackageName(packageName);
                    appVersion.setVersionName(info.versionName);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        appVersion.setVersionCode(info.getLongVersionCode());
                    } else {
                        appVersion.setVersionCode((long) info.versionCode);
                    }
                    versionMap.put(packageName, appVersion);
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.i(TAG, String.format("%s does not exist on this phone", packageName));
            }
        }

        if (versionMap.isEmpty()) {
            Log.i(TAG, "Version file is empty. Skipping upload.");
            return;
        }

        String filePath = appContext.getExternalCacheDir() + File.separator + "version.json";

        //Write JSON file
        try (FileWriter file = new FileWriter(filePath)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Log.i(TAG, "Writing json file to " + filePath);
            gson.toJson(versionMap, file);
            file.flush();
            Log.i(TAG, "Writing version.json completed successfully");
        } catch (IOException e) {
            Log.e(TAG, "Problem writing version.json", e);
            return;
        }

        final String bucket = "clearapps-versions";
        final String key = SystemProperties.getSerialNumber() + File.separator + "version.json";

        AWSCredentialsProvider credentialsProvider = getVersionFileProvider();
        File outputTarget = new File(filePath);

        AwsPublicUploader awsPublicUploader = new AwsPublicUploader(bucket, key, credentialsProvider, null, outputTarget, success -> {
            if (success)
                Log.i(TAG, "Version file successfully sent to aws.");
            else
                Log.e(TAG, "Something went wrong sending the version file to aws.");

            if (outputTarget.exists()) {
                if (outputTarget.delete()) {
                    Log.i(TAG, "file Deleted :" + outputTarget.getPath());
                } else {
                    Log.i(TAG, "file not Deleted :" + outputTarget.getPath());
                }
            }

            return null;
        });

        Log.i(TAG, "Sending version.json to aws.");
        awsPublicUploader.execute();

    }

    private AWSCredentialsProvider getVersionFileProvider() {
        return new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return getVersionFileCredentials();
            }

            @Override
            public void refresh() {

            }
        };
    }

    private AWSCredentials getVersionFileCredentials() {
        return new AWSCredentials() {
            final String accessKey = appContext.getResources().getString(R.string.AccessKeyVersionFileId);
            final String secretKey = appContext.getResources().getString(R.string.SecretKeyVersionFile);
            @Override
            public String getAWSAccessKeyId() {
                return accessKey;
            }

            @Override
            public String getAWSSecretKey() {
                return secretKey;
            }
        };
    }
}
