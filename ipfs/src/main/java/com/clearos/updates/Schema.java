package com.clearos.updates;

import android.provider.BaseColumns;

public class Schema {
    /**
     * This table stores details of all the application versions we
     * know about. Each relates directly back to an entry in TABLE_APP.
     * This information is retrieved from the repositories.
     */
    interface ApkTable {

        String NAME = "fdroid_apk";

        interface Cols extends BaseColumns {
            String _COUNT_DISTINCT = "countDistinct";

            String APP_ID          = "appId";
            String ROW_ID          = "rowid";
            String VERSION_NAME    = "version";
            String REPO_ID         = "repo";
            String HASH            = "hash";
            String VERSION_CODE    = "vercode";
            String NAME            = "apkName";
            String SIZE            = "size";
            String SIGNATURE       = "sig";
            String SOURCE_NAME     = "srcname";
            String MIN_SDK_VERSION = "minSdkVersion";
            String TARGET_SDK_VERSION = "targetSdkVersion";
            String MAX_SDK_VERSION = "maxSdkVersion";
            String OBB_MAIN_FILE   = "obbMainFile";
            String OBB_MAIN_FILE_SHA256 = "obbMainFileSha256";
            String OBB_PATCH_FILE  = "obbPatchFile";
            String OBB_PATCH_FILE_SHA256 = "obbPatchFileSha256";
            String REQUESTED_PERMISSIONS = "permissions";
            String FEATURES        = "features";
            String NATIVE_CODE     = "nativecode";
            String HASH_TYPE       = "hashType";
            String ADDED_DATE      = "added";
            String IS_COMPATIBLE   = "compatible";
            String INCOMPATIBLE_REASONS = "incompatibleReasons";

            interface Repo {
                String VERSION = "repoVersion";
                String ADDRESS = "repoAddress";
            }

            interface Package {
                String PACKAGE_NAME = "package_packageName";
            }

            interface AntiFeatures {
                String ANTI_FEATURES   = "antiFeatures_commaSeparated";
            }

            String[] ALL_COLS = {
                    APP_ID, VERSION_NAME, REPO_ID, HASH, VERSION_CODE, NAME,
                    SIZE, SIGNATURE, SOURCE_NAME, MIN_SDK_VERSION, TARGET_SDK_VERSION, MAX_SDK_VERSION,
                    OBB_MAIN_FILE, OBB_MAIN_FILE_SHA256, OBB_PATCH_FILE, OBB_PATCH_FILE_SHA256,
                    REQUESTED_PERMISSIONS, FEATURES, NATIVE_CODE, HASH_TYPE, ADDED_DATE,
                    IS_COMPATIBLE, INCOMPATIBLE_REASONS,
            };

            String[] ALL = {
                    _ID, APP_ID, Package.PACKAGE_NAME, VERSION_NAME, REPO_ID, HASH, VERSION_CODE, NAME,
                    SIZE, SIGNATURE, SOURCE_NAME, MIN_SDK_VERSION, TARGET_SDK_VERSION, MAX_SDK_VERSION,
                    OBB_MAIN_FILE, OBB_MAIN_FILE_SHA256, OBB_PATCH_FILE, OBB_PATCH_FILE_SHA256,
                    REQUESTED_PERMISSIONS, FEATURES, NATIVE_CODE, HASH_TYPE, ADDED_DATE,
                    IS_COMPATIBLE, Repo.VERSION, Repo.ADDRESS, INCOMPATIBLE_REASONS,
                    AntiFeatures.ANTI_FEATURES,
            };
        }
    }
}
