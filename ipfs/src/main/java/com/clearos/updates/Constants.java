package com.clearos.updates;

public class Constants {
    public static String[] CLEAR_APPS = {
            "com.clearos.marketplace",
            "com.android.calendar",
            "software.clear.clearcare",
            "com.clearos.clearnames",
            "com.clearos.cleargm",
            "com.clearid",
            "com.pri.childrenspace",
            "com.clearos.clearlife",
            "com.clearos.digitallife",
            "com.android.mail",
            "com.clearos.clearmed",
            "com.android.contacts",
            "com.clearos.launcher",
            "com.clearos.clearpay",
            "com.clearos.digitalwallet",
            "com.android.gallery3d",
            "com.clearos.clearscanner",
            "com.clearos.clearsignal",
            "com.clearos.clearweb",
            "com.clearos.clearboard.inputmethod.latin",
            "com.google.android.gms",
            "com.google.android.gsf",
            "com.clearos.clearnav",
            "com.android.vending",
            "com.android.dialer",
            "org.lineageos.updater"
    };
}
