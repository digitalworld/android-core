package com.clearos.updates;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import static com.clearos.updates.AppUpdatesService.START_SERVICE_ACTION;
import static com.clearos.updates.AppUpdatesService.STOP_SERVICE_ACTION;



public class InAppBetaUpdatesService extends Service {
    private AppUpdatesService service;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = START_SERVICE_ACTION;
        if (intent != null) {
            action = intent.getAction();
        }
        if (action != null) {
            String TAG = "InAppUpdatesService";
            if (action.equals(START_SERVICE_ACTION)) {
                Log.i(TAG, "Received Start Service Intent");
                return START_STICKY;
                // your start service code
            } else if (action.equals(STOP_SERVICE_ACTION)) {
                Log.i(TAG, "Received Stop Service Intent");
                stopSelf();
                return START_NOT_STICKY;
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        service = new AppUpdatesService(getApplicationContext(), true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the interface
        return binder;
    }

    private final IBetaAppUpdates.Stub binder = new IBetaAppUpdates.Stub() {
        /**
         * Starts updating the specified version of a package for installation. Download progress callbacks
         * will be triggered if a callback has been registered.
         *
         * @param packageName Name of the package to update.
         * @param versionCode Version of that package to target with the update.
         */
        @Override
        public void startAppUpdate(String packageName, long versionCode) throws RemoteException {
            service.startAnyAppUpdate(packageName, versionCode);
        }

        /**
         * Gets the latest version of the app with given package name.
         *
         * @param packageName Fully-qualified name of package to get latest version code for.
         */
        @Override
        public void getUpdateInfo(String packageName, long versionCode) {
            service.getAnyUpdateInfo(packageName, versionCode);
        }

        @Override
        public void registerCallback(IAppUpdateCallback callback) throws RemoteException {
            service.registerCallback(callback);
        }

        @Override
        public void uregisterCallback(IAppUpdateCallback callback) throws RemoteException {
            service.unregisterCallback(callback);
        }

        /**
         * Ask the service to restart the app after a given time.
         *
         * @param packageName Name of the package to restart.
         * @param delaySeconds How many seconds to wait before restart.
         */
        @Override
        public void restartApp(String packageName, int delaySeconds) throws RemoteException {
            service.restartApp(packageName, delaySeconds);
        }
    };
}