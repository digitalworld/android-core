// IPushNotificationCallback.aidl
package com.clearos.dlt;

import com.clearos.dlt.DecryptedMqttMessage;

oneway interface IPushNotificationCallback {
    /**
     * Callback for push notifications received by ClearLIFE.
     */
    oneway void onTopicsChanged(in List<String> topics);
    oneway void onNotificationError(String message);
    oneway void onMessageReceived(in DecryptedMqttMessage message);
    oneway void onSubscribeTopic(String appDid, String topic, String packageName);
    oneway void onDefaultTopics(String packageName, String appDid);
    oneway void onUnsubscribeTopic(String packageName, String topic);
    oneway void onUnsubscribeDefaultTopics(String packageName);
}