package com.clearos.dlt.fido;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.clearos.dlt.CryptoKey;
import com.clearos.dlt.DidKeys;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import duo.labs.webauthn.models.AttestationObject;
import duo.labs.webauthn.models.AuthenticatorGetAssertionOptions;
import duo.labs.webauthn.models.AuthenticatorGetAssertionResult;
import duo.labs.webauthn.models.AuthenticatorMakeCredentialOptions;
import duo.labs.webauthn.models.NotificationExtras;
import duo.labs.webauthn.models.PublicKeyCredentialDescriptor;
import duo.labs.webauthn.models.RpEntity;
import duo.labs.webauthn.models.UserEntity;
import io.github.novacrypto.hashing.Sha256;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static android.content.ContentValues.TAG;
import static com.clearos.dlt.DataCustodian.JSON;
import static com.clearos.dlt.DataCustodian.getParamsString;

/**
 * Interacts with the server API.
 */
class AuthApi {
    private Context appContext;
    private String homeServer;
    private String masterDid;

    private final ObjectMapper MAPPER =  CBORMapper.builder().build();
    private static final Gson gson = new Gson();

    private OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new AddHeaderInterceptor())
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(40, TimeUnit.SECONDS)
            .connectTimeout(40, TimeUnit.SECONDS)
            .build();

    public AuthApi(Context context, String homeServer, String masterDid) {
        appContext = context;
        this.homeServer = homeServer;
        this.masterDid = masterDid;
    }

    private Call doRequest(Request request, Callback callback) {
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    @SuppressLint("DefaultLocale")
    private URI getURI(String path, Map<String, String> params) throws UnsupportedEncodingException, URISyntaxException {
        return new URI("https", String.format("%s:%d", homeServer, 443), path, getParamsString(params), null);
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     */
    public Call getKeys(DidKeys signingKeys, ListKeysCallback callback) throws Exception {
        Map<String, Object> body = new HashMap<>();
        Gson gson = new Gson();
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/fido/register/list-credentials", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    callback.onListCredentials("getKeys", parseUserCredentials(body));
                }
                else {
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     */
    public Call apiRegisterBegin(DidKeys signingKeys, RegisterBeginCallback callback) throws Exception {

        Map<String, Object> body = new HashMap<>();
        body.put("did", masterDid);
        body.put("attestation", "none");

        Map<String, String> selection = new HashMap<>();
        selection.put("authenticatorAttachment", "platform");
        selection.put("userVerification", "required");
        body.put("authenticatorSelection", selection);

        Gson gson = new Gson();
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/fido/register/begin", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        Log.d("request", "apiRegisterBegin: "+request.toString());
        request = CryptoKey.signRequest(request, signingKeys, masterDid);



        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi", "register begin response: "+response.body());

                        if (response== null){
                            Toast.makeText(appContext, "Server error", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Pair<AuthenticatorMakeCredentialOptions, String> result = parsePublicKeyCredentialCreationOptions(response.body());
                        callback.onSuccess(result.first, result.second);
                    } catch (Exception e) {
                        callback.onFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi", "register begin response: "+response.message());
                    threadSafeToast(appContext, "Server error \n"+"ERROR: AuthApi register begin response: "+response.message());
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param challenge The challenge string returned by [registerRequest].
     * @param response The FIDO2 response object.
     */
    public Call apiRegisterComplete(DidKeys signingKeys, String challenge,
                                    AttestationObject response, ListKeysCallback callback) throws Exception {

        Map<String, Object> body = new HashMap<>();
        body.put("attestationObject", response.asCBOR());
        ClientData cd = new ClientData(challenge, homeServer, ClientData.KeyType.create);
        body.put("clientDataJSON", cd.toJson().getBytes(StandardCharsets.UTF_8));

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/fido/register/complete", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        headers.put("challenge", challenge);

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        byte[] cborBytes = null;
        try {
            cborBytes = objectMapper.writeValueAsBytes(body);
        } catch (JsonProcessingException e) {
            Log.e("CBOR", "Error encoding body as CBOR", e);
        }


        Log.d("body", "body: "+body);

        RequestBody jBody = RequestBody.create(cborBytes);

        Log.d("jBody", "jBody: "+jBody.toString());

        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();


        Log.d("request: ", "apiRegisterComplete: "+request.toString());

        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    Log.d("SUCCESS: AuthAPi", "register complete response: "+body);
                    callback.onListCredentials("registerComplete", parseUserCredentials(body));
                }
                else {
                    Log.d("ERROR: AuthAPi", "register complete response: "+ response.body().string());
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param credentialId The credential ID to be removed.
     */
    public void removeKey(DidKeys signingKeys, String credentialId, Callback callback) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("credentialId", credentialId);
        URI target = getURI("/fido/register/delete", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create("{}", JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                delete(jBody).build();

        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        doRequest(request, callback);
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     */
    public void apiAuthenticateBegin(DidKeys signingKeys, NotificationExtras extras,
                                     AuthenticateBeginCallback callback) throws Exception {
        Map<String, Object> body = new HashMap<>();
        body.put("did", masterDid);

        if (extras != null) {
            body.put("method", extras.getMethod());
            body.put("message_id", extras.getMessageId());
            body.put("reference", extras.getTitle());
        }

        Gson gson = new Gson();
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/fido/authenticate/begin", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    try {
                        Pair<AuthenticatorGetAssertionOptions, String> result = parsePublicKeyCredentialRequestOptions(body);
                        callback.onSuccess(result.first, result.second);
                    } catch (Exception e) {
                        callback.onFailure(call, e);
                    }
                } else {
                    ResponseBody body = response.body();
                    Log.e(TAG, "While requesting FIDO authentication: " + body.string());
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }

    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param challenge The challenge string returned by [authenticateBegin].
     * @param response The assertion response from FIDO2 API.
     */
    public Call apiAuthenticateComplete(DidKeys signingKeys, String challenge,
                                        AuthenticatorGetAssertionResult response,
                                        ListKeysCallback callback) throws Exception {
        Map<String, Object> body = new HashMap<>();
        ClientData clientData = new ClientData(challenge, homeServer, ClientData.KeyType.get);
        body.put("clientDataJSON", clientData.toJson().getBytes(StandardCharsets.UTF_8));
        body.put("authenticatorData", response.authenticatorData);
        body.put("signature", response.signature);
        body.put("userHandle", response.selectedCredentialUserHandle);
        body.put("credentialId", response.selectedCredentialId);

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/fido/authenticate/complete", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        headers.put("challenge", challenge);

        // Sign with the specified keys for the DID.
        Headers headerbuild = Headers.of(headers);
        ObjectMapper objectMapper = new ObjectMapper(new CBORFactory());
        byte[] cborBytes = null;
        try {
            cborBytes = objectMapper.writeValueAsBytes(body);
        } catch (JsonProcessingException e) {
            Log.e("CBOR", "Error encoding body as CBOR", e);
        }
        RequestBody jBody = RequestBody.create(cborBytes);
        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    ResponseBody body = response.body();
                    callback.onListCredentials("authenticateComplete", parseUserCredentials(body));
                }
                else {
                    callback.onFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiGetStoredPassword(DidKeys signingKeys, ResponseCallback callback) throws Exception {

        Map<String, String> params = new HashMap<>();
        URI target = appendUri(getURI("/fido/mobile/vault-keys", params).toString(), "did="+masterDid);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);

        Request request = new Request.Builder()
                .url(target.toURL())
                .headers(headerbuild)
                .get().build();
        Log.d("TAG", "apiGetStoredPassword: "+request);

        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "Get stored password: "+response.body());
                        callback.onResponseSuccess(call, response,0);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiGetPasswordDetails(DidKeys signingKeys, String url, String key, ResponseCallback callback) throws Exception {

        Map<String, String> params = new HashMap<>();
        URI target = appendUri(getURI("/fido/mobile/password", params).toString(), "did="+masterDid);
        target = appendUri(target.toString(), "url="+url);
        target = appendUri(target.toString(), "key="+key);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);

        Request request = new Request.Builder()
                .url(target.toURL())
                .headers(headerbuild)
                .get().build();
        Log.d("TAG", "apiPasswordDetail request: "+request);

        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "password deatil: "+response.body());
                        callback.onResponseSuccess(call, response, 0);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiGetDetails(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback callback, int code) throws Exception {

        /*Map<String, Object> body = new HashMap<>();
        body.put("did", masterDid);
        body.put("attestation", "none");

        Map<String, String> selection = new HashMap<>();
        selection.put("authenticatorAttachment", "platform");
        selection.put("userVerification", "required");
        body.put("authenticatorSelection", selection);*/

        Gson gson = new Gson();
        String json = gson.toJson(jsonObject);

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/fido/mobile/form", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);


        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        Log.d("request", "apimobileFormBegin request: "+request.toString());
        // request = keys.signRequest(request, signingKeys);



        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "deatil: "+response.body());
                        callback.onResponseSuccess(call, response, code);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiAddStore(DidKeys signingKeys, JsonObject jsonObject, ResponseCallback callback, int code) throws Exception {

        /*Map<String, Object> body = new HashMap<>();
        body.put("did", masterDid);
        body.put("attestation", "none");

        Map<String, String> selection = new HashMap<>();
        selection.put("authenticatorAttachment", "platform");
        selection.put("userVerification", "required");
        body.put("authenticatorSelection", selection);*/

        Gson gson = new Gson();
        String json = gson.toJson(jsonObject);

        Map<String, String> params = new HashMap<>();
        URI target = getURI("/fido/mobile/store", params);

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);

        Request request = new Request.Builder().url(target.toURL()).headers(headerbuild).
                post(jBody).build();

        Log.d("request", "apimobileFormBegin request: "+request.toString());
        request = CryptoKey.signRequest(request, signingKeys, masterDid);

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "deatil: "+response.body());
                        callback.onResponseSuccess(call, response, code);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }


    /**
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param callback ResponseHandler.
     */
    public Call apiRemoveStore(DidKeys signingKeys, String key, String category, String provider, ResponseCallback callback, int code) throws Exception {

        /*Map<String, Object> body = new HashMap<>();
        body.put("id", name);
        body.put("category", category);
        body.put("did", signingKeys.did);
        body.put("url", provider);*/

        Map<String, String> params = new HashMap<>();
        URI target = appendUri(getURI("/fido/mobile/remove", params).toString(), "key="+key);
        target = appendUri(target.toString(), "category="+category);
        target = appendUri(target.toString(), "did="+masterDid);
        target = appendUri(target.toString(), "url="+provider);


        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);


        Request request = new Request.Builder()
                .url(target.toURL())
                .headers(headerbuild)
                .delete().build();

        Log.d("request", "apimobileFormBegin request: "+request.toString());
        // request = keys.signRequest(request, signingKeys);



        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onResponseFailure(call, e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() >= 200 && response.code() < 300) {
                    try {

                        Log.d("SUCCESS: AuthApi ", "deatil: "+response.body());
                        callback.onResponseSuccess(call, response, code);
                    } catch (Exception e) {
                        callback.onResponseFailure(call, e);
                    }
                }
                else {
                    Log.d("ERROR: AuthApi ", response.message());
                    callback.onResponseFailure(call, new IOException(response.message()));
                }
            }
        });
    }





    /** To add queryParameter in request URL
     * @param uri request uri
     * @param appendQuery Query parameter with value
     */
    public static URI appendUri(String uri, String appendQuery) throws URISyntaxException {
        URI oldUri = new URI(uri);

        String newQuery = oldUri.getQuery();
        if (newQuery == null) {
            newQuery = appendQuery;
        } else {
            newQuery += "&" + appendQuery;
        }

        URI newUri = new URI(oldUri.getScheme(), oldUri.getAuthority(),
                oldUri.getPath(), newQuery, oldUri.getFragment());

        return newUri;
    }

    private Pair<AuthenticatorGetAssertionOptions, String> parsePublicKeyCredentialRequestOptions(ResponseBody body) throws Exception {
        Map<String,Object> responseMap = (Map<String,Object>) MAPPER.readValue(body.bytes(), Map.class);
        LinkedHashMap<String, Object> dict = (LinkedHashMap<String, Object>)responseMap.get("publicKey");

        AuthenticatorGetAssertionOptions options = new AuthenticatorGetAssertionOptions();
        options.allowCredentialDescriptorList = parseCredentialDescriptors(dict.get("allowCredentials"));
        options.rpId = dict.get("rpId").toString();
        options.requireUserVerification = dict.getOrDefault("userVerification", "").toString().equals("required");

        String challenge = WebSafeEncoder.webSafeBase64Encode((byte[])dict.get("challenge"));
        ClientData data = new ClientData(challenge, homeServer, ClientData.KeyType.get);
        options.clientDataHash = data.getHash();

        return new Pair<>(options, challenge);
    }

    private Pair<AuthenticatorMakeCredentialOptions, String> parsePublicKeyCredentialCreationOptions(
            ResponseBody body) throws Exception {
        byte[] cborBytes = body.bytes();
        Map<String,Object> responseMap = (Map<String,Object>) MAPPER.readValue(cborBytes, Map.class);

        AuthenticatorMakeCredentialOptions options = new AuthenticatorMakeCredentialOptions();
        Map<String, Object> dict = (HashMap<String, Object>)responseMap.get("publicKey");

        options.userEntity = parseUser(dict.getOrDefault("user", null));
        options.rpEntity = parseRp(dict.getOrDefault("rp", null));
        options.credTypesAndPubKeyAlgs = parseParameters(dict.getOrDefault("pubKeyCredParams", null));
        options.excludeCredentialDescriptorList = parseCredentialDescriptors(dict.getOrDefault("excludeCredentials", null));
        options.requireUserVerification = true;

        String challenge = WebSafeEncoder.webSafeBase64Encode((byte[])dict.get("challenge"));
        ClientData data = new ClientData(challenge, homeServer, ClientData.KeyType.create);
        options.clientDataHash = data.getHash();

        return new Pair<>(options, challenge);
    }

    private RpEntity parseRp(Object entry) {
        if (entry != null) {
            Map<String, Object> dict = (Map<String, Object>) entry;
            RpEntity result = new RpEntity();
            result.id = dict.get("id").toString();
            result.name = dict.get("name").toString();
            return result;
        } else {
            return null;
        }
    }

    private List<PublicKeyCredentialDescriptor> parseCredentialDescriptors(Object descriptors) {
        if (descriptors!= null) {
            List<Object> descList = (List<Object>) descriptors;
            List<PublicKeyCredentialDescriptor> result = new ArrayList<>();
            Map<String, Object> entry;
            for (int i = 0; i < descList.size(); i++) {
                entry = (Map<String, Object>)descList.get(i);
                byte[] id = (byte[])entry.get("id");
                result.add(new PublicKeyCredentialDescriptor(
                        "public-key",
                        id,
                        /* transports */ null
                ));
            }
            return result;
        } else {
            return null;
        }
    }

    private UserEntity parseUser(Object entry) {
        if (entry != null) {
            Map<String, Object> dict = (Map<String, Object>)entry;
            UserEntity result = new UserEntity();
            result.id = (byte[]) dict.get("id");
            result.name = dict.get("name").toString();
            result.displayName = dict.get("displayName").toString();
            return result;
        } else {
            return null;
        }
    }

    private List<Pair<String, Long>> parseParameters(Object parameters) {
        if (parameters!= null) {
            List<Object> paramList = (List<Object>) parameters;
            List<Pair<String, Long>> result = new ArrayList<>();
            Map<String, Object> entry;
            for (int i = 0; i < paramList.size(); i++) {
                entry = (Map<String, Object>)paramList.get(i);
                int alg = (int)entry.get("alg");
                String type = entry.get("type").toString();
                try {
                    result.add(new Pair<String, Long>(type, (long)alg));
                } catch (Exception e) {
                    Log.w("ALG", e.getMessage(), e);
                }
            }
            return result;
        } else {
            return null;
        }
    }

    private Map<String, String> getPublicKey(Object entry) {
        LinkedHashMap<String, Object> pKey = (LinkedHashMap<String, Object>) entry;
        Map<String, String> result = new HashMap<>();
        Base64.Encoder encoder = Base64.getEncoder();
        for (String key: pKey.keySet()) {
            Object value = pKey.get(key);
            try {
                Integer iValue = (Integer)value;
                result.put(key, iValue.toString());
            } catch (Exception e) {
                byte[] bValue = (byte[])value;
                result.put(key, encoder.encodeToString(bValue));
            }
        }
        return result;
    }

    private List<Credential> parseUserCredentials(ResponseBody body) throws IOException {
        byte[] cborBytes = body.bytes();
        Map<String,Object> dict = (Map<String,Object>) MAPPER.readValue(cborBytes, Map.class);

        List<Credential> result = new ArrayList<>();
        if (dict.containsKey("credentials")) {
            List<Object> entries = (List<Object>) dict.get("credentials");
            for (int i = 0; i < entries.size(); i++) {
                Map<String, Object> entry = (Map<String, Object>) entries.get(i);
                result.add(new Credential(
                        Base64.getEncoder().encodeToString((byte[])(entry.get("credId"))),
                        getPublicKey(entry.get("publicKey")).toString()
                ));
            }
        }
        return result;
    }

    public void threadSafeToast(Context context, String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, text, Toast.LENGTH_LONG).show();
            }
        });
    }

}
