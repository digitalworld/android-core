package com.clearos.dlt.fido;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import io.github.novacrypto.hashing.Sha256;

import static android.content.ContentValues.TAG;

public class ClientData {
    public static final String KEY_CHALLENGE = "challenge";
    public static final String KEY_ORIGIN = "origin";
    public static final String KEY_TYPE = "type";
    public static final String TYPE_FINISH_ENROLLMENT = "navigator.id.finishEnrollment";
    public static final String TYPE_GET_ASSERTION = "navigator.id.getAssertion";

    private final String challenge;
    private final String origin;
    private final KeyType keyType;

    private static final Gson gson = new Gson();

    public ClientData(String challenge, String origin, KeyType keyType) {
        this.challenge = challenge;
        this.origin = origin;
        this.keyType = keyType;
    }

    public byte[] getHash() {
        String json = toJson();
        Log.d(TAG, String.format("Hashing %s as contents for client data.", json));
        return Sha256.sha256(json.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Converts this client data object to JSON.
     */
    public String toJson() {
        Map<String, String> values = new HashMap<>();
        values.put(KEY_CHALLENGE, challenge);
        values.put(KEY_ORIGIN, String.format("https://%s", origin));
        values.put(KEY_TYPE, String.format("webauthn.%s", keyType.name()));

        return gson.toJson(values);
    }

    enum KeyType {
        create,
        get;
    }
}
