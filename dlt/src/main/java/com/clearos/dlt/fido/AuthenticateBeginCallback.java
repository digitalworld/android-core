package com.clearos.dlt.fido;

import duo.labs.webauthn.models.AuthenticatorGetAssertionOptions;
import okhttp3.Call;

public interface AuthenticateBeginCallback {
    void onSuccess(AuthenticatorGetAssertionOptions options, String challenge);
    void onFailure(Call call, Exception e);
}
