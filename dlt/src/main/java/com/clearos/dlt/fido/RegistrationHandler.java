package com.clearos.dlt.fido;

import java.util.concurrent.TimeoutException;

import duo.labs.webauthn.exceptions.NoBiometricsError;
import duo.labs.webauthn.models.AuthenticatorMakeCredentialOptions;
import okhttp3.Call;

public interface RegistrationHandler extends ListKeysCallback {
    void onBeginSuccess(AuthenticatorMakeCredentialOptions options);
    void onCompleteStarted(Call apiCall);
    void onNoBiometricsError(NoBiometricsError error);
    void onTimeoutError(TimeoutException e);
}
