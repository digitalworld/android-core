package com.clearos.dlt.fido;

import java.util.List;
import okhttp3.Call;

public interface ListKeysCallback {
    void onListCredentials(String source, List<Credential> credentials);
    void onFailure(Call call, Exception e);
}
