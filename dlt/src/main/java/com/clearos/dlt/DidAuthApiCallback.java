package com.clearos.dlt;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.function.Function;

import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class DidAuthApiCallback<T> implements GenericApiCallback<T> {
    private final Function<T, Void> callback;
    private final Class<T> typeParameterClass;
    private final String TAG;
    private DidAuthApi client;
    private int skipStart = 0;
    private int skipEnd = 0;

    private static Gson gson;

    public DidAuthApiCallback(Function<T, Void> _callback, String _TAG, Class<T> _typeParameterClass, DidAuthApi _client) {
        callback = _callback;
        TAG = _TAG;
        typeParameterClass = _typeParameterClass;
        client = _client;
        gson = DidAuthApi.getGson();
    }

    public static Gson getGson() {
        return gson;
    }

    public String getTAG() {
        return TAG;
    }

    public Function<T, Void> getCallback() {
        return callback;
    }

    /**
     * Sets the API client object that should be set on each callback result (allows a nice API
     * for recursive lookups).
     * @param _client Client object with Remote API credentials configured.
     */
    void setClient(DidAuthApi _client) {
        client = _client;
    }

    /**
     * Sets the number of characters to skip from the end of the payload before JSON parsing.
     * This is useful when the target objects are nested in several, single entry JSON mappings.
     */
    public DidAuthApiCallback<T> setSkipEnd(int skipEnd) {
        this.skipEnd = skipEnd;
        return this;
    }

    /**
     * Sets the number of characters to skip from the beginning of the payload before JSON parsing.
     * This is useful when the target objects are nested in several, single entry JSON mappings.
     */
    public DidAuthApiCallback<T> setSkipStart(int skipStart) {
        this.skipStart = skipStart;
        return this;
    }

    private static String className(Object o) {
        Class<?> enclosingClass = o.getClass().getEnclosingClass();
        if (enclosingClass != null) {
            return enclosingClass.getName();
        } else {
            return o.getClass().getName();
        }
    }

    private String className() {
        return className(this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onFailure(Call call, Exception e, Response response) {
        logApiCallFailure(this, TAG, e, response);
        if (typeParameterClass == SuccessResponse.class) {
            SuccessResponse result = new SuccessResponse();
            result.setSuccess(false);
            runCallback((T) result, false);
        } else {
            runCallback(null, false);
        }
    }

    public static void logApiCallFailure(Object o, String TAG, Exception e, Response response) {
        if (e != null) {
            Log.e(TAG, "Api call failed in " + className(o), e);
        }
        if (response != null) {
            ResponseBody body = response.body();
            if (body != null) {
                try {
                    Log.d(TAG, "Api call body: " + body.string());
                } catch (IOException ioError) {
                    Log.e(TAG, "IO error reading body from API call.", e);
                } catch (IllegalStateException illegalStateException) {
                    Log.e(TAG, "Illegal State Exception error reading body from API call.", e);
                }
            }
            Log.d(TAG, String.format("Response code: %d", response.code()));
            Log.d(TAG, "Response message: " + response.message());
            response.close();
        } else
            Log.d(TAG, "Null response from API call.");
    }

    @SuppressWarnings("unchecked")
    public void deserializeResponse(String body, int code) {
        if (typeParameterClass == SuccessResponse.class) {
            SuccessResponse result = new SuccessResponse();
            result.setCodeInRange(code);
            runCallback((T)result, false);
            return;
        }

        if (body == null) {
            Log.e(TAG, "Empty response body from API call.");
            runCallback(null, false);
            return;
        }

        try {
            String subResult = body.substring(skipStart, body.length()-skipEnd);

            // TODO: Remove if else statement once toml file is fixed
            if (subResult.contains("\"is_unlimited\":1")) {
                subResult = subResult.replace("\"is_unlimited\":1", "\"is_unlimited\":true");
            } else if (subResult.contains("\"is_unlimited\":0")) {
                subResult = subResult.replace("\"is_unlimited\":0", "\"is_unlimited\":false");
            }
            // TODO: END

            T result = gson.fromJson(subResult, typeParameterClass);
            if (result instanceof ApiObject) {
                ((ApiObject) result).setApiClient(client);
            }
            runCallback(result, false);
        } catch (Exception e) {
            Log.e(TAG, "Error deserializing from JSON.", e);
            runCallback(null, false);
        }
    }

    @Override
    public void onResponse(String source, Response response) {
        try {
            ResponseBody body = response.body();
            String bodyString = body == null ? null : body.string();
            Log.d(TAG, String.format("Received response %s from %s", bodyString, response.request().url()));
            deserializeResponse(bodyString, response.code());
        } catch (IOException e) {
            Log.e(TAG, "IO error reading response body.", e);
            runCallback(null, false);
        }
    }

    @Override
    public Class<T> getParameterizedClass() {
        return typeParameterClass;
    }

    @Override
    public void runCallback(T result, boolean fromCache) {
        Log.d(TAG, "In a class that is not cache-aware; just shooting regular callback.");
        callback.apply(result);
    }
}
