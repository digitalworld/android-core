package com.clearos.dlt;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import duo.labs.webauthn.models.AuthenticatorGetAssertionResult;
import duo.labs.webauthn.models.NotificationExtras;

public class DecryptedMqttMessage extends MqttMessage implements Parcelable {
    private static final String TAG = "DecryptedMqttMessage";
    private static final String KIND_KEY = "kind";
    private static final String FIDO_KIND = "fido";
    private static final String APPROVAL_KIND = "approval";
    private static final String REFERENCE_KEY = "reference";
    private static final String DOMAIN_KEY = "domain";
    private static final String URL_KEY = "url";
    private static final String TITLE_KEY = "title";
    private static final String MESSAGE_KEY = "message";
    private static final String MESSAGE_ID_KEY = "message_id";
    private static final String BODY_KEY = "body";
    private static final String EXTRAS_KEY = "extras";
    private static final String SIGNATURE_KEY = "signature";
    private static final String FIELDS_KEY = "fields";
    private static final String CLEAR_BODY_EXTRAS_KEY = "clear";

    private Map<String, Object> decryptedPayload = new HashMap<>();
    private String packageName;
    private NotificationExtras extras = null;
    private String fidoJson;

    private static final transient Gson gson = new Gson();
    private transient NotificationField[] fields;

    public DecryptedMqttMessage(MqttMessage message) {
        setDuplicate(message.isDuplicate());
        setId(message.getId());
        setPayload(message.getPayload());
        setQos(message.getQos());
        setRetained(message.isRetained());
    }

    @SuppressWarnings("unchecked")
    public DecryptedMqttMessage(Parcel in) {
        setRetained(in.readInt() == 1);
        setDuplicate(in.readInt() == 1);
        setId(in.readInt());
        setQos(in.readInt());
        setDecryptedPayload(gson.fromJson(in.readString(), decryptedPayload.getClass()));
        setPackageName(in.readString());
        fidoJson = in.readString();
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("(%s): %s: %s", packageName, getPayloadString(TITLE_KEY), getPayloadString(MESSAGE_KEY));
    }

    public void setFields(NotificationField[] fields) {
        this.fields = fields;
    }

    /**
     * Extracts any customized fields from the notification that the user can customize values
     * for.
     */
    @SuppressWarnings("unchecked")
    public NotificationField[] getFields() {
        if (fields == null) {
            Map<String, Object> body = getBody();
            if (body == null)
                return null;

            if (body.containsKey(FIELDS_KEY)) {
                List<Object> f = (List<Object>) body.get(FIELDS_KEY);
                fields = new NotificationField[f.size()];
                for (int i=0; i<f.size(); i++) {
                    Map<String, String> field = (Map<String, String>)f.get(i);
                    fields[i] = new NotificationField(field);
                }
            } else
                fields = new NotificationField[0];
        }
        return fields;
    }

    /**
     * Updates the body of this message with the assertion result JSON.
     * @param result From the authenticator FIDO assertion.
     */
    public void updateBody(AuthenticatorGetAssertionResult result) {
        fidoJson = result.toJson();
    }

    /**
     * Returns the authenticator assertion result if this was a FIDO auth message.
     * @return Null if the kind is not "fido".
     */
    public AuthenticatorGetAssertionResult getAssertionResult() {
        if (fidoJson == null)
            return null;

        return AuthenticatorGetAssertionResult.fromJson(fidoJson);
    }



    /**
     * Turns this MQTT message into a notification extra needed to do FIDO auth.
     * @return Null if this message doesn't have the fido kind.
     */
    public NotificationExtras toNotificationExtras() {
        if (!isFidoNotification() && !isApprovalNotification())
            return null;

        String domain = getBodyString(DOMAIN_KEY);
        if (domain == null) {
            domain = getExtrasString(URL_KEY);
        }
        if (extras == null) {
            extras = new NotificationExtras(
                    getPayloadString(MESSAGE_ID_KEY),
                    domain,
                    getPayloadString(TITLE_KEY),
                    getPayloadString(MESSAGE_KEY),
                    getBody(),
                    getBodyString(SIGNATURE_KEY)
            );
        }

        return extras;
    }

    public String getSignature() {
        return getBodyString(SIGNATURE_KEY);
    }

    public String getTitle() {
        return getPayloadString(TITLE_KEY);
    }

    public String getMessage() {
        return getPayloadString(MESSAGE_KEY);
    }

    /**
     * Checks whether FIDO authentication artifacts are attached to this message.
     * @return True if FIDO authentication completed.
     */
    public boolean isFidoComplete() {
        return fidoJson != null;
    }

    /**
     * Checks whether this push notification was for FIDO authentication.
     * @return True if the `kind` key is present *and* it matches the FIDO kind.
     */
    public boolean isFidoNotification() {
        return getKind().equals(FIDO_KIND);
    }

    /**
     * Checks whether this push notification was for FIDO authentication.
     * @return True if the `kind` key is present *and* it matches the FIDO kind.
     */
    public boolean isApprovalNotification() {
        return getKind().equals(APPROVAL_KIND);
    }

    /**
     * Returns the contents of the reference key in the body, if it exists.
     * @return Null if it does not exist.
     */
    public String getReference() {
        return getBodyString(REFERENCE_KEY);
    }

    /**
     * Returns the unique message ID for this message as a String.
     */
    public String getMessageId() {
        return Integer.toString(getId());
    }

    /**
     * Retrieves the `kind` value from the decrypted message payload.
     * @return Null if the `kind` key is not present.
     */
    public String getKind() {
        return getBodyString(KIND_KEY);
    }

    /**
     * Gets a string from the root dictionary in the payload.
     * @param key Key in the root dictionary to retrieve a value fol.
     * @return Null if the key is not present.
     */
    public String getPayloadString(String key) {
        if (decryptedPayload == null)
            return null;

        if (decryptedPayload.containsKey(key))
            return decryptedPayload.get(key).toString();
        return null;
    }

    /**
     * Checks whether the body includes a custom CLEAR-specific action implemented only for
     * Clear services and applications.
     */
    public boolean hasClearExtras() {
        return getClearCommand() != null;
    }

    /**
     * Returns the custom Clear-specific action command in the body, if it exists.
     * @return null if it is missing.
     */
    public String getClearCommand() {
        return getBodyString(CLEAR_BODY_EXTRAS_KEY);
    }

    /**
     * Retrieve an arbitrary string from the body of the push notification.
     * @param key Key in the `body` mapping of the notification.
     * @return Null if the key is not present.
     */
    public String getBodyString(String key) {
        Map<String, Object> body = getBody();
        if (body == null)
            return null;

        if (body.containsKey(key))
            return body.get(key).toString();
        return null;
    }

    /**
     * Retrieve the body mapping object from the decrypted payload.
     * @return Null if there isn't one.
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getBody() {
        if (decryptedPayload == null)
            return null;

        if (decryptedPayload.containsKey(BODY_KEY)) {
            try {
                return (Map<String, Object>) decryptedPayload.get(BODY_KEY);
            } catch (Exception e) {
                Log.e(TAG, "While trying to cast body from notification; probably malformed.", e);
                return null;
            }
        }
        return null;
    }

    /**
     * Retrieve an arbitrary string from the extras of the push notification.
     * @param key Key in the `extras` mapping of the notification.
     * @return Null if the key is not present.
     */
    public String getExtrasString(String key) {
        Map<String, Object> extras = getExtras();
        if (extras != null & extras.containsKey(key)) {
            return extras.get(key).toString();
        }
        return null;
    }

    /**
     * Retrieve the extras mapping object from the decrypted payload.
     * @return Null if there isn't one.
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getExtras() {
        if (decryptedPayload != null && decryptedPayload.containsKey(EXTRAS_KEY)) {
            try {
                return (Map<String, Object>) decryptedPayload.get(EXTRAS_KEY);
            } catch (Exception e) {
                Log.e(TAG, "While trying to cast extras from notification; probably malformed.", e);
            }
        }
        return null;
    }

    public static DecryptedMqttMessage fromMqttMessage(MqttMessage message, Map<String, Object> decryptedPayload) {
        DecryptedMqttMessage result = new DecryptedMqttMessage(message);
        result.setDecryptedPayload(decryptedPayload);
        return result;
    }

    public DecryptedMqttMessage setPackageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setDecryptedPayload(Map<String, Object> decryptedPayload) {
        this.decryptedPayload = decryptedPayload;
    }

    public Map<String, Object> getDecryptedPayload() {
        return decryptedPayload;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(isRetained() ? 1 : 0);
        dest.writeInt(isDuplicate() ? 1 : 0);
        dest.writeInt(getId());
        dest.writeInt(getQos());
        dest.writeString(gson.toJson(decryptedPayload));
        dest.writeString(packageName);
        dest.writeString(fidoJson);
    }

    public static final Parcelable.Creator<DecryptedMqttMessage> CREATOR
            = new Parcelable.Creator<DecryptedMqttMessage>() {
        public DecryptedMqttMessage createFromParcel(Parcel in) {
            return new DecryptedMqttMessage(in);
        }

        public DecryptedMqttMessage[] newArray(int size) {
            return new DecryptedMqttMessage[size];
        }
    };
}
