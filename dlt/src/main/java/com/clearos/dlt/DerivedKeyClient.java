package com.clearos.dlt;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.clearos.dlt.fido.FidoAuthenticator;
import com.goterl.lazysodium.LazySodiumAndroid;
import com.goterl.lazysodium.SodiumAndroid;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.interfaces.Hash;
import com.goterl.lazysodium.interfaces.KeyDerivation;
import com.goterl.lazysodium.interfaces.Sign;
import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;

import org.bitcoinj.core.Base58;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import duo.labs.webauthn.exceptions.VirgilException;

public class DerivedKeyClient {
    public static final String MQTT_MESSAGE_EXTRA = "mqttMessageExtra";
    public static final String CLEAR_LIFE_NOTIFICATION_ACTION = "com.clearos.clearlife.NOTIFICATION";
    public static final String CLEARLIFE_PACKAGE_NAME = "com.clearos.clearlife";
    public static final String DIGITAL_LIFE_NOTIFICATION_ACTION = "com.clearos.digitallife.NOTIFICATION";
    public static final String DIGITALLIFE_PACKAGE_NAME = "com.clearos.digitallife";

    private ServiceConnection cryptoKeysConnection;
    private ICryptoKeysService cryptoKeysService;
    private final Context appContext;
    private DidKeys appKeys = null;
    private final IRemoteErrorHandler handler;
    private final String packageName;

    private final static LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    private final static KeyDerivation.Lazy keyDFLazy = lazySodium;
    private final static Hash.Native hashNative = lazySodium;
    private final static Sign.Lazy signLazy = lazySodium;
    private static String TAG = "DKC";
    private final Map<String, KeyPair> derivedKeysCache;

    //Map from package name to map of context to callback.
    private final Map<String, Map<String, Function<Boolean, Void>>> registrationCallbacks = new ConcurrentHashMap<>();
    private final Object syncRegistrationCallbacks = new Object();
    private Runnable fallbackCallback = null;

    private List<String> subscribedTopics;
    private final Object syncTopicHandler = new Object();
    private IMqttTopicHandler topicHandler;

    private FidoAuthenticator authenticator;
    private final boolean isDemo;
    private boolean toastKeysConnected = false;

    public DerivedKeyClient(Context appContext, IRemoteErrorHandler handler) {
        this(appContext, handler, appContext.getPackageName(), false);
    }

    public DerivedKeyClient(Context appContext, IRemoteErrorHandler handler, String packageName, boolean isDemo) {
        this.appContext = appContext;
        this.handler = handler;
        this.packageName = packageName;
        derivedKeysCache = new HashMap<>();
        this.isDemo = isDemo;
        TAG = "KeyClient=>" + packageName;
    }

    public DerivedKeyClient(Context appContext) {
        this(appContext, new LoggingRemoteErrorHandler(), appContext.getPackageName(), false);
    }

    public DerivedKeyClient(Context appContext, String packageName) {
        this(appContext, new LoggingRemoteErrorHandler(), packageName, false);
    }

    public void setToastKeysConnected(boolean toastKeysConnected) {
        if (appContext.getPackageName().equals(CLEARLIFE_PACKAGE_NAME) || appContext.getPackageName().equals(DIGITALLIFE_PACKAGE_NAME)) {
            Log.d(TAG, String.format("Setting keys connected toast show to %b", toastKeysConnected));
            this.toastKeysConnected = toastKeysConnected;
        } else
            Log.w(TAG, "Cannot disable keys connected message; not ClearLIFE application");
    }

    public void setTopicHandler(IMqttTopicHandler topicHandler) {
        synchronized (syncTopicHandler) {
            Log.d(TAG, String.format("Setting topic handler to %s from synchronized context.", topicHandler));
            this.topicHandler = topicHandler;
        }
    }

    public void setFallbackCallback(Runnable fallbackCallback) {
        this.fallbackCallback = fallbackCallback;
    }

    public IMqttTopicHandler getTopicHandler() {
        synchronized (syncTopicHandler) {
            Log.d(TAG, String.format("Returning %s as topic handler from synchronized context.", topicHandler));
            return topicHandler;
        }
    }

    /**
     * Checks whether this key client is connected to a service for executing requests.
     * @return True if the service is ready.
     */
    public boolean isReady() {
        return cryptoKeysService != null;
    }

    public List<String> getSubscribedTopics() {
        return subscribedTopics;
    }

    private void addRegistrationCallback(String packageName, String context, Function<Boolean, Void> callback) {
        synchronized (syncRegistrationCallbacks) {
            if (!registrationCallbacks.containsKey(packageName)) {
                Log.d(TAG, String.format("Creating new registration callback map for package %s.", packageName));
                    registrationCallbacks.put(packageName, new ConcurrentHashMap<>());
            }

            Map<String, Function<Boolean, Void>> callbacks = registrationCallbacks.getOrDefault(packageName, null);
            if (callbacks != null) {
                Log.d(TAG, String.format("Adding registration callback for %s and context %s in key client.", packageName, context));
                synchronized (syncRegistrationCallbacks) {
                    callbacks.put(context, callback);
                }
            }
        }
    }

    private void removeRegistrationCallback(String packageName, String context) {
        synchronized (syncRegistrationCallbacks) {
            Map<String, Function<Boolean, Void>> callbacks = registrationCallbacks.getOrDefault(packageName, null);
            if (callbacks != null) {
                Log.d(TAG, String.format("Removing registration callback for %s and context %s in key client.", packageName, context));

                    callbacks.remove(context);
            }
        }
    }

    /**
     * Registers a webhook URL for sending updated push notification authorization signatures.
     */
    public void registerPushNotificationWebHook(String url) {

    }

    /**
     * Shows the UI for FIDO-based authentication if appropriate.
     * @param message The decrypted push notification to authenticate for.
     * @param callback Function to call with the result of authentication. If it fails for whatever
     *                 reason, then the argument with be `false`.
     */
    public void authenticateMessage(DecryptedMqttMessage message, Function<Boolean, Void> callback) {
        if (message.isFidoNotification()) {
            Log.d(TAG, "Processing fido authentication using authenticator at " + authenticator);
            authenticator.authenticate(message, authed -> {
                if (authed != null) {
                    pushMessageOnward(authed.getPackageName(), authed);
                    if (callback != null)
                        callback.apply(true);
                } else {
                    if (callback != null)
                        callback.apply(authed.isFidoComplete());
                }
                    return null;
            });
        } else
            pushMessageOnward(appContext.getPackageName(), message);
    }

    /**
     * Sends a decrypted notification message to ClearLIFE via intent.
     */
    private void sendMessageViaIntent(DecryptedMqttMessage message) {
        Intent fido = new Intent();
        if (Utils.isClearDevice()) {
            fido.setPackage(ClearPackages.life.getVal());
            fido.setAction(CLEAR_LIFE_NOTIFICATION_ACTION);
        } else {
            fido.setPackage(ClearPackages.digitallife.getVal());
            fido.setAction(DIGITAL_LIFE_NOTIFICATION_ACTION);
        }
        Log.d(TAG, String.format("MQTT message: Adding %s as message for notification intent.", message));
        Log.d(TAG, "MQTT message: Payload " + message.getDecryptedPayload());
        fido.putExtra(MQTT_MESSAGE_EXTRA, message);
        fido.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Log.i(TAG, "MQTT message: This is a FIDO/ClearLIFE notification; sending intent to ClearLIFE to do UI auth: " + fido);
        appContext.startActivity(fido);
    }

    /**
     * Notifies relevant packages that have registered callbacks of a push notification that was
     * received. This method can only be called by ClearLIFE.
     * @param packageName Name of the package that the notification was for.
     * @param message Message from the push notification (decrypted).
     */
    public void onNotificationReceived(String packageName, DecryptedMqttMessage message) {
        if (!assertClearLife("onNotificationReceived") && !assertDigitalLife("onNotificationReceived"))
            return;

        if (message.isApprovalNotification() || (message.isFidoNotification() && !message.isFidoComplete())) {
            // Send an intent to the main UI ClearLIFE application to do the FIDO auth.
            sendMessageViaIntent(message);
        } else
            pushMessageOnward(packageName, message);
    }

    private void pushMessageOnward(String packageName, DecryptedMqttMessage message) {
        if (packageName != null && packageName.equals(appContext.getPackageName())) {
            Log.d(TAG, "Push notification received for ClearLIFE directly; pushing to tray.");
            pushNotificationOnNotificationTray(message);
        } else {
            try {
                Log.i(TAG, "Forwarding push notification to package " + packageName);
                cryptoKeysService.onNotificationReceived(packageName, message);
            } catch (RemoteException e) {
                handler.onRemoteError(e);
            }
        }
    }

    private Intent getNotificationPendingIntent(DecryptedMqttMessage message) {
        Intent fido = new Intent();
        if (Utils.isClearDevice()) {
            fido.setPackage(ClearPackages.life.getVal());
            fido.setAction(CLEAR_LIFE_NOTIFICATION_ACTION);
        } else {
            fido.setPackage(ClearPackages.digitallife.getVal());
            fido.setAction(DIGITAL_LIFE_NOTIFICATION_ACTION);
        }
        Log.d(TAG, String.format("Adding %s as message for notification intent.", message));
        fido.putExtra(MQTT_MESSAGE_EXTRA, message);
        fido.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return fido;
    }

    /**
     * Shows a push notification that was received for ClearLIFE. Notifications for other packages
     * are sent to the respective apps (assuming they have registered a callback).
     */
    public void pushNotificationOnNotificationTray(DecryptedMqttMessage message) {
        NotificationManager mNotificationManager =
                (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            Intent intent = getNotificationPendingIntent(message);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(
                    appContext,
                    0,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT
            );
            Notification n = new Notification.Builder(appContext, appContext.getString(R.string.MqttChannelId))
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(appContext, R.color.colorPrimaryDark))
                    .setContentTitle(message.getTitle())
                    .setContentIntent(resultPendingIntent)
                    .setContentText(message.getMessage())
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.ic_clearlife_app_logo)
                    .build();
            int id = message.getId();

            mNotificationManager.notify(id, n);
        } else
            Log.w(TAG, "No notification manager was accessible to send the notification.");
    }

    /**
     * Notifies relevant packages that have registered callbacks of a push notification error.
     * This method can only be called by ClearLIFE.
     * @param packageName Name of the package that the notification was for.
     * @param message Error message from the failed push notification.
     */
    public void onNotificationError(String packageName, String message) throws RemoteException {
        if (!assertClearLife("onNotificationReceived") && !assertDigitalLife("onNotificationReceived"))
            return;

        try {
            cryptoKeysService.onNotificationError(packageName, message);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
        }
    }

    /**
     * Notifies relevant packages that have registered callbacks of a push notification error.
     * This method can only be called by ClearLIFE.
     * @param packageName Name of the package that the notification was for.
     * @param topics List of currently subscribed topics.
     */
    public void onTopicsChanged(String packageName, List<String> topics) throws RemoteException {
        if (!assertClearLife("onNotificationReceived") && !assertDigitalLife("onNotificationReceived"))
            return;

        try {
            cryptoKeysService.onTopicsChanged(packageName, topics);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
        }
    }

    /**
     * Hashes a string message (via UTF8) into SHA256 hash bytes.
     * @param message To hash with SHA256.
     * @throws SodiumException If the hashing fails.
     */
    public byte[] hash(byte[] message) throws SodiumException {
        Log.d(TAG, "SHA256 hash producing bytes from bytes.");
        byte[] hashedBytes = new byte[Hash.SHA256_BYTES];
        if (!hashNative.cryptoHashSha256(hashedBytes, message, message.length)) {
            throw new SodiumException("Unsuccessful sha-256 hash.");
        }
        return hashedBytes;
    }

    /**
     * Hashes a string message (via UTF8) into SHA256 hash bytes.
     * @param message To turn into bytes via UTF-8 encoding.
     * @throws SodiumException If the hashing fails.
     */
    public byte[] hash(String message) throws SodiumException {
        return hash(message.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Derives a key pair from a seed directly using `libsodium` instead of sending an AIDL to the
     * crypto keys service.
     * @param seed Seed to derive from.
     * @throws RemoteException If there is a SodiumException.
     */
    public KeyPair directDeriveFromSeed(Key seed) throws RemoteException {
        try {
            return signLazy.cryptoSignSeedKeypair(seed.getAsBytes());
        } catch (SodiumException e) {
            throw new RemoteException(e.getMessage());
        }
    }

    /**
     * Returns the Base58-encoded SHA256 hash of a message.
     * @param message To turn into bytes via UTF-8 encoding.
     * @throws SodiumException If the hashing fails.
     */
    public String hashInBase58(String message) throws SodiumException {
        return Base58.encode(hash(message));
    }

    public IRemoteErrorHandler getHandler() {
        return handler;
    }

    public Context getAppContext() {
        return appContext;
    }

    /**
     * Returns the DID for the application base keys of this derived key client.
     */
    public String getDid() {
        initializeAppKeys(appContext.getPackageName());
        if (appKeys != null) {
            return appKeys.getDid();
        } else {
            return null;
        }
    }
    /**
     * Derives a new Key from the existing master key.
     * @param keyId Index of the key in the hierarchical list.
     * @param context 8-byte string indicating the context in which `keyId` should be interpreted.
     * @return Derived Key using the master *secret* key and a HKDF.
     * @throws SodiumException if any of the lengths were not correct.
     */
    public Key deriveKey(KeyPair baseKeyPair, long keyId, String context) throws SodiumException {
        Log.d(TAG, String.format("Deriving new key from %s via libsodium directly.", context));
        byte[] masterSeed = new byte[KeyDerivation.MASTER_KEY_BYTES];
        byte[] secretKey = baseKeyPair.getSecretKey().getAsBytes().clone();
        System.arraycopy(secretKey,0, masterSeed,0, KeyDerivation.MASTER_KEY_BYTES);
        Key result = keyDFLazy.cryptoKdfDeriveFromKey(32, keyId, context, Key.fromBytes(masterSeed));

        Arrays.fill(masterSeed, (byte)0);
        Arrays.fill(secretKey, (byte)0);
        return result;
    }

    public KeyPair deriveKeyHierarchy(String context, String chain) {
        Log.d(TAG, String.format("Calling deriveKeyHierarchy via client for %s:%s", context, chain));
        try {
            return cryptoKeysService.deriveKeyHierarchy(context, chain).getKeys();
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    public KeyPair deriveKeyPair(String context, long keyId) {
        @SuppressLint("DefaultLocale") String key = String.format("%s:%d", context, keyId);
        Log.d(TAG, String.format("Calling deriveKeyPair via client for %s", key));
        if (derivedKeysCache.containsKey(key)) return derivedKeysCache.get(key);

        try {
            KeyPair result = cryptoKeysService.deriveKeyPair(appContext.getPackageName(), context, keyId).getKeys();
            derivedKeysCache.put(key, result);
            return result;
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    public KeyPair deriveKeyPair(String packageName, String context, long keyId) {
        if (!packageName.equals(appContext.getPackageName()) && !assertClearLife("deriveKeyPair") && !assertDigitalLife("deriveKeyPair"))
            handler.onGenericError(new Exception("Only ClearLIFE can derive arbitrary package key pairs."));

        @SuppressLint("DefaultLocale") String key = String.format("%s:%d", context, keyId);
        Log.d(TAG, String.format("Calling deriveKeyPair via client for %s and key %s", packageName, key));
        if (derivedKeysCache.containsKey(key)) return derivedKeysCache.get(key);

        try {
            KeyPair result = cryptoKeysService.deriveKeyPair(packageName, context, keyId).getKeys();
            derivedKeysCache.put(key, result);
            return result;
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    public Key deriveSeed(KeyPair baseKeys, String context, long keyId) {
        Log.d(TAG, String.format("Calling deriveSeed via client for %s:%d", context, keyId));
        CryptoKeyPair base = new CryptoKeyPair(baseKeys);
        try {
            return cryptoKeysService.deriveSeed(base, context, keyId).getSeed();
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    private void initializeAppKeys(String packageName) {
        if (appKeys == null) {
            // The null string context uses the calling package information on the server side to
            // derive the keys. As long as the package name doesn't change, the keys will be the
            // same over time.
            Log.d(TAG, "Initializing application keys from null string context.");
            appKeys = getAppKeys(packageName, "", 0);
        }
    }

    public Key deriveSeed(String context, long keyId) {
        Log.d(TAG, String.format("Calling deriveSeed via client for %s:%d", context, keyId));
        initializeAppKeys(appContext.getPackageName());
        CryptoKeyPair base = new CryptoKeyPair(appKeys.getKeys());
        try {
            return cryptoKeysService.deriveSeed(base, context, keyId).getSeed();
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Get's the base application keys (keyId zero; package-based context).
     */
    public DidKeys getAppKeys() {
        initializeAppKeys(appContext.getPackageName());
        return appKeys;
    }

    /**
     * Get's the base application keys (keyId zero; package-based context) for an arbitrary application.
     */
    public DidKeys getAppKeys(String packageName) {
        initializeAppKeys(packageName);
        return appKeys;
    }

    /**
     * Derives a set of keys using the base application keys and the given context/keyId.
     * @param context string to use in deriving the context; can be any length.
     * @param keyId Integer order of the keyId to derive.
     */
    public DidKeys getAppKeys(String context, long keyId) {
        Log.d(TAG, String.format("Calling getAppKeys via client for %s:%d", context, keyId));
        try {
            return cryptoKeysService.getAppKeys(appContext.getPackageName(), context, keyId);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        } catch (NullPointerException e) {
            handler.onGenericError(e);
            return null;
        }
    }

    /**
     * Derives a set of keys using the base application keys and the given context/keyId.
     * @param packageName The name of the package to get application keys for. Only ClearLIFE can
     *                    call this method.
     * @param context string to use in deriving the context; can be any length.
     * @param keyId Integer order of the keyId to derive.
     */
    public DidKeys getAppKeys(String packageName, String context, long keyId) {
        if (!packageName.equals(appContext.getPackageName()) && !assertClearLife("getAppKeys") && !assertDigitalLife("getAppKeys"))
            handler.onGenericError(new Exception("Only ClearLIFE can derive arbitrary package names."));

        Log.d(TAG, String.format("Calling getAppKeys for %s via client for `%s`:%d", packageName, context, keyId));
        try {
            DidKeys result = cryptoKeysService.getAppKeys(packageName, context, keyId);
            Log.d(TAG, String.format("Result from deriving app keys on client side is DID=%s VK=%s",
                    result.getDid(), result.getVerKey()));
            return result;
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Returns the base URL of the home server configured for this user/DID.
     */
    public String getHomeServer() {
        Log.d(TAG, "Calling getHomeServer via client.");
        try {
            return cryptoKeysService.getHomeServer(packageName);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Returns the base URL of the Clear server configured for billing this user/DID with external
     * services.
     */
    public String getClearServer() {
        Log.d(TAG, "Calling getClearServer via client.");
        try {
            return cryptoKeysService.getClearServer(packageName);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Retrieve the framework services identifier from ClearLIFE. Note that only ClearID is allowed
     * to access this method.
     * @return Null if it is unavailable.
     */
    public String getFrameworkServicesId() {
        assertClearId("getFrameworkServicesId");
        try {
            return cryptoKeysService.getFrameworkServicesId();
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Encrypts the user's vault key so that only the specified VerKey can decrypt it. Note that only
     * ClearID can use this method.
     * @param verKey Public key to anonCrypt for.
     */
    public String encryptVaultKey(String verKey) {
        assertClearId("encryptVaultKey");
        try {
            return cryptoKeysService.encryptVaultKey(verKey);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Sets the base URL of the home server configured for this user/DID.
     * @param homeServer new *base* URL to use for the home server (just for this package). E.g. myserver.home.com
     */
    public void setHomeServer(String homeServer) {
        Log.d(TAG, "Calling setHomeServer via client with " + homeServer);
        try {
            cryptoKeysService.setHomeServer(packageName, homeServer);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
        }
    }

    private boolean assertClearPackages(ClearPackages[] packages, String method) {
        boolean failed = false;
        String[] names = new String[packages.length];
        int i=0;
        for (ClearPackages p: packages) {
            names[i++] = p.getVal();
            if (!getAppContext().getPackageName().equals(p.getVal())) {
                failed = true;
            } else {
                failed = false;
                break;
            }
        }

        if (failed) {
            String possible = String.join(", ", names);
            handler.onRemoteError( new RemoteException(String.format("Only %s can use the %s methods. Requester: %s", possible, method, getAppContext().getPackageName())));
            return false;
        }
        return true;
    }

    private boolean assertClearGm(String method) {
        return assertClearPackages(new ClearPackages[]{ ClearPackages.gm }, method);
    }

    private boolean assertClearId(String method) {
        return assertClearPackages(new ClearPackages[]{ ClearPackages.id }, method);
    }

    private boolean assertClearLife(String method) {
        return assertClearPackages(new ClearPackages[]{ ClearPackages.life }, method);
    }

    private boolean assertDigitalLife(String method) {
        return assertClearPackages(new ClearPackages[]{ ClearPackages.digitallife }, method);
    }

    /**
     * Gets the DID of the OYP seed from the crypto service.
     * @return null if there is an error, or the seed doesn't exist.
     */
    public String getOypDid() {
        if (!assertClearPackages(new ClearPackages[]{ ClearPackages.gm, ClearPackages.life, ClearPackages.pay, ClearPackages.wallet }, "OYP")) return null;
        Log.d(TAG, "Calling getOypDid via client.");
        try {
            return cryptoKeysService.getOypDid();
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Signs a message using the OYP private key.
     * @param message Message to sign.
     */
    public String oypSignMessage(String message) {
        if (!assertClearPackages(new ClearPackages[]{ ClearPackages.gm, ClearPackages.life, ClearPackages.pay, ClearPackages.verify, ClearPackages.wallet }, "OYP")) return null;
        Log.d(TAG, "Calling oypSignMessage via client.");
        try {
            return cryptoKeysService.oypSign(message);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /** Returns the user name aliased with the decentralized identity on the phone.
     */
    public String getUserName() {
        Log.d(TAG, "Calling getUserName via client.");
        try {
            return cryptoKeysService.getUserName();
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /** Returns the display name aliased with the decentralized identity on the phone.
     */
    public String getDisplayName() {
        Log.d(TAG, "Calling getDisplay via client.");
        try {
            return cryptoKeysService.getDisplayName();
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    public String vaultCrypt(String message) {
        Log.d(TAG, "Calling vaultCrypt via client.");
        try {
            return cryptoKeysService.vaultCrypt(message);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    public String vaultDecrypt(String message) {
        Log.d(TAG, "Calling vaultDecrypt via client.");
        try {
            return cryptoKeysService.vaultDecrypt(message);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    public String getPluginPassword(String verKey) {
        Log.d(TAG, "Calling getPluginPassword via client.");
        try {
            return cryptoKeysService.getPluginPassword(verKey);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Checks on the registration status of the appKeys for this app.
     */
    public String appKeyRegistrationStatus() {
        Log.d(TAG, "Calling appKeyRegistrationStatus via client.");
        try {
            return cryptoKeysService.appKeyRegistrationStatus(appContext.getPackageName());
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Checks on the registration status of the appKeys for this app.
     */
    public String appKeyRegistrationStatus(String packageName) {
        if (!packageName.equals(appContext.getPackageName()) && !assertClearLife("appKeyRegistration") && !assertDigitalLife("appKeyRegistration"))
            handler.onGenericError(new Exception("Only ClearLIFE can check arbitrary package registrations."));

        Log.d(TAG, "Calling appKeyRegistrationStatus via client.");
        try {
            return cryptoKeysService.appKeyRegistrationStatus(packageName);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    /**
     * Registers app base encryption keys (or rotated keys) with the home server.
     * @return True if the call was initiated successfully. Run `appKeyRegistrationStatus` to check
     * on the actual status of the call.
     */
    public boolean registerAppKeys(String packageName, Function<Boolean, Void> callback) {
        addRegistrationCallback(packageName, "", callback);
        Log.d(TAG, "Calling registerAppKeys via client for null string context.");
        try {
            return cryptoKeysService.registerAppKeys(getRegistrationPackageName(), "");
        } catch (RemoteException e) {
            Log.e(TAG, "Remote error registering application keys. Null callback.");
            handler.onRemoteError(e);
            removeRegistrationCallback(packageName, "");
            callback.apply(false);
            return false;
        }
    }

    /**
     * Registers app base encryption keys (or rotated keys) with the home server.
     * @return True if the call was initiated successfully. Run `appKeyRegistrationStatus` to check
     * on the actual status of the call.
     */
    public boolean registerAppKeys(Function<Boolean, Void> callback) {
        return registerAppKeys(appContext.getPackageName(), callback);
    }

    /**
     * Registers app base encryption keys (or rotated keys) with the home server.
     * @param context Context used to derive sub-keys within that package.
     * @return True if the call was initiated successfully. Run `appKeyRegistrationStatus` to check
     * on the actual status of the call.
     */
    public boolean registerAppKeysWithContext(String packageName, String context, Function<Boolean, Void> callback) {
        Log.d(TAG, "Calling registerAppKeys via client for context "+ context);
        addRegistrationCallback(packageName, context, callback);
        try {
            return cryptoKeysService.registerAppKeys(packageName, context);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            removeRegistrationCallback(packageName, context);
            return false;
        }
    }

    /**
     * Registers app base encryption keys (or rotated keys) with the home server.
     * @param context Context used to derive sub-keys within that package.
     * @return True if the call was initiated successfully. Run `appKeyRegistrationStatus` to check
     * on the actual status of the call.
     */
    public boolean registerAppKeysWithContext(String context, Function<Boolean, Void> callback) {
        return registerAppKeysWithContext(appContext.getPackageName(), context, callback);
    }


    /**
     * Rotates app base encryption keys (or rotated keys) with the home server.
     * @return True if the call was initiated successfully.
     */
    public Long rotateAppKeys() throws RemoteException {
        Log.d(TAG, "Calling rotateAppKeys via client.");
        try {
            return cryptoKeysService.rotateAppKeys(packageName);
        } catch (RemoteException e) {
            handler.onRemoteError(e);
            return null;
        }
    }

    private final ICryptoRemoteCallback cryptoRemoteCallback = new ICryptoRemoteCallback.Stub() {
        @Override
        public void onAppKeyRegistration(String packageName, String context, boolean success) {
            Log.d(TAG, String.format("AppKeyRegistration callback in %s fired for `%s` with success=%b", packageName, context, success));

            synchronized (syncRegistrationCallbacks) {
                Map<String, Function<Boolean, Void>> callbacks = registrationCallbacks.getOrDefault(packageName, null);
                if (callbacks != null) {
                    Function<Boolean, Void> callback = callbacks.getOrDefault(context, null);
                    if (callback != null) {
                        Log.d(TAG, String.format("App-key registration handler firing callback for context %s in package %s", context, packageName));
                        callback.apply(success);
                    } else {
                        Log.d(TAG, String.format("No callback found for package %s and context `%s` in app-key registration handler.", packageName, context));
                        if (fallbackCallback != null)
                            fallbackCallback.run();
                    }

                    callbacks.remove(context);
                } else
                    Log.d(TAG, String.format("Callbacks map for package %s is not available.", packageName));
            }
        }
    };

    private final IPushNotificationCallback mCallback = new IPushNotificationCallback.Stub() {
        @Override
        public void onTopicsChanged(List<String> topics) {
            subscribedTopics = topics;
        }

        @Override
        public void onNotificationError(String message) {
            handler.onRemoteError(new RemoteException("Error in notification system: " + message));
        }

        @Override
        public void onMessageReceived(DecryptedMqttMessage message) {
            Log.d(TAG, "Received new message from crypto keys service: " + message);
            handler.onNotificationReceived(message);
        }

        @Override
        public void onSubscribeTopic(String appDid, String topic, String packageName) {
            Log.d(TAG, String.format("onSubscribeTopic fired for %s and %s; topic is %s", packageName, appDid, topic));
            IMqttTopicHandler topicHandler = getTopicHandler();
            if (topicHandler != null)
                topicHandler.onSubscribeTopic(new MqttTopic(appDid, topic, packageName));
            else
                Log.d(TAG, "Topic handler is null; cannot handle onSubscribeTopic");
        }

        @Override
        public void onDefaultTopics(String packageName, String appDid)  {
            Log.d(TAG, String.format("onDefaultTopics fired for %s and %s", packageName, appDid));
            IMqttTopicHandler topicHandler = getTopicHandler();
            if (topicHandler != null)
                topicHandler.onDefaultTopics(new MqttDefaultTopicEvent(packageName, appDid));
            else
                Log.d(TAG, "Topic handler is null; cannot handle onDefaultTopics");
        }

        @Override
        public void onUnsubscribeTopic(String packageName, String topic) {
            Log.d(TAG, String.format("onUnsubscribeTopic fired for %s and topic %s", packageName, topic));
            IMqttTopicHandler topicHandler = getTopicHandler();
            if (topicHandler != null)
                topicHandler.onUnsubscribeTopic(new MqttUnsubscribeEvent(packageName, topic));
            else
                Log.d(TAG, "Topic handler is null; cannot handle onUnsubscribeTopic");
        }

        @Override
        public void onUnsubscribeDefaultTopics(String packageName) {
            Log.d(TAG, String.format("onUnsubscribeDefaultTopics fired for %s.", packageName));
            IMqttTopicHandler topicHandler = getTopicHandler();
            if (topicHandler != null)
                topicHandler.onUnsubscribeDefaultTopics(new MqttUnsubscribeEvent(packageName));
            else
                Log.d(TAG, "Topic handler is null; cannot handle onUnsubscribeDefaultTopics");
        }
    };

    /**
     * Binds the crypto keys service to the application.
     */
    public ICryptoKeysService create(Context appContext) {
        return create(appContext, 2000, null);
    }

    private void registerNotificationMachinery() {
        try {
            cryptoKeysService.registerNotificationCallback(getRegistrationPackageName(), getDid(), mCallback);
            Log.d(TAG, "Notification callback registered successfully.");
        } catch (RemoteException e) {
            Log.e(TAG, "Error registering for notification callback.", e);
            handler.onRemoteError(e);
        } catch (NullPointerException e) {
            Log.e(TAG, "Error registering for notification callback.", e);
            handler.onGenericError(e);
        }
    }

    public void enableFidoInterface(FragmentActivity context, DidKeys signingKeys, boolean demoAPi) {
        try {
            Log.i(TAG, String.format("Creating FIDO interface for API server; demo = %b", isDemo || demoAPi));
            authenticator = new FidoAuthenticator(context, signingKeys, getHomeServer(),
                    true, false, isDemo || demoAPi);
        } catch (VirgilException e) {
            Log.e(TAG, "Error creating new biometric FIDO authenticator.", e);
        }
    }

    private String getRegistrationPackageName() {
        String registrationName = packageName;
        if (!packageName.equals(appContext.getPackageName())) {
            registrationName = String.format("%s:%s", appContext.getPackageName(), packageName);
            Log.i(TAG, String.format("Using registration package name of %s based on context name %s.", registrationName, appContext.getPackageName()));
        }

        return registrationName;
    }

    /**
     * Binds the crypto keys service.
     * @param timeout Number of milliseconds to wait for service to come up before failing.
     * @param callback Function to call once the service is connected.
     */
    public ICryptoKeysService create(Context appContext, long timeout, Function<DerivedKeyClient, Void> callback) {
        Log.d(TAG, "Creating ICryptoKeysService connection from client.");

        cryptoKeysConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(TAG, "onServiceConnected; wiring up service interface.");
                cryptoKeysService = ICryptoKeysService.Stub.asInterface(service);
                Toast.makeText(appContext, "Derived Keys Connected", Toast.LENGTH_SHORT).show();
                Log.d("ICryptoKeysClient", "Binding - Service connected");

                try {
                    cryptoKeysService.registerCryptoCallback(getRegistrationPackageName(), cryptoRemoteCallback);
                    Log.d(TAG, "Crypto callbacks registered successfully.");
                }catch (RemoteException e) {
                    Log.e(TAG, "Error registering for crypto callbacks.");
                    handler.onRemoteError(e);
                    callback.apply(null);
                    return;
                }

                if (callback != null) {
                    callback.apply(DerivedKeyClient.this);
                }

                // Give the service time to bind properly before executing this notification
                // registration. It depends on the result of a crypto service key call.
                new Handler(appContext.getMainLooper()).postDelayed(() -> registerNotificationMachinery(), 5000);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                cryptoKeysService = null;
                // User doesn't need to know about service disconnects...
                // Toast.makeText(appContext, "Derived Keys Disconnected", Toast.LENGTH_SHORT).show();
                Log.d("ICryptoKeysClient", "Binding - Service disconnected");
            }

            @Override
            public void onBindingDied(ComponentName name) {
                Log.d(TAG, "Binding died for " + name.flattenToString());
            }

            @Override
            public void onNullBinding(ComponentName name) {
                Log.d(TAG, "Null binding for " + name.flattenToString());
            }
        };

        if (cryptoKeysService == null) {
            Intent it = new Intent("CryptoService");
            if (Utils.isClearDevice())
                it.setPackage("com.clearos.clearlife");
            else
                it.setPackage("com.clearos.digitallife");
            it.setAction("ClearID.CryptoService");
            Log.d(TAG, "Binding crypto service from client.");
            if (!appContext.bindService(it, cryptoKeysConnection, Service.BIND_AUTO_CREATE)) {
                Log.e(TAG, "Bind service call was not successful.");
                callback.apply(null);
            } else {
                // Give the service 1.5 seconds to connect before continuing without binding.
                new Handler(appContext.getMainLooper()).postDelayed(() -> {
                    if (cryptoKeysService == null) {
                        callback.apply(null);
                    }
                }, timeout);
            }
        }
        return cryptoKeysService;
    }

    /**
     * Unbinds the crypto derived keys service from this app.
     */
    public void destroy() {
        if (cryptoKeysService != null) {
            // Unregister this client for push notifications.
            try {
                cryptoKeysService.unregisterNotificationCallback(getRegistrationPackageName());
                cryptoKeysService.unregisterCryptoCallback(getRegistrationPackageName());
            } catch (RemoteException e) {
                Log.e(TAG, "Error unregistering from notification/crypto callbacks.");
                handler.onRemoteError(e);
            }
        }

        appContext.unbindService(cryptoKeysConnection);
    }
}