package com.clearos.dlt;

import android.os.Parcel;
import android.os.Parcelable;

import com.goterl.lazysodium.utils.Key;

public class CryptoSeed implements Parcelable {
    private Key seed;

    public CryptoSeed(Key seed) {
        this.seed = seed;
    }

    /**
     * Reconstructs a DidKeys instance from a parcel.
     * @param in Parcel from the AIDL service.
     */
    public CryptoSeed(Parcel in) {
        // Reconstruct the public and private key pairs.
        int seedLen = in.readInt();
        byte[] s = new byte[seedLen];
        in.readByteArray(s);
        seed = Key.fromBytes(s);
    }

    public Key getSeed() {
        return seed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        byte[] s = seed.getAsBytes();
        dest.writeInt(s.length);
        dest.writeByteArray(s);
    }

    public static final Parcelable.Creator<CryptoSeed> CREATOR
            = new Parcelable.Creator<CryptoSeed>() {
        public CryptoSeed createFromParcel(Parcel in) {
            return new CryptoSeed(in);
        }

        public CryptoSeed[] newArray(int size) {
            return new CryptoSeed[size];
        }
    };
}
