package com.clearos.dlt;

public class NotificationMessageResponse {
    private String id;
    private String status;
    private String description;

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
