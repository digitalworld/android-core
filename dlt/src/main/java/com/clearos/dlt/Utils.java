package com.clearos.dlt;

import android.os.Build;

public class Utils {
    public static boolean isClearDevice() {
        return Build.MODEL.contains("ClearPHONE") || Build.MODEL.contains("FreedomPhone");
    }
}
