package com.clearos.dlt;

import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class KeyClientApplication extends Application implements IRemoteErrorHandler {
    private static KeyClientApplication mInstance;
    private final String TAG = "KeyClientApplication";

    private static final Map<String, DerivedKeyClient> packageKeyClients = new ConcurrentHashMap<>();
    private final Object syncSetup = new Object();
    private Map<String, List<Function<KeyClientApplication, Void>>> registrationCallbacks;
    private static final Map<String, Function<DecryptedMqttMessage, Void>> packageNotificationCallbacks = new ConcurrentHashMap<>();
    private static final Map<String, Function<DidKeys, Void>> packageKeyCallbacks = new ConcurrentHashMap<>();
    private static final Map<String, Boolean> packageKeyClientConnected = new ConcurrentHashMap<>();

    @Override
    public void onGenericError(Exception e) {
        Log.e(TAG, "Generic error from derived key client children.", e);
    }
    @Override
    public void onRemoteError(RemoteException e) {
        Log.e(TAG, "Remote error in derived key client from document provider.", e);
    }
    @Override
    public void onIOError(IOException e) {
        Log.e(TAG, "I/O error in decentralized storage from document provider.", e);
    }
    @Override
    public void onNotificationReceived(DecryptedMqttMessage message) {
        Function<DecryptedMqttMessage, Void> notificationCallback = packageNotificationCallbacks.getOrDefault(message.getPackageName(), null);
        if (notificationCallback != null) {
            notificationCallback.apply(message);
        }
        Log.i(TAG, "Notification message received: " + message);
    }

    /**
     * Removes all registration callbacks from the application instance.
     */
    public void clearRegistrationCallbacks(String packageName) {
        registrationCallbacks.remove(packageName);
    }

    /**
     * Adds a function to call once the app has successfully registered.
     */
    public void addRegistrationCallback(String packageName, Function<KeyClientApplication, Void> callback) {
        if (!registrationCallbacks.containsKey(packageName))
            registrationCallbacks.put(packageName, new ArrayList<>());
        List<Function<KeyClientApplication, Void>> callbacks = registrationCallbacks.get(packageName);
        if (callbacks != null)
            callbacks.add(callback);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registrationCallbacks = new ConcurrentHashMap<>();
        mInstance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        for (Map.Entry<String,DerivedKeyClient> entry: packageKeyClients.entrySet()) {
            DerivedKeyClient keyClient = entry.getValue();
            if (keyClient != null) {
                keyClient.destroy();
            }
        }
    }

    public void threadSafeToast(final String message) {
        new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show());
    }

    public DidKeys getAppKeys(String packageName) {
        DerivedKeyClient packageClient = packageKeyClients.getOrDefault(packageName, null);
        if (packageClient != null)
            return packageClient.getAppKeys();
        else
            return null;
    }

    public DidKeys getAppKeys() {
        return getAppKeys(getPackageName());
    }

    /**
     * Checks whether the package with specified name is installed.
     * @param packageName FQN of the package to check.
     * @return True if the package is installed.
     */
    public boolean isPackageInstalled(String packageName) {
        try {
            getApplicationContext().getPackageManager().getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Check is ClearLIFE is installed.
     * @return True if ClearLIFE is installed; otherwise false.
     */
    public boolean isClearLifeInstalled() {
        if (Utils.isClearDevice())
            return isPackageInstalled(getString(R.string.clearLifePackageName));
        else
            return isPackageInstalled(getString(R.string.digitalLifePackageName));
    }

    /**
     * Handles the callback from the key client after registration has happened.
     * @param success When true, the registration worked.
     */
    private void registrationCallbackHandler(String packageName, boolean success) {
        if (success) {
            Log.d(TAG, "Registration successful for DID " + getAppKeys(packageName).getDid());

            // Run all the additional callbacks specified by sub-classes.
            new Handler(getMainLooper()).post(() -> {
                List<Function<KeyClientApplication, Void>> callbacks = registrationCallbacks.getOrDefault(packageName, null);
                if (callbacks != null) {
                    for (Function<KeyClientApplication, Void> cb: callbacks) {
                        cb.apply(KeyClientApplication.this);
                    }
                }

                Function<DidKeys, Void> keysCallback = packageKeyCallbacks.getOrDefault(packageName, null);
                if (keysCallback != null) {
                    Log.d(TAG, "Running callback from registration with appKeys for " + packageName);
                    keysCallback.apply(getAppKeys(packageName));
                }
            });
        } else {
            Log.w(TAG, "App key registration failed.");
            Function<DidKeys, Void> keysCallback = packageKeyCallbacks.getOrDefault(packageName, null);
            if (keysCallback != null) {
                // Even though registration failed, still let the app run; for example: no internet?
                keysCallback.apply(getAppKeys(packageName));
            }
        }
    }

    public void setupKeys(Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback, boolean isDemo) {
        setupKeys(getPackageName(), callback, notificationCallback, isDemo);
    }

    public void setupKeys(Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback) {
        setupKeys(getPackageName(), callback, notificationCallback, false);
    }

    public void setupKeys(String packageName, Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback) {
        setupKeys(packageName, callback, notificationCallback, false);
    }

    public void setupKeys(String packageName, Function<DidKeys, Void> callback, Function<DecryptedMqttMessage, Void> notificationCallback, boolean isDemo) {
        if (!isClearLifeInstalled()) {
            Log.w(TAG, "ClearLIFE is not installed on the phone. No sense in setting up key client.");
            callback.apply(null);
            return;
        }

        synchronized (syncSetup) {
            DerivedKeyClient packageClient = getKeyClient(packageName);
            Boolean packageClientConnected = packageKeyClientConnected.getOrDefault(packageName, Boolean.FALSE);
            // We don't want problems caused by running this from multiple places at the same time.
            if (packageClient != null && packageClientConnected != null && packageClientConnected) {
                callback.apply(packageClient.getAppKeys());
                return;
            }

            // Concurrent hash maps can't handle null values...
            if (callback != null)
                packageKeyCallbacks.put(packageName, callback);
            if (notificationCallback != null)
                packageNotificationCallbacks.put(packageName, notificationCallback);

            packageClient = new DerivedKeyClient(getApplicationContext(), this, packageName, isDemo);
            packageClient.create(getApplicationContext(), 3000, kc -> {
                if (kc == null) {
                    if (Utils.isClearDevice())
                        threadSafeToast(getString(R.string.keysNotConnectedMsg));
                    else
                        threadSafeToast(getString(R.string.keysNotConnectedMsgDigitalLife));
                    Log.e(TAG, "Could not create key client from service");
                    callback.apply(null);
                } else {
                    Log.d(TAG, String.format("Adding %s to concurrent map of package key clients for %s.", kc, packageName));
                    packageKeyClients.put(packageName, kc);
                    try {
                        kc.getAppKeys(packageName);
                    } catch (Exception e) {
                        // This happens if ClearLIFE has not been initialized yet.
                        Log.e(TAG, "No app keys created; probably ClearLIFE is not initialized", e);
                        callback.apply(null);
                        return null;
                    }

                    kc.setFallbackCallback(() -> {
                        // Note that this issue appears to stem from the fact that both ClearLIFE and
                        // ClearGM use derived key clients with the ClearGM package name. The
                        // CryptoKeysService explicitly keeps the ClearLIFE callbacks separate, but
                        // evidently that isn't working. Can't fix right now, too much else to do.
                        // TODO Come back to this and fix it properly.
                        Log.w(TAG, "Using fallback callback to continue setup; some glitch somewhere...");
                        if (callback != null)
                            callback.apply(getAppKeys());
                    });

                    // Also register the app keys now so that third party services will work.
                    Log.d(TAG, "Key application is now registering the app keys for " + packageName);
                    if (!kc.registerAppKeys(packageName, s -> {
                        packageKeyClientConnected.put(packageName, true);
                        registrationCallbackHandler(packageName, s);
                        return null;
                    })) {
                        Log.e(TAG, "Error registering app keys with home server for " + packageName);
                        threadSafeToast(getString(R.string.appRegistrationErrMsg));
                    }
                }
                return null;
            });
        }
    }

    /**
     * Returns one of the extra key clients for an arbitrary package. Note that only ClearLIFE
     * will have non-null custom package derived key clients. Other apps should expect unhandled
     * exceptions.
     * @return Null if it hasn't been initialized.
     */
    public DerivedKeyClient getKeyClient(String packageName) {
        Log.d(TAG, "Returning key client for specified application name " + packageName);
        DerivedKeyClient result = packageKeyClients.getOrDefault(packageName, null);
        Log.d(TAG, "Result of package-specific key-client lookup is " + result);
        return result;
    }

    public DerivedKeyClient getKeyClient() {
        Log.d(TAG, "Returning key client for default application name " + getPackageName());
        return getKeyClient(getPackageName());
    }

    //To create instance of base class
    public static synchronized KeyClientApplication getInstance() {
        return mInstance;
    }

}
