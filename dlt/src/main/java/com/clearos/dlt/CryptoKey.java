package com.clearos.dlt;

import android.os.Build;
import android.content.Context;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;

import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

import com.clearos.dlt.signing.Ed25519Signer;
import com.clearos.dlt.signing.HttpUtil;
import com.clearos.dlt.signing.Signature;
import com.clearos.dlt.signing.Signer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.goterl.lazysodium.LazySodium;
import com.goterl.lazysodium.LazySodiumAndroid;
import com.goterl.lazysodium.SodiumAndroid;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.interfaces.Box;
import com.goterl.lazysodium.interfaces.Hash;
import com.goterl.lazysodium.interfaces.KeyDerivation;
import com.goterl.lazysodium.interfaces.Sign;
import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;
import io.github.novacrypto.SecureCharBuffer;
import io.github.novacrypto.bip39.MnemonicGenerator;
import io.github.novacrypto.bip39.MnemonicValidator;
import io.github.novacrypto.bip39.Validation.InvalidChecksumException;
import io.github.novacrypto.bip39.Validation.InvalidWordCountException;
import io.github.novacrypto.bip39.Validation.UnexpectedWhiteSpaceException;
import io.github.novacrypto.bip39.Validation.WordNotFoundException;
import io.github.novacrypto.bip39.wordlists.English;

import io.github.novacrypto.hashing.Sha256;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;

import org.bitcoinj.core.Base58;
import org.json.JSONArray;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.function.Function;


public class CryptoKey {
    private final static LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    public static String keyStorageDirectory;
    public SecureCharBuffer masterSeedWords;
    protected SecureCharBuffer masterSeedText;
    protected KeyPair master;
    private static final KeyDerivation.Lazy keyDFLazy = lazySodium;
    public final static Sign.Lazy signLazy = lazySodium;
    private static KeyRestorer restorer;
    public Context appContext;
    private SharedPrefs prefs;

    private final static String TAG = "CryptoKey";
    static final String MASTER_SEED_FILE = "masterSeedWord";
    static final String OYP_SEED_FILE = "oypSeedWord";
    static final String OYN_SEED_FILE = "oynSeedWord";
    private final static String KEY_STORE_ALIAS = "clearos-crypto-RSA-padded";

    /**
     * The `keyId`-th master key in a key rotation sense to use for the home server.
     */
    private long masterKeyId;
    public DataCustodian homeServer;


    /**
     * Retrieves the File that the OYN seed will be stored under for a given DID.
     * @param did DID to store for.
     * @return Null if there is an issue.
     */
    public static File getOynSeedFile(Context appContext, String did) {
        String fileName = getOynSeedFileName(did);
        return new File(appContext.getFilesDir(), fileName);
    }


    /**
     * Retrieves the File that the OYP seed will be stored under for a given DID.
     * @param did DID to store for.
     * @return Null if there is an issue.
     */
    public static File getOypSeedFile(Context appContext, String did) {
        String fileName = getOypSeedFileName(did);
        return new File(appContext.getFilesDir(), fileName);
    }


    /**
     * Creates the name of the OYP file to use for a given DID.
     */
    public static String getOynSeedFileName(String did) {
        return String.format("%s_%s", did, OYN_SEED_FILE);
    }

    /**
     * Creates the name of the OYP file to use for a given DID.
     */
    public static String getOypSeedFileName(String did) {
        return String.format("%s_%s", did, OYP_SEED_FILE);
    }

    /**
     * Sets the storage directory to use based on the application context.
     * @param context Application context to use for storage, UI interaction, etc.
     */
    private void setKeyStorageDirectory(Context context) {
        if (context != null) {
            appContext = context;
            keyStorageDirectory = context.getFilesDir().getAbsolutePath();
            prefs = SharedPrefs.getInstance(appContext.getApplicationContext());
        }
    }

    /**
     * Creates a new master key randomly. Stores the corresponding words from BIP39 in the {@link #masterSeedWords}.
     */
    public CryptoKey(Context context) throws SodiumException {
        Key masterSeed = keyDFLazy.cryptoKdfKeygen();
        byte[] seedBytes = masterSeed.getAsBytes();
        Arrays.fill(seedBytes, 0, 16, (byte)0);
        masterSeedText = SecureBase58.encodeSecure(seedBytes);
        makeMasterKeyPair(seedBytes);
        masterKeyId = 0;
        Arrays.fill(seedBytes, (byte)0);
        setKeyStorageDirectory(context);
    }

    /**
     * Creates a new master key from a specific seed, encoded as Base58.
     */
    public CryptoKey(Context context, String base58Seed) throws SodiumException {
        byte[] seedBytes = Base58.decode(base58Seed);
        masterSeedText = SecureCharBuffer.withCapacity(base58Seed.length());
        masterSeedText.append(base58Seed);

        makeMasterKeyPair(seedBytes);
        masterKeyId = 0;
        Arrays.fill(seedBytes, (byte)0);
        setKeyStorageDirectory(context);
    }


    /**
     * Randomly creates a new DID.
     * @return null if there was an error creating the new DID keypair.
     */
    public DidKeys newDid() {
        Key seed = keyDFLazy.cryptoKdfKeygen();
        KeyPair keys = null;
        try {
            keys = signLazy.cryptoSignSeedKeypair(seed.getAsBytes());
        } catch (SodiumException e){
            Log.e("NEW-DID", "Error creating new, random DID.", e);
            return null;
        }

        return toDidKeys(keys);
    }

    /**
     * Initializes a data custodian for interacting with the remote home server.
     * @param URL URL of the home server
     * @param port Port of the home server
     * @param APIKey API Key for requests to the home server.
     */
    public void initDataCustodian(String URL, int port, String APIKey) {
        homeServer = new DataCustodian(URL, port, APIKey, this);
    }

    /**
     * Initializes a data custodian for interacting with the remote home server.
     * @param APIKey API Key for requests to the home server.
     */
    public void initDataCustodian(String APIKey) {
        homeServer = new DataCustodian(APIKey, this);
    }


    /**
     * Gets the *current* public key for the master home server key as Base58 encoded Verkey.
     * @return Public key part of the master home server key pair.
     */
    public String getMasterVerkey() {
        try {
            DidKeys dk = getHomeMaster();
            return dk.getVerKey();
        } catch (SodiumException e) {
            Log.e("HOMEKEY", "Unable to derive home server master key.");
        }
        return null;
    }


    /**
     * Gets the current public key for the master home server key.
     * @return Public key part of the master home server key pair.
     */
    public Key getMasterPublicKey() {
        try {
            DidKeys dk = getHomeMaster();
            return dk.getKeys().getPublicKey();
        } catch (SodiumException e) {
            Log.e("HOMEKEY", "Unable to derive home server master key.");
        }
        return null;
    }

    /**
     * Returns the DID for the master key set of the CryptoKeys instance.
     * @return The DID.
     */
    public String getDid() {
        try {
            DidKeys dk = getHomeMaster(0);
            return dk.getDid();
        } catch (SodiumException e) {
            Log.e("HOMEKEY", "Unable to derive home server master key.");
        }
        return null;
    }


    /**
     * Gets the last 4 digits of the master encryption seed as a hint so the user knows which one
     * they are using.
     * @return Last 4 digits of the encryption seed.
     */
    public String getMasterSeedHint() {
        String revealed;
        int seedLen = masterSeedText.length();
        char[] seed = new char[4];
        for (int ichar = 0; ichar < 4; ichar++) {
            seed[ichar] = masterSeedText.get(ichar+seedLen-4);
        }
        return new String(seed);
    }

    /**
     * Creates a new master key pair using the master seed as a string.
     * @param masterSeedWords the master seed or the  ' '-separated master Seed words.
     * @param isWords True if the masterSeedWords should be interpereted as an array of 24 words.
     * @param keyId Which master home server key to use in a key rotation sense.
     */
    public CryptoKey(Context context, SecureCharBuffer masterSeedWords, boolean isWords, long keyId) throws SodiumException {
        this(context, masterSeedWords, isWords);
        masterKeyId = keyId;
    }

    /**
     * Creates a new master key pair using the master seed as a string.
     * @param masterSeedWords the master seed or the  ' '-separated master Seed words.
     * @param isWords True if the masterSeedWords should be interpereted as an array of 24 words.
     */
    public CryptoKey(Context context, SecureCharBuffer masterSeedWords, boolean isWords) throws SodiumException {
        if (isWords) {
            byte[] wordBytes;
            byte[] masterSeed = new byte[32];
            Arrays.fill(masterSeed, (byte)0);
            try {
                wordBytes = fromWords(masterSeedWords);
                System.arraycopy(wordBytes,0, masterSeed,0, masterSeed.length);
            }
            catch (Exception ex) {
                Log.w("CK-SIGN", "Cannot sign: " + ex.getMessage(), ex);
                return;
            }

            masterSeedText = SecureBase58.encodeSecure(masterSeed);
            makeMasterKeyPair(masterSeed);
            Arrays.fill(wordBytes, (byte)0);
            Arrays.fill(masterSeed, (byte)0);
            setKeyStorageDirectory(context);
        } else {
            masterSeedText = masterSeedWords;
            makeMasterKeyPair(SecureBase58.decodeSecure(masterSeedWords));
            setKeyStorageDirectory(context);
        }
    }

    /**
     * Creates a set of DidKeys from an arbitrary set of seed words.
     * @param seed Seed to create DidKeys for.
     */
    public static DidKeys fromSecureSeed(SecureCharBuffer seed) throws SodiumException {
        byte[] wordBytes;
        byte[] result = new byte[32];
        Arrays.fill(result, (byte)0);
        try {
            wordBytes = Base58.decode(seed.toStringAble().toString());
            System.arraycopy(wordBytes,0, result,0, result.length);
        }
        catch (Exception ex) {
            Log.w("CK-SIGN", "Cannot sign: " + ex.getMessage(), ex);
            return null;
        }

        Arrays.fill(wordBytes, (byte)0);
        return toDidKeys(makeKeyPair(result));
    }

    public static CryptoKey restoreMasterKey(Context context, SecureCharBuffer seedWords,
                                             IKeyChainActivity keyChainActivity,
                                             Function<File, Void> callback) throws SodiumException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        CryptoKey restored = new CryptoKey(context, seedWords, true);
        restored.storeSecure(keyChainActivity, callback);
        return restored;
    }

    /**
     * Securely stores the seed needed to recreate the master key.
     */
    public void storeSecure(IKeyChainActivity keyChainActivity, Function<File, Void> callback) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        storeSecure(appContext, MASTER_SEED_FILE, masterSeedText, keyChainActivity, callback);
    }

    /**
     * Stores an encryption seed securely.
     * @param appContext Context to use for accessing file storage.
     * @param fileName Name to store the encrypted seed under.
     * @param seedText Secure char buffer of the actual seed text.
     */
    public static void storeSecure(Context appContext, String fileName, SecureCharBuffer seedText,
                                   IKeyChainActivity keyChainActivity,
                                   Function<File, Void> callback) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        keyStorageDirectory = appContext.getFilesDir().getAbsolutePath();
        SecureStore ss = new SecureStore(keyStorageDirectory, appContext, keyChainActivity);
        Log.d(TAG, String.format("Store secure called for %s in directory %s", fileName, keyStorageDirectory));
        Thread thread = new Thread(() -> {
            if (appContext.getString(R.string.keyProvider).equals("keystore")) {
                Log.d(TAG, "Secure store operation using android keystore.");
                ss.store(fileName, seedText, KEY_STORE_ALIAS, callback);
            } else {
                Log.d(TAG, "Secure store operation using system certs.");
                ss.store(fileName, seedText, null, callback);
            }
            SystemProperties.backupKeys();
        });
        thread.start();
    }

    /**
     * Securely stores a seed to file in a direct-boot aware way.
     * @param appContext Regular, unprotected application context.
     * @param fileName Name of the file to save th eseed under.
     * @param seedText Secure char buffer of the actual seed text.
     */
    public static void storeSecureDirectBootAware(Context appContext, String fileName,
                                                  SecureCharBuffer seedText,
                                                  IKeyChainActivity keyChainActivity,
                                                  Function<File, Void> callback) throws Exception {
        Context directBootContext = appContext.createDeviceProtectedStorageContext();
        String storageDirectory = directBootContext.getFilesDir().getAbsolutePath();
        SecureStore ss = new SecureStore(storageDirectory, directBootContext, keyChainActivity);
        Thread thread = new Thread(() -> {
            if (appContext.getString(R.string.keyProvider).equals("keystore"))
                ss.store(fileName, seedText, KEY_STORE_ALIAS, true, callback);
            else
                ss.store(fileName, seedText, null, true, callback);
        });
        thread.start();
    }

    /**
     * Decrypts a direct-boot aware file from secure storage.
     * @param fileName Name of the file to load the data from.
     * @return Secure char buffer of the file; null if there was an error.
     */
    public static SecureCharBuffer getDirectBootFile(Context appContext, String fileName) {
        Context directBootContext = appContext.createDeviceProtectedStorageContext();
        try {
            FileInputStream fis = directBootContext.openFileInput(fileName);
            byte[] contents = new byte[fis.available()];
            int len = fis.read(contents);
            fis.close();

            if (len != contents.length) {
                Log.w(TAG, String.format("Direct boot migration of %s didn't get all the bytes: %d vs. %d", fileName, len, contents.length));
                return null;
            }

            KeyPair tempKeys = SecureStore.getTemporaryKeys();
            String b64 = new String(contents, StandardCharsets.UTF_8);

            return anonDecryptSecure(b64, tempKeys);
        } catch (IOException e) {
            Log.e("CRYPTO", "Error reading contents of direct-boot file.", e);
        } catch (SodiumException e) {
            Log.e(TAG, "Error generating temporary keys for decrypting direct-boot OYP code.", e);
        } catch (Exception e) {
            Log.e(TAG, "General error when re-calling storeSecure with seed.", e);
        }

        return null;
    }

    /**
     * Migrates a file created in device-protected storage (direct-boot aware) to its regular
     * location once user files have been unlocked.
     * @param appContext Unprotected application context.
     * @param fileName Name of the file to move.
     * @return True if the renaming succeeded.
     */
    public static boolean migrateDirectBootFile(Context appContext, String originalFileName,
                                                String fileName, IKeyChainActivity keyChainActivity,
                                                Function<File, Void> callback) {
        Context directBootContext = appContext.createDeviceProtectedStorageContext();
        File migratedFile = new File(appContext.getFilesDir().getAbsolutePath(), fileName);
        Log.d(TAG, "Migrating direct boot OYP seed file to " + migratedFile.toString());
        try {
            FileInputStream fis = directBootContext.openFileInput(originalFileName);
            byte[] contents = new byte[fis.available()];
            int len = fis.read(contents);
            fis.close();

            if (len != contents.length) {
                Log.w(TAG, String.format("Direct boot migration of %s didn't get all the bytes: %d vs. %d", originalFileName, len, contents.length));
                callback.apply(migratedFile);
                return false;
            }

            KeyPair tempKeys = SecureStore.getTemporaryKeys();
            String b64 = new String(contents, StandardCharsets.UTF_8);
            SecureCharBuffer seed = anonDecryptSecure(b64, tempKeys);
            Log.d(TAG, "Success decrypting direct-boot OYP seed with temporary keys.");
            storeSecure(appContext, fileName, seed, keyChainActivity, callback);
        } catch (IOException e) {
            Log.e("CRYPTO", "Error migrating encrypted direct-boot file to regular storage.", e);
            callback.apply(migratedFile);
            return false;
        } catch (SodiumException e) {
            Log.e(TAG, "Error generating temporary keys for decrypting direct-boot OYP code.", e);
            callback.apply(migratedFile);
            return false;
        } catch (Exception e) {
            Log.e(TAG, "General error when re-calling storeSecure with seed.", e);
            callback.apply(migratedFile);
            return false;
        }

        callback.apply(migratedFile);
        return true;
    }

    /**
     * Securely verifies whether the given seed matches the master seed for this CryptoKey.
     * @param seed Seed to compare.
     * @return True if the seeds match.
     */
    public boolean verifyMasterSeed(SecureCharBuffer seed) {
        return masterSeedText.equals(seed);
    }

    /**
     * Restores the master key pair from secure OS storage into secure memory.
     */
    public static void fromSecureStorage(Context context, KeyRestorer keyRestorer, IKeyChainActivity keyChainActivity) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        fromSecureStorage(context, MASTER_SEED_FILE, keyRestorer, keyChainActivity);
    }

    /**
     * Restores an arbitrary key pair from secure OS storage into secure memory using its filename.
     */
    public static void fromSecureStorage(Context context, String fileName, KeyRestorer keyRestorer, IKeyChainActivity keyChainActivity) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        restorer = keyRestorer;
        KeyDecrypter kd = new KeyDecrypter(context, keyRestorer, fileName);
        SecureStore ss = new SecureStore(context.getFilesDir().getAbsolutePath(), context, keyChainActivity);
        Thread thread = new Thread(() -> {
            try {
                if (context.getString(R.string.keyProvider).equals("keystore"))
                    ss.load(fileName, KEY_STORE_ALIAS, kd);
                else
                    ss.load(fileName, null, kd);
            } catch (IOException e) {
                Log.e(TAG, "Error loading keys from secure storage", e);
                kd.onKeyDecryptFailure(e);
            }
        });
        thread.start();
    }

    /**
     * Generates a master key pair on the device and stores it in secure memory.
     * @return List of the BIP39 words that the user should remember to restore their key.
     */
    public static CryptoKey generateMasterKey(Context context, IKeyChainActivity keyChainActivity,
                                              Function<File, Void> callback) throws SodiumException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        CryptoKey CK = new CryptoKey(context);
        CK.storeSecure(keyChainActivity, callback);
        return CK;
    }

    /**
     * Signs the request using the current master home key.
     * @param request HTTP request builder object that should have signing headers added to it.
     */
    public Request masterSignRequest(Request request) throws Exception {
        return signRequest(request, getHomeMaster(), getDid());
    }

    /**
     * Signs an HTTP request using a derived key.
     * @param request HTTP request builder object that should have signing headers added to it.
     * @param signerKeys The keys to use for signing the request.
     */
    public static Request signRequest(Request request, DidKeys signerKeys, String signerDid) throws Exception {
        String method = request.method();
        String uri = request.url().toString();
        Map<String, String> headers = HttpUtil.headers(request.headers());
        Request result = request;

        if (!headers.containsKey("date")) {
            // Put the format string into `python` standard format string for UTC time.
            Instant now = Instant.now();
            String dateHeader = now.toString();
            if (dateHeader.contains(".")) {
                dateHeader = dateHeader.replace('T', ' ').replace('Z', '0');
            } else {
                dateHeader = dateHeader.replace('T', ' ').substring(0, dateHeader.length()-2);
            }
            result = request.newBuilder()
                    .addHeader("date", dateHeader)
                    .build();
            // Also recreate the headers map with the added entry.
            headers = HttpUtil.headers(result.headers());
        }

        try {
            Signer<byte[]> signer = (Signer<byte[]>) Signer.signer("ed25519");
            byte[] secret = signerKeys.getKeys().getSecretKey().getAsBytes().clone();
            Signature signature = signer.sign(method, uri, headers, signerKeys.getDid(), secret);
            Arrays.fill(secret, (byte)0);
            result = HttpUtil.setSignature(result, signature, signerDid);
        } catch (Exception ex) {
            Log.w("CK-SIGN", "Cannot sign request: " + ex.getMessage(), ex);
        }
        return result;
    }

    /**
     * Generates a DID and a derived public key.
     * @param keyId Index of the key in the hierarchical list.
     * @param context 8-byte string indicating the context in which `keyId` should be interpreted.
     */
    public DidKeys derivedDid(long keyId, String context, KeyPair baseKeys) throws SodiumException {
        KeyPair derived = deriveKeyPair(keyId, context, baseKeys);
        return toDidKeys(derived);
    }

    /**
     * Converts a cryptographic key pair to a DID and verKey.
     * @param keyPair The public/private keyPair to convert.
     * @return did, verKey and the original keyPair.
     */
    public static DidKeys toDidKeys(KeyPair keyPair) {
        byte[] verKey = keyPair.getPublicKey().getAsBytes();
        byte[] did = new byte[16];
        System.arraycopy(verKey, 0, did, 0, 16);
        String DID = Base58.encode(did);
        String VK = Base58.encode(verKey);
        return new DidKeys(DID, VK, keyPair);
    }

    /**
     * Derives a signing key and creates an identifier for the key.
     * @param i sub-index of the key.
     */
    private DerivedSigner getDerivedSignerHash(int i) throws SodiumException {
        Hash.Native hasher = lazySodium;
        byte[] secretPart = getHomeMaster().getKeys().getSecretKey().getAsBytes().clone();
        byte[] index = BigInteger.valueOf(i).toByteArray();
        byte[] salty = new byte[20 + index.length];
        System.arraycopy(secretPart,0,salty,0,16);
        System.arraycopy(index,0, salty,20, index.length);

        KeyPair derivedI = deriveKeyPair(i, "_derived");
        byte[] hashedBytes = new byte[Hash.SHA256_BYTES];
        if (!hasher.cryptoHashSha256(hashedBytes, salty, salty.length)) {
            throw new SodiumException("Unsuccessful sha-256 hash.");
        }

        Arrays.fill(secretPart, (byte)0);
        Arrays.fill(salty, (byte)0);

        return new DerivedSigner(lazySodium.toHexStr(hashedBytes), toDidKeys(derivedI));
    }


    /**
     * Signs a message to Base64-encoded string using the specified signing keys.
     * @param message Message to sign.
     * @param signingKeys Keys whose private key will be used.
     */
    public static String signMessageToBase64(String message, DidKeys signingKeys) {
        // Build the signature; the QR code shows the message and signature so that the new manager
        // can authenticate with the server.
        byte[] encodedMessage = message.getBytes(StandardCharsets.UTF_8);
        byte[] signedMessage = new byte[Sign.BYTES + encodedMessage.length];
        boolean res = ((Sign.Native)lazySodium).cryptoSign(signedMessage, encodedMessage, encodedMessage.length, signingKeys.getKeys().getSecretKey().getAsBytes());
        if (!res) {
            Log.e("GenericMessageSigning", "Could not sign your message.");
        }

        return Base64.getEncoder().encodeToString(signedMessage);
    }

    public static String signMessageToHex(String message, DidKeys signingKeys) throws SodiumException {
        return signLazy.cryptoSignDetached(message, signingKeys.getKeys().getSecretKey());
    }

    /**
     * Verifies a signature created by `signMessageToHex`.
     */
    public static boolean verifyHexSignature(String signature, String message, Key publicKey) {
        return signLazy.cryptoSignVerifyDetached(signature, message, publicKey);
    }

    /**
     * Gets the short key context for an application/package.
     */
    public static String getShortKeyContext(String packageName) {
        // This matches the same convention as the python packages on home server.
        byte[] target = String.format("app/%s", packageName).getBytes();
        byte[] hash = Sha256.sha256(target);
        return LazySodium.toHex(hash).substring(0, 8).toLowerCase();
    }


    private DidKeys findDecryptionKey(String packageName, String verKey) {
        // The majority of the apps will end up using the notification DID for encryption because
        // that is what home server uses. The app-specific DIDs are registered by ClearLIFE on
        // the ClearPHONE with userKeys.getDid() as the notification DID. That DID and its current
        // public key are looked up home server-side for encryption. Before we start enumerating
        // all the possible rotated keys for the `packageName`, let's first see if our verkey matches
        // the current userKeys.
        try {
            DidKeys homeMaster = getHomeMaster(0);
            if (homeMaster.getVerKey().equals(verKey)) {
                return homeMaster;
            }
        } catch (SodiumException e) {
            Log.d(TAG, "While comparing home master to notification verkey.", e);
        }

        DidKeys baseKeys = CryptoKeysService.deriveAppKeys(this, packageName, 0);
        if (baseKeys == null)
            return null;
        if (baseKeys.getVerKey().equals(verKey))
            return baseKeys;

        String keyContext = CryptoKeysService.getAppKeyContext(packageName);
        DidKeys result = null;
        try {
            long maxKeyId = getCurrentAppKeyId(keyContext);
            for (int i = 0; i<maxKeyId; i++) {
                DidKeys previous = CryptoKeysService.deriveAppKeys(this, packageName, i);
                if (previous.getVerKey().equals(verKey)) {
                    result = previous;
                    break;
                }
            }
            return result;
        } catch (IOException e) {
            Log.e(TAG, "Error retrieving latest app keyId for rotation.", e);
            return null;
        }
    }

    /**
     * Finds the key that has public key part matching `verKey`.
     * @param verKey The Base58-encoded public key part of the keypair to match.
     * @return The keypair if it is part of this CryptoKey; otherwise null.
     */
    private DidKeys findDecryptionKey(String verKey) throws SodiumException {
        DidKeys result = null;
        if (verKey.equals(getMasterVerkey())) {
            return getHomeMaster();
        } else {
            // Check the previous master keys we may have used to see if one of them matches.
            for (int ikey = 0; ikey < masterKeyId; ikey++) {
                DidKeys previous = getHomeMaster(ikey);
                if (previous.getVerKey().equals(verKey)) {
                    result = previous;
                    break;
                }
            }

            if (result == null) {
                // It doesn't hurt to decrypt with the actual master key since that never leaves the
                // edge device.
                DidKeys masterKeys = toDidKeys(master);
                if (masterKeys.getVerKey().equals(verKey)) {
                    result = masterKeys;
                }
            }

            return result;
        }
    }

    /**
     * Decrypts the body of a push notification sent by a data custodian server.
     * @param payload Byte contents of the MQTT message.
     * @return An unencrypted JSON string.
     */
    public byte[] decryptMqttMessage(String packageName, byte[] payload) throws SodiumException {
        byte[] _verkey = Arrays.copyOfRange(payload,0,32);
        String verkey = Base58.encode(_verkey);
        DidKeys decrypter = findDecryptionKey(packageName, verkey);
        if (decrypter != null) {
            byte[] cipher = Arrays.copyOfRange(payload, 32, payload.length);
            return anonDecryptBytes(cipher, decrypter.getKeys());
        } else {
            Log.w("MQTT-DECRYPT", "Unable to find a decryption key that matches " + verkey);
            return null;
        }
    }

    /**
     * Decrypts the body of a push notification sent by a data custodian server.
     * @param verkey The public key that the notification was encrypted for.
     * @param contents The encrypted JSON string.
     * @return An unencrypted JSON string.
     */
    public String decryptPushNotification(String verkey, String contents) throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, SodiumException, IOException {
        DidKeys decrypter = findDecryptionKey(verkey);
        if (decrypter != null) {
            return anonDecrypt(contents, decrypter.getKeys());
        } else {
            Log.w("PUSH-DECRYPT", "Unable to find a decryption key that matches " + verkey);
            return null;
        }
    }

    public static String anonCryptSecure(SecureCharBuffer message, Key publicKey) throws SodiumException {
        final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
        final Box.Native boxNative = lazySodium;
        final Sign.Native signNative = lazySodium;

        byte[] edPkBytes = publicKey.getAsBytes();
        byte[] curvePkBytes = new byte[Sign.CURVE25519_PUBLICKEYBYTES];
        boolean pkSuccess = signNative.convertPublicKeyEd25519ToCurve25519(curvePkBytes, edPkBytes);

        if (!pkSuccess){
            throw new SodiumException("Could not convert public key to Curve25519.");
        }

        byte[] byteMsg = SecureBase58.decodeSecure(message);
        int _mLen = byteMsg.length;
        int _cLen = Box.SEALBYTES + _mLen;
        byte[] cipher = new byte[_cLen];

        boolean res =
                boxNative.cryptoBoxSeal(
                        cipher,
                        byteMsg,
                        _mLen,
                        curvePkBytes
                );

        Arrays.fill(byteMsg, (byte)0);
        if (!res) {
            throw new SodiumException("Could not encrypt your message.");
        }

        return Base64.getEncoder().encodeToString(cipher);
    }

    public static String anonCrypt(byte[] message, Key publicKey) throws SodiumException {
        final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());

        byte[] edPkBytes = publicKey.getAsBytes();
        byte[] curvePkBytes = new byte[Sign.CURVE25519_PUBLICKEYBYTES];
        boolean pkSuccess = ((Sign.Native) lazySodium).convertPublicKeyEd25519ToCurve25519(curvePkBytes, edPkBytes);

        if (!pkSuccess){
            throw new SodiumException("Could not convert public key to Curve25519.");
        }

        int _mLen = message.length;
        int _cLen = Box.SEALBYTES + _mLen;
        byte[] cipher = new byte[_cLen];
        boolean res =
                ((Box.Native) lazySodium).cryptoBoxSeal(
                        cipher,
                        message,
                        _mLen,
                        curvePkBytes
                );

        if (!res) {
            throw new SodiumException("Could not encrypt your message.");
        }

        return Base64.getEncoder().encodeToString(cipher);
    }

    public static String anonCrypt(String message, Key publicKey) throws SodiumException {
        return anonCrypt(message.getBytes(), publicKey);
    }

    /**
     * Decrypts an anonymously encrypted message.
     * @param message Encrypted message encoded as Base64.
     * @param decryptionKeys Keypair that includes the public key that the message was encrypted for.
     * @return Decrypted string of the message.
     * @throws SodiumException If the decryption fails.
     */
    public static SecureCharBuffer anonDecryptSecure(String message, KeyPair decryptionKeys) throws SodiumException {
        final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
        final Box.Native boxNative = lazySodium;
        final Sign.Lazy signLazy = lazySodium;

        KeyPair curve25519 = signLazy.convertKeyPairEd25519ToCurve25519(decryptionKeys);
        byte[] cipher = Base64.getDecoder().decode(message);
        byte[] byteMsg = new byte[cipher.length - Box.SEALBYTES];
        boolean res =
                boxNative.cryptoBoxSealOpen(
                        byteMsg,
                        cipher,
                        cipher.length,
                        curve25519.getPublicKey().getAsBytes(),
                        curve25519.getSecretKey().getAsBytes()
                );

        if (!res) {
            throw new SodiumException("Could not decrypt your message.");
        }

        return SecureBase58.encodeSecure(byteMsg);
    }


    /**
     * Decrypts the message using the current master home server key.
     * @param message Encrypted message anonCrypted for the master verkey.
     * @return Decrypted message string.
     */
    public String anonDecryptMaster(String message) throws SodiumException {
        return CryptoKey.anonDecrypt(message, getHomeMaster().getKeys());
    }

    /**
     * Decrypts an anonymously encrypted message.
     * @param cipher Encrypted message bytes.
     * @param decryptionKeys Keypair that includes the public key that the message was encrypted for.
     * @return Decrypted string of the message.
     * @throws SodiumException If the decryption fails.
     */
    private static byte[] anonDecryptBytes(byte[] cipher, KeyPair decryptionKeys) throws SodiumException {
        final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
        final Box.Native boxNative = lazySodium;
        final Sign.Lazy signLazy = lazySodium;

        KeyPair curve25519 = signLazy.convertKeyPairEd25519ToCurve25519(decryptionKeys);
        byte[] byteMsg = new byte[cipher.length - Box.SEALBYTES];

        boolean res =
                boxNative.cryptoBoxSealOpen(
                        byteMsg,
                        cipher,
                        cipher.length,
                        curve25519.getPublicKey().getAsBytes(),
                        curve25519.getSecretKey().getAsBytes()
                );

        if (!res) {
            throw new SodiumException("Could not decrypt your message.");
        }

        return byteMsg;
    }

    /**
     * Decrypts an anonymously encrypted message.
     * @param message Encrypted message encoded as Base64.
     * @param decryptionKeys Keypair that includes the public key that the message was encrypted for.
     * @return Decrypted string of the message.
     * @throws SodiumException If the decryption fails.
     */
    public static String anonDecrypt(String message, KeyPair decryptionKeys) throws SodiumException {
        byte[] cipher = Base64.getDecoder().decode(message);
        byte[] byteMsg = anonDecryptBytes(cipher, decryptionKeys);
        return new String(byteMsg);
    }

    /**
     * Decrypts an anonymously encrypted message.
     * @param cipher Encrypted message.
     * @param decryptionKeys Keypair that includes the public key that the message was encrypted for.
     * @return Decrypted string of the message.
     * @throws SodiumException If the decryption fails.
     */
    public static String anonDecryptUtf8(byte[] cipher, KeyPair decryptionKeys) throws SodiumException {
        byte[] byteMsg = anonDecryptBytes(cipher, decryptionKeys);
        return new String(byteMsg);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public static String getApkKeyHash(Context appContext) {
        String result = null;
        try {
            android.content.pm.Signature[] signatures;
            final PackageInfo packageInfo = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), PackageManager.GET_SIGNING_CERTIFICATES);
            signatures = packageInfo.signingInfo.getApkContentsSigners();
            for (android.content.pm.Signature signature : signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                if (result == null) {
                    result = Base64.getEncoder().encodeToString(md.digest());
                    Log.i("FIRST HASH:", result);
                } else {
                    Log.i("MY KEY HASH:", Base64.getEncoder().encodeToString(md.digest()));
                }
            }
        } catch (Exception e) {
            Log.e("APK-KEY", "Cannot generate APK key hash", e);
        }
        return result;
    }

    public static String getPackageSignatures(Context appContext) {
        StringBuilder sigHashes = new StringBuilder();
        try {
            android.content.pm.Signature[] signatures;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                final PackageInfo packageInfo = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), PackageManager.GET_SIGNING_CERTIFICATES);
                signatures = packageInfo.signingInfo.getApkContentsSigners();
            } else {
                signatures = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            }

            final MessageDigest md = MessageDigest.getInstance("SHA");
            for (android.content.pm.Signature signature : signatures) {
                md.update(signature.toByteArray());
                final String signatureBase64 = Base64.getEncoder().encodeToString(md.digest());
                sigHashes.append(signatureBase64);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            Log.e("PACKSIGS", "Unable to retrieve package signatures.");
        }

        if (sigHashes.length() > 0) {
            try {
                final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
                return ((Hash.Lazy)lazySodium).cryptoHashSha256(sigHashes.toString());
            } catch (SodiumException e) {
                Log.e("PACKSIGS", "Error hashing combined packet signatures");
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Signs the message using the specified keys.
     */
    public String sign(String message, DidKeys signerKeys) throws GeneralSecurityException {
        Ed25519Signer ed25519Signer = new Ed25519Signer();
        byte[] secret = signerKeys.getKeys().getSecretKey().getAsBytes().clone();
        byte[] signature = ed25519Signer.sign(message.getBytes(), secret);
        Arrays.fill(secret, (byte)0);
        return Base64.getEncoder().encodeToString(signature);
    }

    /**
     * Returns the proof of hardware keypair for the mobile phone that was placed there before it
     * was shipped to the user.
     */
    private KeyPair getProofOfHardwareKeys() throws SodiumException {
        Key derivedSeed = deriveKey(0, "hardware");
        byte[] secret = derivedSeed.getAsBytes();
        KeyPair hardKeys = makeKeyPair(secret);
        Arrays.fill(secret, (byte)0);
        return hardKeys;
    }

    /**
     * Registers the master Did with the home server. This should only be called once per phone.
     * @param name Name that the home server should associated with the base master DID.
     * @param displayName Display name that home server should use for the DID.
     * @param icon URL for an icon to display with the DID at home server.
     * @param callback asynchronous HTTP response/failure handler.
     */
    public Call publishMasterKey(String name, String displayName, String icon, Callback callback) throws Exception {
        // Publish the key including pre-rotation information.
        DidKeys ensuer = getHomeMaster(masterKeyId+1);
        DidKeys homeMaster = getHomeMaster();
        // TODO: this should not be a hard-coded serial number...
        String serial = "vDxS43fatekMLVlb3";
        DidKeys serialKeys = toDidKeys(getProofOfHardwareKeys());
        return homeServer.publishMasterKey(getDid(), homeMaster.getVerKey(), serial, serialKeys, ensuer,
                name, displayName, icon, callback);
    }

    public DidKeys getAppKeyPair() throws IOException, SodiumException {
        String signatures = getPackageSignatures(appContext);
        // Unfortunately, the key deriver only accepts 8 characters... There is unlikely to be a
        // clash between application certificates on the first 8 characters of the combined sig hash.
        String keyContext = signatures.substring(0, 8);
        long keyId = getCurrentAppKeyId(signatures);
        return derivedDid(keyId, keyContext, null);
    }

    /**
     * Publishes the app crypto key pair for the current keyId.
     */
    public Call publishAppKey(Context appContext, Callback callback) throws Exception {
        return setAppKey(appContext, 0, callback);
    }

    public Call publishAppKey(String packageName, String packageContext, KeyPair baseKeys, Callback callback) throws Exception {
        return setAppKey(packageName, packageContext, 0, baseKeys, callback);
    }

    long getCurrentAppKeyId(String signatures) throws IOException {
        String value = prefs.getValue(signatures);
        if (value == null) {
            return 0;
        } else {
            Gson gson = new Gson();
            Type stringStringMap = new TypeToken<Map<String, String>>(){}.getType();
            Map<String, String> keyMap = gson.fromJson(value, stringStringMap);
            return Long.parseLong(value);
        }
    }

    private void setCurrentAppKeyId(String signatures, long keyId) {
        Map<String, String> keyMap = new HashMap<>();
        keyMap.put("altered", Instant.now().toString());
        keyMap.put("keyId", Long.toString(keyId));

        // Serialize the key and timestamp as JSON, then write to file.
        Gson gson = new Gson();
        String json = gson.toJson(keyMap);
        prefs.saveValue(signatures, json);
    }

    /**
     * Rotates the keyId for the app.
     */
    public Call rotateAppKey(Context appContext, Callback callback) throws Exception {
        String signatures = getPackageSignatures(appContext);
        assert signatures != null;
        long keyId = getCurrentAppKeyId(signatures);
        Call result = setAppKey(appContext, keyId + 1, callback);
        setCurrentAppKeyId(signatures,keyId + 1);
        return result;
    }

    /**
     * Publishes a derived public key of an app to a home server. Note that if keyId is incremented,
     * a rotation operation will automatically be performed at the home server.
     */
    private Call setAppKey(Context appContext, long keyId, Callback callback) throws Exception {
        String signatures = getPackageSignatures(appContext);
        assert signatures != null;

        // Unfortunately, the key deriver only accepts 8 characters... There is unlikely to be a
        // clash between application certificates on the first 8 characters of the combined sig hash.
        String keyContext = signatures.substring(0, 8);
        return setAppKey(appContext.getPackageName(), keyContext, keyId, null, callback);
    }

    /**
     * Rotates the keyId for the app.
     */
    public Call rotateAppKey(String packageName, String keyContext, KeyPair baseKeys, Callback callback) throws Exception {
        long keyId = getCurrentAppKeyId(keyContext);
        Call result = setAppKey(packageName, keyContext, keyId + 1, baseKeys, callback);
        setCurrentAppKeyId(keyContext,keyId + 1);
        return result;
    }

    /**
     * Publishes a derived public key of an app to a home server. Note that if keyId is incremented,
     * a rotation operation will automatically be performed at the home server.
     * @param keyContext String context unique to the app to register with home server.
     * @param keyId KeyId to use as current key.
     */
    public Call setAppKey(String packageName, String keyContext, long keyId, KeyPair baseKeys, Callback callback) throws Exception {
        Log.d(TAG, String.format("Setting app key for %s and context `%s`", packageName, keyContext));
        DidKeys zero = derivedDid(0, keyContext, baseKeys);
        DidKeys signer = derivedDid(keyId, keyContext, baseKeys);
        DidKeys erster = null;
        if (keyId > 1) {
            erster = derivedDid(keyId - 1, keyContext, baseKeys);
        }
        DidKeys ensuer = derivedDid(keyId + 1, keyContext, baseKeys);

        // Publish the key including pre-rotation information.
        return homeServer.publishAppKey(packageName,
                zero.getDid(), signer.getDid(), erster, signer, ensuer, callback);
    }

    /**
     * Decrypts the specified byte[] that was encoded to uint8 array using Base58.
     * @param bytes Bytes to convert to Uint8.
     */
    public static int[] toUInt8(byte[] bytes) {
        int[] uint8 = new int[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            uint8[i] = bytes[i] & 0xFF;
        }
        return uint8;
    }

    /**
     * Derives a hierarchy of keypairs, returning the final one in the chain.
     * @param context Context to use for key derivation.
     * @param chain A '/'-separated chain of `long` key identifiers for the derivation at each stage.
     */
    public KeyPair deriveKeyHierarchy(String context, String chain) throws SodiumException {
        return deriveKeyHierarchy(context, chain, master);
    }

    /**
     * Derives a hierarchy of keypairs, returning the final one in the chain.
     * @param context Context to use for key derivation.
     * @param chain A '/'-separated chain of `long` key identifiers for the derivation at each stage.
     * @param baseKeys Base key-pair from which to derive the chain of keys.
     */
    public KeyPair deriveKeyHierarchy(String context, String chain, KeyPair baseKeys) throws SodiumException {
        String[] parts = chain.split("/");
        Key currentKey;
        KeyPair currentKeyPair = baseKeys;
        byte[] currentSeed;
        for (String part: parts) {
            long keyId = Long.parseLong(part);
            currentKey = deriveKey(keyId, context);
            currentSeed = currentKey.getAsBytes();
            currentKeyPair = makeKeyPair(currentSeed);
        }

        return currentKeyPair;
    }

    /**
     * Encrypts a value using the current vault encryption key.
     * @param value Value to encrypt.
     */
    public String vaultCrypt(String value) throws SodiumException {
        Key derivedSeed = deriveKey(0, "usrvault");
        byte[] secret = derivedSeed.getAsBytes();
        KeyPair vaultKeys = makeKeyPair(secret);
        Arrays.fill(secret, (byte)0);
        return anonCrypt(value, vaultKeys.getPublicKey());
    }

    public JSONArray encryptArray(JSONArray jsonArray) throws SodiumException {
       JSONArray jsonArray1 = null;

       for (int i =0; i<jsonArray.length(); i++){


       }


       return jsonArray1;
    }

    /**
     * Decrypts a value encrypted by the current vault encryption key.
     * @param value Value returned by `vaultCrypt`
     */
    public String vaultDecrypt(String value) throws SodiumException {
        Key derivedSeed = deriveKey(0, "usrvault");
        byte[] secret = derivedSeed.getAsBytes();
        KeyPair vaultKeys = makeKeyPair(secret);
        Arrays.fill(secret, (byte)0);
        return anonDecrypt(value, vaultKeys);
    }

    /**
     * Encrypts a derived vault key so that only the plug-in with secret key corresponding to `verKey` can decrypt it.
     * @param verKey Base58-encoded public key that the plug-in is using for encryption.
     * @return Encrypted vault seed, encoded as Base64.
     * @throws SodiumException If the encryption fails.
     */
    public String encryptVaultKey(String verKey) throws SodiumException {
        Key derivedSeed = deriveKey(0, "usrvault");
        String base64Seed = Base64.getEncoder().encodeToString(derivedSeed.getAsBytes());
        byte[] verkeyBytes = Base58.decode(verKey);
        return anonCrypt(base64Seed, Key.fromBytes(verkeyBytes));
    }

    public String getPluginPassword(String verKey) throws SodiumException {
        Key derivedSeed = deriveKey(0, "pluginpw");
        String base64Seed = Base64.getEncoder().encodeToString(derivedSeed.getAsBytes());
        byte[] verkeyBytes = Base58.decode(verKey);
        return anonCrypt(base64Seed, Key.fromBytes(verkeyBytes));
    }

    /**
     * Returns a specific "master" key for interacting with the home server. Use rotateHomeMaster to change
     * the masterKeyId used for derived key generation.
     * @return The `keyId`-th master key for use with the home server.
     */
    public DidKeys getHomeMaster(long keyId) throws SodiumException {
        KeyPair homeKey = deriveKeyPair(keyId, "homesrvr");
        return toDidKeys(homeKey);
    }

    /**
     * Returns the "master" key for interacting with the home server. Use rotateHomeMaster to change
     * the masterKeyId used for derived key generation.
     * @return The *current* home master key.
     */
    public DidKeys getHomeMaster() throws SodiumException {
        KeyPair homeKey = deriveKeyPair(masterKeyId, "homesrvr");
        return toDidKeys(homeKey);
    }

    /**
     * Derives a new KeyPair using a hierarchical derivation function.
     * @param keyId Index of the key in the hierarchical list.
     * @param context 8-byte string indicating the context in which `keyId` should be interpreted.
     * @throws SodiumException if any of the lengths were not correct.     *
     */
    public KeyPair deriveKeyPair(long keyId, String context) throws SodiumException {
        Key derivedSeed = deriveKey(keyId, context);
        return signLazy.cryptoSignSeedKeypair(derivedSeed.getAsBytes());
    }

    /**
     * Derives a new KeyPair using a hierarchical derivation function.
     * @param keyId Index of the key in the hierarchical list.
     * @param context 8-byte string indicating the context in which `keyId` should be interpreted.
     * @param baseKeys base key pair from which to derive.
     * @throws SodiumException if any of the lengths were not correct.     *
     */
    public KeyPair deriveKeyPair(long keyId, String context, KeyPair baseKeys) throws SodiumException {
        Log.d(TAG, String.format("Deriving key pair with id=%d; context=%s from baseKeys=%s",
                keyId,
                context,
                baseKeys == null ? "" : baseKeys.getPublicKey().getAsHexString()
        ));

        if (baseKeys == null)
            return deriveKeyPair(keyId, context);
        else {
            Key derivedSeed = deriveKey(baseKeys, keyId, context);
            return signLazy.cryptoSignSeedKeypair(derivedSeed.getAsBytes());
        }
    }

    /**
     * Derives a key pair from a custom seed.
     */
    public static KeyPair fromKey(Key seed) throws SodiumException {
        return signLazy.cryptoSignSeedKeypair(seed.getAsBytes());
    }

    public static KeyPair deriveKeyPair(KeyPair baseKeyPair, long keyId, String context) throws SodiumException {
        Key seed = deriveKey(baseKeyPair, keyId, context);
        return fromKey(seed);
    }

    /**
     * Derives a new Key from the existing master key.
     * @param keyId Index of the key in the hierarchical list.
     * @param context 8-byte string indicating the context in which `keyId` should be interpreted.
     * @return Derived Key using the master *secret* key and a HKDF.
     * @throws SodiumException if any of the lengths were not correct.
     */
    public Key deriveKey(long keyId, String context) throws SodiumException {
        return deriveKey(master, keyId, context);
    }

    /**
     * Derives a new Key from the existing master key.
     * @param keyId Index of the key in the hierarchical list.
     * @param context 8-byte string indicating the context in which `keyId` should be interpreted.
     * @return Derived Key using the master *secret* key and a HKDF.
     * @throws SodiumException if any of the lengths were not correct.
     */
    public static Key deriveKey(KeyPair baseKeyPair, long keyId, String context) throws SodiumException {
        byte[] masterSeed = new byte[KeyDerivation.MASTER_KEY_BYTES];
        byte[] secretKey = baseKeyPair.getSecretKey().getAsBytes().clone();
        System.arraycopy(secretKey,0, masterSeed,0, KeyDerivation.MASTER_KEY_BYTES);
        Key result = keyDFLazy.cryptoKdfDeriveFromKey(32, keyId, context, Key.fromBytes(masterSeed));

        Arrays.fill(masterSeed, (byte)0);
        Arrays.fill(secretKey, (byte)0);
        return result;
    }

    /**
     * Creates the master public/secret key pair.
     * @param masterSeed master seed phrase to use; this is usually generated randomly but may be passed in explicitly.
     * @throws SodiumException if LazySodium library encounters an uhandled exception while generating the crypto seed pair.
     */
    private void makeMasterKeyPair(byte[] masterSeed) throws SodiumException {
        master = signLazy.cryptoSignSeedKeypair(masterSeed);
        masterSeedWords = toWords(Arrays.copyOfRange(masterSeed, 16, 32));
    }

    /**
     * Returns the DID of the master seed keypair. This is different from the prime derived DID that
     * interacts with everything else.
     * @return Null if the master key is no longer in scope. The master key only exists for a short
     *         while until the derived keys are in place.
     */
    public String getMasterDid() {
        if (master != null)
            return toDidKeys(master).getDid();
        return null;
    }

    /**
     * Generates a new cryptographic keypair for the given seed.
     * @param seed Seed bytes for the new keypair.
     */
    public static KeyPair makeKeyPair(byte[] seed) throws SodiumException {
        return signLazy.cryptoSignSeedKeypair(seed);
    }


    /**
     * Generates the master seed word using the BIP39 word list.
     * @param seedWords BIP39 word list that the user was supposed to remember.
     * @return Master seed word to use in {@link #makeMasterKeyPair(byte[])} as a byte array.
     */
    private static byte[] fromWords(SecureCharBuffer seedWords) throws WordNotFoundException, UnexpectedWhiteSpaceException, InvalidChecksumException, InvalidWordCountException {
        List<StringBuilder> words = new ArrayList<>();
        StringBuilder current = new StringBuilder();
        for (int ichar = 0; ichar < seedWords.length(); ichar++) {
            char single = seedWords.get(ichar);
            if (single == ' ') {
                words.add(current);
                current = new StringBuilder();
            } else {
                current.append(single);
            }
        }
        words.add(current);
        // We use only 12 words and a 16-byte key. Sort out the initial zeros.
        byte[] result = new byte[32];
        Arrays.fill(result, (byte)0);
        byte[] wordBytes = MnemonicValidator
                .ofWordList(English.INSTANCE)
                .calculateEntropy(words);
        System.arraycopy(wordBytes, 0, result, 16, 16);
        return result;
    }

    /**
     * Converts a master seed into the a set of words using the BIP39 word list. This method uses 3 hex characters to
     * discover the index for each word.
     * @param seed Master seed word generated randomly.
     * @return List of the words in BIP39 word list. This will have a length 3 times smaller than the length of the
     * seed word (i.e. the ASCII-decoded byte array specified in `seed`.
     */
    private static SecureCharBuffer toWords(byte[] seed) {
        SecureCharBuffer secure = new SecureCharBuffer();
        new MnemonicGenerator(English.INSTANCE)
                .createMnemonic(seed, secure::append);
        return secure;
    }
}

class DerivedSigner {
    final String hash;
    final DidKeys keys;

    DerivedSigner(String hashId, DidKeys didKeys) {
        hash = hashId;
        keys = didKeys;
    }
}