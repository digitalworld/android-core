package com.clearos.dlt;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public enum NotificationFieldType {
    @SerializedName("str")
    STRING("str"),
    @SerializedName("int")
    INTEGER("int"),
    @SerializedName("float")
    FLOAT("float"),
    @SerializedName("bool")
    BOOL("bool");

    private final String value;
    public String getValue() {
        return this.value;
    }

    private NotificationFieldType(String value) {
        this.value = value;
    }

    private static final Map<String, NotificationFieldType> lookup = new HashMap<>();
    static {
        for(NotificationFieldType type : NotificationFieldType.values()) {
            lookup.put(type.getValue(), type);
        }
    }

    public static NotificationFieldType get(String value) {
        return lookup.get(value);
    }
}
