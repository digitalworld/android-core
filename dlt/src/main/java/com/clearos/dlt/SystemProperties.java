package com.clearos.dlt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

/**
 * Gives access to the system properties store. The system properties store contains a list of
 * string key-value pairs.
 */
@SuppressLint("PrivateApi")
public class SystemProperties {
    private static final String GFS_BACKUP_KEYS_PROP = "cleargfs.init";
    private static final String SERIALNO_PROP = "cleargm.serialno";

    private static final String TAG = "SystemProperties";

    private static final Class<?> SP = getSystemPropertiesClass();

    public static String getSerialNumber() {
        return get(SERIALNO_PROP, "");
    }

    /**
     * Backs up the ClearLIFE keys to secure OS-level storage.
     */
    public static void backupKeys() {
        set(GFS_BACKUP_KEYS_PROP, "keys");
    }

    public static void set(String key, String value) {
        try {
            assert SP != null;
            SP.getMethod("set", String.class, String.class).invoke(null, key, value);
        } catch (Exception e) {
            Log.e(TAG, String.format("Error setting system property value %s=%s", key, value));
        }
    }

    public static String getOrDefault(String key, String defaultValue) {
        String value = get(key);
        if (value == null)
            return defaultValue;
        return value;
    }

    /**
     * Get the value for the given key.
     */
    public static String get(String key) {
        try {
            assert SP != null;
            return (String) SP.getMethod("get", String.class).invoke(null, key);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Get the value for the given key.
     *
     * @return if the key isn't found, return def if it isn't null, or an empty string otherwise
     */
    public static String get(String key, String def) {
        try {
            assert SP != null;
            return (String) SP.getMethod("get", String.class, String.class).invoke(null, key, def);
        } catch (Exception e) {
            return def;
        }
    }

    /**
     * Get the value for the given key, returned as a boolean. Values 'n', 'no', '0', 'false' or
     * 'off' are considered false. Values 'y', 'yes', '1', 'true' or 'on' are considered true. (case
     * sensitive). If the key does not exist, or has any other value, then the default result is
     * returned.
     *
     * @param key
     *     the key to lookup
     * @param def
     *     a default value to return
     * @return the key parsed as a boolean, or def if the key isn't found or is not able to be
     * parsed as a boolean.
     */
    public static boolean getBoolean(String key, boolean def) {
        try {
            return (Boolean) SP.getMethod("getBoolean", String.class, boolean.class)
                    .invoke(null, key, def);
        } catch (Exception e) {
            return def;
        }
    }

    /**
     * Get the value for the given key, and return as an integer.
     *
     * @param key
     *     the key to lookup
     * @param def
     *     a default value to return
     * @return the key parsed as an integer, or def if the key isn't found or cannot be parsed
     */
    public static int getInt(String key, int def) {
        try {
            return (Integer) SP.getMethod("getInt", String.class, int.class).invoke(null, key, def);
        } catch (Exception e) {
            return def;
        }
    }

    /**
     * Get the value for the given key, and return as a long.
     *
     * @param key
     *     the key to lookup
     * @param def
     *     a default value to return
     * @return the key parsed as a long, or def if the key isn't found or cannot be parsed
     */
    public static long getLong(String key, long def) {
        try {
            return (Long) SP.getMethod("getLong", String.class, long.class).invoke(null, key, def);
        } catch (Exception e) {
            return def;
        }
    }

    private static Class<?> getSystemPropertiesClass() {
        try {
            return Class.forName("android.os.SystemProperties");
        } catch (ClassNotFoundException shouldNotHappen) {
            return null;
        }
    }

    private SystemProperties() {
        throw new AssertionError("no instances");
    }

}
