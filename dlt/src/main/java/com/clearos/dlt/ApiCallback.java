package com.clearos.dlt;

import okhttp3.Call;
import okhttp3.Response;

public interface ApiCallback {
    void onFailure(Call call, Exception e, Response response);
    void onResponse(String source, Response response);
}
