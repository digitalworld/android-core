package com.clearos.dlt;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.security.KeyChainAliasCallback;
import android.util.Log;
import android.util.Pair;

import com.goterl.lazysodium.LazySodiumAndroid;
import com.goterl.lazysodium.SodiumAndroid;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.interfaces.Hash;
import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Stream;

import io.github.novacrypto.SecureCharBuffer;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class CryptoKeysService extends Service implements KeyRestorer, IKeyChainActivity {
    private CryptoKey userKeys = null;
    private Map<String,DidKeys> genericKeys = null;

    private static final String REFRESH_OYP_KEYS_ACTION = "com.clearos.clearlife.REFRESH_KEYS";
    private static final String REFRESH_OYP_KEYS_ACTION_DL = "com.clearos.digitallife.REFRESH_KEYS";

    private static final LazySodiumAndroid lazySodium = new LazySodiumAndroid(new SodiumAndroid());
    private static final Hash.Lazy lazyHash = lazySodium;
    private final static String TAG = "CryptoKeysService";
    public static final String SUCCESS_STATUS = "success";
    public static final String USER_NAME_KEY = "masterUserName";
    public static final String USER_DISPLAY_NAME_KEY = "masterDisplayName";
    public static final String CLEARLIFE_PACKAGE = "com.clearos.clearlife";
    public static final String DIGITALLIFE_PACKAGE = "com.clearos.digitallife";

    private final Map<String, IPushNotificationCallback> callbacks = new ConcurrentHashMap<>();
    private final Map<String, ICryptoRemoteCallback> cryptoCallbacks = new ConcurrentHashMap<>();

    private final Set<ICryptoRemoteCallback> clearLifeCallbacks = ConcurrentHashMap.newKeySet();
    private final CountDownLatch restoreKeysLatch = new CountDownLatch(1);
    private final Map<String, CountDownLatch> restoreLatches = new HashMap<>();

    private static final List<String> oypWhitelistedPackages = new ArrayList<>(Arrays.asList(
            "com.clearos.clearlife",
            "com.clearos.cleargm",
            "com.clearos.clearpay",
            "com.clearos.oypverifier",
            "com.clearos.digitallife",
            "com.clearos.digitalwallet")
    );

    @Override
    public void onRestoreError(Exception e) {
        Log.d(TAG, "Error restoring keys for AIDL service.", e);
        synchronized (restoreKeysLatch) {
            restoreKeysLatch.countDown();
        }
    }

    @Override
    public void onRestoreSuccess(CryptoKey cryptoKey) {
        userKeys = cryptoKey;
        userKeys.initDataCustodian(getString(R.string.apiKey));
        Log.d(TAG, "Keys restored successfully.");
        synchronized (restoreKeysLatch) {
            restoreKeysLatch.countDown();
        }
    }

    @Override
    public void onGenericRestoreSuccess(DidKeys restoredKeys, String fileName, SecureCharBuffer seed) {
        genericKeys.put(fileName, restoredKeys);
        synchronized (restoreLatches) {
            CountDownLatch rLatch = restoreLatches.get(fileName);
            if (rLatch != null) {
                rLatch.countDown();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        String action = intent.getAction();
        if (action != null && (action.equals(REFRESH_OYP_KEYS_ACTION) || action.equals(REFRESH_OYP_KEYS_ACTION_DL))) {
            Log.i(TAG, "OYP keys refresh requested via service intent.");
            getOypKeys(true);
        }

        Log.d(TAG, "onStartCommand called in DLT cryptoKeys service.");
        return START_STICKY;
    }

    public static Intent getOypRefreshIntent(Context appContext, String serviceName) {
        Intent serviceIntent = new Intent();
        serviceIntent.setClassName(appContext, serviceName);
        if (Utils.isClearDevice())
            serviceIntent.setAction(REFRESH_OYP_KEYS_ACTION);
        else
            serviceIntent.setPackage(REFRESH_OYP_KEYS_ACTION_DL);
        return serviceIntent;
    }

    /**
     * Starts restoration of the OYP keys from file.
     */
    private void getOypKeys() {
        getOypKeys(false);
    }

    /**
     * Starts restoration of the OYP keys from file.
     */
    private void getOypKeys(boolean refresh) {
        File oypSeedFile = CryptoKey.getOypSeedFile(getApplicationContext(), userKeys.getDid());
        if (!refresh && genericKeys.containsKey(oypSeedFile.getName())) return;

        CountDownLatch rL = new CountDownLatch(1);
        restoreLatches.put(oypSeedFile.getName(), rL);
        try {
            CryptoKey.fromSecureStorage(this, oypSeedFile.getName(), this, this);
        } catch (Exception e) {
            Log.d(TAG, "Error restoring OYP keys in AIDL service.", e);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate called in DLT cryptoKeys service.");
        Thread.setDefaultUncaughtExceptionHandler((paramThread, paramThrowable) -> {
            //Catch your exception
            Log.e(TAG, "Uncaught exception in cryptoKeys service.", paramThrowable);
            // Without System.exit() this will not work.
            System.exit(2);
        });

        new Handler().postDelayed(() -> {
            try {
                CryptoKey.fromSecureStorage(this, this, this);
            } catch (Exception e) {
                Log.d(TAG, "Error restoring keys for AIDL service.", e);
            }

        }, 250);

        genericKeys = new HashMap<>();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Cleaning up cryptoKey service.");
        super.onDestroy();
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.d(TAG, "Rebind called with " + intent.toString());
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "Unbind called with " + intent.toString());
        return super.onUnbind(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the interface
        Log.d(TAG, "onBind called in DLT cryptoKeyService.");
        Log.d(TAG, binder.toString());
        return binder;
    }

    private Collection<String> getCallingPackages() {
        int caller = Binder.getCallingUid();
        if (caller == 0) {
            return null;
        }
        String [] packages = getPackageManager().getPackagesForUid(caller);
        List<String> result = null;
        if (packages != null) {
            result = Arrays.asList(packages);
        }
        return result;
    }

    /**
     * Checks whether the specified package name matches the OS UID making the call.
     * @param packageName Name of the package requesting access.
     * @throws RemoteException if the package name specified doesn't match process UID of the caller.
     */
    private void verifyPackageIdentity(String packageName) throws RemoteException {
        Collection<String> packages = getCallingPackages();
        if (packages != null) {
            if (!packages.contains(packageName) && !packages.contains(CLEARLIFE_PACKAGE) && !packages.contains(DIGITALLIFE_PACKAGE)) {
                throw new RemoteException(String.format("Specified package name %s does not match process UID for caller (%s).",
                        packageName, String.join(", ", packages)));
            }
        } else {
            throw new RemoteException("Process UID for caller has no associated package name.");
        }
    }

    private void verifyClearId() throws RemoteException {
        Collection<String> packages = getCallingPackages();
        if (packages != null) {
            if (!packages.contains("com.clearid")) {
                String packageNames = String.join(",", packages);
                throw new RemoteException("Only ClearID can call this AIDL method. Actual package: " + packageNames);
            }
        } else {
            throw new RemoteException("Only ClearID can call this AIDL method.");
        }
    }

    private void verifyClearOyp() throws RemoteException {
        Collection<String> packages = getCallingPackages();
        String allowed = String.join(", ", oypWhitelistedPackages);
        if (packages != null) {
            if (Stream.of(packages.toArray(new String[0])).noneMatch(oypWhitelistedPackages::contains)) {
                throw new RemoteException(String.format("Only %s can call this AIDL method.", allowed));
            }
        } else {
            throw new RemoteException(String.format("Only %s can call this AIDL method.", allowed));
        }
    }

    /**
     * Converts a string context into an 8 character context for use by libsodium.
     * @param context Any string context; if its length > 8, the first 8 digits of its SHA256 hash
     *                are returned.
     */
    public static String contextToShortContext(String context) throws RemoteException {
        String result;
        if (context.length() > 8) {
            try {
                result = lazyHash.cryptoHashSha256(context);
            } catch (SodiumException e) {
                throw new RemoteException("Error hashing package names via libsodium.");
            }

            return result.substring(0, 8);
        } else
            return context;
    }

    private String getApplicationKeyContext(String packageName) throws RemoteException {
        return getApplicationKeyContext(packageName, "");
    }

    public static DidKeys deriveAppKeys(CryptoKey userKeys, String packageName, long keyId) {
        String context = getAppKeyContext(packageName);
        try {
            KeyPair baseKeys = userKeys.deriveKeyPair(keyId, context);
            return CryptoKey.toDidKeys(baseKeys);
        } catch (SodiumException e) {
            Log.e(TAG, "Error deriving app keys for package " + packageName);
            return null;
        }
    }

    public static String getAppKeyContext(String packageName) {
        try {
            return getPackageKeyContext(Collections.singletonList(packageName), "");
        } catch (RemoteException e) {
            Log.e(TAG, "Error getting application key context statically.", e);
            return null;
        }
    }

    private static String getPackageKeyContext(Collection<String> packages, String context) throws RemoteException {
        if (packages == null) {
            throw new RemoteException("No package name associated with the requesting process.");
        } else {
            if (context.isEmpty()) {
                StringBuilder pkgHashes = new StringBuilder();
                for (String packName : packages) {
                    pkgHashes.append(packName);
                }
                String allPackages = pkgHashes.toString();
                Log.d(TAG, "Creating application key context using " + allPackages);
                return contextToShortContext(allPackages + '/' + context);
            } else {
                return contextToShortContext(context);
            }
        }
    }

    private String getApplicationKeyContext(String packageName, String context) throws RemoteException {
        Collection<String> packages = getCallingPackages();
        if (packages == null) {
            throw new RemoteException("No package name associated with the requesting process.");
        } else {
            if (context.isEmpty()) {
                StringBuilder pkgHashes = new StringBuilder();
                for (String packName : packages) {
                    pkgHashes.append(packName);
                }
                String allPackages = pkgHashes.toString();
                if (packageName != null && !allPackages.contains(packageName) && !allPackages.contains(CLEARLIFE_PACKAGE) && !allPackages.contains(DIGITALLIFE_PACKAGE))
                    throw new RemoteException("Not authorized to derive package keys for " + packageName);

                if (packageName != null && !allPackages.contains(packageName)) {
                    Log.d(TAG, "Creating arbitrary application key context using " + packageName);
                    return contextToShortContext(packageName + '/' + context);
                } else {
                    Log.d(TAG, "Creating application key context using " + allPackages);
                    return contextToShortContext(allPackages + '/' + context);
                }
            } else {
                return contextToShortContext(context);
            }
        }
    }

    private KeyPair getApplicationDerivedKey(String packageName, long keyId) throws RemoteException {
        try {
            String context = getApplicationKeyContext(packageName);
            return userKeys.deriveKeyPair(keyId, context);
        } catch (SodiumException e) {
            throw new RemoteException("Error deriving key pair via libsodium.");
        }
    }

    private String getFirstCallingPackage() {
        Collection<String> packages = getCallingPackages();
        if (packages != null) {
            Log.d(TAG, "Returning first of of the calling packages " + Arrays.toString(packages.toArray()));
            return packages.toArray(new String[0])[0];
        } else {
            Log.e(TAG, "Cannot determine calling package identity.");
            return null;
        }
    }

    private void waitLatch() {
        try {
            if (restoreKeysLatch.getCount() > 0) {
                Log.d(TAG, "Waiting for user keys to be restored.");
                synchronized (restoreKeysLatch) {
                    restoreKeysLatch.wait(2500);
                }
            }
        } catch (InterruptedException e) {
            Log.w(TAG, "Thread interrupted while waiting for key restoration.");
        }
    }

    private void triggerKeysRegisteredCallback(Pair<String, String> registrationName, String context, boolean success) {
        List<ICryptoRemoteCallback> callbacks = new ArrayList<>();
        if (registrationName.first != null) {
            Log.d(TAG, String.format("Callback key %s contains the calling package name %s", registrationName.second, registrationName.first));
            callbacks.addAll(clearLifeCallbacks);
        } else {
            Log.d(TAG, String.format("Using callback for remote package %s.", registrationName.second));
            callbacks.add(cryptoCallbacks.getOrDefault(registrationName.second, null));
        }

        Log.d(TAG, String.format("Triggering keys registered callback for %s in %s", registrationName.second, registrationName.first));
        for (ICryptoRemoteCallback callback: callbacks) {
            if (callback != null) {
                try {
                    callback.onAppKeyRegistration(registrationName.second, context, success);
                } catch (RemoteException e) {
                    Log.e(TAG, "Error calling client back with app key registration.", e);
                }
            } else {
                Log.d(TAG, String.format("No callback interface was registered to trigger app key registration for %s and context `%s`", registrationName.second, context));
            }
        }
    }

    /**
     * Returns a file object for the `gfs.id` file in ClearLIFE app files.
     */
    @SuppressLint("SdCardPath")
    public static File getGfsFile() {
        if (Utils.isClearDevice())
            return new File("/data/data/com.clearos.clearlife/files/gfs.id");
        else
            return new File("/data/data/com.clearos.digitallife/files/gfs.id");
    }

    public static String getGsfIdentifier() throws RemoteException {
        File gfsFile = getGfsFile();
        if (gfsFile.exists()) {
            try {
                List<String> contents = Files.readAllLines(gfsFile.toPath());
                String line = contents.get(0);
                String[] parts = line.split("\\|");
                if (parts[0].equals("android_id"))
                    return parts[1];
                else
                    return null;
            } catch (IOException e) {
                Log.e(TAG, "Error reading the contents of the GFS ID file.", e);
                throw new RemoteException(e.getMessage());
            }
        } else
            Log.w(TAG, "GFS ID file does not exist.");
        return null;
    }

    /**
     * Returns the name of the package to use for indexing callbacks.
     * @param packageName The name of the package being derived.
     * @return The name of the calling package if it matches `packageName`.
     */
    private String getCallbackPackageName(String packageName) {
        String callingPackageName = getFirstCallingPackage();
        String callbackPackageName = callingPackageName;
        if (!callingPackageName.equals(packageName)) {
            // We must be doing one of the "arbitrary" registration/derivation moves from
            // ClearLIFE.
            callbackPackageName = String.format("%s:%s", callingPackageName, packageName);
        }
        return callbackPackageName;
    }

    private Pair<String, String> getRegistrationPackageNamePair(String packageName) throws RemoteException {
        if (packageName.contains(":")) {
            String[] parts = packageName.split(":");
            String callingPackage = getFirstCallingPackage();
            if (!parts[0].equals(callingPackage)) {
                throw new RemoteException(String.format("Calling package %s doesn't match registration package name %s.", callingPackage, parts[0]));
            }
            return new Pair<>(parts[0], parts[1]);
        } else
            return new Pair<>(null, packageName);
    }

    private final ICryptoKeysService.Stub binder = new ICryptoKeysService.Stub() {
        @Override
        public String getOypDid() throws RemoteException {
            Log.d(TAG, "Getting DID for OYP keys.");
            verifyClearOyp();
            getOypKeys();

            File oypSeedFile = CryptoKey.getOypSeedFile(getApplicationContext(), userKeys.getDid());
            CountDownLatch rL = restoreLatches.get(oypSeedFile.getName());
            if (rL != null) {
                try {
                    rL.await(2, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Log.d(TAG, "Error awaiting restore latch for OYP keys.");
                }
                DidKeys oypKeys = genericKeys.getOrDefault(oypSeedFile.getName(), null);
                if (oypKeys != null) return oypKeys.getDid();
                else return null;
            } else return null;
        }

        @Override
        public String oypSign(String message) throws RemoteException {
            Log.d(TAG, "Signing message with OYP keys.");
            verifyClearOyp();

            File oypSeedFile = CryptoKey.getOypSeedFile(getApplicationContext(), userKeys.getDid());
            DidKeys oypKeys = genericKeys.getOrDefault(oypSeedFile.getName(), null);
            if (oypKeys != null) return CryptoKey.signMessageToBase64(message, oypKeys);
            else return null;
        }

        @Override
        public void onNotificationReceived(String packageName, DecryptedMqttMessage message) throws RemoteException {
            IPushNotificationCallback callback = callbacks.getOrDefault(packageName, null);
            if (callback != null) {
                Log.d(TAG, String.format("Firing onMessageReceived callback via %s for %s", callback, packageName));
                callback.onMessageReceived(message);
            } else
                Log.w(TAG, String.format("Cannot forward notification to %s, no callback registered.", packageName));
        }

        @Override
        public void onNotificationError(String packageName, String message) throws RemoteException {
            IPushNotificationCallback callback = callbacks.getOrDefault(packageName, null);
            if (callback != null) {
                callback.onNotificationError(message);
            }
        }

        @Override
        public void onTopicsChanged(String packageName, List<String> topics) throws RemoteException {
            IPushNotificationCallback callback = callbacks.getOrDefault(packageName, null);
            if (callback != null) {
                callback.onTopicsChanged(topics);
            }
        }

        @Override
        public CryptoKeyPair deriveKeyHierarchy(String context, String chain) throws RemoteException {
            waitLatch();
            KeyPair baseKeys = getApplicationDerivedKey(null,0);
            KeyPair result = null;
            try {
                 result = userKeys.deriveKeyHierarchy(contextToShortContext(context), chain, baseKeys);
            } catch (SodiumException e) {
                Log.d("CKSVC", "Error deriving key hierarchy in AIDL service.", e);
            }
            return new CryptoKeyPair(result);
        }

        @Override
        public CryptoKeyPair deriveKeyPair(String packageName, String context, long keyId) throws RemoteException {
            waitLatch();
            KeyPair baseKeys = getApplicationDerivedKey(packageName,0);
            KeyPair result = null;
            try {
                result = userKeys.deriveKeyPair(keyId, contextToShortContext(context), baseKeys);
            } catch (SodiumException e) {
                Log.d("CKSVC", "Error deriving key pair in AIDL service.", e);
            }
            return new CryptoKeyPair(result);
        }

        @Override
        public CryptoSeed deriveSeed(CryptoKeyPair baseKeys, String context, long keyId) throws RemoteException {
            waitLatch();
            Key result = null;
            try {
                result = userKeys.deriveKey(baseKeys.getKeys(), keyId, contextToShortContext(context));
            } catch (SodiumException e) {
                Log.d("CKSVC", "Error deriving crypto seed in AIDL service.", e);
            }
            return new CryptoSeed(result);
        }

        @Override
        public DidKeys getAppKeys(String packageName, String context, long keyId) throws RemoteException {
            waitLatch();
            DidKeys result = null;
            try {
                if (context != null) {
                    if (context.length() == 0) {
                        Log.d(TAG, "Deriving application keys for null string context for " + packageName);
                        KeyPair baseKeys = getApplicationDerivedKey(packageName, keyId);
                        result = CryptoKey.toDidKeys(baseKeys);
                    } else {
                        Log.d(TAG, String.format("Deriving applications for %s with context %s", packageName, context));
                        KeyPair baseKeys = getApplicationDerivedKey(packageName, 0);
                        result = CryptoKey.toDidKeys(userKeys.deriveKeyPair(keyId, contextToShortContext(context), baseKeys));
                    }
                }
            } catch (SodiumException e) {
                Log.d("CKSVC", "Error deriving app keys in AIDL service.", e);
            }

            if (result != null)
                Log.d(TAG, String.format("Result from deriving app keys is DID=%s, verkey=%s", result.getDid(), result.getVerKey()));
            return result;
        }

        @Override
        public String getPluginPassword(String verKey) throws RemoteException {
            verifyClearId();
            String result = null;
            try {
                result = userKeys.getPluginPassword(verKey);
            } catch (SodiumException e) {
                Log.d("getPluginPassword", "Error vault encrypting password for plugin via AIDL service.", e);
            }
            return result;
        }

        @Override
        public String encryptVaultKey(String verKey) throws RemoteException {
            verifyClearId();
            String result = null;
            try {
                result = userKeys.encryptVaultKey(verKey);
            } catch (SodiumException e) {
                Log.d("encryptVaultKey", "Error encrypting vault key for plugin via AIDL service.", e);
            }
            return result;
        }

        @Override
        public String vaultCrypt(String value) throws RemoteException {
            verifyClearId();
            String result = null;
            try {
                result = userKeys.vaultCrypt(value);
            } catch (SodiumException e) {
                Log.d("CKSVC", "Error vault encrypting string in AIDL service.", e);
            }
            return result;
        }

        @Override
        public String vaultDecrypt(String value) throws RemoteException {
            verifyClearId();
            String result = null;
            try {
                result = userKeys.vaultDecrypt(value);
            } catch (SodiumException e) {
                Log.d("CKSVC", "Error vault decrypting string in AIDL service.", e);
            }
            return result;
        }

        @Override
        public String getHomeServer(String packageName) throws RemoteException {
            verifyPackageIdentity(packageName);
            SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
            String result = prefs.getValue(String.format("%s:homeServerBaseURL", packageName));
            if (result == null) {
                result = prefs.getValue("homeServerBaseURL");
                if (result == null) result = getString(R.string.defaultHomeServer);
            }
            return result;
        }

        @Override
        public String getClearServer(String packageName) throws RemoteException {
            verifyPackageIdentity(packageName);
            // This is a hard-coded value that never changes.
            return getString(R.string.defaultClearServer);
        }

        @Override
        public void setHomeServer(String packageName, String homeServer) throws RemoteException {
            verifyPackageIdentity(packageName);
            SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
            prefs.saveValue(String.format("%s:homeServerBaseURL", packageName), homeServer);
        }

        @Override
        public String getUserName() {
            SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
            String result = prefs.getValue(USER_NAME_KEY);
            if (result == null) {
                result = getString(R.string.defaultUserName);
            }
            return result;
        }

        @Override
        public String getDisplayName() {
            SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
            String result = prefs.getValue(USER_DISPLAY_NAME_KEY);
            if (result == null) {
                result = getString(R.string.defaultDisplayName);
            }
            return result;
        }

        @Override
        public String getFrameworkServicesId() throws RemoteException {
            verifyClearId();
            Log.d(TAG, "Checking for GSF ID file existence at /data/cleargm/gfs.id");
            return getGsfIdentifier();
        }

        @Override
        public String appKeyRegistrationStatus(String packageName) throws RemoteException {
            String keyContext = getApplicationKeyContext(packageName);
            SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
            String prefsKey = String.format("register/app/%s/%s", packageName, keyContext);
            return prefs.getValue(prefsKey);
        }

        @Override
        public void registerNotificationCallback(String packageName, String appDid, IPushNotificationCallback callback) throws RemoteException {
            Pair<String, String> registrationPackageName = getRegistrationPackageNamePair(packageName);
            if (registrationPackageName.first != null) {
                Log.d(TAG, "The registration package name for the notification callback is custom, not registering push notifications.");
                return;
            }

            if (packageName.equals(getPackageName())) {
                // For ClearLIFE, we don't want the storage key client process to clobber our callback
                // to the main thread key client.
                if (callbacks.getOrDefault(packageName, null) == null)
                    callbacks.put(packageName, callback);
            } else
                callbacks.put(packageName, callback);

            IPushNotificationCallback mqtt = callbacks.getOrDefault(getPackageName(), null);
            if (mqtt != null) {
                Log.i(TAG, String.format("Setting up default topics for package %s and DID %s", packageName, appDid));
                mqtt.onDefaultTopics(packageName, appDid);
            } else
                Log.w(TAG, String.format("Subscribe default topics call for %s and %s, but no MQTT key client.", packageName, appDid));
        }

        @Override
        public void unregisterNotificationCallback(String packageName) throws RemoteException {
            Pair<String, String> registrationPackageName = getRegistrationPackageNamePair(packageName);
            if (registrationPackageName.first != null) {
                Log.d(TAG, "The registration package name for the notification callback is custom, not unregistering push notifications.");
                return;
            }
            callbacks.remove(packageName);

            IPushNotificationCallback mqtt = callbacks.getOrDefault(getPackageName(), null);
            if (mqtt != null)
                mqtt.onUnsubscribeDefaultTopics(packageName);
            else
                Log.w(TAG, String.format("Unsubscribe default topics called for %s, but no MQTT key client.", packageName));
        }

        @Override
        public void subscribeTopic(String appDid, String topic) throws RemoteException {
            String packageName = getFirstCallingPackage();
            IPushNotificationCallback mqtt = callbacks.getOrDefault(getPackageName(), null);
            if (mqtt != null)
                mqtt.onSubscribeTopic(appDid, topic, packageName);
            else
                Log.w(TAG, String.format("Subscribe topic call for %s and %s, but no MQTT key client.", packageName, topic));
        }

        @Override
        public void registerCryptoCallback(String packageName, ICryptoRemoteCallback callback) throws RemoteException {
            Pair<String, String> registrationPackageName = getRegistrationPackageNamePair(packageName);

            Log.d(TAG, "Callback package name registered as " + packageName);
            if (registrationPackageName.first != null) {
                // For ClearLIFE, don't keep overwriting, keep all of them.
                Log.d(TAG, "Adding yet another crypto callback to ClearLIFE set.");
                clearLifeCallbacks.add(callback);
            } else {
                Log.d(TAG, String.format("Added a crypto callback for %s: %s", registrationPackageName.second, callback));
                cryptoCallbacks.put(registrationPackageName.second, callback);
            }
        }

        @Override
        public void unregisterCryptoCallback(String packageName) throws RemoteException {
            Pair<String, String> registrationPackageName = getRegistrationPackageNamePair(packageName);
            if (registrationPackageName.first == null) {
                Log.d(TAG, "Callback package name unregistered as " + registrationPackageName.second);
                cryptoCallbacks.remove(registrationPackageName.second);
            }
        }

        /**
         * Registers application keys with the home server so that `didauth` will work. If context
         * is not null, the app *derived* keys will be registered as sub-keys of the packages
         * global app keys.
         */
        @Override
        public boolean registerAppKeys(String packageName, String context) throws RemoteException {
            Pair<String, String> registrationName = getRegistrationPackageNamePair(packageName);

            waitLatch();
            String keyContext = getApplicationKeyContext(registrationName.second, context);
            SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
            String prefsKey = String.format("register/app/%s/%s", registrationName.second, keyContext);

            // Check to see if they are already registered; if so, just return true.
            String currentStatus = prefs.getValue(prefsKey);
            if (currentStatus != null && currentStatus.equals(SUCCESS_STATUS)) {
                Log.d(TAG, "Returning cached registration status from shared prefs.");
                triggerKeysRegisteredCallback(registrationName, context, true);
                return true;
            }

            String packName = registrationName.second;
            if (!context.equals("")) {
                packName = registrationName.second + '/' + context;
                Log.d(TAG, "Adjusted package name context to " + packName);
            }

            try {
                KeyPair baseKeys = null;
                if (context.length() > 0)
                    baseKeys = getApplicationDerivedKey(registrationName.second,0);
                Log.d(TAG, "Publishing app key using base keys " + baseKeys);
                userKeys.publishAppKey(packName, keyContext, baseKeys, new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        Log.e(TAG, "Error publishing app keys", e);
                        prefs.saveValue(prefsKey, "failed: " + e.getMessage());
                        triggerKeysRegisteredCallback(registrationName, context, false);
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        if (200 <= response.code() && response.code() < 300) {
                            Log.d(TAG, "Successful HTTP response code; caching result in shared prefs.");
                            prefs.saveValue(prefsKey, SUCCESS_STATUS);
                            triggerKeysRegisteredCallback(registrationName, context,true);
                        } else {
                            ResponseBody body = response.body();
                            if (body != null) {
                                prefs.saveValue(prefsKey, String.format("error: %s; %s", response.message(), body.string()));
                            } else {
                                prefs.saveValue(prefsKey, String.format("error: %s", response.message()));
                            }
                            Log.d(TAG, "IO error in register app keys request: " + prefs.getValue(prefsKey));
                            triggerKeysRegisteredCallback(registrationName, context,false);
                        }
                    }
                });
                return true;
            } catch (Exception e) {
                Log.e(TAG, String.format("Error registering app keys for %s with home server.", keyContext), e);
                triggerKeysRegisteredCallback(registrationName, context, false);
                return false;
            }
        }

        /**
         * Rotates application keys with the home server so that `didauth` will work with a new set of keys.
         */
        @Override
        public long rotateAppKeys(String packageName) throws RemoteException {
            waitLatch();
            String keyContext = getApplicationKeyContext(packageName);
            SharedPrefs prefs = SharedPrefs.getInstance(getApplicationContext());
            String prefsKey = String.format("rotate/app/%s", keyContext);

            try {
                KeyPair baseKeys = getApplicationDerivedKey(packageName, 0);
                userKeys.rotateAppKey(packageName, keyContext, baseKeys, new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        prefs.saveValue(prefsKey, "failed: " + e.getMessage());
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        if (200 <= response.code() && response.code() < 300) {
                            prefs.saveValue(prefsKey, SUCCESS_STATUS);
                        } else {
                            ResponseBody body = response.body();
                            if (body != null) {
                                prefs.saveValue(prefsKey, String.format("error: %s; %s", response.message(), body.string()));
                            } else {
                                prefs.saveValue(prefsKey, String.format("error: %s", response.message()));
                            }
                        }
                    }
                });
                return userKeys.getCurrentAppKeyId(keyContext);
            } catch (Exception e) {
                Log.e(TAG, String.format("Error rotating app keys for %s with home server.", keyContext));
                return -1L;
            }
        }
    };

    @Override
    public void installCertificate(Intent installIntent, Function<Integer, Void> resultCallback) {
        Log.i(TAG, "Service should not be installing the certificate.");
    }

    @Override
    public void chooseCertificate(KeyChainAliasCallback callback) {
        Log.i(TAG, "Service cannot choose certificate; this should have been done already by activity.");
    }

    @Override
    public void onGrantError(String message) {
        Log.e(TAG, "Error obtaining grant for system keychain: " + message);
    }

    @Override
    public void onGrantSuccess(String alias) {
        Log.i(TAG, "Grant success from system keychain.");
    }

    @Override
    public String getPkcsPassword() {
        Log.w(TAG, "Service is being asked for a PKCS password; this should be handled by activity.");
        return null;
    }
}
