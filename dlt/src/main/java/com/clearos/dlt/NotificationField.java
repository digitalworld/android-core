package com.clearos.dlt;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class NotificationField {
    private final String name;
    private final String desc;
    private final NotificationFieldType type;
    @SerializedName("default")
    private final String defaultValue;

    private transient String actualValue = null;

    public NotificationField(Map<String, String> values) {
        name = values.getOrDefault("name", null);
        desc = values.getOrDefault("desc", null);
        String t = values.getOrDefault("type", "str");
        type = NotificationFieldType.get(t);
        defaultValue = values.getOrDefault("default", null);
    }

    public void setActualValue(String actualValue) {
        this.actualValue = actualValue;
    }

    public String getActualValue() {
        if (actualValue == null)
            actualValue = defaultValue;
        return actualValue;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public NotificationFieldType getType() {
        return type;
    }

    public String getDefaultValue() {
        if (defaultValue == null)
            return "";
        return defaultValue;
    }
}
