package com.clearos.dlt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.clearos.dlt.fido.AddHeaderInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.clearos.dlt.DataCustodian.JSON;
import static com.clearos.dlt.DataCustodian.getParamsString;

public class DidAuthApi {
    private String server;
    private static final String TAG = "DidAuthApi";
    private boolean useHttps = true;
    private int port;

    private final OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new AddHeaderInterceptor())
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(40, TimeUnit.SECONDS)
            .connectTimeout(40, TimeUnit.SECONDS)
            .build();

    private static Gson gson;
    static {
        gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new GsonIsoDateSerializer())
                .create();
    }

    public static Gson getGson() {
        return gson;
    }

    /**
     * Sets a custom gson serializer. Since the API includes its own internal adapters, a builder
     * is passed in so that it can be further customized by the API class.
     */
    public static void setGson(GsonBuilder builder) {
        DidAuthApi.gson = builder.registerTypeAdapter(Date.class, new GsonIsoDateSerializer())
                            .create();
    }

    public DidAuthApi setUseHttps(boolean useHttps) {
        this.useHttps = useHttps;
        return this;
    }

    public boolean isUseHttps() {
        return useHttps;
    }

    public DidAuthApi setPort(int port) {
        this.port = port;
        if (port != 443)
            useHttps = false;
        return this;
    }

    public int getPort() {
        return port;
    }

    public DidAuthApi(String server) {
        this.server = server;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    @SuppressLint("DefaultLocale")
    URI getURI(String path) throws UnsupportedEncodingException, URISyntaxException {
        if (useHttps)
            return new URI("https", String.format("%s:%d", server, 443), path, DataCustodian.getParamsString(new HashMap<>()), null);
        else
            return new URI("http", String.format("%s:%d", server, port), path, DataCustodian.getParamsString(new HashMap<>()), null);

    }

    @SuppressLint("DefaultLocale")
    private URI getURI(String path, Map<String, String> params) throws UnsupportedEncodingException, URISyntaxException {
        if (useHttps)
            return new URI("https", String.format("%s:%d", server, 443), path, getParamsString(params), null);
        else
            return new URI("http", String.format("%s:%d", server, port), path, getParamsString(params), null);
    }

    private Call doRequest(Request request, Callback callback) {
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    /**
     * Performs a generic HTTP PUT request with DID-authentication signatures in the headers.
     *
     * @param path        Relative path to the API endpoint to query.
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param body        Object to include in the body of the post.
     * @param callback    On success/failure, callback functions to execute.
     * @return The call object to inspect progress and possibly cancel the call.
     */
    public Call put(final String path, DidKeys signingKeys, Object body, final ApiCallback callback) {
        return putOrPost(false, path, signingKeys, body, callback);
    }

    /**
     * Performs a generic HTTP PUT request with DID-authentication signatures in the headers.
     *
     * @param path        Relative path to the API endpoint to query.
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param body        Object to include in the body of the post.
     * @param cache       Object that can access the cache or hold the API callback routine.
     * @return The call object to inspect progress and possibly cancel the call.
     */
    public Call put(final String path, DidKeys signingKeys, Object body, final ApiCacheCombiner<?> cache) {
        if (!cache.restore()) {
            Log.d(TAG, "Performing regular PUT since the cache does not exist to " + path);
            return putOrPost(false, path, signingKeys, body, cache);
        } else
            return null;
    }

    /**
     * Performs a generic HTTP POST request with DID-authentication signatures in the headers.
     *
     * @param path        Relative path to the API endpoint to query.
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param body        Object to include in the body of the post.
     * @param cache       Object that can access the cache or hold the API callback routine.
     * @return The call object to inspect progress and possibly cancel the call.
     */
    public Call post(final String path, DidKeys signingKeys, Object body, final ApiCacheCombiner<?> cache) {
        if (!cache.restore()) {
            Log.d(TAG, "Performing regular POST since the cache does not exist to " + path);
            return putOrPost(true, path, signingKeys, body, cache);
        } else
            return null;
    }

    /**
     * Performs a generic HTTP POST request with DID-authentication signatures in the headers.
     *
     * @param path        Relative path to the API endpoint to query.
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param body        Object to include in the body of the post.
     * @param callback    On success/failure, callback functions to execute.
     * @return The call object to inspect progress and possibly cancel the call.
     */
    public Call post(final String path, DidKeys signingKeys, Object body, final ApiCallback callback) {
        return putOrPost(true, path, signingKeys, body, callback);
    }

    /**
     * Performs a generic HTTP PUT or POST request with DID-authentication signatures in the headers.
     *
     * @param path        Relative path to the API endpoint to query.
     * @param signingKeys The DID and crypto keys to use in making the request.
     * @param body        Object to include in the body of the post.
     * @param callback    On success/failure, callback functions to execute.
     * @return The call object to inspect progress and possibly cancel the call.
     */
    private Call putOrPost(boolean isPost, final String path, DidKeys signingKeys, Object body, final ApiCallback callback) {
        String json = gson.toJson(body);

        Map<String, String> params = new HashMap<>();
        URI target;
        try {
            target = getURI(path, params);
        } catch (Exception e) {
            Log.e(TAG, String.format("URL format error on %s", path), e);
            callback.onFailure(null, e, null);
            return null;
        }

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();

        // Sign with the master key for the DID.
        Headers headerbuild = Headers.of(headers);
        RequestBody jBody = RequestBody.create(json, JSON);
        Request request;

        try {
            Request.Builder builder = new Request.Builder().url(target.toURL()).headers(headerbuild);
            if (isPost) {
                request = builder.post(jBody).build();
            } else {
                request = builder.put(jBody).build();
            }

            if (signingKeys != null) {
                Log.d(TAG, String.format("Signing request with DID %s and verkey %s", signingKeys.getDid(), signingKeys.getVerKey()));
                request = CryptoKey.signRequest(request, signingKeys, signingKeys.getDid());
            }
        } catch (Exception e) {
            Log.e(TAG, "Failure callback fired due to request signing error.", e);
            callback.onFailure(null, e, null);
            return null;
        }

        Log.d(TAG, String.format("Performing %s API request to %s with JSON=%s and headers %s", isPost ? "POST" : "PUT", target.toString(), json, request.headers().toString()));

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e, null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (response.code() >= 200 && response.code() < 300) {
                    callback.onResponse(path, response);
                } else {
                    Log.e(TAG, String.format("API Call failed on %s with %s", path, response.message()));
                    callback.onFailure(call, new IOException(response.message()), response);
                }
            }
        });
    }

    public Call get(final String path, Map<String, String> params, DidKeys signingKeys, final ApiCacheCombiner<?> cache) {
        if (!cache.restore()) {
            Log.d(TAG, "Performing regular GET since the cache does not exist to " + path);
            return get(path, params, signingKeys, cache);
        } else
            return null;
    }

    public Call get(final String path, Map<String, String> params, DidKeys signingKeys, final ApiCallback callback) {
        URI target;
        try {
            target = getURI(path, params);
        } catch (Exception e) {
            Log.e(TAG, "Failure callback being called due to malformed URI.", e);
            callback.onFailure(null, e, null);
            return null;
        }

        // Generate the headers for the request, including the API key.
        Map<String, String> headers = new HashMap<>();
        Headers headerbuild = Headers.of(headers);
        Request request;

        try {
            request = new Request.Builder()
                    .url(target.toURL())
                    .headers(headerbuild)
                    .get().build();
            if (signingKeys != null)
                request = CryptoKey.signRequest(request, signingKeys, signingKeys.getDid());
        } catch (Exception e) {
            Log.e(TAG, "Failure callback fired due to request signing error.", e);
            callback.onFailure(null, e, null);
            return null;
        }
        Log.d(TAG, String.format("Performing API GET to %s with request %s and headers %s", path, request, request.headers().toString()));

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e, null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (response.code() >= 200 && response.code() < 300) {
                    try {
                        callback.onResponse(path, response);
                    } catch (Exception e) {
                        Log.e(TAG, "Error while calling onResponse method of callback", e);
                        callback.onFailure(call, e, response);
                    }
                } else {
                    Log.d(TAG, String.format("Error callback fired due to non-2** response code: %s => %s", path, response.message()));
                    callback.onFailure(call, new IOException(response.message()), response);
                }
            }
        });
    }

    public Call delete(final String path, final ApiCallback callback) {
        return delete(path, callback, null);
    }

    public Call delete(final String path, final ApiCallback callback, DidKeys signingKeys) {
        return delete(path, null, callback, signingKeys);
    }

    public Call delete(final String path, Map<String, String> queryParams, final ApiCacheCombiner<?> cache, DidKeys signingKeys) {
        if (!cache.restore()) {
            Log.d(TAG, "Performing regular DELETE since the cache does not exist to " + path);
            return delete(path, queryParams, cache, signingKeys);
        } else
            return null;
    }

    public Call delete(final String path, Map<String, String> queryParams, final ApiCallback callback, DidKeys signingKeys) {
        URI target;
        try {
            target = getURI(path, queryParams);
        } catch (Exception e) {
            Log.e(TAG, "Failure callback being called due to malformed URI.", e);
            callback.onFailure(null, e, null);
            return null;
        }

        Map<String, String> headers = new HashMap<>();
        Headers headerbuild = Headers.of(headers);
        Request request;

        try {
            request = new Request.Builder()
                    .url(target.toURL())
                    .headers(headerbuild)
                    .delete().build();
        } catch (Exception e) {
            Log.e(TAG, "Failure callback being called due to failed request builder.", e);
            callback.onFailure(null, e, null);
            return null;
        }
        Log.d(TAG, path + ": " + request);

        // Sign the request with didauth headers if necessary.
        if (signingKeys != null) {
            try {
                request = CryptoKey.signRequest(request, signingKeys, signingKeys.getDid());
            } catch (Exception e) {
                Log.e(TAG, "Failure callback fired due to request signing error.", e);
                callback.onFailure(null, e, null);
            }
        }

        Log.d(TAG, String.format("Performing API DELETE to %s with request %s", path, request));

        // Finally make the API endpoint call.
        return doRequest(request, new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure(call, e, null);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (response.code() >= 200 && response.code() < 300) {
                    try {
                        callback.onResponse(path, response);
                    } catch (Exception e) {
                        Log.e(TAG, "Error while calling onResponse method of callback", e);
                        callback.onFailure(call, e, response);
                    }
                }
                else {
                    Log.d(TAG, String.format("Error callback fired due to non-2** response code: %s => %s", path, response.message()));
                    callback.onFailure(call, new IOException(response.message()), response);
                }
            }
        });
    }

    /**
     * Sends a push notification. If the DID is not registered with the home server, it will be
     * looked up using KERI.
     * @param message Unencrypted message to send. It will be encrypted by the home server for the
     *                target DID.
     * @return Call that can be cancelled if necessary.
     */
    public Call sendPushNotification(DidKeys signingKeys, PushNotification message, Function<PushNotificationResponse, Void> callback) {
        String path = "/id/did/notify";
        return post(path, signingKeys, message, new DidAuthApiCallback<>(callback, "sendPushNotification", PushNotificationResponse.class, this));
    }

}