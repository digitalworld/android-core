package com.clearos.dlt;

import android.content.Intent;
import android.security.KeyChainAliasCallback;

import java.util.function.Function;

public interface IKeyChainActivity {
    void installCertificate(Intent installIntent, Function<Integer, Void> resultCallback);
    void chooseCertificate(KeyChainAliasCallback callback);
    void onGrantError(String message);
    void onGrantSuccess(String alias);
    String getPkcsPassword();
}
