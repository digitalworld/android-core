package com.clearos.dlt;

import android.util.Log;

import com.google.gson.Gson;
import com.goterl.lazysodium.exceptions.SodiumException;

import java.time.Instant;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DidAuthEncryptedRequest<T> {
    private final static String TAG = "DidAuthEncryptedRequest";
    private final T body;
    private final DidKeys keys;

    private static Gson gson;

    public DidAuthEncryptedRequest(DidKeys keys, T body) {
        this.body = body;
        this.keys = keys;

        gson = DidAuthApi.getGson();
    }

    public Map<String, String> getEncryptedPayload() {
        Map<String, String> result = new HashMap<>();

        long expirationSeconds = Instant.now().getEpochSecond() + TimeUnit.DAYS.toSeconds(24*7);
        String expiration = String.format(Locale.getDefault(), "%d", expirationSeconds);

        result.put("message", expiration);
        try {
            result.put("signature", CryptoKey.signMessageToHex(expiration, keys));

            String payload = gson.toJson(body);
            String encrypted = CryptoKey.anonCrypt(payload, keys.getKeys().getPublicKey());
            result.put("payload", encrypted);
        } catch (SodiumException e) {
            Log.e(TAG, "While trying to sign the notification auth expiration message.");
            result.put("signature", "");
        }

        return result;
    }
}
