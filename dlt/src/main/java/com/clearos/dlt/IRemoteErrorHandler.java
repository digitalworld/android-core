package com.clearos.dlt;

import android.os.RemoteException;

import java.io.IOException;

public interface IRemoteErrorHandler {
    void onRemoteError(RemoteException e);
    void onGenericError(Exception e);
    void onIOError(IOException e);
    void onNotificationReceived(DecryptedMqttMessage message);
}
