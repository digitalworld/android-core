package com.clearos.dlt;

import android.os.Parcel;
import android.os.Parcelable;

import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;

import java.util.Arrays;

public class DidKeys implements Parcelable {
    private String did;
    private String verKey;
    private KeyPair keys;

    /**
     * Reconstructs a DidKeys instance from a parcel.
     * @param in Parcel from the AIDL service.
     */
    public DidKeys(Parcel in) {
        did = in.readString();
        verKey = in.readString();

        // Reconstruct the public and private key pairs.
        int pkLen = in.readInt();
        int skLen = in.readInt();
        byte[] pk = new byte[pkLen];
        in.readByteArray(pk);
        byte[] sk = new byte[skLen];
        in.readByteArray(sk);
        Key publicKey = Key.fromBytes(pk);
        Key secretKey = Key.fromBytes(sk);
        keys = new KeyPair(publicKey, secretKey);
    }

    DidKeys(String DID, String VK, KeyPair keyPair) {
        did = DID;
        verKey = VK;
        keys = keyPair;
    }

    public KeyPair getKeys() {
        return keys;
    }
    public String getDid() {
        return did;
    }
    public String getVerKey() {
        return verKey;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(did);
        dest.writeString(verKey);
        byte[] pk = keys.getPublicKey().getAsBytes();
        byte[] sk = keys.getSecretKey().getAsBytes();
        dest.writeInt(pk.length);
        dest.writeInt(sk.length);
        dest.writeByteArray(pk);
        dest.writeByteArray(sk);
        Arrays.fill(sk, (byte)0);
    }

    public static final Parcelable.Creator<DidKeys> CREATOR
            = new Parcelable.Creator<DidKeys>() {
        public DidKeys createFromParcel(Parcel in) {
            return new DidKeys(in);
        }

        public DidKeys[] newArray(int size) {
            return new DidKeys[size];
        }
    };
}
