package com.clearos.dlt;

import android.util.Log;

import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.KeyPair;

public class DidAuthStandaloneApi extends DidAuthApi {
    private static final String TAG = "DidAuthStandaloneApi";
    private DidKeys keys;

    public DidAuthStandaloneApi(DidKeys signingKeys, String homeServer) {
        super(homeServer);
        this.keys = signingKeys;
        Log.i(TAG, "Standalone API is auth-ing using DID " + keys.getDid());
    }

    public DidKeys getKeys() {
        return keys;
    }

    /**
     * Creates a new random DID that can be used to identify family members, circles, devices, etc.
     *
     * @param context A string identifying the context in which to derive a deterministic key.
     * @param keyId   An integer specifying the key number (if rotation is being used, increment this number). Or just use 0.
     */
    public DidKeys newDid(String context, long keyId) {
        KeyPair pair = keys.getKeys();
        try {
            return CryptoKey.toDidKeys(CryptoKey.deriveKeyPair(pair, keyId, context));
        } catch (SodiumException e) {
            Log.e(TAG, "While deriving new key pair", e);
            return null;
        }
    }
}
