package com.clearos.dlt;

public class ApiObject {

    private transient DidAuthApi apiClient;

    /**
     * Sets the API client that this object can use when attempting recursive lookups of nested
     * objects.
     */
    public void setApiClient(DidAuthApi apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Gets the API client that this object can use when attempting recursive lookups of nested
     * objects.
     */
    public DidAuthApi getApiClient() {
        return apiClient;
    }
}
