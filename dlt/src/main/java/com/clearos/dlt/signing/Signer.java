package com.clearos.dlt.signing;
// Adapted from https://github.com/bcgov/http-did-auth-proxy

import android.util.Log;
import org.tomitribe.auth.signatures.Signatures;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Base64;
import java.util.Map;

public abstract class Signer<KEY> {

    public static Signer<?> signer(String signingKeyType) {

        if (RsaSigner.supports(signingKeyType)) return new RsaSigner();
        if (Ed25519Signer.supports(signingKeyType)) return new Ed25519Signer();
        if (Secp256k1Signer.supports(signingKeyType)) return new Secp256k1Signer();

        throw new IllegalArgumentException("Unknown signing key type: " + signingKeyType);
    }

    public abstract String algorithm();
    public abstract KEY signingKey(String signingKeyString) throws GeneralSecurityException;
    public abstract byte[] sign(byte[] signingBytes, KEY signingKey) throws GeneralSecurityException;

    public Signature sign(String method, String uri, Map<String, String> headers, String signingDid, KEY signingKey) throws GeneralSecurityException, IOException {
        Log.d("SIGNER", "Method: " + method);
        Log.d("SIGNER", "URI: " + uri);
        Log.d("SIGNER", "Headers: " + headers);

        List<String> signedHeaderNames = HttpUtil.signedHeaderNames(headers);
        Log.d("SIGNER", "Signed header names: " + signedHeaderNames);

        String signingString = Signatures.createSigningString(signedHeaderNames, method, uri, headers);
        byte[] signingBytes = signingString.getBytes(StandardCharsets.UTF_8);
        Log.d("SIGNER", "Signing string: " + signingString);

        byte[] signatureBytes = this.sign(signingBytes, signingKey);
        String signatureString = Base64.getEncoder().encodeToString(signatureBytes);
        Log.d("SIGNER", "Signature string: " + signatureString);

        Signature signature = new Signature(signingDid, this.algorithm(), signatureString, signedHeaderNames);
        Log.d("SIGNER", "Signature: " + signature);

        return signature;
    }
}