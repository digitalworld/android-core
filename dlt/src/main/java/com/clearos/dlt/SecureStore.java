package com.clearos.dlt;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Handler;
import android.security.KeyChain;
import android.security.KeyChainAliasCallback;
import android.security.KeyChainException;
import android.util.Log;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.UserNotAuthenticatedException;
import android.widget.Toast;

import com.clearos.dlt.biometric.BiometricCallback;
import com.clearos.dlt.biometric.BiometricManager;
import com.goterl.lazysodium.exceptions.SodiumException;
import com.goterl.lazysodium.utils.Key;
import com.goterl.lazysodium.utils.KeyPair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;

import javax.crypto.Cipher;

import io.github.novacrypto.SecureCharBuffer;

import static com.clearos.dlt.CryptoKey.makeKeyPair;

public class SecureStore implements BiometricCallback {
    private static final String TAG = "SecureStore";
    static final String TEMPORARY_KEY = "dbSecureStoreKey";

    private final KeyStore ks;
    public String storagePath;
    private final BiometricManager authManager;
    private Context appContext;
    private Map<String, String> currentOperation;
    private SecureCharBuffer currentContents;
    private KeyDecrypter decrypter;
    private int attempts = 0;

    private IKeyChainActivity keyChainActivity;
    private boolean grantRequested = false;
    private Function<File, Void> cryptoCallback = null;

    public SecureStore(String storageDir, Context context, IKeyChainActivity keyChainActivity) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        ks = KeyStore.getInstance("AndroidKeyStore");
        ks.load(null);
        storagePath = storageDir;
        authManager = new BiometricManager.BiometricBuilder(context)
                .setTitle(context.getString(R.string.biometric_title))
                .setSubtitle(context.getString(R.string.biometric_subtitle))
                .setDescription(context.getString(R.string.biometric_description))
                .setNegativeButtonText(context.getString(R.string.biometric_negative_button_text))
                .build();

        appContext = context.getApplicationContext();
        this.keyChainActivity = keyChainActivity;
    }

    private void threadSafeToast(String message) {
        new Handler(appContext.getMainLooper()).post(() -> {
            Log.d(TAG, "Showing threadsafe toasting message: " + message);
            Toast.makeText(appContext, message, Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void onSdkVersionNotSupported() {
        threadSafeToast(appContext.getString(R.string.biometric_error_sdk_not_supported));
    }

    @Override
    public void onBiometricAuthenticationNotSupported() {
        threadSafeToast(appContext.getString(R.string.biometric_error_hardware_not_supported));
    }

    @Override
    public void onBiometricAuthenticationNotAvailable() {
        threadSafeToast(appContext.getString(R.string.biometric_error_fingerprint_not_available));
        new Handler(appContext.getMainLooper()).postDelayed(() -> {
            Intent updateIntent = new Intent();
            if (Utils.isClearDevice())
                updateIntent.setComponent(new ComponentName("com.clearos.clearlife", "com.clearos.clearlife.activity.SetupFingerprintActivity"));
            else
                updateIntent.setComponent(new ComponentName("com.clearos.digitallife", "com.clearos.digitallife.activity.SetupFingerprintActivity"));
            updateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            updateIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            appContext.startActivity(updateIntent);
        },500);
    }

    @Override
    public void onBiometricAuthenticationPermissionNotGranted() {
        threadSafeToast(appContext.getString(R.string.biometric_error_permission_not_granted));
    }

    @Override
    public void onBiometricAuthenticationInternalError(String error) {
        threadSafeToast(error);
    }

    @Override
    public void onAuthenticationFailed() {
        Log.w(TAG, "Decryption of master key failed.");
        //Toast.makeText(appContext, appContext.getString(R.string.biometric_failure), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAuthenticationCancelled() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_cancelled), Toast.LENGTH_LONG).show();
        Activity activity = getActivity(appContext);
        if (activity != null)
            activity.finish();
        authManager.cancelAuthentication();
    }

    private Activity getActivity(Context context) {
        if (context == null) {
            return null;
        }
        else if (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            else {
                return getActivity(((ContextWrapper) context).getBaseContext());
            }
        }
        return null;
    }

    private void handleOperationContinuation() {
        if (currentOperation == null || currentOperation.isEmpty()) {
            return;
        }

        String operation = currentOperation.get("operation");
        String fileName = currentOperation.get("fileName");
        String keyAlias = currentOperation.get("keyAlias");

        if (operation != null && operation.equals("store")) {
            SecureCharBuffer contents = currentContents;
            try {
                store(fileName, contents, keyAlias, this.cryptoCallback);
            } catch (Exception e) {
                Log.e("RE-CRYPTO", "Failed to store key material on successful auth.");
            }
        } else if (operation != null && operation.equals("load")) {
            try {
                load(fileName, keyAlias, decrypter);
                // Empty the current operation since it has executed.
                currentOperation.clear();
            } catch (Exception e) {
                Log.e("RE-CRYPTO", "Failed to reload key material on successful auth.");
            }
        }
    }

    @Override
    public void onAuthenticationSuccessful() {
        Toast.makeText(appContext, appContext.getString(R.string.biometric_success), Toast.LENGTH_LONG).show();
        handleOperationContinuation();
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
//        Toast.makeText(getApplicationContext(), helpString, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
//        Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_LONG).show();
    }

    /**
     * Decrypts a value from secure storage.
     * @param fileName Name of the file that the original value was encrypted into.
     * @param keyAlias Alias for the OS secure key to use for the decryption.
     */
    public void load(String fileName, String keyAlias, KeyDecrypter keyDecrypter) throws IOException {
        decrypter = keyDecrypter;
        Path target = Paths.get(storagePath, fileName);
        byte[] contents;
        contents = Files.readAllBytes(target);

        try {
            SecureCharBuffer decryptedKey = loadEnclaveKeys(keyAlias, contents);
            keyDecrypter.onKeyDecryptSuccess(decryptedKey);
        } catch (UserNotAuthenticatedException e) {
            // Authenticate the user and then try again.
            attempts += 1;
            if (attempts < 5) {
                if (currentOperation == null || currentOperation.isEmpty()) {
                    currentOperation = new HashMap<>();
                    currentOperation.put("operation", "load");
                    currentOperation.put("fileName", fileName);
                    currentOperation.put("keyAlias", keyAlias);
                }
                if (keyAlias != null)
                    authManager.authenticate(this);
            }
        } catch (Exception e) {
            keyDecrypter.onKeyDecryptFailure(e);
            Log.e("CRYPTO", "Unable to derive keys from secure enclave.");
        }
    }

    /**
     * Encrypts the master key using the secure enclave.
     * @param keyAlias Name of the key from the key store to use.
     * @return Bytes of the encrypted master key.
     */
    private String storeEnclaveKeys(String keyAlias, SecureCharBuffer masterKey) throws Exception {
        java.security.KeyPair keys;
        if (keyAlias == null)
            keys = getKeyPair();
        else
            keys = getKeyPair(keyAlias, true);

        if (keys != null) {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.ENCRYPT_MODE, keys.getPublic());
            return Base64.getEncoder().encodeToString(c.doFinal(SecureBase58.decodeSecure(masterKey)));
        } else
            throw new UserNotAuthenticatedException("Could not get keychain key grant; user needs to give permission.");
    }

    /**
     * Decrypts the master key encrypted using `storeEnclaveKeys`.
     * @param keyAlias Name of the key from the key store to use.
     * @param encrypted Bytes returned by `storeEnclaveKeys`.
     * @return Secure buffer with the contents of the master key.
     */
    private SecureCharBuffer loadEnclaveKeys(String keyAlias, byte[] encrypted) throws Exception {
        java.security.KeyPair keys = getKeyPair(keyAlias, true);
        if (keys != null) {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.DECRYPT_MODE, keys.getPrivate());
            String encString = new String(encrypted);
            byte[] decrypted = c.doFinal(Base64.getDecoder().decode(encString));
            SecureCharBuffer result = SecureBase58.encodeSecure(decrypted);
            Arrays.fill(decrypted, (byte) 0);
            return result;
        } else
            throw new UserNotAuthenticatedException("System KeyChain not available for decryption.");
    }

    /**
     * Writes an encrypted copy of a string to disk using keys from secure storage.
     * @param fileName the name that the encrypted contents should be written under.
     * @param contents message to write securely to disk.
     * @param keyAlias alias of the key to use for encryption and decryption.
     */
    public void store(String fileName, SecureCharBuffer contents, String keyAlias, Function<File, Void> callback) {
        store(fileName, contents, keyAlias, false, callback);
    }

    public static KeyPair getTemporaryKeys() throws SodiumException {
        Key temporary = Key.fromPlainString(TEMPORARY_KEY + TEMPORARY_KEY);
        byte[] secret = temporary.getAsBytes();
        return makeKeyPair(secret);
    }

    /**
     * Writes an encrypted copy of a string to disk using keys from secure storage.
     * @param fileName the name that the encrypted contents should be written under.
     * @param contents message to write securely to disk.
     * @param keyAlias alias of the key to use for encryption and decryption. If set to null, then
     *                 the system KeyChain will be used instead.
     * @param directBoot true if the storage should be direct-boot aware.
     */
    public void store(String fileName, SecureCharBuffer contents, String keyAlias, boolean directBoot, Function<File, Void> callback) {
        this.cryptoCallback = callback;
        File target = new File(storagePath, fileName);
        String encrypted = null;
        try {
            if (directBoot) {
                // We don't have a user fingerprint yet, so we can't encrypt with the regular keys.
                // Rather, anon-crypt with a random key.
                KeyPair tempKeys = getTemporaryKeys();
                encrypted = CryptoKey.anonCryptSecure(contents, tempKeys.getPublicKey());
            } else
                encrypted = storeEnclaveKeys(keyAlias, contents);
        } catch (UserNotAuthenticatedException e) {
            // Authenticate the user and then try again.
            currentOperation = new HashMap<>();
            currentOperation.put("operation", "store");
            currentOperation.put("fileName", fileName);
            currentContents = contents;
            currentOperation.put("keyAlias", keyAlias);
            if (keyAlias != null)
                authManager.authenticate(this);
            // The other case where the keyAlias is null means we are using system KeyChain. In that
            // case, the getKeyPair() will trigger the relevant interface callbacks to install/grant
            // the certificate.
        } catch (Exception e) {
            Log.e("CRYPTO", "Unable to derive keys from secure enclave.");
            callback.apply(null);
            return;
        }

        if (encrypted != null) {
            FileOutputStream fos;
            try {
                if (directBoot)
                    fos = appContext.openFileOutput(fileName, Context.MODE_PRIVATE);
                else
                    fos = new FileOutputStream(target);
                fos.write(encrypted.getBytes(StandardCharsets.UTF_8));
                fos.close();
                callback.apply(target);
            } catch (IOException e) {
                Log.e("CRYPTO", "Error saving encrypted keys to file.");
                callback.apply(null);
            }
        } else
            callback.apply(null);
    }

    /**
     * Returns a system-wide keypair to use for the encryption/decryption.
     * @return Null if there is an error generating the keypair.
     */
    public java.security.KeyPair getKeyPair() {
        try {
            PrivateKey sk = KeyChain.getPrivateKey(appContext, Certificate.ENCRYPTION_CERT_ALIAS);
            X509Certificate[] chain = KeyChain.getCertificateChain(appContext, Certificate.ENCRYPTION_CERT_ALIAS);
            if (chain != null && chain.length > 0) {
                X509Certificate last = chain[chain.length-1];
                PublicKey pk = last.getPublicKey();
                return new java.security.KeyPair(pk, sk);
            } else {
                if (!grantRequested) {
                    new Handler(appContext.getMainLooper()).post(() -> keyChainActivity.chooseCertificate(aliasCallback));
                } else {
                    String password = keyChainActivity.getPkcsPassword();
                    Intent installIntent = Certificate.getInstallIntent(password);
                    if (installIntent != null)
                        new Handler(appContext.getMainLooper()).post(() -> keyChainActivity.installCertificate(installIntent, code -> {
                            switch (code) {
                                case Activity.RESULT_OK:
                                    grantRequested = false;
                                    handleOperationContinuation();
                                    break;
                                case Activity.RESULT_CANCELED:
                                    keyChainActivity.onGrantError("Certificate not installed.");
                                    break;
                            }
                            return null;
                        }));
                    else
                        keyChainActivity.onGrantError("Could not create a certificate for encryption.");
                }
                return null;
            }
        } catch (KeyChainException e) {
            Log.e(TAG, "Error retrieving existing alias certificates from keychain.", e);
            if (e.getCause() instanceof IllegalStateException)
                new Handler(appContext.getMainLooper()).post(() -> keyChainActivity.chooseCertificate(aliasCallback));
            return null;
        } catch (Exception ise) {
            Log.e(TAG, "Calling get certificate chain from wrong thread.", ise);
            return null;
        }
    }

    KeyChainAliasCallback aliasCallback = alias -> {
        if (alias != null && alias.equals(Certificate.ENCRYPTION_CERT_ALIAS)) {
            keyChainActivity.onGrantSuccess(alias);
            grantRequested = true;
            handleOperationContinuation();
        }
        else {
            if (grantRequested) {
                keyChainActivity.onGrantError(String.format("Alias %s did not receive a grant from the user for system keychain.", alias));
                Toast.makeText(appContext, appContext.getString(R.string.biometric_cancelled), Toast.LENGTH_LONG).show();
            } else {
                grantRequested = true;
                handleOperationContinuation();
            }
        }
    };

    /**
     * Gets the key pair from the Android key store, but with the option of recreating
     * @param alias Name of the key pair to get or recreate.
     * @param recreateKeys When true, if the keypair can't be retrieved, generate a new one.
     * @return Null if there is an error that can't be recovered.
     */
    public java.security.KeyPair getKeyPair(String alias, boolean recreateKeys) throws UnrecoverableEntryException, NoSuchAlgorithmException, KeyStoreException {
        java.security.KeyPair result = null;
        try {
            Log.d(TAG, "First attempt at restoring key pair from keystore.");
            result = getKeyPair(alias);
        } catch (Exception e) {
            Log.e(TAG, "There was an error retrieving the key from the keystore; should we remove it?", e);
            if (recreateKeys) {
                Log.i(TAG, String.format("Recreating keypair for alias %s since it can't be retrieved.", alias));
                if (ks.containsAlias(alias)) {
                    Log.d(TAG, String.format("Deleting keypair with alias %s from keystore.", alias));
                    try {
                        ks.deleteEntry(alias);
                    } catch (Exception f) {
                        Log.e(TAG, "Error trying to delete the keypair from the keystore", e);
                        return null;
                    }
                    result = getKeyPair(alias);
                }
            } else
                throw e;
        }
        return result;
    }

    /**
     * Gets a keyPair from the secure storage of the OS. If it doesn't exist, it will be created.
     * @param alias to use when storing/retrieving the key.
     */
    public java.security.KeyPair getKeyPair(String alias) throws KeyStoreException, UnrecoverableEntryException, NoSuchAlgorithmException {
        java.security.KeyPair result = null;
        PrivateKey privateKey;
        PublicKey publicKey;

        try {
            if (!ks.containsAlias(alias)) {
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(
                        KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
                keyPairGenerator.initialize(
                        new KeyGenParameterSpec.Builder(
                                alias, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                                .setDigests(KeyProperties.DIGEST_SHA256,
                                        KeyProperties.DIGEST_SHA384,
                                        KeyProperties.DIGEST_SHA512)
                                // Only permit the private key to be used if the user authenticated
                                // within the last five minutes.
                                .setUserAuthenticationRequired(true)
                                .setUserAuthenticationValidityDurationSeconds(5 * 60)
                                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                                .setKeySize(3072)
                                .build());
                result = keyPairGenerator.generateKeyPair();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ks.containsAlias(alias)) {
            privateKey = (PrivateKey) ks.getKey(alias, null);
            publicKey = ks.getCertificate(alias).getPublicKey();
            result = new java.security.KeyPair(publicKey, privateKey);
        }

        return result;
    }
}
