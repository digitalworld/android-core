package com.clearos.dlt;

import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public interface IAbstractService<T> {
    void registerCallbacks(String packageName) throws RemoteException;
    void unregisterCallbacks(T service, String packageName) throws RemoteException;
    T getServiceStubInterface(IBinder service);
    Intent getServiceIntent();
}
