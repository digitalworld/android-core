package duo.labs.webauthn.util;

import android.os.CancellationSignal;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricPrompt;

import java.security.Signature;
import java.util.function.Function;

import duo.labs.webauthn.Authenticator;
import duo.labs.webauthn.exceptions.VirgilException;
import duo.labs.webauthn.models.AttestationObject;
import duo.labs.webauthn.models.AuthenticatorMakeCredentialOptions;
import duo.labs.webauthn.models.PublicKeyCredentialSource;

public class BiometricMakeCredentialCallback extends BiometricPrompt.AuthenticationCallback {
    private static final String TAG = "BiometricMakeCredentialCallback";

    private final Authenticator authenticator;
    private final AuthenticatorMakeCredentialOptions options;
    private final PublicKeyCredentialSource credentialSource;
    private final Function<AttestationObject, Void> callback;
    private final CancellationSignal signal;

    public BiometricMakeCredentialCallback(Authenticator authenticator,
                                           AuthenticatorMakeCredentialOptions options,
                                           PublicKeyCredentialSource credentialSource,
                                           Function<AttestationObject, Void> callback,
                                           CancellationSignal signal) {
        super();
        this.authenticator = authenticator;
        this.options = options;
        this.credentialSource = credentialSource;
        this.callback = callback;
        this.signal = signal;
    }

    @Override
    public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        Log.d(TAG, "Authentication Succeeded");

        // retrieve biometricprompt-approved signature
        Signature signature = result.getCryptoObject().getSignature();

        AttestationObject attestationObject;
        try {
            attestationObject = authenticator.makeInternalCredential(options, credentialSource, signature);
        } catch (VirgilException exception) {
            Log.w(TAG, "Failed makeInternalCredential: " + exception.toString());
            onAuthenticationFailed();
            return;
        }
        callback.apply(attestationObject);
    }

    @Override
    public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
        if (errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
            onAuthenticationCancelled();
        }
        Log.d(TAG, "authentication error");
        callback.apply(null);
    }

    @Override
    public void onAuthenticationFailed() {
        // this happens on a bad fingerprint read -- don't cancel/error if this happens
        super.onAuthenticationFailed();
        Log.d(TAG, "authentication failed");
    }

    public void onAuthenticationCancelled() {
        Log.d(TAG, "authentication cancelled");
        if (signal != null)
            signal.cancel();
        callback.apply(null);
    }
}
