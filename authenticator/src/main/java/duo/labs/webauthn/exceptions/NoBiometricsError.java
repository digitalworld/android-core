package duo.labs.webauthn.exceptions;

public class NoBiometricsError extends Exception {
    public NoBiometricsError(String message) {
        super(message);
    }
}
