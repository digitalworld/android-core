package duo.labs.webauthn.models;

import java.util.Map;
import java.util.function.Function;

public class NotificationExtras {
    private static final String PUSH_METHOD = "push";

    private final Map<String, Object> body;
    private final String messageId;
    private final String title;
    private final String message;
    private final String signature;
    private final String domain;

    public NotificationExtras(String messageId, String domain, String title, String message, Map<String, Object> body, String signature) {
        this.messageId = messageId;
        this.domain = domain;
        this.body = body;
        this.title = title;
        this.message = message;
        this.signature = signature;
    }

    /**
     * Validates that the signature and domain on the push notification matches the current KERI public key
     * for the sender's DID.
     * @param callback Argument will be true if the KERI lookup and signature verification pass;
     *                 false otherwise.
     */
    public void validate(Function<Boolean, Void> callback) {
        // TODO: actually perform the KERI lookup.
        callback.apply(true);
    }

    public String getMessage() {
        return message;
    }

    public String getMessageId() {
        return messageId;
    }

    /**
     * Gets the method being authenticated; since this is a notification extra, its method is
     * always `PUSH_METHOD`.
     */
    public String getMethod() {
        return PUSH_METHOD;
    }

    public String getDomain() {
        return domain;
    }

    public Map<String, Object> getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }

    public String getSignature() {
        return signature;
    }
}
